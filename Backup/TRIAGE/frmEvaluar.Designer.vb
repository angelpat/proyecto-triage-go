<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEvaluar
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmEvaluar))
        Me.gbxValoracion = New System.Windows.Forms.GroupBox
        Me.cbxPaciente = New System.Windows.Forms.ComboBox
        Me.txtSexo = New System.Windows.Forms.TextBox
        Me.txtDerechohabiencia = New System.Windows.Forms.TextBox
        Me.txtEdad = New System.Windows.Forms.TextBox
        Me.lblSexo = New System.Windows.Forms.Label
        Me.lblDerechohabiencia = New System.Windows.Forms.Label
        Me.lblEdad = New System.Windows.Forms.Label
        Me.lblPaterno = New System.Windows.Forms.Label
        Me.gbxSignos = New System.Windows.Forms.GroupBox
        Me.txtTemp = New System.Windows.Forms.TextBox
        Me.txtPulso = New System.Windows.Forms.TextBox
        Me.txtRespiracion = New System.Windows.Forms.TextBox
        Me.txtPaDiastolica = New System.Windows.Forms.TextBox
        Me.txtPaSistolica = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.btnEvaluar = New System.Windows.Forms.Button
        Me.cbxEstado = New System.Windows.Forms.ComboBox
        Me.lblRespiracion = New System.Windows.Forms.Label
        Me.lblGradosC = New System.Windows.Forms.Label
        Me.lblFrecCard = New System.Windows.Forms.Label
        Me.lblPulso = New System.Windows.Forms.Label
        Me.lblTemp = New System.Windows.Forms.Label
        Me.Label2 = New System.Windows.Forms.Label
        Me.lblPresionA = New System.Windows.Forms.Label
        Me.gbxPrioridadSug = New System.Windows.Forms.GroupBox
        Me.panPrioridad = New System.Windows.Forms.Panel
        Me.lblColor = New System.Windows.Forms.Label
        Me.lblActuacion = New System.Windows.Forms.Label
        Me.lblDenominacion = New System.Windows.Forms.Label
        Me.lblNivel = New System.Windows.Forms.Label
        Me.gbxDxPresuntivo = New System.Windows.Forms.GroupBox
        Me.txtMotivo = New System.Windows.Forms.TextBox
        Me.ToolStrip1 = New System.Windows.Forms.ToolStrip
        Me.btnNuevo = New System.Windows.Forms.ToolStripButton
        Me.btnGuardar = New System.Windows.Forms.ToolStripButton
        Me.btnCancelar = New System.Windows.Forms.ToolStripButton
        Me.tmrPrioridad = New System.Windows.Forms.Timer(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.gbxPrioridadSel = New System.Windows.Forms.GroupBox
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.LblTipoUrgencia = New System.Windows.Forms.Label
        Me.cbxTipoUrgencia = New System.Windows.Forms.ComboBox
        Me.lblNivelAsig = New System.Windows.Forms.Label
        Me.cbxPrioridadSel = New System.Windows.Forms.ComboBox
        Me.gbxValoracion.SuspendLayout()
        Me.gbxSignos.SuspendLayout()
        Me.gbxPrioridadSug.SuspendLayout()
        Me.panPrioridad.SuspendLayout()
        Me.gbxDxPresuntivo.SuspendLayout()
        Me.ToolStrip1.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxPrioridadSel.SuspendLayout()
        Me.SuspendLayout()
        '
        'gbxValoracion
        '
        Me.gbxValoracion.BackColor = System.Drawing.SystemColors.Control
        Me.gbxValoracion.Controls.Add(Me.cbxPaciente)
        Me.gbxValoracion.Controls.Add(Me.txtSexo)
        Me.gbxValoracion.Controls.Add(Me.txtDerechohabiencia)
        Me.gbxValoracion.Controls.Add(Me.txtEdad)
        Me.gbxValoracion.Controls.Add(Me.lblSexo)
        Me.gbxValoracion.Controls.Add(Me.lblDerechohabiencia)
        Me.gbxValoracion.Controls.Add(Me.lblEdad)
        Me.gbxValoracion.Controls.Add(Me.lblPaterno)
        Me.gbxValoracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxValoracion.Location = New System.Drawing.Point(12, 28)
        Me.gbxValoracion.Name = "gbxValoracion"
        Me.gbxValoracion.Size = New System.Drawing.Size(632, 126)
        Me.gbxValoracion.TabIndex = 0
        Me.gbxValoracion.TabStop = False
        Me.gbxValoracion.Text = " Valoración inicial urgencias "
        '
        'cbxPaciente
        '
        Me.cbxPaciente.FormattingEnabled = True
        Me.cbxPaciente.Location = New System.Drawing.Point(71, 34)
        Me.cbxPaciente.Name = "cbxPaciente"
        Me.cbxPaciente.Size = New System.Drawing.Size(542, 21)
        Me.cbxPaciente.TabIndex = 0
        '
        'txtSexo
        '
        Me.txtSexo.BackColor = System.Drawing.SystemColors.Window
        Me.txtSexo.Location = New System.Drawing.Point(71, 87)
        Me.txtSexo.Name = "txtSexo"
        Me.txtSexo.ReadOnly = True
        Me.txtSexo.Size = New System.Drawing.Size(142, 20)
        Me.txtSexo.TabIndex = 5
        '
        'txtDerechohabiencia
        '
        Me.txtDerechohabiencia.BackColor = System.Drawing.SystemColors.Window
        Me.txtDerechohabiencia.Location = New System.Drawing.Point(220, 87)
        Me.txtDerechohabiencia.Name = "txtDerechohabiencia"
        Me.txtDerechohabiencia.ReadOnly = True
        Me.txtDerechohabiencia.Size = New System.Drawing.Size(393, 20)
        Me.txtDerechohabiencia.TabIndex = 6
        '
        'txtEdad
        '
        Me.txtEdad.BackColor = System.Drawing.SystemColors.Window
        Me.txtEdad.Location = New System.Drawing.Point(13, 87)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(51, 20)
        Me.txtEdad.TabIndex = 4
        '
        'lblSexo
        '
        Me.lblSexo.AutoSize = True
        Me.lblSexo.Location = New System.Drawing.Point(68, 70)
        Me.lblSexo.Name = "lblSexo"
        Me.lblSexo.Size = New System.Drawing.Size(35, 13)
        Me.lblSexo.TabIndex = 12
        Me.lblSexo.Text = "Sexo"
        '
        'lblDerechohabiencia
        '
        Me.lblDerechohabiencia.AutoSize = True
        Me.lblDerechohabiencia.Location = New System.Drawing.Point(216, 70)
        Me.lblDerechohabiencia.Name = "lblDerechohabiencia"
        Me.lblDerechohabiencia.Size = New System.Drawing.Size(110, 13)
        Me.lblDerechohabiencia.TabIndex = 4
        Me.lblDerechohabiencia.Text = "Derechohabiencia"
        '
        'lblEdad
        '
        Me.lblEdad.AutoSize = True
        Me.lblEdad.Location = New System.Drawing.Point(10, 70)
        Me.lblEdad.Name = "lblEdad"
        Me.lblEdad.Size = New System.Drawing.Size(36, 13)
        Me.lblEdad.TabIndex = 3
        Me.lblEdad.Text = "Edad"
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Location = New System.Drawing.Point(10, 37)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(61, 13)
        Me.lblPaterno.TabIndex = 0
        Me.lblPaterno.Text = "Paciente:"
        '
        'gbxSignos
        '
        Me.gbxSignos.BackColor = System.Drawing.SystemColors.Control
        Me.gbxSignos.Controls.Add(Me.txtTemp)
        Me.gbxSignos.Controls.Add(Me.txtPulso)
        Me.gbxSignos.Controls.Add(Me.txtRespiracion)
        Me.gbxSignos.Controls.Add(Me.txtPaDiastolica)
        Me.gbxSignos.Controls.Add(Me.txtPaSistolica)
        Me.gbxSignos.Controls.Add(Me.Label1)
        Me.gbxSignos.Controls.Add(Me.btnEvaluar)
        Me.gbxSignos.Controls.Add(Me.cbxEstado)
        Me.gbxSignos.Controls.Add(Me.lblRespiracion)
        Me.gbxSignos.Controls.Add(Me.lblGradosC)
        Me.gbxSignos.Controls.Add(Me.lblFrecCard)
        Me.gbxSignos.Controls.Add(Me.lblPulso)
        Me.gbxSignos.Controls.Add(Me.lblTemp)
        Me.gbxSignos.Controls.Add(Me.Label2)
        Me.gbxSignos.Controls.Add(Me.lblPresionA)
        Me.gbxSignos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxSignos.Location = New System.Drawing.Point(13, 243)
        Me.gbxSignos.Name = "gbxSignos"
        Me.gbxSignos.Size = New System.Drawing.Size(632, 88)
        Me.gbxSignos.TabIndex = 2
        Me.gbxSignos.TabStop = False
        Me.gbxSignos.Text = " Signos vitales "
        '
        'txtTemp
        '
        Me.txtTemp.Location = New System.Drawing.Point(258, 23)
        Me.txtTemp.MaxLength = 4
        Me.txtTemp.Name = "txtTemp"
        Me.txtTemp.Size = New System.Drawing.Size(34, 20)
        Me.txtTemp.TabIndex = 10
        '
        'txtPulso
        '
        Me.txtPulso.Location = New System.Drawing.Point(258, 54)
        Me.txtPulso.MaxLength = 3
        Me.txtPulso.Name = "txtPulso"
        Me.txtPulso.Size = New System.Drawing.Size(34, 20)
        Me.txtPulso.TabIndex = 12
        '
        'txtRespiracion
        '
        Me.txtRespiracion.Location = New System.Drawing.Point(86, 54)
        Me.txtRespiracion.MaxLength = 2
        Me.txtRespiracion.Name = "txtRespiracion"
        Me.txtRespiracion.Size = New System.Drawing.Size(57, 20)
        Me.txtRespiracion.TabIndex = 11
        '
        'txtPaDiastolica
        '
        Me.txtPaDiastolica.Location = New System.Drawing.Point(116, 23)
        Me.txtPaDiastolica.MaxLength = 3
        Me.txtPaDiastolica.Name = "txtPaDiastolica"
        Me.txtPaDiastolica.Size = New System.Drawing.Size(27, 20)
        Me.txtPaDiastolica.TabIndex = 9
        '
        'txtPaSistolica
        '
        Me.txtPaSistolica.Location = New System.Drawing.Point(86, 23)
        Me.txtPaSistolica.MaxLength = 3
        Me.txtPaSistolica.Name = "txtPaSistolica"
        Me.txtPaSistolica.Size = New System.Drawing.Size(27, 20)
        Me.txtPaSistolica.TabIndex = 8
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(418, 26)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 17
        Me.Label1.Text = "Estado alerta"
        '
        'btnEvaluar
        '
        Me.btnEvaluar.Location = New System.Drawing.Point(506, 52)
        Me.btnEvaluar.Name = "btnEvaluar"
        Me.btnEvaluar.Size = New System.Drawing.Size(117, 23)
        Me.btnEvaluar.TabIndex = 14
        Me.btnEvaluar.Text = "Evaluar"
        Me.btnEvaluar.UseVisualStyleBackColor = True
        '
        'cbxEstado
        '
        Me.cbxEstado.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxEstado.FormattingEnabled = True
        Me.cbxEstado.Items.AddRange(New Object() {"Orientada", "Desorientado", "Estuporoso"})
        Me.cbxEstado.Location = New System.Drawing.Point(506, 23)
        Me.cbxEstado.Name = "cbxEstado"
        Me.cbxEstado.Size = New System.Drawing.Size(117, 21)
        Me.cbxEstado.TabIndex = 13
        '
        'lblRespiracion
        '
        Me.lblRespiracion.AutoSize = True
        Me.lblRespiracion.Location = New System.Drawing.Point(8, 58)
        Me.lblRespiracion.Name = "lblRespiracion"
        Me.lblRespiracion.Size = New System.Drawing.Size(74, 13)
        Me.lblRespiracion.TabIndex = 14
        Me.lblRespiracion.Text = "Respiración"
        '
        'lblGradosC
        '
        Me.lblGradosC.AutoSize = True
        Me.lblGradosC.Location = New System.Drawing.Point(294, 26)
        Me.lblGradosC.Name = "lblGradosC"
        Me.lblGradosC.Size = New System.Drawing.Size(20, 13)
        Me.lblGradosC.TabIndex = 8
        Me.lblGradosC.Text = "°C"
        '
        'lblFrecCard
        '
        Me.lblFrecCard.AutoSize = True
        Me.lblFrecCard.Location = New System.Drawing.Point(294, 58)
        Me.lblFrecCard.Name = "lblFrecCard"
        Me.lblFrecCard.Size = New System.Drawing.Size(34, 13)
        Me.lblFrecCard.TabIndex = 4
        Me.lblFrecCard.Text = "F. C."
        '
        'lblPulso
        '
        Me.lblPulso.AutoSize = True
        Me.lblPulso.Location = New System.Drawing.Point(214, 59)
        Me.lblPulso.Name = "lblPulso"
        Me.lblPulso.Size = New System.Drawing.Size(38, 13)
        Me.lblPulso.TabIndex = 3
        Me.lblPulso.Text = "Pulso"
        '
        'lblTemp
        '
        Me.lblTemp.AutoSize = True
        Me.lblTemp.Location = New System.Drawing.Point(214, 26)
        Me.lblTemp.Name = "lblTemp"
        Me.lblTemp.Size = New System.Drawing.Size(42, 13)
        Me.lblTemp.TabIndex = 2
        Me.lblTemp.Text = "Temp."
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(149, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(45, 13)
        Me.Label2.TabIndex = 1
        Me.Label2.Text = "mm/hg"
        '
        'lblPresionA
        '
        Me.lblPresionA.AutoSize = True
        Me.lblPresionA.Location = New System.Drawing.Point(8, 26)
        Me.lblPresionA.Name = "lblPresionA"
        Me.lblPresionA.Size = New System.Drawing.Size(72, 13)
        Me.lblPresionA.TabIndex = 0
        Me.lblPresionA.Text = "Presión art."
        '
        'gbxPrioridadSug
        '
        Me.gbxPrioridadSug.BackColor = System.Drawing.SystemColors.Control
        Me.gbxPrioridadSug.Controls.Add(Me.panPrioridad)
        Me.gbxPrioridadSug.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPrioridadSug.Location = New System.Drawing.Point(13, 344)
        Me.gbxPrioridadSug.Name = "gbxPrioridadSug"
        Me.gbxPrioridadSug.Size = New System.Drawing.Size(315, 88)
        Me.gbxPrioridadSug.TabIndex = 3
        Me.gbxPrioridadSug.TabStop = False
        Me.gbxPrioridadSug.Text = " Prioridad sugerida "
        '
        'panPrioridad
        '
        Me.panPrioridad.BackColor = System.Drawing.Color.Transparent
        Me.panPrioridad.Controls.Add(Me.lblColor)
        Me.panPrioridad.Controls.Add(Me.lblActuacion)
        Me.panPrioridad.Controls.Add(Me.lblDenominacion)
        Me.panPrioridad.Controls.Add(Me.lblNivel)
        Me.panPrioridad.Location = New System.Drawing.Point(6, 14)
        Me.panPrioridad.Name = "panPrioridad"
        Me.panPrioridad.Size = New System.Drawing.Size(303, 68)
        Me.panPrioridad.TabIndex = 3
        '
        'lblColor
        '
        Me.lblColor.AutoSize = True
        Me.lblColor.Location = New System.Drawing.Point(142, 2)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(36, 13)
        Me.lblColor.TabIndex = 6
        Me.lblColor.Text = "Color"
        '
        'lblActuacion
        '
        Me.lblActuacion.AutoSize = True
        Me.lblActuacion.Location = New System.Drawing.Point(3, 49)
        Me.lblActuacion.Name = "lblActuacion"
        Me.lblActuacion.Size = New System.Drawing.Size(126, 13)
        Me.lblActuacion.TabIndex = 5
        Me.lblActuacion.Text = "Tiempo de actuación"
        '
        'lblDenominacion
        '
        Me.lblDenominacion.AutoSize = True
        Me.lblDenominacion.Location = New System.Drawing.Point(3, 26)
        Me.lblDenominacion.Name = "lblDenominacion"
        Me.lblDenominacion.Size = New System.Drawing.Size(87, 13)
        Me.lblDenominacion.TabIndex = 4
        Me.lblDenominacion.Text = "Denominación"
        '
        'lblNivel
        '
        Me.lblNivel.AutoSize = True
        Me.lblNivel.Location = New System.Drawing.Point(3, 2)
        Me.lblNivel.Name = "lblNivel"
        Me.lblNivel.Size = New System.Drawing.Size(36, 13)
        Me.lblNivel.TabIndex = 3
        Me.lblNivel.Text = "Nivel"
        '
        'gbxDxPresuntivo
        '
        Me.gbxDxPresuntivo.BackColor = System.Drawing.SystemColors.Control
        Me.gbxDxPresuntivo.Controls.Add(Me.txtMotivo)
        Me.gbxDxPresuntivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxDxPresuntivo.Location = New System.Drawing.Point(12, 160)
        Me.gbxDxPresuntivo.Name = "gbxDxPresuntivo"
        Me.gbxDxPresuntivo.Size = New System.Drawing.Size(632, 77)
        Me.gbxDxPresuntivo.TabIndex = 1
        Me.gbxDxPresuntivo.TabStop = False
        Me.gbxDxPresuntivo.Text = " Diagnóstico presuntivo "
        '
        'txtMotivo
        '
        Me.txtMotivo.Location = New System.Drawing.Point(11, 20)
        Me.txtMotivo.Multiline = True
        Me.txtMotivo.Name = "txtMotivo"
        Me.txtMotivo.Size = New System.Drawing.Size(612, 46)
        Me.txtMotivo.TabIndex = 7
        '
        'ToolStrip1
        '
        Me.ToolStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.btnNuevo, Me.btnGuardar, Me.btnCancelar})
        Me.ToolStrip1.Location = New System.Drawing.Point(0, 0)
        Me.ToolStrip1.Name = "ToolStrip1"
        Me.ToolStrip1.Size = New System.Drawing.Size(657, 25)
        Me.ToolStrip1.TabIndex = 5
        Me.ToolStrip1.Text = "ToolStrip1"
        '
        'btnNuevo
        '
        Me.btnNuevo.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnNuevo.Image = Global.TRIAGE.My.Resources.Resources.DocumentHS
        Me.btnNuevo.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnNuevo.Name = "btnNuevo"
        Me.btnNuevo.Size = New System.Drawing.Size(23, 22)
        Me.btnNuevo.Text = "Nuevo"
        '
        'btnGuardar
        '
        Me.btnGuardar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnGuardar.Image = Global.TRIAGE.My.Resources.Resources.saveHS
        Me.btnGuardar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(23, 22)
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.Visible = False
        '
        'btnCancelar
        '
        Me.btnCancelar.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.btnCancelar.Image = Global.TRIAGE.My.Resources.Resources.onebit_33
        Me.btnCancelar.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.btnCancelar.Name = "btnCancelar"
        Me.btnCancelar.Size = New System.Drawing.Size(23, 22)
        Me.btnCancelar.Text = "ToolStripButton1"
        '
        'tmrPrioridad
        '
        Me.tmrPrioridad.Interval = 750
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 444)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(657, 22)
        Me.StatusStrip1.TabIndex = 11
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'gbxPrioridadSel
        '
        Me.gbxPrioridadSel.BackColor = System.Drawing.SystemColors.Control
        Me.gbxPrioridadSel.Controls.Add(Me.btnAceptar)
        Me.gbxPrioridadSel.Controls.Add(Me.LblTipoUrgencia)
        Me.gbxPrioridadSel.Controls.Add(Me.cbxTipoUrgencia)
        Me.gbxPrioridadSel.Controls.Add(Me.lblNivelAsig)
        Me.gbxPrioridadSel.Controls.Add(Me.cbxPrioridadSel)
        Me.gbxPrioridadSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPrioridadSel.Location = New System.Drawing.Point(329, 344)
        Me.gbxPrioridadSel.Name = "gbxPrioridadSel"
        Me.gbxPrioridadSel.Size = New System.Drawing.Size(315, 88)
        Me.gbxPrioridadSel.TabIndex = 4
        Me.gbxPrioridadSel.TabStop = False
        Me.gbxPrioridadSel.Text = " Seleccionar prioridad "
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(211, 58)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(95, 23)
        Me.btnAceptar.TabIndex = 18
        Me.btnAceptar.Text = "ACEPTAR"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'LblTipoUrgencia
        '
        Me.LblTipoUrgencia.AutoSize = True
        Me.LblTipoUrgencia.Location = New System.Drawing.Point(13, 25)
        Me.LblTipoUrgencia.Name = "LblTipoUrgencia"
        Me.LblTipoUrgencia.Size = New System.Drawing.Size(103, 13)
        Me.LblTipoUrgencia.TabIndex = 17
        Me.LblTipoUrgencia.Text = "Tipo de urgencia"
        '
        'cbxTipoUrgencia
        '
        Me.cbxTipoUrgencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoUrgencia.FormattingEnabled = True
        Me.cbxTipoUrgencia.Items.AddRange(New Object() {"AEV", "UC", "UNC"})
        Me.cbxTipoUrgencia.Location = New System.Drawing.Point(122, 22)
        Me.cbxTipoUrgencia.Name = "cbxTipoUrgencia"
        Me.cbxTipoUrgencia.Size = New System.Drawing.Size(138, 21)
        Me.cbxTipoUrgencia.TabIndex = 15
        '
        'lblNivelAsig
        '
        Me.lblNivelAsig.AutoSize = True
        Me.lblNivelAsig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNivelAsig.Location = New System.Drawing.Point(13, 63)
        Me.lblNivelAsig.Name = "lblNivelAsig"
        Me.lblNivelAsig.Size = New System.Drawing.Size(36, 13)
        Me.lblNivelAsig.TabIndex = 11
        Me.lblNivelAsig.Text = "Nivel"
        '
        'cbxPrioridadSel
        '
        Me.cbxPrioridadSel.BackColor = System.Drawing.SystemColors.Window
        Me.cbxPrioridadSel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPrioridadSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPrioridadSel.FormattingEnabled = True
        Me.cbxPrioridadSel.Items.AddRange(New Object() {"  I  -  R o j o ", "  II  -  A m a r i ll o", "  III  -  V e r d e", "  IV  -  A z u l"})
        Me.cbxPrioridadSel.Location = New System.Drawing.Point(55, 60)
        Me.cbxPrioridadSel.Name = "cbxPrioridadSel"
        Me.cbxPrioridadSel.Size = New System.Drawing.Size(138, 21)
        Me.cbxPrioridadSel.TabIndex = 16
        '
        'frmEvaluar
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(657, 466)
        Me.Controls.Add(Me.gbxPrioridadSel)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.ToolStrip1)
        Me.Controls.Add(Me.gbxDxPresuntivo)
        Me.Controls.Add(Me.gbxPrioridadSug)
        Me.Controls.Add(Me.gbxSignos)
        Me.Controls.Add(Me.gbxValoracion)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmEvaluar"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "TRIAGE - Evaluación de Pacientes"
        Me.gbxValoracion.ResumeLayout(False)
        Me.gbxValoracion.PerformLayout()
        Me.gbxSignos.ResumeLayout(False)
        Me.gbxSignos.PerformLayout()
        Me.gbxPrioridadSug.ResumeLayout(False)
        Me.panPrioridad.ResumeLayout(False)
        Me.panPrioridad.PerformLayout()
        Me.gbxDxPresuntivo.ResumeLayout(False)
        Me.gbxDxPresuntivo.PerformLayout()
        Me.ToolStrip1.ResumeLayout(False)
        Me.ToolStrip1.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxPrioridadSel.ResumeLayout(False)
        Me.gbxPrioridadSel.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents gbxValoracion As System.Windows.Forms.GroupBox
    Friend WithEvents gbxSignos As System.Windows.Forms.GroupBox
    Friend WithEvents gbxPrioridadSug As System.Windows.Forms.GroupBox
    Friend WithEvents lblPaterno As System.Windows.Forms.Label
    Friend WithEvents lblDerechohabiencia As System.Windows.Forms.Label
    Friend WithEvents lblEdad As System.Windows.Forms.Label
    Friend WithEvents lblSexo As System.Windows.Forms.Label
    Friend WithEvents lblFrecCard As System.Windows.Forms.Label
    Friend WithEvents lblPulso As System.Windows.Forms.Label
    Friend WithEvents lblTemp As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents lblPresionA As System.Windows.Forms.Label
    Friend WithEvents gbxDxPresuntivo As System.Windows.Forms.GroupBox
    Friend WithEvents txtMotivo As System.Windows.Forms.TextBox
    Friend WithEvents ToolStrip1 As System.Windows.Forms.ToolStrip
    Friend WithEvents lblGradosC As System.Windows.Forms.Label
    Friend WithEvents btnNuevo As System.Windows.Forms.ToolStripButton
    Friend WithEvents btnGuardar As System.Windows.Forms.ToolStripButton
    Friend WithEvents txtEdad As System.Windows.Forms.TextBox
    Friend WithEvents txtDerechohabiencia As System.Windows.Forms.TextBox
    Friend WithEvents txtSexo As System.Windows.Forms.TextBox
    Friend WithEvents panPrioridad As System.Windows.Forms.Panel
    Friend WithEvents lblActuacion As System.Windows.Forms.Label
    Friend WithEvents lblDenominacion As System.Windows.Forms.Label
    Friend WithEvents lblNivel As System.Windows.Forms.Label
    Friend WithEvents tmrPrioridad As System.Windows.Forms.Timer
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblRespiracion As System.Windows.Forms.Label
    Friend WithEvents btnEvaluar As System.Windows.Forms.Button
    Friend WithEvents cbxEstado As System.Windows.Forms.ComboBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents gbxPrioridadSel As System.Windows.Forms.GroupBox
    Friend WithEvents lblNivelAsig As System.Windows.Forms.Label
    Friend WithEvents cbxPrioridadSel As System.Windows.Forms.ComboBox
    Friend WithEvents txtPaSistolica As System.Windows.Forms.TextBox
    Friend WithEvents txtPaDiastolica As System.Windows.Forms.TextBox
    Friend WithEvents txtRespiracion As System.Windows.Forms.TextBox
    Friend WithEvents txtPulso As System.Windows.Forms.TextBox
    Friend WithEvents txtTemp As System.Windows.Forms.TextBox
    Friend WithEvents LblTipoUrgencia As System.Windows.Forms.Label
    Friend WithEvents cbxTipoUrgencia As System.Windows.Forms.ComboBox
    Friend WithEvents cbxPaciente As System.Windows.Forms.ComboBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents btnCancelar As System.Windows.Forms.ToolStripButton

End Class
