<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPacientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListadoPacientes))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvListado = New System.Windows.Forms.DataGridView
        Me.cmsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmAtender = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmEliminar = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmpediatria = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmTriage = New System.Windows.Forms.ToolStripMenuItem
        Me.btnRefrescar = New System.Windows.Forms.Button
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.IdT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colHoraEval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Materno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Edad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsMenu.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdT, Me.Prioridad, Me.Numero, Me.colHoraEval, Me.Curp, Me.Paterno, Me.Materno, Me.Nombre, Me.Sexo, Me.Edad, Me.colNumCtrl})
        Me.dgvListado.ContextMenuStrip = Me.cmsMenu
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(10, 104)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(840, 310)
        Me.dgvListado.TabIndex = 0
        Me.dgvListado.TabStop = False
        '
        'cmsMenu
        '
        Me.cmsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmMenu, Me.ToolStripSeparator1, Me.tsmAtender, Me.tsmEliminar, Me.tsmpediatria, Me.tsmTriage})
        Me.cmsMenu.Name = "cmsOpciones"
        Me.cmsMenu.Size = New System.Drawing.Size(188, 120)
        '
        'tsmMenu
        '
        Me.tsmMenu.Name = "tsmMenu"
        Me.tsmMenu.Size = New System.Drawing.Size(187, 22)
        Me.tsmMenu.Text = "Menu"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(184, 6)
        '
        'tsmAtender
        '
        Me.tsmAtender.Name = "tsmAtender"
        Me.tsmAtender.Size = New System.Drawing.Size(187, 22)
        Me.tsmAtender.Text = "Atender Consulta"
        '
        'tsmEliminar
        '
        Me.tsmEliminar.Name = "tsmEliminar"
        Me.tsmEliminar.Size = New System.Drawing.Size(187, 22)
        Me.tsmEliminar.Text = "Eliminar"
        '
        'tsmpediatria
        '
        Me.tsmpediatria.Name = "tsmpediatria"
        Me.tsmpediatria.Size = New System.Drawing.Size(187, 22)
        Me.tsmpediatria.Text = "Urgencias Pediatricas"
        '
        'tsmTriage
        '
        Me.tsmTriage.Name = "tsmTriage"
        Me.tsmTriage.Size = New System.Drawing.Size(187, 22)
        Me.tsmTriage.Text = "Atendido Triage"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefrescar.Location = New System.Drawing.Point(710, 438)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(140, 23)
        Me.btnRefrescar.TabIndex = 3
        Me.btnRefrescar.Text = "Refrescar lista"
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(13, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 479)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(865, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'IdT
        '
        Me.IdT.HeaderText = "IdTriage"
        Me.IdT.Name = "IdT"
        Me.IdT.ReadOnly = True
        Me.IdT.Visible = False
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'Numero
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero.DefaultCellStyle = DataGridViewCellStyle3
        Me.Numero.HeaderText = "No."
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Visible = False
        Me.Numero.Width = 35
        '
        'colHoraEval
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHoraEval.DefaultCellStyle = DataGridViewCellStyle4
        Me.colHoraEval.HeaderText = "Hora Evaluación"
        Me.colHoraEval.Name = "colHoraEval"
        Me.colHoraEval.ReadOnly = True
        '
        'Curp
        '
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Curp.DefaultCellStyle = DataGridViewCellStyle5
        Me.Curp.HeaderText = "CURP"
        Me.Curp.Name = "Curp"
        Me.Curp.ReadOnly = True
        Me.Curp.Visible = False
        Me.Curp.Width = 155
        '
        'Paterno
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paterno.DefaultCellStyle = DataGridViewCellStyle6
        Me.Paterno.HeaderText = "Apellido Paterno"
        Me.Paterno.Name = "Paterno"
        Me.Paterno.ReadOnly = True
        Me.Paterno.Width = 150
        '
        'Materno
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Materno.DefaultCellStyle = DataGridViewCellStyle7
        Me.Materno.HeaderText = "Apellido Materno"
        Me.Materno.Name = "Materno"
        Me.Materno.ReadOnly = True
        Me.Materno.Width = 150
        '
        'Nombre
        '
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle8
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 150
        '
        'Sexo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sexo.DefaultCellStyle = DataGridViewCellStyle9
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        Me.Sexo.Width = 40
        '
        'Edad
        '
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Edad.DefaultCellStyle = DataGridViewCellStyle10
        Me.Edad.HeaderText = "Edad"
        Me.Edad.Name = "Edad"
        Me.Edad.ReadOnly = True
        Me.Edad.Width = 40
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Visible = False
        '
        'frmListadoPacientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(865, 501)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvListado)
        Me.Controls.Add(Me.btnRefrescar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmListadoPacientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de pacientes"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsMenu.ResumeLayout(False)
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnRefrescar As System.Windows.Forms.Button
    Friend WithEvents cmsMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmAtender As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmEliminar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsmpediatria As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents IdT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHoraEval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Edad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
