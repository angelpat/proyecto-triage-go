Public Class frmBuscarPaciente
    Dim cnn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
    Dim lector As System.Data.SqlClient.SqlDataReader
    Dim comando As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand
    Dim vynContinua As Boolean = True


#Region "Comentarios"
    'La Busqueda de pacientes, busca pacientes que esten hospitalizados
#End Region

    Function ValidaCampos() As Boolean
        If txtPaterno.Text = "" And txtMaterno.Text = "" And txtNombre.Text = "" Then
            MessageBox.Show("No ha indicado algun criterio de busqueda", "Advertencia")
            txtPaterno.Focus()
            Return False
        End If
        Return True
    End Function

    Private Sub RefrescarDatos()
        frmPrincipalTriage.VchIdPacienteG = dgvPacientes.SelectedCells(0).Value
        txtPaterno.Text = dgvPacientes.SelectedCells(1).Value
        txtMaterno.Text = dgvPacientes.SelectedCells(2).Value
        txtNombre.Text = dgvPacientes.SelectedCells(3).Value
    End Sub

    Private Sub txtPaterno_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPaterno.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtMaterno.Focus()
        End If
    End Sub

    Private Sub txtMaterno_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtMaterno.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            txtNombre.Focus()
        End If
    End Sub

    Private Sub txtNombre_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtNombre.KeyPress
        If e.KeyChar = Microsoft.VisualBasic.ChrW(13) Then
            btnBuscarPaciente.Focus()
        End If
    End Sub

    Private Sub btnBuscarPaciente_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnBuscarPaciente.Click
        Try
            Dim VchCadena As String
            Dim Edad As String
            dgvPacientes.Rows.Clear()
            If ValidaCampos() Then
                VchCadena = "SELECT PAC.IDPACIENTE, PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE, PAC.CSEXO, GETDATE(), PAC.DFECHANACIMIENTO, ADM.CNUM_CONTROL " & _
                            "FROM CTL_PACIENTES PAC, HGC_ADMISION ADM WHERE PAC.IDPACIENTE=ADM.IDPACIENTE AND ADM.CSTATUS='H' AND ADM.IDSERVICIO = '55' " & _
                            "AND PAC.CPATERNO LIKE '" + txtPaterno.Text + "%' AND PAC.CMATERNO LIKE '" + txtMaterno.Text + "%' AND PAC.CNOMBRE LIKE'%" + txtNombre.Text + "%' " & _
                            "ORDER BY PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE"
                comando = New SqlClient.SqlCommand(VchCadena, cnn)
                cnn.Close()
                cnn.Open()
                lector = comando.ExecuteReader
                If lector.HasRows Then
                    vynContinua = True
                    While lector.Read
                        frmPrincipalTriage.VchIdPacienteG = lector.GetString(0)
                        txtPaterno.Text = lector.GetString(1)
                        txtMaterno.Text = lector.GetString(2)
                        txtNombre.Text = lector.GetString(3)
                        Edad = frmEvaluar.ObtenerEdad(lector.GetDateTime(5).Date, lector.GetDateTime(6).Date).ToString
                        If lector.GetString(0).Length > 0 Then
                            dgvPacientes.Rows.Add(lector.GetString(0), lector.GetString(1), lector.GetString(2), lector.GetString(3), lector.GetString(4), Edad)
                        End If
                    End While
                End If
                cnn.Close()
                RefrescarDatos()
            End If
        Catch ex As Exception
            MessageBox.Show("El paciente no se encuetra en la Base de Datos o no escribi� los datos correctamente", "Error no esperado")
            'MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Sub

    Private Sub dgvPacientes_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPacientes.CellClick
        RefrescarDatos()
    End Sub

    Private Sub dgvPacientes_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles dgvPacientes.KeyDown, dgvPacientes.KeyUp
        RefrescarDatos()
    End Sub

    Private Sub btnNuevaBusqueda_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevaBusqueda.Click
        txtPaterno.Clear()
        txtMaterno.Clear()
        txtNombre.Clear()
        dgvPacientes.Rows.Clear()
        txtPaterno.Focus()
    End Sub

    'Private Sub btnRegresar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRegresar.Click
    '    Salir()
    'End Sub

    'Private Sub dgvPacientes_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPacientes.CellDoubleClick
    '    Salir()
    'End Sub

    'Private Sub Salir()
    '    If frmTriage.Visible = True Then
    '        frmTriage.BuscarPaciente()
    '        frmTriage.SeleccionaUnidadMedica()
    '    End If
    '    Close()
    'End Sub

    Private Sub frmBuscarPaciente_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Try
            Dim VchCadena As String
            Dim Edad As String
            dgvPacientes.Rows.Clear()
            VchCadena = "SELECT PAC.IDPACIENTE, PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE, PAC.CSEXO, GETDATE(), PAC.DFECHANACIMIENTO, ADM.CNUM_CONTROL " & _
                            "FROM CTL_PACIENTES PAC, HGC_ADMISION ADM WHERE PAC.IDPACIENTE=ADM.IDPACIENTE AND ADM.CSTATUS='H' AND ADM.IDSERVICIO = '55' " & _
                            "AND adm.idareamedica = '02' and adm.ccve_cama is null ORDER BY PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE"
            comando = New SqlClient.SqlCommand(VchCadena, cnn)
            cnn.Close()
            cnn.Open()
            lector = comando.ExecuteReader
            If lector.HasRows Then
                vynContinua = True
                While lector.Read
                    frmPrincipalTriage.VchIdPacienteG = lector.GetString(0)
                    txtPaterno.Text = lector.GetString(1)
                    txtMaterno.Text = lector.GetString(2)
                    txtNombre.Text = lector.GetString(3)
                    Edad = frmEvaluar.ObtenerEdad(lector.GetDateTime(5).Date, lector.GetDateTime(6).Date).ToString
                    If lector.GetString(0).Length > 0 Then
                        dgvPacientes.Rows.Add(lector.GetString(0), lector.GetString(1), lector.GetString(2), lector.GetString(3), lector.GetString(4), Edad)
                    End If
                End While
            End If
            cnn.Close()
            RefrescarDatos()

        Catch ex As Exception
            MessageBox.Show("No hay pacientes en espera del Triage", "Error no esperado")
            'MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Sub

    Private Sub dgvPacientes_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvPacientes.CellContentClick

    End Sub
End Class