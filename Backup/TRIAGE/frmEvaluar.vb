Imports System.Data.SqlClient
Imports Microsoft.VisualBasic.ControlChars
Public Class frmEvaluar
    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader
    Dim varPaciente As String
    'Dim varCnum_control As String
    Public VchIdUMedica As String

#Region "Comentarios"
    '================================================================================='
    '                 Descripci�n de las funciones y procedimientos
    '================================================================================='
    ' ObtenerEdadPaciente() - Obtiene la edad del paciente
    ' ObtenerPrioridad() - Regresa la prioridad evaluada 
    ' BuscaPacienteLista() - Busca un paciente en la lista de espera con Estado 'E'
    ' RegresaEmpleado() -Regresa el nombre completo del empleado
    ' SoloNumeros() - Valida solo numeros 
    ' SoloNumerosD() - Valida solo numeros decimales
#End Region

#Region "Actualizar cama"
    Private Function SeleccionaCama() As Int32
        Dim NoCama As Int32
        Dim VchCadena As String
        cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
        VchCadena = "SELECT MIN(CCVE_CAMA) AS NOCAMA FROM CTL_CAMAS WHERE IDSERVICIO=55 AND IDAREAMEDICA=2 AND CSTATUS='D'"
        comando = New SqlClient.SqlCommand(VchCadena, cnn)
        cnn.Close()
        cnn.Open()
        lector = comando.ExecuteReader
        If lector.HasRows Then
            While lector.Read
                NoCama = lector.GetInt32(0)
            End While
        End If
        Return NoCama
    End Function

    'Private Function SeleccionaIdNum_Control() As String
    '    Dim IdNum_Control As String
    '    Dim VchCadena As String
    '    IdNum_Control = ""
    '    cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
    '    VchCadena = "SELECT CNUM_CONTROL FROM HGC_ADMISION WHERE IDPACIENTE='" + frmPrincipalTriage.VchIdPacienteG + "' AND CSTATUS='H' and idservicio = '55'"
    '    comando = New SqlClient.SqlCommand(VchCadena, cnn)
    '    cnn.Close()
    '    cnn.Open()
    '    If comando.ExecuteScalar().ToString() <> "" Then
    '        IdNum_Control = comando.ExecuteScalar()
    '    End If
    '    Return IdNum_Control
    'End Function

    'Private Sub ActualizaClaveCama(ByVal cCve_Cama As String, ByVal IdNumControl As String, ByVal TipoUrgencia As String)
    '    Dim VchCadena As String
    '    Try
    '        'TipoUrgencia->Actualiza el campo observaciones
    '        VchCadena = "UPDATE HGC_ADMISION SET CCVE_CAMA='" + cCve_Cama + "', CCOMENTARIO='" + TipoUrgencia + "' WHERE CNUM_CONTROL='" + IdNumControl + "'"
    '        comando = New SqlClient.SqlCommand(VchCadena, cnn)
    '        cnn.Close()
    '        cnn.Open()
    '        lector = comando.ExecuteReader
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message.ToString, "Error no esperado")
    '    End Try
    'End Sub

    'Private Sub ActualizaEstadoCama(ByVal cCve_Cama As String)
    '    Dim VchCadena As String
    '    Try
    '        VchCadena = "UPDATE CTL_CAMAS SET CSTATUS='O' WHERE CCVE_CAMA='" + cCve_Cama + "'"
    '        'TipoUrgencia->Actualiza el campo observaciones
    '        VchCadena = "UPDATE HGC_ADMISION SET CCVE_CAMA='" + cCve_Cama + "', CCOMENTARIO='" + TipoUrgencia + "' WHERE CNUM_CONTROL='" + IdNumControl + "'"
    '        comando = New SqlClient.SqlCommand(VchCadena, cnn)
    '        cnn.Close()
    '        cnn.Open()
    '        lector = comando.ExecuteReader
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message.ToString, "Error no esperado")
    '    End Try
    'End Sub
#End Region

    Public Function RegresaEmpleado() As String
        Try
            Dim VchCadena As String
            cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
            VchCadena = "SELECT CPATERNO, CMATERNO, CNOMBRE FROM PERSONAL WHERE IDPERSONAL='" + frmPrincipalTriage.VchIdEmpleadoG + "'"
            comando = New SqlClient.SqlCommand(VchCadena, cnn)
            cnn.Close()
            cnn.Open()
            lector = comando.ExecuteReader
            If lector.HasRows Then
                While lector.Read
                    If lector.GetString(0) <> "" Then
                        Return (lector.GetString(0) + " " + lector.GetString(1) + " " + lector.GetString(2))
                    End If
                End While
            End If
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        Return ("")
    End Function

#Region "Valorar"
    Public Function ObtenerEdad(ByVal FechaActual As Date, ByVal FechaNacimiento As Date) As String
        'http://msdn2.microsoft.com/es-es/library/b5xbyt6f(VS.80).aspx
        'FechaActual -> Fecha del servidor
        Dim Meses As Integer = DateDiff("m", FechaNacimiento, FechaActual)
        If Meses < 12 Then
            Return Meses.ToString + " m"
        Else
            Return (Meses \ 12).ToString + " a"
        End If
    End Function

    Function ObtenerPrioridad() As Integer
        Dim Puntos As Integer = PresionSistolica() + Temperatura() + Respiraciones() + FrecuenciaCardiaca() + (cbxEstado.SelectedIndex() + 1)
        If Puntos >= 12 Then
            Return 1
        ElseIf Puntos >= 9 And Puntos <= 11 Then
            Return 2
        ElseIf Puntos >= 6 And Puntos <= 8 Then
            Return 3
        ElseIf Puntos <= 5 Then
            Return 4
        End If
    End Function

    Function PresionSistolica() As Integer
        Dim PresionS As Integer
        PresionS = txtPaSistolica.Text
        If PresionS >= 90 And PresionS <= 140 Then  '90,...,140
            Return 1
        End If
        If PresionS > 140 And PresionS <= 180 Then  '141,...,180
            Return 2
        End If
        If PresionS < 90 Or PresionS > 180 Then     '0,...,89 y 181,...,999
            Return 3
        End If
    End Function

    Function Temperatura() As Integer
        Dim Temp As Double
        Temp = txtTemp.Text
        If Temp >= 35.5 And Temp <= 37.5 Then  '35.5,...,37.5
            Return 1
        End If
        If Temp > 37.5 And Temp < 39 Then      '37.6,...,38.9
            Return 2
        End If
        If Temp < 35.5 Or Temp >= 39 Then      '0,...,35 y 39,...,99
            Return 3
        End If
    End Function

    Function Respiraciones() As Integer
        Dim Respiracion As Integer
        Respiracion = txtRespiracion.Text
        If Respiracion >= 10 And Respiracion <= 20 Then    '10,...,20
            Return 1
        End If
        If Respiracion > 20 And Respiracion <= 30 Then     '21,...,30
            Return 2
        End If
        If Respiracion < 10 Or Respiracion > 30 Then       '0,...,9 y 31,...99
            Return 3
        End If
    End Function

    Function FrecuenciaCardiaca() As Integer
        Dim FrecuenciaC As Integer
        FrecuenciaC = txtPulso.Text
        If FrecuenciaC >= 50 And FrecuenciaC <= 90 Then    '50,...,90
            Return 1
        End If
        If FrecuenciaC > 90 And FrecuenciaC <= 120 Then    '91,...,120
            Return 2
        End If
        If FrecuenciaC < 50 Or FrecuenciaC > 120 Then      '0,...,49 y 121,...,999
            Return 3
        End If
    End Function
#End Region

    Function ValidarDatos() As Boolean
        If cbxPaciente.Text = "" Then
            MessageBox.Show("No ha seleccionado al paciente", "Advertencia")
            Return False
        End If

        If txtPaSistolica.Text = "" Then
            MessageBox.Show("Falta ingresar Presion Sist�lica", "Advertencia")
            Return False
        End If

        If txtPaDiastolica.Text = "" Then
            MessageBox.Show("Falta ingresar Presion Diast�lica", "Advertencia")
            Return False
        End If

        If txtTemp.Text = "  ." Then
            MessageBox.Show("Falta ingresar la Temperatura", "Advertencia")
            Return False
        End If

        If txtRespiracion.Text = "" Then
            MessageBox.Show("Falta ingresar la Respiraci�n", "Advertencia")
            Return False
        End If

        If txtPulso.Text = "" Then
            MessageBox.Show("Falta ingresar el Pulso", "Advertencia")
            Return False
        End If

        If cbxEstado.SelectedIndex() = -1 Then
            MessageBox.Show("Falta seleccionar el Estado de alerta", "Advertencia")
            Return False
        End If

        Return True
    End Function

    Function NuevoIDTRIAGE() As String
        Dim VchCadena As String
        Dim VchIdTriage As String = 0
        cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
        VchCadena = "SELECT MAX(IDTRIAGE)+1 FROM ZTRI_TRIAGE"
        comando = New SqlClient.SqlCommand(VchCadena, cnn)
        cnn.Close()
        cnn.Open()
        lector = comando.ExecuteReader
        While lector.Read
            If lector.IsDBNull(0) = True Then
                VchIdTriage = "1"
            Else
                VchIdTriage = lector.GetInt32(0)
            End If
        End While
        Return (VchIdTriage)
    End Function

    Private Sub MostrarPrioridad(ByVal Prioridad As Integer)
        If lblNivel.Text = "Nivel" Then
            lblNivel.Text = lblNivel.Text + " " + System.Convert.ToString(Prioridad)
        End If
        tmrPrioridad.Enabled = True
        Select Case Prioridad
            Case 1
                lblColor.Text = "Rojo"
                lblDenominacion.Text = "Atenci�n inmediata"
                lblActuacion.Text = "Valoraci�n m�dica < 5 min"
                panPrioridad.BackColor = Color.Red
            Case 2
                lblColor.Text = "Amarillo"
                lblDenominacion.Text = "Urgencia"
                lblActuacion.Text = "Valoraci�n m�dica < 15 min"
                panPrioridad.BackColor = Color.Yellow
            Case 3
                lblColor.Text = "Verde"
                lblDenominacion.Text = "Urgencia menor"
                lblActuacion.Text = "Valoraci�n m�dica 15 a 30 min"
                'panPrioridad.BackColor = Color.Green
                panPrioridad.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
            Case 4
                lblColor.Text = "Azul"
                lblDenominacion.Text = "No urgente"
                lblActuacion.Text = "Valoraci�n m�dica > 30 min"
                panPrioridad.BackColor = Color.SkyBlue
                'panPrioridad.BackColor = Color.FromArgb(8, 49, 99) ' Color Azul logo del Hospital
        End Select
    End Sub

    Public Sub SeleccionaUnidadMedica()
        Dim VchCadena As String
        cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
        VchCadena = "SELECT IDUMEDICA, CNOMBRE FROM CTL_UMEDICAS WHERE IDUMEDICA='" + VchIdUMedica + "' ORDER BY CNOMBRE"
        comando = New SqlClient.SqlCommand(VchCadena, cnn)
        cnn.Close()
        cnn.Open()
        lector = comando.ExecuteReader
        If lector.HasRows Then
            While lector.Read
                If lector.GetString(0) Then
                    txtDerechohabiencia.Text = lector.GetString(1)
                End If
            End While
        End If
    End Sub

    Public Sub CargaPacientes()
        Try
            lblNombreRevisor.Text = RegresaEmpleado()
            cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlClient.SqlCommand("SELECT p.CPATERNO+' '+p.CMATERNO+' '+p.CNOMBRE, a.cnum_control,p.IDPACIENTE " & _
                " FROM CTL_PACIENTES p, hgc_admision a  WHERE a.cstatus = 'H'" & _
                " and a.idservicio = '55' and a.idareamedica = '11' and a.ccve_cama is null" & _
                " and p.idpaciente = a.idpaciente", cnn)
            cnn.Open()
            lector = comando.ExecuteReader()
            cbxPaciente.Items.Clear()
            cbxPaciente.Text = ""
            While lector.Read()
                cbxPaciente.Items.Add(lector.Item(0).ToString + " - " + lector.Item(1).ToString + " - " + lector.Item(2).ToString)
            End While
            lector.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los pacientes." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try
        cnn.Close()
    End Sub

    Private Sub frmPrincipal_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If frmPrincipalTriage.vchTipo <> "01" And frmPrincipalTriage.vchTipo <> "02" Then
            Me.gbxDxPresuntivo.Enabled = False
            Me.gbxSignos.Enabled = False
            Me.gbxPrioridadSug.Enabled = False
            Me.gbxPrioridadSel.Enabled = False
        End If
        CargaPacientes()
    End Sub

    Private Sub tmrPrioridad_Tick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tmrPrioridad.Tick
        If panPrioridad.BackColor = Color.Transparent Then
            MostrarPrioridad(ObtenerPrioridad())
        Else
            panPrioridad.BackColor = Color.Transparent
        End If
    End Sub

    Private Sub btnNuevo_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnNuevo.Click
        Try
            CargaPacientes()

            tmrPrioridad.Enabled = False
            txtEdad.Clear()
            txtSexo.Clear()
            txtDerechohabiencia.Clear()

            txtMotivo.Clear()
            txtPaSistolica.Clear()
            txtPaDiastolica.Clear()
            txtTemp.Clear()
            txtRespiracion.Clear()
            txtPulso.Clear()
            cbxEstado.SelectedIndex() = -1
            cbxTipoUrgencia.SelectedIndex() = -1
            cbxPrioridadSel.SelectedIndex() = -1
            cbxPrioridadSel.BackColor = Color.White

            lblNivel.Text = "Nivel"
            lblDenominacion.Text = "Denominaci�n"
            lblActuacion.Text = "Tiempo de actuaci�n"
            lblColor.Text = "Color"

            panPrioridad.BackColor = Color.Transparent
            cbxPaciente.Focus()

        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los pacientes." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try
    End Sub

    Private Sub panPrioridad_DoubleClick(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles panPrioridad.DoubleClick
        tmrPrioridad.Enabled = False
    End Sub

    Private Sub cbxPrioridadSel_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPrioridadSel.SelectedIndexChanged
        If cbxPrioridadSel.SelectedIndex() = 0 Then
            cbxPrioridadSel.BackColor = Color.Red
        ElseIf cbxPrioridadSel.SelectedIndex() = 1 Then
            cbxPrioridadSel.BackColor = Color.Yellow
        ElseIf cbxPrioridadSel.SelectedIndex() = 2 Then
            'cbxPrioridadSel.BackColor = Color.Green
            cbxPrioridadSel.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
        ElseIf cbxPrioridadSel.SelectedIndex() = 3 Then
            cbxPrioridadSel.BackColor = Color.SkyBlue
            'cbxPrioridadSel.BackColor = Color.FromArgb(8, 49, 99) ' Color Azul logo del Hospital
        End If
    End Sub

    Private Sub btnEvaluar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEvaluar.Click
        Dim VchPrioridad As Integer
        If ValidarDatos() Then
            VchPrioridad = ObtenerPrioridad()
            MostrarPrioridad(VchPrioridad)
            cbxPrioridadSel.SelectedIndex = (VchPrioridad - 1)
            cbxTipoUrgencia.Focus()
        End If
    End Sub

    Private Sub Guardar_TRIAGE(ByVal IdNumControl As String, ByVal idpaciente As String, Optional ByVal ccve_cama As String = "null")

        Dim VchCadena As String
        Dim Prioridad As String
        Dim IdTriage As String = NuevoIDTRIAGE()
        Dim EstadoAlerta As String
        Dim Respiracion As String = 0
        Dim Pulso As String = 0
        Dim Nota As String
        Dim strEstado As String
        Dim vinEdad As Int32
        Dim transaccion As SqlTransaction

        Try
            cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
            cnn.Open()
            comando = New SqlClient.SqlCommand("", cnn)
            transaccion = cnn.BeginTransaction()
            comando.Transaction = transaccion

            If cbxEstado.SelectedIndex() = 0 Then
                EstadoAlerta = "O"  'Orientada                
            ElseIf cbxEstado.SelectedIndex() = 1 Then
                EstadoAlerta = "D"  'Desorientado
            ElseIf cbxEstado.SelectedIndex() = 2 Then
                EstadoAlerta = "E"  'Estuporoso
            End If

            Prioridad = System.Convert.ToString(cbxPrioridadSel.SelectedIndex() + 1)
            If Prioridad = 1 Then
                strEstado = "O"
                'ElseIf Prioridad = 4 Then
                '    strEstado = "A"
            Else
                strEstado = "E"
            End If

            ' MODIFICA CAMA/SERVICIO
            If Prioridad > 1 Then
                VchCadena = "UPDATE CTL_CAMAS SET CSTATUS='O' WHERE CCVE_CAMA='" + ccve_cama + "'"
                comando.CommandText = VchCadena
                comando.ExecuteNonQuery()
                VchCadena = "UPDATE HGC_ADMISION SET CCVE_CAMA='" + ccve_cama + "', CCOMENTARIO='" + cbxTipoUrgencia.Text + "', idareamedica = '02' WHERE CNUM_CONTROL='" + IdNumControl + "'"
                comando.CommandText = VchCadena
                comando.ExecuteNonQuery()
            ElseIf Prioridad = 1 Then
                VchCadena = "Select ((datediff (dd,dfechanacimiento, getdate()) - cast((datediff (yy,dfechanacimiento, getdate())/4) as int)  ) / 365) " & _
                    "from ctl_pacientes where idpaciente = '" & idpaciente & "'"
                comando.CommandText = VchCadena
                vinEdad = comando.ExecuteScalar()

                If vinEdad < 16 Then        'PASA A URGENCIAS PEDIATRICAS
                    VchCadena = "UPDATE HGC_ADMISION SET CCOMENTARIO='" + cbxTipoUrgencia.Text + "', idareamedica = '03' WHERE CNUM_CONTROL='" + IdNumControl + "'"
                    comando.CommandText = VchCadena
                    comando.ExecuteNonQuery()
                Else                                  'PASA A HOSPITALIZACI�N URGENCIAS
                    VchCadena = "UPDATE HGC_ADMISION SET CCOMENTARIO='" + cbxTipoUrgencia.Text + "', idareamedica = '01' WHERE CNUM_CONTROL='" + IdNumControl + "'"
                    comando.CommandText = VchCadena
                    comando.ExecuteNonQuery()
                End If
            End If

            ' Inserta en tabla de Triage
            VchCadena = "INSERT INTO ZTRI_TRIAGE (IDTRIAGE, IDPACIENTE, IDPERSONAL, MOTIVO, PASISTOLICA, PADIASTOLICA, " & _
            "TEMPERATURA, RESPIRACION, PULSO, ESTADOALERTA, ESTADO, FECHAE, PRIORIDAD, CREADO_POR, F_CREACION, " & _
            " MODIFICADO_POR, F_MODIFICACION, CNUM_CONTROL) VALUES ('" + IdTriage + "','" + varPaciente + "','" + frmPrincipalTriage.VchIdEmpleadoG + "','" + txtMotivo.Text + "','" + txtPaSistolica.Text + "','" + txtPaDiastolica.Text + " ','" + txtTemp.Text + "','" + txtRespiracion.Text + "','" + txtPulso.Text + "','" + EstadoAlerta + "','" & strEstado & "',GETDATE(),'" + Prioridad + "','" + frmPrincipalTriage.VchIdEmpleadoG + "',GETDATE(),'" + frmPrincipalTriage.VchIdEmpleadoG + "',GETDATE(), " + IdNumControl + ")"
            comando.CommandText = VchCadena
            comando.ExecuteNonQuery()

            ' Ahora inserta en la tabla de notas medicas para el medico del consultorio/hospitalizaci�n urg
            Nota = CrLf & txtMotivo.Text.ToUpper() & CrLf & CrLf & "Presi�n Arterial: " & txtPaSistolica.Text & " / " & txtPaDiastolica.Text & CrLf & CrLf & "Temperatura: " & txtTemp.Text & "�C" & CrLf & CrLf & _
                "Frecuencia Cardiaca: " & txtPulso.Text & CrLf & CrLf & "Frecuencia Respiratoria: " & txtRespiracion.Text & CrLf & CrLf & "Estado de alerta: " & cbxEstado.Text

            VchCadena = "INSERT INTO HGC_NOTA_EVOLUCION_ENC(cnum_control, cnum_nota_evolucion, ccve_titulonota, dfecha_nota, idservicio, idareamedica, cnota, idpersonal_res, idpersonal_sup, cfirma_nota, cstatus) " & _
                "VALUES(" & IdNumControl & ",1,'21',Getdate(),55, '11', '" & Nota & "','" & frmPrincipalTriage.VchIdEmpleadoG & "','" & frmPrincipalTriage.VchIdEmpleadoG & "','N','A')"
            comando.CommandText = VchCadena
            comando.ExecuteNonQuery()

            'If Prioridad = 4 Then '   ya se fue

            '    ' DAR EL ALTA A LA RGUOUEOUEEEE
            '    VchCadena = "update hgc_admision set cstatus = 'A', cindicaciones_egreso = 'VALORADO EN TRIAGE', idpersonal_alta = '" & frmPrincipalTriage.VchIdEmpleadoG & "', " & _
            '        "dfecha_alta = getdate(), ctipo_alta = '3' where cnum_control = " & IdNumControl
            '    comando.CommandText = VchCadena
            '    comando.ExecuteNonQuery()

            '    VchCadena = "update ctl_pacientes set cstatus = 'A' where idpaciente = '" & varPaciente & "'"
            '    comando.CommandText = VchCadena
            '    comando.ExecuteNonQuery()
            'End If
            transaccion.Commit()
            MessageBox.Show("Informaci�n guardada correctamente", "Triage")
            btnNuevo_Click(New Object, New System.EventArgs)
        Catch ex As Exception
            transaccion.Rollback()
            MessageBox.Show(ex.Message.ToString, "Triage - Error inesperado")
        End Try
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click, btnAceptar.Click
        Dim cCve_Cama As String
        If cbxTipoUrgencia.SelectedIndex() = -1 Then
            MessageBox.Show("Falta seleccionar el Tipo de urgencia", "Advertencia")
            Exit Sub
        End If

        If cbxPrioridadSel.SelectedIndex = -1 Then
            MessageBox.Show("Debe indicarse la prioridad de atenci�n.", "Advertencia")
            Exit Sub
        End If

        If (ValidarDatos() = True) And (BuscaPacienteLista(cbxPaciente.Text.Split("-")(2).Trim()) = False) Then
            If Me.cbxPrioridadSel.SelectedIndex > 0 Then
                cCve_Cama = SeleccionaCama().ToString
            End If
            Guardar_TRIAGE(cbxPaciente.Text.Split("-")(1).Trim(), cbxPaciente.Text.Split("-")(2).Trim(), cCve_Cama)
        End If
    End Sub

    Public Function BuscaPacienteLista(ByVal IDPaciente As String) As Boolean
        Try
            comando = New SqlClient.SqlCommand("SELECT IDPACIENTE FROM ZTRI_TRIAGE WHERE IDPACIENTE='" + IDPaciente + "' AND ESTADO='E'", cnn)
            cnn.Close()
            cnn.Open()
            lector = comando.ExecuteReader
            While lector.Read
                If lector.GetString(0) = frmPrincipalTriage.VchIdPacienteG Then
                    MessageBox.Show("El paciente ya se encuentra en lista de espera", "Advertencia")
                    Return True
                End If
            End While
            Return False
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Function

    Private Sub txtPaterno_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs)
        If e.KeyValue = Keys.F2 Then
            frmBuscarPaciente.Show()
        End If
    End Sub

    Private Sub txtMotivo_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtMotivo.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtPaSistolica.Focus()
        End If
    End Sub

    Private Sub txtPaSistolica_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPaSistolica.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtPaDiastolica.Focus()
        End If
    End Sub

    Private Sub txtPaDiastolica_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPaDiastolica.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtTemp.Focus()
        End If
    End Sub

    Private Sub txtTemp_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtTemp.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtRespiracion.Focus()
        End If
    End Sub

    Private Sub txtRespiracion_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtRespiracion.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtPulso.Focus()
        End If
    End Sub

    Private Sub txtPulso_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtPulso.KeyDown
        If e.KeyValue = Keys.Enter Then
            cbxEstado.Focus()
        End If
    End Sub

    Private Sub cbxEstado_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles cbxEstado.KeyDown
        If e.KeyValue = Keys.Enter Then
            btnEvaluar.Focus()
        End If
    End Sub

    Private Sub txtPaSistolica_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtPaSistolica.KeyPress, txtRespiracion.KeyPress, txtPulso.KeyPress, txtPaDiastolica.KeyPress
        e.Handled = SoloNumeros(System.Convert.ToInt16(Asc(e.KeyChar)))
    End Sub

    Private Sub txtTemp_KeyPress(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyPressEventArgs) Handles txtTemp.KeyPress
        e.Handled = SoloNumerosD(System.Convert.ToInt16(Asc(e.KeyChar)))
    End Sub

    Private Function SoloNumeros(ByVal KCode As Int16) As Boolean
        If (KCode >= 48 And KCode <= 57) Or KCode = 8 Then '[0,...,9], [BackSpace]
            Return False
        Else
            Return True
        End If
    End Function

    Private Function SoloNumerosD(ByVal KCode As Int16) As Boolean
        If (KCode >= 48 And KCode <= 57) Or KCode = 8 Or KCode = 46 Then '[0,...,9], [BackSpace], [.]
            Return False
        Else
            Return True
        End If
    End Function

    Private Sub cbxPaciente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPaciente.SelectedIndexChanged
        If cbxPaciente.SelectedIndex <> -1 Then
            btnCancelar.Visible = True
            Try
                Dim Values As String()
                Dim VchCadena As String

                Values = cbxPaciente.SelectedItem.ToString.Split("-")
                varPaciente = Values(2).Trim(" ")
                frmPrincipalTriage.VchIdPacienteG = Values(2).Trim(" ")
                'varCnum_control = Values(1).Trim(" ")

                cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
                VchCadena = "SELECT P.IDPACIENTE, P.IDUMEDICA, P.CPATERNO, P.CMATERNO, P.CNOMBRE, GETDATE(), P.DFECHANACIMIENTO, P.CSEXO, Z.DERECHOHABIENCIA FROM CTL_PACIENTES P, ZVW_PACIENTES_DATOS_GRALES Z " & _
                            " WHERE P.IDPACIENTE='" + varPaciente + "' AND Z.IDPACIENTE = P.IDPACIENTE"
                comando = New SqlClient.SqlCommand(VchCadena, cnn)
                cnn.Open()
                lector = comando.ExecuteReader
                If lector.HasRows Then
                    While lector.Read
                        VchIdUMedica = lector.GetString(1)
                        'txtPaterno.Text = lector.GetString(2)
                        'txtMaterno.Text = lector.GetString(3)
                        'txtNombre.Text = lector.GetString(4)
                        txtEdad.Text = ObtenerEdad(lector.GetDateTime(5).Date, lector.GetDateTime(6).Date)
                        If lector.GetString(7) = "M" Or lector.GetString(7) = "m" Then
                            txtSexo.Text = "Masculino"
                        Else
                            txtSexo.Text = "Femenino"
                        End If
                        txtDerechohabiencia.Text = lector.GetString(8)
                    End While
                End If
                cnn.Close()
            Catch ex As Exception
                MessageBox.Show("Ocurrio un error al seleccionar un paciente de la lista." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
            End Try
        Else
            btnCancelar.Visible = False
        End If
    End Sub

    'Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
    '    If cbxPaciente.SelectedIndex > -1 AndAlso MessageBox.Show("Se cancelar�", "TRIAGE") Then
    '    End If
    'End Sub

    Private Sub btnCancelar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnCancelar.Click
        If cbxPaciente.SelectedIndex > -1 AndAlso MessageBox.Show("�El paciente se ha retirado sin ser valorado por TRIAGE?", "TRIAGE", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            Dim transaccion As SqlTransaction
            Try
                cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
                cnn.Open()
                comando = New SqlClient.SqlCommand("", cnn)
                transaccion = cnn.BeginTransaction()
                comando.Transaction = transaccion

                ' Cancela Ingreso
                comando.CommandText = "update hgc_admision set cstatus = 'C', cnota_cancelado = 'EL PACIENTE SE RETIRO SIN SER VALORADO POR TRAIGE' where cnum_control = " & cbxPaciente.Text.Split("-")(1).Trim()
                comando.ExecuteNonQuery()
                comando.CommandText = "update ctl_pacientes set cstatus = 'A' where idpaciente = '" & cbxPaciente.Text.Split("-")(2).Trim() & "'"
                comando.ExecuteNonQuery()
                transaccion.Commit()
                MessageBox.Show("Informaci�n guardada correctamente", "Triage")
                btnNuevo_Click(New Object, New System.EventArgs)
            Catch ex As Exception
                transaccion.Rollback()
                MessageBox.Show(ex.Message.ToString, "Triage - Error inesperado")
            End Try
        End If
    End Sub
End Class
