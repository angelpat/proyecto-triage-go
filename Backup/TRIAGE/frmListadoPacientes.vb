Imports System.Data.SqlClient
Public Class frmListadoPacientes
    'Dim cnn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
    'Dim lector As System.Data.SqlClient.SqlDataReader
    'Dim comando As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand
    Dim IdTRIAGE As Integer

    Private Sub RefrescaLista()
        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim Edad As String
        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand
        IdTRIAGE = 0
        Try
            VchCadena = "SELECT TRI.IDTRIAGE, TRI.PRIORIDAD, PAC.IDPACIENTE, PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE, PAC.CSEXO, GETDATE(), PAC.DFECHANACIMIENTO, " & _
                "A.cnum_control, ISNULL(a.choja_hosp_alta,'') alta, convert(varchar(8),tri.fechae,108) HoraEval " & _
                        " FROM ZTRI_TRIAGE TRI inner join CTL_PACIENTES PAC on pac.IDPACIENTE = tri.IDPACIENTE inner join hgc_admision a on a.cnum_control = tri.cnum_control " & _
                        " WHERE TRI.ESTADO='E' and a.idservicio = '55' and a.idareamedica = '02' ORDER BY TRI.PRIORIDAD, TRI.FECHAE"
            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()
            comando.CommandText = "exec zsptri_updateprioridad"
            comando.ExecuteNonQuery()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read
                    'If lectorhere("ALTA") <> "S" Then
                    If IdTRIAGE = 0 Then
                        IdTRIAGE = lectorhere.GetInt32(0) 'Asigna el 1ero de la lista IdTRIAGE 
                    End If
                    Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString
                    dgvListado.Rows.Add(lectorhere.GetInt32(0), lectorhere.GetInt32(1), Numero, lectorhere("horaeval").ToString(), lectorhere.GetString(2), lectorhere.GetString(3), lectorhere.GetString(4), lectorhere.GetString(5), lectorhere.GetString(6), Edad, lectorhere("cnum_control").ToString())
                    'Else
                    'TerminaConsulta("A", lectorhere(0).ToString(), lectorhere("cnum_control"), lectorhere("idpaciente").ToString())
                    'End If
                    Numero = Numero + 1
                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()
    End Sub

    'Private Sub Recolorear()
    '    Dim VchCadena As String
    '    Dim Numero As Integer = 1
    '    Dim Edad As String
    '    Dim lectorhere
    '    Dim cnn As SqlConnection
    '    Dim comando As SqlCommand
    '    IdTRIAGE = 0
    '    Try
    '        VchCadena = "SELECT TRI.IDTRIAGE, TRI.PRIORIDAD, PAC.IDPACIENTE, PAC.CPATERNO, PAC.CMATERNO, PAC.CNOMBRE, PAC.CSEXO, GETDATE(), PAC.DFECHANACIMIENTO, " & _
    '            "A.cnum_control, ISNULL(a.choja_hosp_alta,'') alta, convert(varchar(8),tri.fechae,108) HoraEval " & _
    '                    " FROM ZTRI_TRIAGE TRI inner join CTL_PACIENTES PAC on pac.IDPACIENTE = tri.IDPACIENTE inner join hgc_admision a on a.cnum_control = tri.cnum_control " & _
    '                    " WHERE TRI.ESTADO='E' and a.idservicio = '55' and a.idareamedica = '02' ORDER BY TRI.PRIORIDAD, TRI.FECHAE"
    '        cnn = New SqlConnection(frmPrincipalTriage.Conexion)
    '        comando = New SqlCommand(VchCadena, cnn)

    '        cnn.Open()
    '        lectorhere = comando.ExecuteReader
    '        If lectorhere.HasRows Then
    '            While lectorhere.Read
    '                'If lectorhere("ALTA") <> "S" Then
    '                If IdTRIAGE = 0 Then
    '                    IdTRIAGE = lectorhere.GetInt32(0) 'Asigna el 1ero de la lista IdTRIAGE 
    '                End If
    '                Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString
    '                dgvListado.Rows.Add(lectorhere.GetInt32(0), lectorhere.GetInt32(1), Numero, lectorhere("horaeval").ToString(), lectorhere.GetString(2), lectorhere.GetString(3), lectorhere.GetString(4), lectorhere.GetString(5), lectorhere.GetString(6), Edad, lectorhere("cnum_control").ToString())
    '                'Else
    '                'TerminaConsulta("A", lectorhere(0).ToString(), lectorhere("cnum_control"), lectorhere("idpaciente").ToString())
    '                'End If
    '                Numero = Numero + 1
    '            End While
    '        End If
    '        lectorhere.Close()
    '    Catch ex As Exception
    '        MessageBox.Show(ex.Message.ToString, "Error no esperado")
    '    End Try
    '    cnn.Close()
    'End Sub

    Private Sub TerminaConsulta(ByVal Estado As String, ByVal Id As String, Optional ByVal NumControl As Long = Nothing, Optional ByVal idpaciente As String = Nothing)
        Dim transaccion As SqlTransaction
        Dim cnn As SqlConnection
        Try
            'Estado: 'A' -> Atendido, 'E'-> Espera, 'R'-> Se Retiro, 'O' -> Pasa a Observaci�n
            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            Dim comando As SqlCommand = New SqlCommand("", cnn)
            cnn.Open()
            transaccion = cnn.BeginTransaction()
            comando.Transaction = transaccion
            comando.CommandText = "UPDATE ZTRI_TRIAGE SET ESTADO='" + Estado + "', FECHAS=GETDATE(), MODIFICADO_POR='" + frmPrincipalTriage.VchIdEmpleadoG + "', F_MODIFICACION=GETDATE() WHERE IDTRIAGE='" + Id + "'"
            comando.ExecuteNonQuery()
            If Estado = "R" Then
                comando.CommandText = "update hgc_admision set cstatus = 'C', cindicaciones_egreso = 'VALORADO EN TRIAGE. NO SE PRESENT� A CONSULTA.', idpersonal_alta = '" & frmPrincipalTriage.VchIdEmpleadoG & "', " & _
                    "dfecha_alta = getdate(), ctipo_alta = '6' where cnum_control = " & NumControl
                comando.ExecuteNonQuery()
                comando.CommandText = "update ctl_pacientes set cstatus = 'A' where idpaciente = '" & idpaciente & "'"
                comando.ExecuteNonQuery()
            End If
            transaccion.Commit()
        Catch ex As Exception
            transaccion.Rollback()
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()
    End Sub

    Private Sub frmListadoPacientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RefrescaLista()
        lblNombreRevisor.Text = frmEvaluar.RegresaEmpleado()
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
    End Sub

    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting
        If dgvListado.Columns(e.ColumnIndex).Name = "Prioridad" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()
                If ((stringValue.IndexOf("1") > -1)) Then
                    e.CellStyle.BackColor = Color.Red
                    e.CellStyle.SelectionBackColor = Color.Red
                End If
                If ((stringValue.IndexOf("2") > -1)) Then
                    e.CellStyle.BackColor = Color.Yellow
                    e.CellStyle.SelectionBackColor = Color.Yellow
                    e.CellStyle.SelectionForeColor = Color.Black
                End If
                If ((stringValue.IndexOf("3") > -1)) Then
                    e.CellStyle.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                    e.CellStyle.SelectionBackColor = Color.FromArgb(115, 189, 58)
                    e.CellStyle.SelectionForeColor = Color.Black
                End If
                If ((stringValue.IndexOf("4") > -1)) Then
                    e.CellStyle.BackColor = Color.SkyBlue
                    e.CellStyle.SelectionBackColor = Color.SkyBlue
                    'e.CellStyle.BackColor = Color.FromArgb(8, 49, 99) ' Color Azul logo del Hospital
                End If
            End If
        End If
    End Sub

    Private Sub btnRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefrescar.Click
        dgvListado.Rows.Clear()
        RefrescaLista()
    End Sub

    Private Sub btnAtender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        TerminaConsulta("A", IdTRIAGE)
        dgvListado.Rows.Clear()
        RefrescaLista()
    End Sub

    Private Sub tsmAtender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmAtender.Click, tsmpediatria.Click
        Try
            Dim IdT As String
            IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
            dgvListado.Rows.Clear()
            TerminaConsulta("A", IdT) 'A=Atendido
            RefrescaLista()
        Catch ex As Exception
            MessageBox.Show("No hay paciente seleccionado", "Advertencia")
        End Try
    End Sub

    Private Sub tsmEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmEliminar.Click
        Try
            If MsgBox("�Desea eliminar paciente?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                Dim IdT As String
                IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
                CambiaEstadoHGC_ADMISION(dgvListado.SelectedRows(0).Cells(3).Value.ToString)
                dgvListado.Rows.Clear()
                TerminaConsulta("R", IdT) 'R=Retirado
                RefrescaLista()
            End If
        Catch ex As Exception
            MessageBox.Show("No hay paciente seleccionado", "Advertencia")
        End Try
    End Sub

    Private Sub CambiaEstadoHGC_ADMISION(ByVal IdPaciente As String)
        Dim VchCadena As String
        Dim comando As SqlCommand
        Dim cnn As SqlConnection
        Dim lector As SqlDataReader
        Try
            VchCadena = "UPDATE HGC_ADMISION SET CSTATUS='C' WHERE IDPACIENTE='" + IdPaciente + "' AND CSTATUS='H' AND IDSERVICIO = '55'"
            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand(VchCadena, cnn)
            cnn.Open()
            lector = comando.ExecuteReader
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Sub

    Private Sub tsmTriage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmTriage.Click
        Try

            Dim IdT As String
            IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
            CambiaEstadoHGC_ADMISION(dgvListado.SelectedRows(0).Cells(3).Value.ToString)
            dgvListado.Rows.Clear()
            TerminaConsulta("R", IdT) 'R=Retirado
            RefrescaLista()

        Catch ex As Exception
            MessageBox.Show("No hay paciente seleccionado", "Advertencia")
        End Try
    End Sub

    Private Sub dgvListado_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellDoubleClick
        If e.RowIndex >= 0 AndAlso dgvListado.Item("idT", e.RowIndex).Value IsNot Nothing Then
            If frmPrincipalTriage.vchTipo = "01" Then
                TerminaConsulta("A", dgvListado.Item("idT", e.RowIndex).Value)
                dgvListado.Rows.Clear()
                RefrescaLista()
            ElseIf frmPrincipalTriage.vchTipo = "04" AndAlso MsgBox("�El paciente " & dgvListado.Item("PATERNO", e.RowIndex).Value.ToString() & " " & dgvListado.Item("materno", e.RowIndex).Value.ToString() & " " & _
                dgvListado.Item("nombre", e.RowIndex).Value.ToString() & " se ha retirado sin ser valorado?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
                CambiaEstadoHGC_ADMISION(dgvListado.Item("curp", e.RowIndex).Value)
                TerminaConsulta("R", dgvListado.Item("idT", e.RowIndex).Value, dgvListado.Item("colNumCtrl", e.RowIndex).Value, dgvListado.Item("curp", e.RowIndex).Value)
                dgvListado.Rows.Clear()
                RefrescaLista()
            End If
        End If
    End Sub
End Class
