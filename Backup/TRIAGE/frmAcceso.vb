Public Class frmAcceso
    Dim cnn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection("")
    Dim lector As System.Data.SqlClient.SqlDataReader
    Dim comando As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand

    Public confstr(3) As String

    Public Sub config()
        Dim cad As String
        If IO.File.Exists("C:\\conf.lol") Then
            confstr(0) = IO.File.OpenText("C:\\conf.lol").ReadToEnd
            confstr = confstr(0).Split(Nothing)
            confstr(1) = confstr(2)
            confstr(2) = confstr(4)
        Else
            confstr(0) = "hgjms1"
            confstr(1) = "SIEC"
            confstr(2) = "SIGHO"
            IO.File.WriteAllLines("C:\\conf.lol", confstr)
        End If
        cad = "Data Source=" + confstr(0) + ";Initial Catalog=" + confstr(1) + ";User Id=Triage" + ";Password=" + confstr(2) + ";" ' "Server=(local); database=master; integrated security=yes" '
        'cad = "Data Source=IVAN-LAP\SQLEXPRESS;Initial Catalog=siec;Integrated Security=True"
        confstr(3) = cad
    End Sub

    Public Sub RegistraLogin()
        Try
            comando = New SqlClient.SqlCommand("EXEC siec.dbo.zSP_LogLogin @Sistema = 'Triage', @Login = '" & txtUsuario.Text & "'", cnn)
            cnn.Close()
            cnn.Open()
            comando.ExecuteNonQuery()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Sub
    Function BuscarUsuarioTriage() As Boolean
        Try
            comando = New SqlClient.SqlCommand("SELECT zusuarios.IDPERSONAL, isnull(pun.idtipopersonal,'') FROM ZUSUARIOS inner join personal_unidades pun on zusuarios.idpersonal = pun.idpersonal WHERE LOGIN='" + txtUsuario.Text + " 'AND PASSWORD='" + txtContrasena.Text + "'", cnn)
            cnn.Close()
            cnn.Open()
            lector = comando.ExecuteReader
            While lector.Read
                If lector.GetString(0) <> "" Then
                    frmPrincipalTriage.VchIdEmpleadoG = lector.GetString(0)
                    frmPrincipalTriage.vchTipo = lector.GetString(1)
                    Return True
                End If
            End While
            Return False
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
    End Function

    Private Sub btnSalir_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSalir.Click
        Close()
    End Sub

    Private Sub btnEntrar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEntrar.Click
        If txtUsuario.Text <> "" And txtContrasena.Text <> "" Then
            If BuscarUsuarioTriage() Then
                RegistraLogin()
                frmPrincipalTriage.Show()
                frmPrincipalTriage.Text = frmPrincipalTriage.Text + "  [" + txtUsuario.Text.ToUpper + "]"
            Else
                MessageBox.Show("Usuario incorrecto", "Advertencia")
                txtUsuario.Clear()
                txtContrasena.Clear()
                txtUsuario.Focus()
            End If
        Else
            MessageBox.Show("Faltan llenar datos", "Advertencia")
            txtUsuario.Focus()
        End If
    End Sub

    Private Sub frmAcceso_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        config()
        frmPrincipalTriage.Conexion = confstr(3)
        cnn.ConnectionString = frmPrincipalTriage.Conexion
        txtUsuario.Focus()
    End Sub

    Private Sub txtUsuario_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtUsuario.KeyDown
        If e.KeyValue = Keys.Enter Then
            txtContrasena.Focus()
        End If
    End Sub

    Private Sub txtContrasena_KeyDown(ByVal sender As System.Object, ByVal e As System.Windows.Forms.KeyEventArgs) Handles txtContrasena.KeyDown
        If e.KeyValue = Keys.Enter Then
            btnEntrar.Focus()
        End If
    End Sub

End Class