Public Class frmPrincipalTriage
    Public Conexion As String = ""
    'Public Conexion As String = "Data Source=10.3.14.93;Initial Catalog=SIEC;User ID=sa;Password=siec"
    Public VchIdPacienteG As String
    Public VchIdEmpleadoG As String
    Public vchTipo As String

    Private Sub EvaluarPacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EvaluarPacientesToolStripMenuItem.Click
        frmEvaluar.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        frmAcceso.Close()
    End Sub

    Private Sub ListadoDePacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePacientesToolStripMenuItem.Click
        frmListadoPacientes.Show()
    End Sub

    Private Sub frmPrincipalTriage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmAcceso.Hide()
        If vchTipo <> "01" And vchTipo <> "02" Then
            EvaluarPacientesToolStripMenuItem.Visible = False
        End If
    End Sub

    Private Sub frmPrincipalTriage_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub
End Class