USE [test]
GO
/****** Objeto:  Table [dbo].[zTRI_Cuestionario]    Fecha de la secuencia de comandos: 08/07/2012 14:06:09 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zTRI_Cuestionario](
	[idCuestionario] [int] IDENTITY(1,1) NOT NULL,
	[idTriage] [int] NOT NULL,
	[idPersonal] [varchar](18) COLLATE Latin1_General_CI_AI NOT NULL,
	[idRespuesta] [int] NOT NULL,
	[centroSalud] [varchar](255) COLLATE Latin1_General_CI_AI NULL,
	[fechaEvaluacion] [datetime] NOT NULL,
	[observacion] [varchar](2000) COLLATE Latin1_General_CI_AI NULL,
 CONSTRAINT [PK_zTRI_Cuestionario] PRIMARY KEY CLUSTERED 
(
	[idCuestionario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF



USE [test]
GO
/****** Objeto:  Table [dbo].[zTRI_Respuestas]    Fecha de la secuencia de comandos: 08/07/2012 14:07:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[zTRI_Respuestas](
	[idRespuesta] [int] IDENTITY(1,1) NOT NULL,
	[respuesta] [varchar](75) COLLATE Latin1_General_CI_AI NULL,
 CONSTRAINT [PK_zTRI_Respuestas] PRIMARY KEY CLUSTERED 
(
	[idRespuesta] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF