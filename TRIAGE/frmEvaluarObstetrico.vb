﻿Imports TRIAGE.TriageGO
Imports System.Globalization

Public Class frmEvaluarObstetrico
    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader
    ' Dim NombreValores As NombreValoresGO = New NombreValoresGO()
    Public fecha_adm As String
    Public FIntriage As String
    Public Dfechanac As String
    Dim idPaciente As String
    Public idreferido As String
    Dim cNumControl As String
    Dim idderechoabiencia As String
    Dim FORMTESP As New CultureInfo("es-ES")
    Dim PRUEBA As String
    Dim estado As String
    Dim valoracion1 As String
    'Public VchIdUMedica As String
    Dim buspacientes As BuscarPacientes = New BuscarPacientes()
    Dim buscarunidad As BuscarUnidad = New BuscarUnidad()
    Dim formulario As LimIniFormulario = New LimIniFormulario()
    Dim valGO As ValEvaluarObstetrico = New ValEvaluarObstetrico()
    Dim valoracion As EvalTriGO = New EvalTriGO()

    Private objTGORevaluado As TriageGO = New TriageGO()

    Dim guardar As AlmacenaValoresGO = New AlmacenaValoresGO()
    Dim revaluados As RebuscarPacientes = New RebuscarPacientes()
    Dim prioridad As Integer
    Dim preDiagAdm As String
    Private _derechohabiencia As Integer
    Private _dfecha_ingreso As Integer
    Dim db As conexionbd = New conexionbd
    Public Revaluar1 As Boolean
    Public valorado As String
    Dim real As Boolean

    Private Property DataGridview As Object

    'validar cbxunidad,cbxpruebaVIH,cbxpruebadesifilis 
    Private Sub Seleccionar_unidad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtReferido.Click
        If rbtReferido.Checked = False Then
            cbxUnidad1.Enabled = False
            cbxUnidad1.Visible = False
        Else
            cbxUnidad1.Enabled = True
            cbxUnidad1.Visible = True
        End If
    End Sub
    Private Sub Seleccionar_pruebaVIH(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtPruebaVIHA.CheckedChanged, rbtPruebaVIHB.CheckedChanged
        If rbtPruebaVIHA.Checked = True Then
            cbxResultadoVIH.Enabled = True
            cbxResultadoVIH.SelectedIndex = 0
        ElseIf rbtPruebaVIHB.Checked Then
            cbxResultadoVIH.Enabled = False
            cbxResultadoVIH.SelectedIndex = -1
        End If
    End Sub
    Private Sub Seleccionar_pruebasifilis(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles rbtPruebaSifilisA.CheckedChanged, rbtPruebaSifilisB.Click
        If rbtPruebaSifilisA.Checked = True Then
            cbxResultadoSIFILIS.Enabled = True
            cbxResultadoSIFILIS.SelectedIndex = 0
        ElseIf rbtPruebaSifilisB.Checked Then
            cbxResultadoSIFILIS.Enabled = False
            cbxResultadoSIFILIS.SelectedIndex = -1
        End If
    End Sub
    
    'COMBO BUSCADOR DEL PACIENTE Y SUS DATOS DINAMICOS
    Private Sub cbxPaciente_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbxPaciente.SelectedIndexChanged

        Dim valor As String



        valor = Convert.ToString(cbxPaciente.SelectedValue)
        If valor = "System.Data.DataRowView" Or valor = "" Then
            valor = 0
        End If

        'If valorado = "EV" Then
        'If cbxPaciente.SelectedValue = 0 Then
        Try


            Dim strSQL As String
            db.validarbd()
            cnn = New SqlClient.SqlConnection(db.conexion)

            strSQL = "exec zspTRI_selInfoPaciente '" & valor & "' "

            'P.IDPACIENTE, P.IDUMEDICA, P.CPATERNO, P.CMATERNO, P.CNOMBRE, GETDATE() fIniTriage, 
            'P.DFECHANACIMIENTO, P.CSEXO, Z.DERECHOHABIENCIA, A.DFECHA_INGRESO,A.cnum_control,A.cdiagnostico


            comando = New SqlClient.SqlCommand(strSQL, cnn)
            cnn.Open()
            lector = comando.ExecuteReader
            'If  Then
            If lector.HasRows Then
                Dim curp As Integer = lector.GetOrdinal("Idpaciente")
                Dim derechohabiencia As Integer = lector.GetOrdinal("derechohabiencia")
                Dim dfecha_ingreso As Integer = lector.GetOrdinal("DFECHA_INGRESO")
                Dim dfechaactual As Integer = lector.GetOrdinal("fIniTriage")
                Dim Dfechanacimiento As Integer = lector.GetOrdinal("DFECHANACIMIENTO")
                preDiagAdm = lector.GetOrdinal("cdiagnostico")
                cNumControl = lector.GetOrdinal("cnum_control")
                idderechoabiencia = lector.GetOrdinal("idTipoDerechohabiente")
                While lector.Read
                    txtCurp.Text = lector.GetString(curp)
                    idderechoabiencia = lector.GetString(idderechoabiencia)
                    txtDerechohabiencia.Text = lector.GetString(derechohabiencia)
                    cNumControl = lector.GetInt64(cNumControl)
                    fecha_adm = lector.GetDateTime(dfecha_ingreso).ToString(System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"))
                    'Format(lector.GetDateTime(dfechaactual), "MM\/dd\/yyyy HH:mm:ss")
                    FIntriage = lector.GetDateTime(dfechaactual).ToString(System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"))
                    Dfechanac = lector.GetDateTime(Dfechanacimiento).ToString(System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"))
                    txtMotAtencion.Text = lector.GetString(preDiagAdm)
                End While
            End If
            'End If


            If valor = 0 Then
            Else
                txtfechadeingreso.Text = CDate(fecha_adm).ToString("g")
                txtfechaactual.Text = CDate(FIntriage).ToString("g")
                txtEdad.Text = (DateDiff("m", Dfechanac, FIntriage) \ 12).ToString
            End If
            If valorado <> "EV" Then
                'cbxPaciente.Items.Clear()
                objTGORevaluado = revaluados.Devolvervalores(cNumControl)
                valGO.RecibeObjTGOReavaular(objTGORevaluado)
                If txtCurp.Text <> "" Then
                    'objTGORevaluado.concatenar()
                    Dim prioridad = valGO.ObtenerPrioridad()
                    valoracion.Valestado(objTGORevaluado.mivalortriage)
                End If


            End If
            cnn.Close()
        Catch ex As Exception

            MessageBox.Show("Ocurrio un error al seleccionar un paciente de la lista." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try



    End Sub
    'CARGADOR DEL LOAD
    Private Sub frmEvaluarObstetrico_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
    End Sub
    'Muestra el estado de al persona y el valor sugerido de la persona
    Private Sub mostrarPrioridad(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEvaluar.Click
        Dim objTriageGO As New TriageGO
        If valGO.validaFormulario() Then
            If valorado <> "EV" Then
                valGO.llenaObjTriageGo()
            End If
            valGO.llenaObjTriageGo()
            btnAceptar.Enabled = True
            Dim prioridad1 As Integer
            prioridad1 = valGO.ObtenerPrioridad()
            valoracion1 = valoracion.Valestado(prioridad1)
        End If
    End Sub
    'alacena los valores dentro de 
    Public Sub btnAceptar_Click(sender As Object, e As EventArgs) Handles btnAceptar.MouseClick
        '  idPaciente, frmPrincipalTriage.VchIdEmpleadoG, miCNum_Control, idreferido, txtDerechohabiencia.Text, fecha_adm, FIntriage
        Dim objTriageGO As New TriageGO()
        objTriageGO.miIdPaciente = txtCurp.Text
        objTriageGO.miIdPersonal = frmPrincipalTriage.VchIdEmpleadoG
        objTriageGO.miCNum_Control = CNumcontrol
        objTriageGO.miidderechoabiencia = idderechoabiencia
        objTriageGO.miidreferido = idreferido
        objTriageGO.miFechaIntriage = FIntriage
        objTriageGO.miPaSistolica = txtPaSistolica.Text
        objTriageGO.miPaDiastolica = txtPaDiastolica.Text
        objTriageGO.miPulso = txtFrecCard.Text
        objTriageGO.miIndiceDeChoque = txtIndChoque.Text
        objTriageGO.miTemperatura = txtTemperatura.Text
        objTriageGO.miGlucosa = txtGlucosa.Text
        objTriageGO.miRespiracion = txtRespiracion.Text
        objTriageGO.miPeso = txtPeso.Text
        objTriageGO.MIAltura = txtAltura.Text
        objTriageGO.IMC = txtIMC.Text
        objTriageGO.Fcf = txtFCFFeto.Text
        objTriageGO.miEdadGesta = txtEdadGestacional.Text
        objTriageGO.miValoracion = Textvaloracion.Text
        objTriageGO.valortriage = valoracion1
        objTriageGO.status = estado
        objTriageGO.revisado = real
        'var checkedButton = container.Controls.OfType<RadioButton>().FirstOrDefault(r => r.Checked);

        guardar.AlmacenaValoresGO(objTriageGO)
        formulario.limpiarFormulario()

        'cbxPaciente.Items.Clear()
        If valorado = "EV" Then
            formulario.inicializaFormulario()
            buspacientes.cargaPacientes()
            'FormateaColor(tabObstetrico)
            cbxPaciente.SelectedIndex = -1
        Else

            revaluados.RecargaPacientes()
            'FormateaColor(tabObstetrico)
            cbxPaciente.SelectedIndex = -1
            formulario.limpiarFormulario()
        End If
        btnAceptar.Enabled = False
        '  buspacientes.cargaPacientes()


    End Sub



    Public Sub seleccion()
        If Revaluar1 Then
            formulario.inicializaFormulario()
            valorado = "EV"
            btnAceptar.Enabled = False
            idreferido = 0
            buspacientes.cargaPacientes()
            buscarunidad.Mostar_Unidad(1)
            Me.Text = "Evaluar Triage"
            Me.Show()
            Me.Top = (Me.Height - Me.Height) / 2
            Me.Left = (Me.Width - Me.Width) / 2
            cbxPaciente.SelectedIndex = -1
            real = True
            estado = "EV"
        Else
            'cbxPaciente.Items.Clear()
            valorado = "RE"
            MessageBox.Show("entraste a reevaluar")
            revaluados.RecargaPacientes()
            lblMotatencion.Text = "Motivo de Revaluacion"
            txtMotAtencion.BackColor = Color.White
            txtMotAtencion.ReadOnly = False
            btnAceptar.Enabled = False
            Me.Text = "Revaluar Triage"
            gbxValoracion.Text = "Pacientes revalorados "
            Me.Show()
            Me.Top = (Me.Height - Me.Height) / 2
            Me.Left = (Me.Width - Me.Width) / 2
            estado = "REV"
            real = True
        End If

    End Sub


    Private Sub cbxUnidad1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxUnidad1.SelectedIndexChanged

        idreferido = Convert.ToString(cbxUnidad1.SelectedValue)
    End Sub


    Private Sub cbxPrioridadSel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxPrioridadSel.SelectedIndexChanged
        valoracion1 = cbxPrioridadSel.SelectedItem.ToString
    End Sub

    'Public Sub FormateaColor(ByVal control As Object)
    '    Dim contHijo As Control
    '    Try
    '        For Each contHijo In control.controls

    '                If TypeOf contHijo Is Label Then
    '                    '      contHijo.BackColor = System.Drawing.Color.Pink
    '                    '       contHijo.Text = "hola"
    '                DirectCast(contHijo, Label).BackColor = Color.Black
    '                Else

    '                    FormateaColor(contHijo)
    '                End If

    '        Next
    '    Catch ex As Exception
    '        Throw New System.Exception(System.String.Concat("Nro. Error(",
    '            Err.Number.ToString, ") - ", ex.Message, vbCrLf, ex.StackTrace,
    '            vbCrLf))
    '    End Try
    'End Sub


End Class