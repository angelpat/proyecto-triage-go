﻿Imports System.Data.SqlClient

Public Class frmCambiarPrioridad

    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim IdTRIAGE As Integer
    Dim NumControl As String
    Dim idPaciente As String
    Dim msgVaidacion As String
    Dim tipoUrg As String

    Private Sub LimpiaFormulario()
        dgvListado.Rows.Clear()
        txtPrioridad.Text = ""
        txtHora.Text = ""
        txtPaciente.Text = ""
        txtSexo.Text = ""
        txtEdad.Text = ""
        txtObservacion.Text = ""
        txtMotivo.Text = ""
        cbxPrioridadSel.SelectedIndex() = -1
        txtPrioridad.BackColor = Color.White
    End Sub

    Private Sub RefrescaLista()

        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim Edad As String
        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand

        Try

            VchCadena = "EXEC zspTRI_selListaPacientes 0" '0 -> Se trae la lista completa

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()
            comando.CommandText = "EXEC zspTRI_updPrioridad"
            comando.ExecuteNonQuery()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read
                    Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString
                    dgvListado.Rows.Add(lectorhere.GetInt32(0), lectorhere.GetInt32(1), Numero, lectorhere("horaeval").ToString(), lectorhere.GetString(2), lectorhere.GetString(3), lectorhere.GetString(4), lectorhere.GetString(5), lectorhere.GetString(6), Edad, lectorhere("cnum_control").ToString(), lectorhere("motivo").ToString(), lectorhere("tipoUrgencia").ToString())
                    Numero = Numero + 1
                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()

    End Sub


    Private Sub CambiarPrioridad()

        Dim sql As String
        Dim prioridad As String
        Dim edad As Int32
        prioridad = System.Convert.ToString(cbxPrioridadSel.SelectedIndex() + 1)

        Try

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            cnn.Open()
            comando = New SqlCommand("", cnn)

            If prioridad = 1 Then

                sql = "Select ((datediff (dd,dfechanacimiento, getdate()) - cast((datediff (yy,dfechanacimiento, getdate())/4) as int)  ) / 365) " & _
                    "from ctl_pacientes where idpaciente = '" & idPaciente & "'"
                comando.CommandText = sql
                edad = comando.ExecuteScalar()

                If edad < 16 Then       'PASA A URGENCIAS PEDIATRICAS
                    sql = "UPDATE HGC_ADMISION SET CCOMENTARIO='" + tipoUrg + "', idareamedica = '03' WHERE CNUM_CONTROL='" + NumControl + "'"
                    comando.CommandText = sql
                    comando.ExecuteNonQuery()
                Else                    'PASA A HOSPITALIZACIÓN URGENCIAS
                    sql = "UPDATE HGC_ADMISION SET CCOMENTARIO='" + tipoUrg + "', idareamedica = '01' WHERE CNUM_CONTROL='" + NumControl + "'"
                    comando.CommandText = sql
                    comando.ExecuteNonQuery()
                End If
            Else
                sql = "UPDATE zTRI_Triage SET prioridad = " & prioridad & ",fechaE = GETDATE(), " & _
                      "motivo = motivo + ' - CAMBIO DE PRIORIDAD DEL ' + CAST( prioridad AS VARCHAR(01)) + ' AL ' +  CAST(" & prioridad & " AS VARCHAR(01)) " & _
                      "+ ' POR: ' + '" & txtMotivo.Text & "' WHERE IdTRIAGE = " & IdTRIAGE

                comando.CommandText = sql
                comando.ExecuteNonQuery()
            End If

        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()

    End Sub



    Private Sub ValidaCampos()

        msgVaidacion = ""

        If txtPrioridad.Text = "" Then
            msgVaidacion = "Debe seleccionar un paciente"
        ElseIf txtMotivo.Text.Length = 0 Then
            msgVaidacion = "Debe ingresar el motivo"
        ElseIf txtMotivo.Text.Length >= 1 And txtMotivo.Text.Length <= 5 Then
            msgVaidacion = "El motivo debe ser mayor a 5 letras o caracteres"
        ElseIf cbxPrioridadSel.SelectedIndex = -1 Then
            msgVaidacion = "Debe seleccionar una prioridad"
        ElseIf txtPrioridad.Text = cbxPrioridadSel.SelectedIndex + 1 Then
            msgVaidacion = "No se puede asignar la misma prioridad"
        End If

    End Sub


    Private Sub frmCambiarPrioridad_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        RefrescaLista()

    End Sub

    Private Sub dgvListado_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellClick
        If e.RowIndex >= 0 AndAlso dgvListado.Item("idT", e.RowIndex).Value IsNot Nothing Then

            IdTRIAGE = dgvListado.Item("idT", e.RowIndex).Value
            idPaciente = dgvListado.Item("CURP", e.RowIndex).Value
            NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
            txtPrioridad.Text = dgvListado.Item("Prioridad", e.RowIndex).Value
            txtHora.Text = dgvListado.Item("colHoraEval", e.RowIndex).Value
            txtPaciente.Text = dgvListado.Item("Paterno", e.RowIndex).Value & " " & dgvListado.Item("Materno", e.RowIndex).Value & " " & dgvListado.Item("Nombre", e.RowIndex).Value
            txtObservacion.Text = dgvListado.Item("motivo", e.RowIndex).Value
            tipoUrg = dgvListado.Item("tipoUrgencia", e.RowIndex).Value

            If (dgvListado.Item("Sexo", e.RowIndex).Value = "M") Then
                txtSexo.Text = "Masculino"
            Else
                txtSexo.Text = "Femenino"
            End If

            txtEdad.Text = dgvListado.Item("Edad", e.RowIndex).Value

            If (dgvListado.Item("Prioridad", e.RowIndex).Value = "1") Then
                txtPrioridad.BackColor = Color.Red
            ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "2") Then
                txtPrioridad.BackColor = Color.Yellow
            ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "3") Then
                txtPrioridad.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
            ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "4") Then
                txtPrioridad.BackColor = Color.SkyBlue
            End If

            'btnAtendido.Enabled = True
            'btnEliminar.Enabled = True

        End If
    End Sub

    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting
        If dgvListado.Columns(e.ColumnIndex).Name = "Prioridad" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()

                If ((stringValue.IndexOf("1") > -1)) Then
                    e.CellStyle.BackColor = Color.Red
                    e.CellStyle.SelectionBackColor = Color.Red

                ElseIf ((stringValue.IndexOf("2") > -1)) Then
                    e.CellStyle.BackColor = Color.Yellow
                    e.CellStyle.SelectionBackColor = Color.Yellow
                    e.CellStyle.SelectionForeColor = Color.Black

                ElseIf ((stringValue.IndexOf("3") > -1)) Then
                    e.CellStyle.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                    e.CellStyle.SelectionBackColor = Color.FromArgb(115, 189, 58)
                    e.CellStyle.SelectionForeColor = Color.Black

                ElseIf ((stringValue.IndexOf("4") > -1)) Then
                    e.CellStyle.BackColor = Color.SkyBlue
                    e.CellStyle.SelectionBackColor = Color.SkyBlue
                    'e.CellStyle.BackColor = Color.FromArgb(8, 49, 99) ' Color Azul logo del Hospital
                End If
            End If
        End If
    End Sub

    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        ValidaCampos()

        If msgVaidacion <> "" Then
            MessageBox.Show(msgVaidacion)
        Else            
            CambiarPrioridad()
            LimpiaFormulario()
            RefrescaLista()
        End If

    End Sub

    Private Sub btnRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefrescar.Click
        LimpiaFormulario()
        RefrescaLista()
    End Sub

End Class