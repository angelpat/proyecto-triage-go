
Public Class frmPrincipalTriage
    Dim cnn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection("")
    Dim acceso As frmAcceso
    Public Conexion As String = ""
    'Public Conexion As String = "Data Source=10.3.14.93;Initial Catalog=SIEC;User ID=sa;Password=siec"
    Public VchIdPacienteG As String
    Public VchIdEmpleadoG As String
    Public vchTipo As String
    Public vchTipoTriage As String
    Public Revaluar As Boolean

    Private Sub EvaluarPacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EvaluarPacientesToolStripMenuItem.Click
        frmEvaluar.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmAcceso.Close()
    End Sub

    Private Sub ListadoDePacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePacientesToolStripMenuItem.Click
        frmListadoPacientesGO.Show()
    End Sub

    Private Sub frmPrincipalTriage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        If vchTipoTriage = "URGENCIAS" Then
            lblTipo.Text = "URGENCIAS"
            pnlGineco.Visible = False
            pnlGineco1.Visible = False

            '  MessageBox.Show("URGENCIAS", "URGENCIAS", MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Exclamation)


        ElseIf vchTipoTriage = "GINECOBTRETICIA" Then
            lblTipo.Text = "GINECOBTRETICIA"
            Me.BackColor = Color.WhiteSmoke

            menugineco.Visible = False
            Me.imghospital.Location = New Point(0, 0)
            Me.imghospital.Location = New Point(0, 0)
            validaAcceso()
            '  MessageBox.Show("GINECOBTRETICIAS", "GINECOBTRETICIAS", MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Exclamation)

        End If
        lblTipo.Left = (Me.ClientSize.Width / 2) - (lblTipo.Width / 2)
        frmAcceso.Hide()



    End Sub

    Private Sub ListadoDePacientesTSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePacientesTSToolStripMenuItem.Click
        frmListadoPacientesTSCVGO.Show()
    End Sub

    Private Sub TSCodigoAzulToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSCodigoAzulToolStripMenuItem.Click
        frmRptTSCodigoAzul.Show()
    End Sub

    Public Sub validaAcceso()
        EvaluarPacientesToolStripMenuItem.Visible = False
        TrabajoSocialToolStripMenuItem.Visible = True
        If vchTipo = "01" Or vchTipo = "02" Then '01,02,09 -> Medico, Enfermero, Informatica

        ElseIf vchTipo = "07" Then '07 -> Trabajo Social
            TrabajoSocialToolStripMenuItem.Visible = True
            pnlGineco.Visible = False
            pnlGineco1.Visible = True
            pnlGineco1.Location = New Point(31, 74)
            btnRepyEst.Visible = False
            lblRepyEst.Visible = False
            btnSeguimiento.Location = New Point(3, 81)
            lblSeguimiento.Location = New Point(76, 98)
           
        ElseIf vchTipo = "04" Or vchTipo = "09" And frmAcceso.cboTipoTriage.Text = "GINECOBTRETICIA" Then
            pnlGineco.Visible = True
            pnlGineco1.Visible = True
            
        End If
    End Sub

    Private Sub CambiarPrioridadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarPrioridadToolStripMenuItem.Click
        frmCambiarPrioridad.Show()
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        frmEvaluarObstetrico.Show()
    End Sub

    Private Sub SeguimientoToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SeguimientoToolStripMenuItem.Click
        frmSeguimiento.Show()
    End Sub


    Private Sub btncrsesion_Click_1(sender As Object, e As EventArgs) Handles btncrsesion.Click
        frmAcceso.Show()
        Me.Close()
    End Sub

    Private Sub btnGO_Click(sender As Object, e As EventArgs) Handles btnGO.Click
        frmEvaluarObstetrico.Revaluar1 = True
        frmEvaluarObstetrico.seleccion()
    End Sub

    Public Sub btnRevaluarGO_Click(sender As Object, e As EventArgs) Handles btnRevaluarGO.Click
        frmEvaluarObstetrico.Revaluar1 = False
        frmEvaluarObstetrico.seleccion()
    End Sub

    Private Sub btnLisPaciente_Click(sender As Object, e As EventArgs) Handles btnLisPaciente.Click
        frmListadoPacientesGO.Show()
    End Sub

    Private Sub btnSeguimiento_Click(sender As Object, e As EventArgs) Handles btnSeguimiento.Click
        frmSeguimientoGO.Show()
    End Sub

    Private Sub lblTipoPersonal_Click(sender As Object, e As EventArgs) Handles lblTipoPersonal.Click

    End Sub

    Private Sub btnCodVerde_Click(sender As Object, e As EventArgs) Handles btnCodVerde.Click
        frmListadoPacientesTSCVGO.Show()
    End Sub
End Class
