﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPacientesTSCVGO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvListado = New System.Windows.Forms.DataGridView()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.gbxPaciente = New System.Windows.Forms.GroupBox()
        Me.cbxUnidad2 = New System.Windows.Forms.ComboBox()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.txtDerechohabiencia = New System.Windows.Forms.TextBox()
        Me.lblCS = New System.Windows.Forms.Label()
        Me.lblDerechohabiencia = New System.Windows.Forms.Label()
        Me.lblPaciente = New System.Windows.Forms.Label()
        Me.btnGuardar = New System.Windows.Forms.Button()
        Me.btnRetirado = New System.Windows.Forms.Button()
        Me.txtObservacion = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.rbtNoAcudio = New System.Windows.Forms.RadioButton()
        Me.rbtNoMedico = New System.Windows.Forms.RadioButton()
        Me.rbtNoInformacion = New System.Windows.Forms.RadioButton()
        Me.rbtNoFicha = New System.Windows.Forms.RadioButton()
        Me.rbtBuenaAtencion = New System.Windows.Forms.RadioButton()
        Me.gbxCuestionario = New System.Windows.Forms.GroupBox()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.IdT = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colHoraEval = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Observaciones = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idderechohab = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Derechohabiencia = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.gbxCuestionario.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdT, Me.Prioridad, Me.colHoraEval, Me.Nombre, Me.colNumCtrl, Me.Curp, Me.Observaciones, Me.idderechohab, Me.Derechohabiencia})
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle7
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(14, 104)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle8.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle8.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle8.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle8.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle8.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle8.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle8
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(840, 300)
        Me.dgvListado.TabIndex = 0
        Me.dgvListado.TabStop = False
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 691)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(868, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TRIAGE.My.Resources.Resources.Captura
        Me.PictureBox1.Location = New System.Drawing.Point(14, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.gbxPaciente.Controls.Add(Me.cbxUnidad2)
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtDerechohabiencia)
        Me.gbxPaciente.Controls.Add(Me.lblCS)
        Me.gbxPaciente.Controls.Add(Me.lblDerechohabiencia)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(14, 414)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(334, 223)
        Me.gbxPaciente.TabIndex = 1
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'cbxUnidad2
        '
        Me.cbxUnidad2.FormattingEnabled = True
        Me.cbxUnidad2.Location = New System.Drawing.Point(1, 186)
        Me.cbxUnidad2.Name = "cbxUnidad2"
        Me.cbxUnidad2.Size = New System.Drawing.Size(327, 21)
        Me.cbxUnidad2.TabIndex = 111
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(0, 51)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(322, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtDerechohabiencia
        '
        Me.txtDerechohabiencia.BackColor = System.Drawing.SystemColors.Window
        Me.txtDerechohabiencia.Location = New System.Drawing.Point(0, 120)
        Me.txtDerechohabiencia.Name = "txtDerechohabiencia"
        Me.txtDerechohabiencia.ReadOnly = True
        Me.txtDerechohabiencia.Size = New System.Drawing.Size(322, 20)
        Me.txtDerechohabiencia.TabIndex = 8
        Me.txtDerechohabiencia.TabStop = False
        '
        'lblCS
        '
        Me.lblCS.AutoSize = True
        Me.lblCS.Location = New System.Drawing.Point(-2, 161)
        Me.lblCS.Name = "lblCS"
        Me.lblCS.Size = New System.Drawing.Size(119, 13)
        Me.lblCS.TabIndex = 9
        Me.lblCS.Text = "CS Correspondiente"
        '
        'lblDerechohabiencia
        '
        Me.lblDerechohabiencia.AutoSize = True
        Me.lblDerechohabiencia.Location = New System.Drawing.Point(3, 87)
        Me.lblDerechohabiencia.Name = "lblDerechohabiencia"
        Me.lblDerechohabiencia.Size = New System.Drawing.Size(110, 13)
        Me.lblDerechohabiencia.TabIndex = 7
        Me.lblDerechohabiencia.Text = "Derechohabiencia"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(3, 30)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'btnGuardar
        '
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(714, 652)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(140, 23)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnRetirado
        '
        Me.btnRetirado.Enabled = False
        Me.btnRetirado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetirado.Location = New System.Drawing.Point(14, 652)
        Me.btnRetirado.Name = "btnRetirado"
        Me.btnRetirado.Size = New System.Drawing.Size(140, 23)
        Me.btnRetirado.TabIndex = 3
        Me.btnRetirado.Text = "Retiro voluntario"
        Me.btnRetirado.UseVisualStyleBackColor = True
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(6, 171)
        Me.txtObservacion.MaxLength = 4000
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(488, 46)
        Me.txtObservacion.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 148)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Observación:"
        '
        'rbtNoAcudio
        '
        Me.rbtNoAcudio.AutoSize = True
        Me.rbtNoAcudio.Location = New System.Drawing.Point(44, 28)
        Me.rbtNoAcudio.Name = "rbtNoAcudio"
        Me.rbtNoAcudio.Size = New System.Drawing.Size(83, 17)
        Me.rbtNoAcudio.TabIndex = 0
        Me.rbtNoAcudio.TabStop = True
        Me.rbtNoAcudio.Text = "No acudio"
        Me.rbtNoAcudio.UseVisualStyleBackColor = True
        '
        'rbtNoMedico
        '
        Me.rbtNoMedico.AutoSize = True
        Me.rbtNoMedico.Location = New System.Drawing.Point(44, 51)
        Me.rbtNoMedico.Name = "rbtNoMedico"
        Me.rbtNoMedico.Size = New System.Drawing.Size(109, 17)
        Me.rbtNoMedico.TabIndex = 1
        Me.rbtNoMedico.TabStop = True
        Me.rbtNoMedico.Text = "No hay médico"
        Me.rbtNoMedico.UseVisualStyleBackColor = True
        '
        'rbtNoInformacion
        '
        Me.rbtNoInformacion.AutoSize = True
        Me.rbtNoInformacion.Location = New System.Drawing.Point(44, 97)
        Me.rbtNoInformacion.Name = "rbtNoInformacion"
        Me.rbtNoInformacion.Size = New System.Drawing.Size(140, 17)
        Me.rbtNoInformacion.TabIndex = 3
        Me.rbtNoInformacion.TabStop = True
        Me.rbtNoInformacion.Text = "Falta de información"
        Me.rbtNoInformacion.UseVisualStyleBackColor = True
        '
        'rbtNoFicha
        '
        Me.rbtNoFicha.AutoSize = True
        Me.rbtNoFicha.Location = New System.Drawing.Point(44, 74)
        Me.rbtNoFicha.Name = "rbtNoFicha"
        Me.rbtNoFicha.Size = New System.Drawing.Size(121, 17)
        Me.rbtNoFicha.TabIndex = 2
        Me.rbtNoFicha.TabStop = True
        Me.rbtNoFicha.Text = "No alcanzo ficha"
        Me.rbtNoFicha.UseVisualStyleBackColor = True
        '
        'rbtBuenaAtencion
        '
        Me.rbtBuenaAtencion.AutoSize = True
        Me.rbtBuenaAtencion.Location = New System.Drawing.Point(44, 120)
        Me.rbtBuenaAtencion.Name = "rbtBuenaAtencion"
        Me.rbtBuenaAtencion.Size = New System.Drawing.Size(239, 17)
        Me.rbtBuenaAtencion.TabIndex = 4
        Me.rbtBuenaAtencion.TabStop = True
        Me.rbtBuenaAtencion.Text = "Me gusta mas la atención del hospital"
        Me.rbtBuenaAtencion.UseVisualStyleBackColor = True
        '
        'gbxCuestionario
        '
        Me.gbxCuestionario.Controls.Add(Me.rbtBuenaAtencion)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoFicha)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoInformacion)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoMedico)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoAcudio)
        Me.gbxCuestionario.Controls.Add(Me.Label1)
        Me.gbxCuestionario.Controls.Add(Me.txtObservacion)
        Me.gbxCuestionario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCuestionario.Location = New System.Drawing.Point(354, 414)
        Me.gbxCuestionario.Name = "gbxCuestionario"
        Me.gbxCuestionario.Size = New System.Drawing.Size(500, 223)
        Me.gbxCuestionario.TabIndex = 2
        Me.gbxCuestionario.TabStop = False
        Me.gbxCuestionario.Text = "¿Por qué no consulto en el Centro de Salud?"
        '
        'Timer1
        '
        '
        'IdT
        '
        Me.IdT.HeaderText = "IdTriage"
        Me.IdT.Name = "IdT"
        Me.IdT.ReadOnly = True
        Me.IdT.Visible = False
        Me.IdT.Width = 60
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'colHoraEval
        '
        Me.colHoraEval.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHoraEval.DefaultCellStyle = DataGridViewCellStyle3
        Me.colHoraEval.HeaderText = "Hora Evaluación"
        Me.colHoraEval.Name = "colHoraEval"
        Me.colHoraEval.ReadOnly = True
        Me.colHoraEval.Width = 115
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle4
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 75
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Visible = False
        Me.colNumCtrl.Width = 76
        '
        'Curp
        '
        Me.Curp.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Curp.DefaultCellStyle = DataGridViewCellStyle5
        Me.Curp.HeaderText = "CURP"
        Me.Curp.Name = "Curp"
        Me.Curp.ReadOnly = True
        Me.Curp.Width = 66
        '
        'Observaciones
        '
        Me.Observaciones.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Observaciones.DefaultCellStyle = DataGridViewCellStyle6
        Me.Observaciones.HeaderText = "Observaciones"
        Me.Observaciones.Name = "Observaciones"
        Me.Observaciones.ReadOnly = True
        Me.Observaciones.Width = 116
        '
        'idderechohab
        '
        Me.idderechohab.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.idderechohab.HeaderText = "derecho"
        Me.idderechohab.Name = "idderechohab"
        Me.idderechohab.ReadOnly = True
        Me.idderechohab.Width = 78
        '
        'Derechohabiencia
        '
        Me.Derechohabiencia.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.Derechohabiencia.HeaderText = "derechohabiencia"
        Me.Derechohabiencia.Name = "Derechohabiencia"
        Me.Derechohabiencia.ReadOnly = True
        Me.Derechohabiencia.Width = 133
        '
        'frmListadoPacientesTSCVGO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(868, 713)
        Me.Controls.Add(Me.btnRetirado)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCuestionario)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvListado)
        Me.Name = "frmListadoPacientesTSCVGO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de pacientes (Trabajo Social)"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.gbxCuestionario.ResumeLayout(False)
        Me.gbxCuestionario.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents txtDerechohabiencia As System.Windows.Forms.TextBox
    Friend WithEvents lblCS As System.Windows.Forms.Label
    Friend WithEvents lblDerechohabiencia As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnRetirado As System.Windows.Forms.Button
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rbtNoAcudio As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoMedico As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoInformacion As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoFicha As System.Windows.Forms.RadioButton
    Friend WithEvents rbtBuenaAtencion As System.Windows.Forms.RadioButton
    Friend WithEvents gbxCuestionario As System.Windows.Forms.GroupBox
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents cbxUnidad2 As System.Windows.Forms.ComboBox
    Friend WithEvents IdT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHoraEval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Observaciones As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idderechohab As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Derechohabiencia As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
