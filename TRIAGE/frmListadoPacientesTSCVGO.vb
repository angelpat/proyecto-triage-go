﻿Imports System.Data.SqlClient

Public Class frmListadoPacientesTSCVGO

    Dim IdTRIAGE As Integer
    Dim idSeleccionado As Integer
    Dim NumControl As String
    Dim idPaciente As String
    Dim UNIDAD As BuscarUnidad = New BuscarUnidad
    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader


    Private Function RegresaRespuesta() As Integer

        Dim respuesta As Integer = 0

        If rbtNoAcudio.Checked = True Then
            respuesta = 1
        ElseIf rbtNoMedico.Checked = True Then
            respuesta = 2
        ElseIf rbtNoFicha.Checked = True Then
            respuesta = 3
        ElseIf rbtNoInformacion.Checked = True Then
            respuesta = 4
        ElseIf rbtBuenaAtencion.Checked = True Then
            respuesta = 5
        End If

        Return respuesta

    End Function


    Private Sub CargaRespuesta(ByVal respuesta As Integer)

        If respuesta = 1 Then
            rbtNoAcudio.Checked = True
        ElseIf respuesta = 2 Then
            rbtNoMedico.Checked = True
        ElseIf respuesta = 3 Then
            rbtNoFicha.Checked = True
        ElseIf respuesta = 4 Then
            rbtNoInformacion.Checked = True
        ElseIf respuesta = 5 Then
            rbtBuenaAtencion.Checked = True
        Else
            LimpiaRespuesta()
        End If

    End Sub


    Private Sub LimpiaRespuesta()
        rbtNoAcudio.Checked = False
        rbtNoMedico.Checked = False
        rbtNoFicha.Checked = False
        rbtNoInformacion.Checked = False
        rbtBuenaAtencion.Checked = False
    End Sub


    Private Sub LimpiaFormulario()

        txtDerechohabiencia.Text = ""
        txtObservacion.Text = ""
        LimpiaRespuesta()
    End Sub


    Private Sub ActivaFormulario(ByVal respuesta As Integer)

        If respuesta > 0 Then
            gbxPaciente.Enabled = False
            gbxCuestionario.Enabled = False
            CargaRespuesta(respuesta)
            btnGuardar.Enabled = False
            btnRetirado.Enabled = True
        Else
            gbxPaciente.Enabled = True
            gbxCuestionario.Enabled = True
            LimpiaRespuesta()
            btnGuardar.Enabled = True
            btnRetirado.Enabled = False
        End If

    End Sub





    Private Sub RefrescaLista()
        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim Edad As String
        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand
        Dim evaluado As String
        IdTRIAGE = 0
        Try

            VchCadena = "zspTRI_ColorVerdeListaGO"

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()
            comando.CommandText = "exec zspTRI_updPrioridad"
            comando.ExecuteNonQuery()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read

                    dgvListado.Rows.Add(lectorhere.getvalue(0), lectorhere.getvalue(1), lectorhere.getvalue(3), lectorhere.getvalue(2), lectorhere.getvalue(5), lectorhere.getvalue(4), lectorhere.getvalue(6), lectorhere.getvalue(7), lectorhere.getvalue(8))

                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado - RefrescaLista()")
        End Try
        cnn.Close()
    End Sub


    Private Sub frmListadoPacientesTS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmPrincipalTriage.validaAcceso()
        UNIDAD.Mostar_Unidad(2)
        RefrescaLista()
        lblNombreRevisor.Text = frmEvaluar.RegresaEmpleado()
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
        Timer1.Interval = 1000
        ' Enable timer.
        Timer1.Enabled = True
    End Sub


    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting
        If dgvListado.Columns(e.ColumnIndex).Name = "Prioridad" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()

                If ((stringValue.IndexOf("3") > -1)) Then
                    e.CellStyle.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                    e.CellStyle.SelectionBackColor = Color.FromArgb(115, 189, 58)
                    e.CellStyle.SelectionForeColor = Color.Black
                End If
            End If
        ElseIf dgvListado.Columns(e.ColumnIndex).Name = "TS" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()
                If ((stringValue.IndexOf("si") > -1)) Then
                    e.CellStyle.BackColor = Color.SkyBlue
                    e.CellStyle.SelectionBackColor = Color.SkyBlue
                End If
            End If
        End If
    End Sub


    'Private Sub dgvListado_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellDoubleClick
    '    idSeleccionado = dgvListado.Item("idT", e.RowIndex).Value
    '    idPaciente = dgvListado.Item("CURP", e.RowIndex).Value
    '    NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
    '    CargaFormulario(idSeleccionado)
    '    txtCS.Focus()
    'End Sub


    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Dim resp As Integer = RegresaRespuesta()

        Dim transaccion As SqlTransaction

        Try
            cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
            cnn.Open()
            comando = New SqlClient.SqlCommand("", cnn)
            transaccion = cnn.BeginTransaction()
            comando.Transaction = transaccion

            '        comando.CommandText = "exec zspTRI_insCuestionario " + idSeleccionado.ToString() + ", '" + frmPrincipalTriage.VchIdEmpleadoG + "', " + resp.ToString() + ", '" + txtCS.Text + "', '" + txtObservacion.Text + "'"
            comando.ExecuteNonQuery()
            transaccion.Commit()

            If (MessageBox.Show("¿El paciente se retiro voluntariamente?", "Titulo", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2) = MsgBoxResult.Yes) Then
                ''     frmListadoPacientesGO.CambiaEstadoHGC_ADMISION(idPaciente)
                ' frmListadoPacientesGO.TerminaConsulta("R", idSeleccionado.ToString(), NumControl, idPaciente)
                MsgBox("Registro guardado satisfactoriamente")
            End If

            dgvListado.Rows.Clear()
            RefrescaLista()
            LimpiaFormulario()
            btnGuardar.Enabled = False

        Catch ex As Exception
            transaccion.Rollback()
            MessageBox.Show(ex.Message.ToString, "Triage Error inesperado - btnGuardar_Click()")
        End Try


    End Sub





    'Private Function ValidaFormulario(ByVal resp As Integer) As Boolean

    '    Dim Validado As Boolean = False

    '    If txtCS.Text = "" Then
    '        MsgBox("El Centro de Salud no debe estar vacio")
    '    ElseIf txtCS.Text.Length <= 3 Then
    '        MsgBox("El Centro de Salud debe contener mas de 3 caracateres")
    '    ElseIf resp = 0 Then
    '        MsgBox("Debe seleccionar una respuesta")
    '    Else
    '        Validado = True
    '    End If

    '    Return Validado

    'End Function


    Private Sub btnRetirado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetirado.Click
        '' frmListadoPacientesGO.CambiaEstadoHGC_ADMISION(idPaciente)
        ' frmListadoPacientesGO.TerminaConsulta("R", idSeleccionado.ToString(), NumControl, idPaciente)


        MsgBox("Registro guardado satisfactoriamente")
        LimpiaFormulario()
        ActivaFormulario(0)
        btnGuardar.Enabled = False
    End Sub


    Private Sub dgvListado_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellClick
        If e.RowIndex >= 0 AndAlso dgvListado.Item("idT", e.RowIndex).Value IsNot Nothing Then
            idSeleccionado = dgvListado.Item("idT", e.RowIndex).Value
            idPaciente = dgvListado.Item("CURP", e.RowIndex).Value
            NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
            txtPaciente.Text = dgvListado.Item("Nombre", e.RowIndex).Value
            txtDerechohabiencia.Text = dgvListado.Item("Derechohabiencia", e.RowIndex).Value
            btnRetirado.Enabled = True
            btnGuardar.Enabled = True
        End If
    End Sub


    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        dgvListado.Rows.Clear()
        Me.RefrescaLista()
    End Sub




End Class