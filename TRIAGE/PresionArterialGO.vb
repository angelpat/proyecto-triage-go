﻿Public Class PresionArterialGO
    Public Function Sistolica(ByRef rsistolica As Integer) As Integer
        If rsistolica >= 100 And rsistolica <= 130 Then
            Return 1
        ElseIf rsistolica >= 131 And rsistolica <= 159 Or rsistolica >= 90 And rsistolica <= 99 Then
            Return 2
        ElseIf rsistolica > 160 And rsistolica < 89 Then
            Return 3
        End If
    End Function


    Public Function Diastolica(ByRef rDiastolica As Integer) As Integer
        If rDiastolica >= 60 And rDiastolica <= 90 Then
            Return 1
        ElseIf rDiastolica >= 91 And rDiastolica <= 109 Or rDiastolica >= 51 And rDiastolica <= 59 Then
            Return 2
        ElseIf rDiastolica > 110 And rDiastolica < 50 Then
            Return 3
        End If

    End Function
End Class
