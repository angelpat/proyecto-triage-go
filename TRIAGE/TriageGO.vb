﻿Public Class TriageGO
    'Dim idempleado As frmPrincipalTriage = New frmPrincipalTriage
    Public miCNum_Control As Int64
    Public miIdPaciente As String
    Public miIdPersonal As String
    Public miFechaS As String
    Public miFechaIntriage As String
    Public miValoracion As String
    Public miidreferido As String
    Public miidderechoabiencia As String
    Public miPaSistolica As Double
    Public miPaDiastolica As Double
    Public miIndiceDeChoque As Double
    Public miPulso As Double
    Public miRespiracion As Double
    Public miTemperatura As Double
    Public miGlucosa As Double
    Public miPeso As Double
    Public MIAltura As Double
    Public IMC As Double
    Public Fcf As Double
    Public miEdadGesta As Double
    Public status As String
    Public valortriage As String
    Public miestconciencia As String
    Public miHemorragia As String
    Public miDolorObstetrico As String
    Public miCrisisConvulsiva As String
    Public miNauseas As String
    Public miRespiracion1 As String
    Public miColorPIel As String
    Public miSangradoTrans As String
    Public miCefalea As String
    Public miAcufenos As String
    Public miFosfenos As String
    Public miEpigastralgia As String
    Public miAmaurosis As String
    Public miSondromeFebril As String
    Public miLiquidoAmniotico As String
    Public mIMotalidadFetal As String
    Public miCesarea As String
    Public miTrauma As String
    Public miSangrado12 As String
    Public misObservaciones As String
    Public miCardiopatia As String
    Public miNefropatia As String
    Public miHematopatia As String
    Public miEndocrinopatia As String
    Public miHepatopatia As String
    Public miVIH As String
    Public miSifilis As String
    Public mitirauroanalisis As String
    Public mivalortriage As String
    Public revisado As Boolean
 





    Public Sub concatenar()
        frmEvaluarObstetrico.txtFrecCard.Text = miPulso
        frmEvaluarObstetrico.txtPaSistolica.Text = miPaSistolica
        frmEvaluarObstetrico.txtPaDiastolica.Text = miPaDiastolica
        frmEvaluarObstetrico.txtIndChoque.Text = Math.Round(frmEvaluarObstetrico.txtFrecCard.Text / frmEvaluarObstetrico.txtPaSistolica.Text, 3)
        frmEvaluarObstetrico.txtTemperatura.Text = miTemperatura
        frmEvaluarObstetrico.txtGlucosa.Text = miGlucosa
        frmEvaluarObstetrico.txtRespiracion.Text = miRespiracion
        frmEvaluarObstetrico.txtPeso.Text = miPeso
        frmEvaluarObstetrico.txtAltura.Text = MIAltura
        frmEvaluarObstetrico.txtIMC.Text = Math.Round(frmEvaluarObstetrico.txtPeso.Text / Math.Pow(frmEvaluarObstetrico.txtAltura.Text, 2), 2)
        frmEvaluarObstetrico.txtFCFFeto.Text = Fcf
        frmEvaluarObstetrico.txtEdadGestacional.Text = miEdadGesta
        frmEvaluarObstetrico.txtMotAtencion.Text = misObservaciones

    End Sub


    Public Sub New()

    End Sub
    


    Public Property EstadoConciencia() As String
        Get
            Dim value As String

            If frmEvaluarObstetrico.rbtEstadoConcienciaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtEstadoConcienciaB.Text
            ElseIf frmEvaluarObstetrico.rbtEstadoConcienciaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtEstadoConcienciaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtEstadoConcienciaM.Text
            End If
            Me.miestconciencia = value
            Return Me.miestconciencia

        End Get

        Set(value As String)
            Me.miestconciencia = value
        End Set
    End Property



    Public Property Hemorragia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtHemorragiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtHemorragiaB.Text
            ElseIf frmEvaluarObstetrico.rbtHemorragiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtHemorragiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtHemorragiaA.Text
            End If
            Return value
        End Get
        Set(value As String)
            Me.miHemorragia = value
        End Set

    End Property
    Public Property DolorObstetrico() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtDolorObstetricoB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtDolorObstetricoB.Text
            ElseIf frmEvaluarObstetrico.rbtDolorObstetricoM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtDolorObstetricoM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtDolorObstetricoA.Text
            End If
            Return value
        End Get
        Set(value As String)
            Me.miDolorObstetrico = value
        End Set

    End Property

    Public Property CrisisConvulsivas() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtCrisisConvulsivaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtCrisisConvulsivaB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtCrisisConvulsivaA.Text

            End If
            Return value

        End Get
        Set(value As String)
            Me.miCrisisConvulsiva = value
        End Set
    End Property

    Public Property Nausea() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtNauseaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtNauseaB.Text
            ElseIf frmEvaluarObstetrico.rbtNauseaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtNauseaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtNauseaA.Text

            End If
            Return value

        End Get
        Set(value As String)
            Me.miNauseas = value
        End Set

    End Property

    Public Property Respiracion() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtRespiracionB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtRespiracionB.Text
            ElseIf frmEvaluarObstetrico.rbtRespiracionM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtRespiracionM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtRespiracionA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miRespiracion1 = value
        End Set

    End Property

    Public Property ColorPiel() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtPalidezB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtPalidezB.Text
            ElseIf frmEvaluarObstetrico.rbtPalidezM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtPalidezM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtPalidezA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miColorPIel = value
        End Set

    End Property

    Public Property SangradoTransvaginal() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtSangradotransB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtSangradotransB.Text
            ElseIf frmEvaluarObstetrico.rbtSangradotransM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtSangradotransM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtSangradotransA.Text
            End If
            Return value


        End Get
        Set(value As String)
            Me.miSangradoTrans = value
        End Set


    End Property



    Public Property Cefalea() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtCefaleaB.Checked = True Then

                value = "B-" + frmEvaluarObstetrico.rbtCefaleaB.Text
            ElseIf frmEvaluarObstetrico.rbtCefaleaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtCefaleaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtCefaleaA.Text
            End If
            Return value


        End Get
        Set(value As String)
            Me.miCefalea = value
        End Set


    End Property



    Public Property Acufenos() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtAcufenosB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtAcufenosB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtAcufenosA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miAcufenos = value
        End Set


    End Property

    Public Property Fosfenos() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtFosfenosB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtFosfenosB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtFosfenosA.Text
            End If
            Return value


        End Get
        Set(value As String)
            Me.miFosfenos = value
        End Set

    End Property

    Public Property Epigastralgia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtEpigastragiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtEpigastragiaB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtEpigastragiaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miEpigastralgia = value
        End Set


    End Property

    Public Property Amaurosis() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtAmaurosisB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtAmaurosisB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtAmaurosisA.Text
            End If
            Return value


        End Get
        Set(value As String)
            Me.miAmaurosis = value
        End Set


    End Property

    Public Property SindromeFebril() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtSindromeFebrilB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtSindromeFebrilB.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtSindromeFebrilA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miSondromeFebril = value
        End Set


    End Property

    Public Property LiquidoAmniotico() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtLiquidoAmnioticoB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtLiquidoAmnioticoB.Text
            ElseIf frmEvaluarObstetrico.rbtLiquidoAmnioticoA1.Checked = True Then
                value = "A1-" + frmEvaluarObstetrico.rbtLiquidoAmnioticoA1.Text
            Else
                value = "A2-" + frmEvaluarObstetrico.rbtLiquidoAmnioticoA2.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miLiquidoAmniotico = value
        End Set


    End Property

    Public Property MotilidadFetal() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtMotilidadFetalB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtMotilidadFetalB.Text
            ElseIf frmEvaluarObstetrico.rbtMotilidadFetalM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtMotilidadFetalM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtMotilidadFetalA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.mIMotalidadFetal = value
        End Set


    End Property

    Public Property CesareaCirUterina() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtCirUterinaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtCirUterinaB.Text
            Else
                value = "M-" + frmEvaluarObstetrico.rbtCirUterinaM.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miCesarea = value
        End Set

    End Property

    Public Property TraumaAgresion() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtTraumaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtTraumaB.Text
            ElseIf frmEvaluarObstetrico.rbtTraumaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtTraumaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtTraumaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miTrauma = value
        End Set


    End Property
    Public Property Cardiopatia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtCardiopatiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtCardiopatiaB.Text
            ElseIf frmEvaluarObstetrico.rbtCardiopatiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtCardiopatiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtCardiopatiaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miCardiopatia = value
        End Set


    End Property
    Public Property Nefropatia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtNefropatiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtNefropatiaB.Text
            ElseIf frmEvaluarObstetrico.rbtNefropatiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtNefropatiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtNefropatiaA.Text
            End If
            Return value

        End Get
        Set(value As String)

            Me.miNefropatia = value
        End Set


    End Property
    Public Property Hematopatia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtHematopatiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtHematopatiaB.Text
            ElseIf frmEvaluarObstetrico.rbtHematopatiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtHematopatiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtHematopatiaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miHematopatia = value
        End Set


    End Property
    Public Property Endocrinologia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtEndocrinopatiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtEndocrinopatiaB.Text
            ElseIf frmEvaluarObstetrico.rbtEndocrinopatiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtEndocrinopatiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtEndocrinopatiaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miEndocrinopatia = value
        End Set


    End Property
    Public Property Hepatopiatia() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtHepatopatiaB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtHepatopatiaB.Text
            ElseIf frmEvaluarObstetrico.rbtHepatopatiaM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtHepatopatiaM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtHepatopatiaA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miHepatopatia = value
        End Set
    End Property
    Public Property sangrado12() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtSangradoB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtSangradoB.Text
            ElseIf frmEvaluarObstetrico.rbtSangradoM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtSangradoM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtSangradoA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.miSangrado12 = value
        End Set

    End Property




    Public Property PruebaVIH() As String

        Get
            Dim value As String

            If frmEvaluarObstetrico.rbtPruebaVIHB.Checked = True Then
                value = "B-" + "NO"
            Else
                Dim combobox As frmEvaluarObstetrico = New frmEvaluarObstetrico()

                value = "A-" + combobox.cbxResultadoVIH.SelectedItem.ToString
            End If
            Return value
        End Get
        Set(value As String)
            Me.miVIH = value
        End Set

    End Property

    Public Property PruebaSifilis() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtPruebaSifilisB.Checked = True Then
                value = "B-" + "NO"
            Else
                value = "A-" + frmEvaluarObstetrico.cbxResultadoSIFILIS.SelectedItem.ToString
            End If
            Return value

        End Get
        Set(value As String)
            Me.miSifilis = value
        End Set

    End Property


    Public Property TiraUroanalisis() As String

        Get
            Dim value As String
            If frmEvaluarObstetrico.rbtTiraUroanalisisB.Checked = True Then
                value = "B-" + frmEvaluarObstetrico.rbtTiraUroanalisisB.Text
            ElseIf frmEvaluarObstetrico.rbtTiraUroanalisisM.Checked = True Then
                value = "M-" + frmEvaluarObstetrico.rbtTiraUroanalisisM.Text
            Else
                value = "A-" + frmEvaluarObstetrico.rbtTiraUroanalisisA.Text
            End If
            Return value

        End Get
        Set(value As String)
            Me.mitirauroanalisis = value
        End Set
    End Property




End Class
