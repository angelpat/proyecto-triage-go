﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeguimiento
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvListado = New System.Windows.Forms.DataGridView()
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cStatus = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.paciente = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.csexo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.hora_ingreso = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.area_inciial = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.area_actual = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.medico_nota = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fecha_nota = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colNumCtrl, Me.cStatus, Me.paciente, Me.csexo, Me.hora_ingreso, Me.area_inciial, Me.area_actual, Me.medico_nota, Me.fecha_nota})
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle2
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(2, 65)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(922, 300)
        Me.dgvListado.TabIndex = 1
        Me.dgvListado.TabStop = False
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.MaxInputLength = 10
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        '
        'cStatus
        '
        Me.cStatus.HeaderText = "cStatus"
        Me.cStatus.Name = "cStatus"
        Me.cStatus.ReadOnly = True
        '
        'paciente
        '
        Me.paciente.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.paciente.HeaderText = "PACIENTE"
        Me.paciente.Name = "paciente"
        Me.paciente.ReadOnly = True
        Me.paciente.Width = 93
        '
        'csexo
        '
        Me.csexo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.csexo.HeaderText = "SEXO"
        Me.csexo.Name = "csexo"
        Me.csexo.ReadOnly = True
        Me.csexo.Width = 65
        '
        'hora_ingreso
        '
        Me.hora_ingreso.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.hora_ingreso.HeaderText = "INGRESO"
        Me.hora_ingreso.Name = "hora_ingreso"
        Me.hora_ingreso.ReadOnly = True
        Me.hora_ingreso.Width = 88
        '
        'area_inciial
        '
        Me.area_inciial.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.area_inciial.HeaderText = "AREA INICIAL"
        Me.area_inciial.Name = "area_inciial"
        Me.area_inciial.ReadOnly = True
        Me.area_inciial.Width = 104
        '
        'area_actual
        '
        Me.area_actual.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.area_actual.HeaderText = "AREA ACTUAL"
        Me.area_actual.Name = "area_actual"
        Me.area_actual.ReadOnly = True
        Me.area_actual.Width = 107
        '
        'medico_nota
        '
        Me.medico_nota.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.medico_nota.HeaderText = "NOTA"
        Me.medico_nota.Name = "medico_nota"
        Me.medico_nota.ReadOnly = True
        Me.medico_nota.Width = 66
        '
        'fecha_nota
        '
        Me.fecha_nota.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.fecha_nota.HeaderText = "FECHA NOTA"
        Me.fecha_nota.Name = "fecha_nota"
        Me.fecha_nota.ReadOnly = True
        Me.fecha_nota.Width = 101
        '
        'frmSeguimiento
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(936, 426)
        Me.Controls.Add(Me.dgvListado)
        Me.Name = "frmSeguimiento"
        Me.Text = "Seguimiento"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cStatus As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents paciente As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents csexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents hora_ingreso As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents area_inciial As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents area_actual As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents medico_nota As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fecha_nota As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
