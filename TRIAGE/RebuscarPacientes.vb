﻿Imports System.Data.SqlClient

Public Class RebuscarPacientes
    Dim dt As DataTable
    Dim Da As New SqlDataAdapter
    Dim cnn As New SqlClient.SqlConnection
    Dim comando As New SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader
    Public numcontrol As Object
    Dim prediagadm As String
    Dim CNumcontrol As String
    Dim db As conexionbd = New conexionbd()
    Public LIMPIAR As LimIniFormulario = New LimIniFormulario()

    Public Sub RecargaPacientes()
        Try
            db.validarbd()
            cnn = New SqlClient.SqlConnection(db.conexion)
            With comando
                .CommandType = CommandType.Text
                .CommandText = "zspTRI_PacientesRevaluadosGO;"
                .Connection = cnn
            End With

            Da.SelectCommand = comando
            dt = New DataTable

            ''''''prueba
      
            ''''''fin prueba

            Da.Fill(dt)

            With frmEvaluarObstetrico.cbxPaciente
                .DataSource = dt
                .DisplayMember = "nombre"
                .ValueMember = "cnum_control"
            End With

            'frmEvaluarObstetrico.valorado = "RE"
            'frmEvaluarObstetrico.cbxPaciente.SelectedIndex = -1
            cnn.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los pacientes." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try


    End Sub


    Public Function Devolvervalores(ByRef numerocontrol) As TriageGO
        'LIMPIAR.limpiarFormulario()
        Dim ObjsGO As ValEvaluarObstetrico = New ValEvaluarObstetrico()
        Dim objTGO As TriageGO = New TriageGO
        Try

            db.validarbd()
            cnn = New SqlClient.SqlConnection(db.conexion)

            With comando
                .CommandType = CommandType.Text
                .CommandText = "exec zspTRI_RebuscarpacienteGO '" & numerocontrol & "' "
                .Connection = cnn
            End With
            'TODO -> Crear variables para obtener las posiciones ordinales de cada uno de los

            '_____________________________________prueba sergio
            cnn.Open()
            lector = comando.ExecuteReader
            'If  Then
            If lector.HasRows Then
                While lector.Read
                    With objTGO
                        'SIGNOS VITALES
                        ' MAMA
                        objTGO.miPaSistolica = lector.GetValue(8)
                        objTGO.miPaDiastolica = lector.GetValue(9)
                        objTGO.miPulso = lector.GetValue(10)
                        objTGO.miTemperatura = lector.GetValue(12)
                        objTGO.miGlucosa = lector.GetValue(13)
                        objTGO.miRespiracion = lector.GetValue(14)
                        objTGO.miPeso = lector.GetValue(15)
                        objTGO.MIAltura = lector.GetValue(16)
                        objTGO.Fcf = lector.GetValue(18)
                        objTGO.miEdadGesta = lector.GetValue(19)

                        '-----------------------------------------------
                        objTGO.miestconciencia = lector.GetValue(20)
                        objTGO.miHemorragia = lector.GetValue(21)
                        objTGO.miDolorObstetrico = lector.GetValue(22)
                        objTGO.miCrisisConvulsiva = lector.GetValue(23)
                        objTGO.miNauseas = lector.GetValue(24)
                        objTGO.Respiracion = lector.GetValue(25)
                        objTGO.miColorPIel = lector.GetValue(26)
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objTGO.miSangradoTrans = lector.GetValue(27)
                        objTGO.miCefalea = lector.GetValue(28)
                        objTGO.miAcufenos = lector.GetValue(29)
                        objTGO.miFosfenos = lector.GetValue(30)
                        objTGO.miEpigastralgia = lector.GetValue(31)
                        objTGO.miAmaurosis = lector.GetValue(32)
                        objTGO.miSondromeFebril = lector.GetValue(33)
                        objTGO.miLiquidoAmniotico = lector.GetValue(34)
                        objTGO.mIMotalidadFetal = lector.GetValue(35)
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objTGO.CesareaCirUterina = lector.GetValue(36)
                        objTGO.TraumaAgresion = lector.GetValue(37)
                        objTGO.sangrado12 = lector.GetValue(38)
                        objTGO.misObservaciones = lector.GetValue(39)
                        ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                        objTGO.miCardiopatia = lector.GetValue(40)
                        objTGO.miNefropatia = lector.GetValue(41)
                        objTGO.miHematopatia = lector.GetValue(42)
                        objTGO.miEndocrinopatia = lector.GetValue(43)
                        objTGO.miHepatopatia = lector.GetValue(44)
                        objTGO.miVIH = lector.GetValue(45)
                        objTGO.miSifilis = lector.GetValue(46)
                        objTGO.mitirauroanalisis = lector.GetValue(47)
                        objTGO.mivalortriage = Mid(lector.GetValue(49), 1, 1)
                    End With

                End While

            End If
            'End If
            cnn.Close()
            '_____________________________________fin prueba sergio





            'Da.SelectCommand = comando
            'dt = New DataTable

            ''''''prueba

            ''''''fin prueba

            'Da.Fill(dt)


            'For Each row As DataRow In dt.Rows
            '    For q As Integer = 0 To 20
            '        ValoresValorados.Add(CStr(row(q)))
            '        q = q + 0
            '    Next
            'Next

            'frmEvaluarObstetrico.valorado = "RE"
            'frmEvaluarObstetrico.cbxPaciente.SelectedIndex = -1

            cnn.Close()
            If numerocontrol <> "" Then
                objTGO.concatenar()
            End If
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar los pacientes." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try


        Return objTGO
    End Function

End Class
