﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmEvaluarObstetrico
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.tabObstetrico = New System.Windows.Forms.TabControl()
        Me.tabPagTriage = New System.Windows.Forms.TabPage()
        Me.txtMotAtencion = New System.Windows.Forms.TextBox()
        Me.lblMotatencion = New System.Windows.Forms.Label()
        Me.gbxSignosVitales = New System.Windows.Forms.GroupBox()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lblPulsoFeto = New System.Windows.Forms.Label()
        Me.lblFCFeto = New System.Windows.Forms.Label()
        Me.txtFCFFeto = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.lblEdadGestacional = New System.Windows.Forms.Label()
        Me.txtEdadGestacional = New System.Windows.Forms.TextBox()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lblPresionA = New System.Windows.Forms.Label()
        Me.lblrpm = New System.Windows.Forms.Label()
        Me.lblM = New System.Windows.Forms.Label()
        Me.txtIndChoque = New System.Windows.Forms.TextBox()
        Me.lblPulso = New System.Windows.Forms.Label()
        Me.lblIndChoque = New System.Windows.Forms.Label()
        Me.lblFrecCard = New System.Windows.Forms.Label()
        Me.lblMmHg = New System.Windows.Forms.Label()
        Me.txtPaSistolica = New System.Windows.Forms.TextBox()
        Me.txtPaDiastolica = New System.Windows.Forms.TextBox()
        Me.Label20 = New System.Windows.Forms.Label()
        Me.txtFrecCard = New System.Windows.Forms.TextBox()
        Me.lblRespiracion = New System.Windows.Forms.Label()
        Me.txtIMC = New System.Windows.Forms.TextBox()
        Me.lblKg = New System.Windows.Forms.Label()
        Me.lblIMC = New System.Windows.Forms.Label()
        Me.txtRespiracion = New System.Windows.Forms.TextBox()
        Me.txtPeso = New System.Windows.Forms.TextBox()
        Me.lblPeso = New System.Windows.Forms.Label()
        Me.lblTemp = New System.Windows.Forms.Label()
        Me.txtGlucosa = New System.Windows.Forms.TextBox()
        Me.lblGradosC = New System.Windows.Forms.Label()
        Me.txtAltura = New System.Windows.Forms.TextBox()
        Me.txtTemperatura = New System.Windows.Forms.TextBox()
        Me.lblGlucosa = New System.Windows.Forms.Label()
        Me.lblAltura = New System.Windows.Forms.Label()
        Me.gbxSintomas = New System.Windows.Forms.GroupBox()
        Me.pnlPalidez = New System.Windows.Forms.Panel()
        Me.lblPalidez = New System.Windows.Forms.Label()
        Me.rbtPalidezA = New System.Windows.Forms.RadioButton()
        Me.rbtPalidezM = New System.Windows.Forms.RadioButton()
        Me.rbtPalidezB = New System.Windows.Forms.RadioButton()
        Me.pnlEstConciencia = New System.Windows.Forms.Panel()
        Me.lblEstadoConciencia = New System.Windows.Forms.Label()
        Me.rbtEstadoConcienciaA = New System.Windows.Forms.RadioButton()
        Me.rbtEstadoConcienciaM = New System.Windows.Forms.RadioButton()
        Me.rbtEstadoConcienciaB = New System.Windows.Forms.RadioButton()
        Me.pnlDisnea = New System.Windows.Forms.Panel()
        Me.lblRespiracon1 = New System.Windows.Forms.Label()
        Me.rbtRespiracionA = New System.Windows.Forms.RadioButton()
        Me.rbtRespiracionM = New System.Windows.Forms.RadioButton()
        Me.rbtRespiracionB = New System.Windows.Forms.RadioButton()
        Me.pnlNausea = New System.Windows.Forms.Panel()
        Me.lblNausea = New System.Windows.Forms.Label()
        Me.rbtNauseaA = New System.Windows.Forms.RadioButton()
        Me.rbtNauseaM = New System.Windows.Forms.RadioButton()
        Me.rbtNauseaB = New System.Windows.Forms.RadioButton()
        Me.pnlHemorragia = New System.Windows.Forms.Panel()
        Me.lblHemorragia = New System.Windows.Forms.Label()
        Me.rbtHemorragiaA = New System.Windows.Forms.RadioButton()
        Me.rbtHemorragiaM = New System.Windows.Forms.RadioButton()
        Me.rbtHemorragiaB = New System.Windows.Forms.RadioButton()
        Me.pnlDolorEpigastrico = New System.Windows.Forms.Panel()
        Me.lblCrisisConvulsiva = New System.Windows.Forms.Label()
        Me.rbtCrisisConvulsivaA = New System.Windows.Forms.RadioButton()
        Me.rbtCrisisConvulsivaB = New System.Windows.Forms.RadioButton()
        Me.pnlDolorObstetrico = New System.Windows.Forms.Panel()
        Me.lblDolorObstetrico = New System.Windows.Forms.Label()
        Me.rbtDolorObstetricoA = New System.Windows.Forms.RadioButton()
        Me.rbtDolorObstetricoM = New System.Windows.Forms.RadioButton()
        Me.rbtDolorObstetricoB = New System.Windows.Forms.RadioButton()
        Me.tabPagInterrogatorio = New System.Windows.Forms.TabPage()
        Me.lblObservaciones = New System.Windows.Forms.Label()
        Me.Textvaloracion = New System.Windows.Forms.TextBox()
        Me.pnlSangradoTrans = New System.Windows.Forms.GroupBox()
        Me.Panel10 = New System.Windows.Forms.Panel()
        Me.lblfetal = New System.Windows.Forms.Label()
        Me.rbtMotilidadFetalA = New System.Windows.Forms.RadioButton()
        Me.rbtMotilidadFetalM = New System.Windows.Forms.RadioButton()
        Me.rbtMotilidadFetalB = New System.Windows.Forms.RadioButton()
        Me.Panel9 = New System.Windows.Forms.Panel()
        Me.Panel8 = New System.Windows.Forms.Panel()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.RadioButton13 = New System.Windows.Forms.RadioButton()
        Me.RadioButton14 = New System.Windows.Forms.RadioButton()
        Me.RadioButton15 = New System.Windows.Forms.RadioButton()
        Me.lblsindromefebril = New System.Windows.Forms.Label()
        Me.rbtSindromeFebrilA = New System.Windows.Forms.RadioButton()
        Me.rbtSindromeFebrilB = New System.Windows.Forms.RadioButton()
        Me.Panel7 = New System.Windows.Forms.Panel()
        Me.lblliquidoamn = New System.Windows.Forms.Label()
        Me.rbtLiquidoAmnioticoA2 = New System.Windows.Forms.RadioButton()
        Me.rbtLiquidoAmnioticoA1 = New System.Windows.Forms.RadioButton()
        Me.rbtLiquidoAmnioticoB = New System.Windows.Forms.RadioButton()
        Me.Panel6 = New System.Windows.Forms.Panel()
        Me.lblepigastralgia = New System.Windows.Forms.Label()
        Me.rbtEpigastragiaA = New System.Windows.Forms.RadioButton()
        Me.rbtEpigastragiaB = New System.Windows.Forms.RadioButton()
        Me.Panel5 = New System.Windows.Forms.Panel()
        Me.lblfosfenos = New System.Windows.Forms.Label()
        Me.rbtFosfenosA = New System.Windows.Forms.RadioButton()
        Me.rbtFosfenosB = New System.Windows.Forms.RadioButton()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.lblsangradotrans = New System.Windows.Forms.Label()
        Me.rbtSangradotransA = New System.Windows.Forms.RadioButton()
        Me.rbtSangradotransM = New System.Windows.Forms.RadioButton()
        Me.rbtSangradotransB = New System.Windows.Forms.RadioButton()
        Me.pnlHiper = New System.Windows.Forms.Panel()
        Me.lblAcufos = New System.Windows.Forms.Label()
        Me.rbtAcufenosA = New System.Windows.Forms.RadioButton()
        Me.rbtAcufenosB = New System.Windows.Forms.RadioButton()
        Me.pnlSudoracion = New System.Windows.Forms.Panel()
        Me.lblamaurosis = New System.Windows.Forms.Label()
        Me.rbtAmaurosisA = New System.Windows.Forms.RadioButton()
        Me.rbtAmaurosisB = New System.Windows.Forms.RadioButton()
        Me.pnlCefalea = New System.Windows.Forms.Panel()
        Me.lblCefalea = New System.Windows.Forms.Label()
        Me.rbtCefaleaA = New System.Windows.Forms.RadioButton()
        Me.rbtCefaleaM = New System.Windows.Forms.RadioButton()
        Me.rbtCefaleaB = New System.Windows.Forms.RadioButton()
        Me.gbxInfoEmbarazo = New System.Windows.Forms.GroupBox()
        Me.pnlSangrado = New System.Windows.Forms.Panel()
        Me.lblSangrado = New System.Windows.Forms.Label()
        Me.rbtSangradoA = New System.Windows.Forms.RadioButton()
        Me.rbtSangradoM = New System.Windows.Forms.RadioButton()
        Me.rbtSangradoB = New System.Windows.Forms.RadioButton()
        Me.pnlCesarea = New System.Windows.Forms.Panel()
        Me.lblCirUterina = New System.Windows.Forms.Label()
        Me.rbtCirUterinaM = New System.Windows.Forms.RadioButton()
        Me.rbtCirUterinaB = New System.Windows.Forms.RadioButton()
        Me.pnlTrauma = New System.Windows.Forms.Panel()
        Me.lblTrauma = New System.Windows.Forms.Label()
        Me.rbtTraumaA = New System.Windows.Forms.RadioButton()
        Me.rbtTraumaM = New System.Windows.Forms.RadioButton()
        Me.rbtTraumaB = New System.Windows.Forms.RadioButton()
        Me.TabpagObservaciones = New System.Windows.Forms.TabPage()
        Me.gbxEnfAsociadas = New System.Windows.Forms.GroupBox()
        Me.pnlHemopatia = New System.Windows.Forms.Panel()
        Me.lblHematopatia = New System.Windows.Forms.Label()
        Me.rbtHematopatiaA = New System.Windows.Forms.RadioButton()
        Me.rbtHematopatiaM = New System.Windows.Forms.RadioButton()
        Me.rbtHematopatiaB = New System.Windows.Forms.RadioButton()
        Me.pnlEndocrinopatia = New System.Windows.Forms.Panel()
        Me.lblEndocrinopatia = New System.Windows.Forms.Label()
        Me.rbtEndocrinopatiaA = New System.Windows.Forms.RadioButton()
        Me.rbtEndocrinopatiaM = New System.Windows.Forms.RadioButton()
        Me.rbtEndocrinopatiaB = New System.Windows.Forms.RadioButton()
        Me.pnlNefropatia = New System.Windows.Forms.Panel()
        Me.lblNefropatia = New System.Windows.Forms.Label()
        Me.rbtNefropatiaA = New System.Windows.Forms.RadioButton()
        Me.rbtNefropatiaM = New System.Windows.Forms.RadioButton()
        Me.rbtNefropatiaB = New System.Windows.Forms.RadioButton()
        Me.pnlCardiopatia = New System.Windows.Forms.Panel()
        Me.lblCardiopatia = New System.Windows.Forms.Label()
        Me.rbtCardiopatiaA = New System.Windows.Forms.RadioButton()
        Me.rbtCardiopatiaM = New System.Windows.Forms.RadioButton()
        Me.rbtCardiopatiaB = New System.Windows.Forms.RadioButton()
        Me.pnlHepatopatia = New System.Windows.Forms.Panel()
        Me.lblHepatopatia = New System.Windows.Forms.Label()
        Me.rbtHepatopatiaA = New System.Windows.Forms.RadioButton()
        Me.rbtHepatopatiaM = New System.Windows.Forms.RadioButton()
        Me.rbtHepatopatiaB = New System.Windows.Forms.RadioButton()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.lblTiraUroanalisis = New System.Windows.Forms.Label()
        Me.rbtTiraUroanalisisA = New System.Windows.Forms.RadioButton()
        Me.rbtTiraUroanalisisM = New System.Windows.Forms.RadioButton()
        Me.rbtTiraUroanalisisB = New System.Windows.Forms.RadioButton()
        Me.Panel3 = New System.Windows.Forms.Panel()
        Me.cbxResultadoSIFILIS = New System.Windows.Forms.ComboBox()
        Me.lblPruebaSifilis = New System.Windows.Forms.Label()
        Me.rbtPruebaSifilisA = New System.Windows.Forms.RadioButton()
        Me.rbtPruebaSifilisB = New System.Windows.Forms.RadioButton()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.cbxResultadoVIH = New System.Windows.Forms.ComboBox()
        Me.lblPruebaVIH = New System.Windows.Forms.Label()
        Me.rbtPruebaVIHA = New System.Windows.Forms.RadioButton()
        Me.rbtPruebaVIHB = New System.Windows.Forms.RadioButton()
        Me.txtEdad = New System.Windows.Forms.TextBox()
        Me.lblEdad = New System.Windows.Forms.Label()
        Me.gbxValoracion = New System.Windows.Forms.GroupBox()
        Me.txtCurp = New System.Windows.Forms.TextBox()
        Me.lblCurp = New System.Windows.Forms.Label()
        Me.lblAños = New System.Windows.Forms.Label()
        Me.txtfechaactual = New System.Windows.Forms.TextBox()
        Me.txtfechadeingreso = New System.Windows.Forms.TextBox()
        Me.lblFechaAhora = New System.Windows.Forms.Label()
        Me.rbtReferido = New System.Windows.Forms.CheckBox()
        Me.cbxPaciente = New System.Windows.Forms.ComboBox()
        Me.txtDerechohabiencia = New System.Windows.Forms.TextBox()
        Me.lblFechaIngreso = New System.Windows.Forms.Label()
        Me.lblDerechohabiencia = New System.Windows.Forms.Label()
        Me.lblPaciente = New System.Windows.Forms.Label()
        Me.gbxPrioridadSug = New System.Windows.Forms.GroupBox()
        Me.pnlPrioridad = New System.Windows.Forms.Panel()
        Me.lblActuacion = New System.Windows.Forms.Label()
        Me.lblDenominacion = New System.Windows.Forms.Label()
        Me.lblNivel = New System.Windows.Forms.Label()
        Me.lblColor = New System.Windows.Forms.Label()
        Me.gbxEvaluacion = New System.Windows.Forms.GroupBox()
        Me.btnEvaluar = New System.Windows.Forms.Button()
        Me.gbxPrioridadSel = New System.Windows.Forms.GroupBox()
        Me.btnAceptar = New System.Windows.Forms.Button()
        Me.LblTipoUrgencia = New System.Windows.Forms.Label()
        Me.cbxTipoUrgencia = New System.Windows.Forms.ComboBox()
        Me.lblNivelAsig = New System.Windows.Forms.Label()
        Me.cbxPrioridadSel = New System.Windows.Forms.ComboBox()
        Me.cbxUnidad1 = New System.Windows.Forms.ComboBox()
        Me.tabObstetrico.SuspendLayout()
        Me.tabPagTriage.SuspendLayout()
        Me.gbxSignosVitales.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.gbxSintomas.SuspendLayout()
        Me.pnlPalidez.SuspendLayout()
        Me.pnlEstConciencia.SuspendLayout()
        Me.pnlDisnea.SuspendLayout()
        Me.pnlNausea.SuspendLayout()
        Me.pnlHemorragia.SuspendLayout()
        Me.pnlDolorEpigastrico.SuspendLayout()
        Me.pnlDolorObstetrico.SuspendLayout()
        Me.tabPagInterrogatorio.SuspendLayout()
        Me.pnlSangradoTrans.SuspendLayout()
        Me.Panel10.SuspendLayout()
        Me.Panel9.SuspendLayout()
        Me.Panel8.SuspendLayout()
        Me.Panel7.SuspendLayout()
        Me.Panel6.SuspendLayout()
        Me.Panel5.SuspendLayout()
        Me.Panel1.SuspendLayout()
        Me.pnlHiper.SuspendLayout()
        Me.pnlSudoracion.SuspendLayout()
        Me.pnlCefalea.SuspendLayout()
        Me.gbxInfoEmbarazo.SuspendLayout()
        Me.pnlSangrado.SuspendLayout()
        Me.pnlCesarea.SuspendLayout()
        Me.pnlTrauma.SuspendLayout()
        Me.TabpagObservaciones.SuspendLayout()
        Me.gbxEnfAsociadas.SuspendLayout()
        Me.pnlHemopatia.SuspendLayout()
        Me.pnlEndocrinopatia.SuspendLayout()
        Me.pnlNefropatia.SuspendLayout()
        Me.pnlCardiopatia.SuspendLayout()
        Me.pnlHepatopatia.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.Panel3.SuspendLayout()
        Me.Panel4.SuspendLayout()
        Me.gbxValoracion.SuspendLayout()
        Me.gbxPrioridadSug.SuspendLayout()
        Me.pnlPrioridad.SuspendLayout()
        Me.gbxEvaluacion.SuspendLayout()
        Me.gbxPrioridadSel.SuspendLayout()
        Me.SuspendLayout()
        '
        'tabObstetrico
        '
        Me.tabObstetrico.Controls.Add(Me.tabPagTriage)
        Me.tabObstetrico.Controls.Add(Me.tabPagInterrogatorio)
        Me.tabObstetrico.Controls.Add(Me.TabpagObservaciones)
        Me.tabObstetrico.Location = New System.Drawing.Point(13, 128)
        Me.tabObstetrico.Name = "tabObstetrico"
        Me.tabObstetrico.SelectedIndex = 0
        Me.tabObstetrico.Size = New System.Drawing.Size(729, 462)
        Me.tabObstetrico.TabIndex = 10
        '
        'tabPagTriage
        '
        Me.tabPagTriage.BackColor = System.Drawing.SystemColors.Window
        Me.tabPagTriage.Controls.Add(Me.txtMotAtencion)
        Me.tabPagTriage.Controls.Add(Me.lblMotatencion)
        Me.tabPagTriage.Controls.Add(Me.gbxSignosVitales)
        Me.tabPagTriage.Controls.Add(Me.gbxSintomas)
        Me.tabPagTriage.Location = New System.Drawing.Point(4, 22)
        Me.tabPagTriage.Name = "tabPagTriage"
        Me.tabPagTriage.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPagTriage.Size = New System.Drawing.Size(721, 436)
        Me.tabPagTriage.TabIndex = 0
        Me.tabPagTriage.Text = "Triage"
        '
        'txtMotAtencion
        '
        Me.txtMotAtencion.BackColor = System.Drawing.Color.Moccasin
        Me.txtMotAtencion.Enabled = False
        Me.txtMotAtencion.Location = New System.Drawing.Point(9, 18)
        Me.txtMotAtencion.Multiline = True
        Me.txtMotAtencion.Name = "txtMotAtencion"
        Me.txtMotAtencion.Size = New System.Drawing.Size(709, 32)
        Me.txtMotAtencion.TabIndex = 147
        '
        'lblMotatencion
        '
        Me.lblMotatencion.AutoSize = True
        Me.lblMotatencion.Font = New System.Drawing.Font("Arial Rounded MT Bold", 9.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMotatencion.Location = New System.Drawing.Point(6, 4)
        Me.lblMotatencion.Name = "lblMotatencion"
        Me.lblMotatencion.Size = New System.Drawing.Size(121, 14)
        Me.lblMotatencion.TabIndex = 146
        Me.lblMotatencion.Text = "Motivo de Atencion:"
        '
        'gbxSignosVitales
        '
        Me.gbxSignosVitales.Controls.Add(Me.GroupBox3)
        Me.gbxSignosVitales.Controls.Add(Me.GroupBox2)
        Me.gbxSignosVitales.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxSignosVitales.Location = New System.Drawing.Point(3, 54)
        Me.gbxSignosVitales.Name = "gbxSignosVitales"
        Me.gbxSignosVitales.Size = New System.Drawing.Size(709, 175)
        Me.gbxSignosVitales.TabIndex = 50
        Me.gbxSignosVitales.TabStop = False
        Me.gbxSignosVitales.Text = "Signos vitales"
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lblPulsoFeto)
        Me.GroupBox3.Controls.Add(Me.lblFCFeto)
        Me.GroupBox3.Controls.Add(Me.txtFCFFeto)
        Me.GroupBox3.Controls.Add(Me.Label4)
        Me.GroupBox3.Controls.Add(Me.lblEdadGestacional)
        Me.GroupBox3.Controls.Add(Me.txtEdadGestacional)
        Me.GroupBox3.Location = New System.Drawing.Point(503, 13)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(200, 153)
        Me.GroupBox3.TabIndex = 161
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Feto"
        '
        'lblPulsoFeto
        '
        Me.lblPulsoFeto.AutoSize = True
        Me.lblPulsoFeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPulsoFeto.Location = New System.Drawing.Point(42, 48)
        Me.lblPulsoFeto.Name = "lblPulsoFeto"
        Me.lblPulsoFeto.Size = New System.Drawing.Size(26, 13)
        Me.lblPulsoFeto.TabIndex = 58
        Me.lblPulsoFeto.Text = "FCF"
        '
        'lblFCFeto
        '
        Me.lblFCFeto.AutoSize = True
        Me.lblFCFeto.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFCFeto.Location = New System.Drawing.Point(127, 49)
        Me.lblFCFeto.Name = "lblFCFeto"
        Me.lblFCFeto.Size = New System.Drawing.Size(29, 13)
        Me.lblFCFeto.TabIndex = 0
        Me.lblFCFeto.Text = "F. C."
        '
        'txtFCFFeto
        '
        Me.txtFCFFeto.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtFCFFeto.Location = New System.Drawing.Point(76, 45)
        Me.txtFCFFeto.MaxLength = 5
        Me.txtFCFFeto.Name = "txtFCFFeto"
        Me.txtFCFFeto.Size = New System.Drawing.Size(36, 20)
        Me.txtFCFFeto.TabIndex = 59
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.Location = New System.Drawing.Point(127, 94)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(51, 13)
        Me.Label4.TabIndex = 109
        Me.Label4.Text = "Semanas"
        '
        'lblEdadGestacional
        '
        Me.lblEdadGestacional.AutoSize = True
        Me.lblEdadGestacional.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdadGestacional.Location = New System.Drawing.Point(5, 81)
        Me.lblEdadGestacional.Name = "lblEdadGestacional"
        Me.lblEdadGestacional.Size = New System.Drawing.Size(63, 26)
        Me.lblEdadGestacional.TabIndex = 107
        Me.lblEdadGestacional.Text = "Edad " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Gestacional"
        Me.lblEdadGestacional.TextAlign = System.Drawing.ContentAlignment.BottomRight
        '
        'txtEdadGestacional
        '
        Me.txtEdadGestacional.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtEdadGestacional.Location = New System.Drawing.Point(76, 85)
        Me.txtEdadGestacional.MaxLength = 5
        Me.txtEdadGestacional.Name = "txtEdadGestacional"
        Me.txtEdadGestacional.Size = New System.Drawing.Size(36, 20)
        Me.txtEdadGestacional.TabIndex = 60
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lblPresionA)
        Me.GroupBox2.Controls.Add(Me.lblrpm)
        Me.GroupBox2.Controls.Add(Me.lblM)
        Me.GroupBox2.Controls.Add(Me.txtIndChoque)
        Me.GroupBox2.Controls.Add(Me.lblPulso)
        Me.GroupBox2.Controls.Add(Me.lblIndChoque)
        Me.GroupBox2.Controls.Add(Me.lblFrecCard)
        Me.GroupBox2.Controls.Add(Me.lblMmHg)
        Me.GroupBox2.Controls.Add(Me.txtPaSistolica)
        Me.GroupBox2.Controls.Add(Me.txtPaDiastolica)
        Me.GroupBox2.Controls.Add(Me.Label20)
        Me.GroupBox2.Controls.Add(Me.txtFrecCard)
        Me.GroupBox2.Controls.Add(Me.lblRespiracion)
        Me.GroupBox2.Controls.Add(Me.txtIMC)
        Me.GroupBox2.Controls.Add(Me.lblKg)
        Me.GroupBox2.Controls.Add(Me.lblIMC)
        Me.GroupBox2.Controls.Add(Me.txtRespiracion)
        Me.GroupBox2.Controls.Add(Me.txtPeso)
        Me.GroupBox2.Controls.Add(Me.lblPeso)
        Me.GroupBox2.Controls.Add(Me.lblTemp)
        Me.GroupBox2.Controls.Add(Me.txtGlucosa)
        Me.GroupBox2.Controls.Add(Me.lblGradosC)
        Me.GroupBox2.Controls.Add(Me.txtAltura)
        Me.GroupBox2.Controls.Add(Me.txtTemperatura)
        Me.GroupBox2.Controls.Add(Me.lblGlucosa)
        Me.GroupBox2.Controls.Add(Me.lblAltura)
        Me.GroupBox2.Location = New System.Drawing.Point(6, 13)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(491, 153)
        Me.GroupBox2.TabIndex = 160
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Madre"
        '
        'lblPresionA
        '
        Me.lblPresionA.AutoSize = True
        Me.lblPresionA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPresionA.Location = New System.Drawing.Point(21, 33)
        Me.lblPresionA.Name = "lblPresionA"
        Me.lblPresionA.Size = New System.Drawing.Size(45, 26)
        Me.lblPresionA.TabIndex = 150
        Me.lblPresionA.Text = "Tension" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "arterial"
        Me.lblPresionA.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblrpm
        '
        Me.lblrpm.AutoSize = True
        Me.lblrpm.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblrpm.Location = New System.Drawing.Point(281, 126)
        Me.lblrpm.Name = "lblrpm"
        Me.lblrpm.Size = New System.Drawing.Size(24, 13)
        Me.lblrpm.TabIndex = 157
        Me.lblrpm.Text = "rpm"
        '
        'lblM
        '
        Me.lblM.AutoSize = True
        Me.lblM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblM.Location = New System.Drawing.Point(446, 87)
        Me.lblM.Name = "lblM"
        Me.lblM.Size = New System.Drawing.Size(15, 13)
        Me.lblM.TabIndex = 152
        Me.lblM.Text = "m"
        '
        'txtIndChoque
        '
        Me.txtIndChoque.BackColor = System.Drawing.Color.White
        Me.txtIndChoque.Location = New System.Drawing.Point(73, 121)
        Me.txtIndChoque.MaxLength = 5
        Me.txtIndChoque.Name = "txtIndChoque"
        Me.txtIndChoque.ReadOnly = True
        Me.txtIndChoque.Size = New System.Drawing.Size(60, 20)
        Me.txtIndChoque.TabIndex = 1590
        '
        'lblPulso
        '
        Me.lblPulso.AutoSize = True
        Me.lblPulso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPulso.Location = New System.Drawing.Point(6, 73)
        Me.lblPulso.Name = "lblPulso"
        Me.lblPulso.Size = New System.Drawing.Size(60, 26)
        Me.lblPulso.TabIndex = 151
        Me.lblPulso.Text = "Frecuencia" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Cardiaca"
        Me.lblPulso.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblIndChoque
        '
        Me.lblIndChoque.AutoSize = True
        Me.lblIndChoque.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIndChoque.Location = New System.Drawing.Point(8, 114)
        Me.lblIndChoque.Name = "lblIndChoque"
        Me.lblIndChoque.Size = New System.Drawing.Size(58, 26)
        Me.lblIndChoque.TabIndex = 158
        Me.lblIndChoque.Text = "Indice " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "de choque"
        Me.lblIndChoque.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'lblFrecCard
        '
        Me.lblFrecCard.AutoSize = True
        Me.lblFrecCard.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFrecCard.Location = New System.Drawing.Point(120, 80)
        Me.lblFrecCard.Name = "lblFrecCard"
        Me.lblFrecCard.Size = New System.Drawing.Size(23, 13)
        Me.lblFrecCard.TabIndex = 0
        Me.lblFrecCard.Text = "lpm"
        '
        'lblMmHg
        '
        Me.lblMmHg.AutoSize = True
        Me.lblMmHg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMmHg.Location = New System.Drawing.Point(149, 42)
        Me.lblMmHg.Name = "lblMmHg"
        Me.lblMmHg.Size = New System.Drawing.Size(40, 13)
        Me.lblMmHg.TabIndex = 0
        Me.lblMmHg.Text = "mm/hg"
        '
        'txtPaSistolica
        '
        Me.txtPaSistolica.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPaSistolica.Location = New System.Drawing.Point(73, 39)
        Me.txtPaSistolica.MaxLength = 3
        Me.txtPaSistolica.Name = "txtPaSistolica"
        Me.txtPaSistolica.Size = New System.Drawing.Size(34, 20)
        Me.txtPaSistolica.TabIndex = 51
        '
        'txtPaDiastolica
        '
        Me.txtPaDiastolica.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPaDiastolica.Location = New System.Drawing.Point(111, 39)
        Me.txtPaDiastolica.MaxLength = 3
        Me.txtPaDiastolica.Name = "txtPaDiastolica"
        Me.txtPaDiastolica.Size = New System.Drawing.Size(34, 20)
        Me.txtPaDiastolica.TabIndex = 52
        '
        'Label20
        '
        Me.Label20.AutoSize = True
        Me.Label20.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label20.Location = New System.Drawing.Point(287, 84)
        Me.Label20.Name = "Label20"
        Me.Label20.Size = New System.Drawing.Size(34, 13)
        Me.Label20.TabIndex = 156
        Me.Label20.Text = "mg/dl"
        '
        'txtFrecCard
        '
        Me.txtFrecCard.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtFrecCard.Location = New System.Drawing.Point(73, 80)
        Me.txtFrecCard.MaxLength = 3
        Me.txtFrecCard.Name = "txtFrecCard"
        Me.txtFrecCard.Size = New System.Drawing.Size(45, 20)
        Me.txtFrecCard.TabIndex = 53
        '
        'lblRespiracion
        '
        Me.lblRespiracion.AutoSize = True
        Me.lblRespiracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespiracion.Location = New System.Drawing.Point(166, 117)
        Me.lblRespiracion.Name = "lblRespiracion"
        Me.lblRespiracion.Size = New System.Drawing.Size(63, 26)
        Me.lblRespiracion.TabIndex = 154
        Me.lblRespiracion.Text = "Frecunecia " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "respiratoria"
        Me.lblRespiracion.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtIMC
        '
        Me.txtIMC.BackColor = System.Drawing.Color.White
        Me.txtIMC.Location = New System.Drawing.Point(393, 125)
        Me.txtIMC.MaxLength = 5
        Me.txtIMC.Name = "txtIMC"
        Me.txtIMC.ReadOnly = True
        Me.txtIMC.Size = New System.Drawing.Size(57, 20)
        Me.txtIMC.TabIndex = 123
        '
        'lblKg
        '
        Me.lblKg.AutoSize = True
        Me.lblKg.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblKg.Location = New System.Drawing.Point(446, 45)
        Me.lblKg.Name = "lblKg"
        Me.lblKg.Size = New System.Drawing.Size(19, 13)
        Me.lblKg.TabIndex = 155
        Me.lblKg.Text = "kg"
        '
        'lblIMC
        '
        Me.lblIMC.AutoSize = True
        Me.lblIMC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblIMC.Location = New System.Drawing.Point(350, 127)
        Me.lblIMC.Name = "lblIMC"
        Me.lblIMC.Size = New System.Drawing.Size(26, 13)
        Me.lblIMC.TabIndex = 122
        Me.lblIMC.Text = "IMC"
        Me.lblIMC.TextAlign = System.Drawing.ContentAlignment.MiddleRight
        '
        'txtRespiracion
        '
        Me.txtRespiracion.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtRespiracion.Location = New System.Drawing.Point(232, 126)
        Me.txtRespiracion.MaxLength = 5
        Me.txtRespiracion.Name = "txtRespiracion"
        Me.txtRespiracion.Size = New System.Drawing.Size(49, 20)
        Me.txtRespiracion.TabIndex = 56
        '
        'txtPeso
        '
        Me.txtPeso.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtPeso.Location = New System.Drawing.Point(393, 42)
        Me.txtPeso.MaxLength = 5
        Me.txtPeso.Name = "txtPeso"
        Me.txtPeso.Size = New System.Drawing.Size(47, 20)
        Me.txtPeso.TabIndex = 57
        '
        'lblPeso
        '
        Me.lblPeso.AutoSize = True
        Me.lblPeso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPeso.Location = New System.Drawing.Point(350, 46)
        Me.lblPeso.Name = "lblPeso"
        Me.lblPeso.Size = New System.Drawing.Size(31, 13)
        Me.lblPeso.TabIndex = 153
        Me.lblPeso.Text = "Peso"
        '
        'lblTemp
        '
        Me.lblTemp.AutoSize = True
        Me.lblTemp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTemp.Location = New System.Drawing.Point(198, 43)
        Me.lblTemp.Name = "lblTemp"
        Me.lblTemp.Size = New System.Drawing.Size(37, 13)
        Me.lblTemp.TabIndex = 140
        Me.lblTemp.Text = "Temp."
        '
        'txtGlucosa
        '
        Me.txtGlucosa.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtGlucosa.Location = New System.Drawing.Point(232, 81)
        Me.txtGlucosa.MaxLength = 5
        Me.txtGlucosa.Name = "txtGlucosa"
        Me.txtGlucosa.Size = New System.Drawing.Size(49, 20)
        Me.txtGlucosa.TabIndex = 55
        '
        'lblGradosC
        '
        Me.lblGradosC.AutoSize = True
        Me.lblGradosC.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGradosC.Location = New System.Drawing.Point(285, 43)
        Me.lblGradosC.Name = "lblGradosC"
        Me.lblGradosC.Size = New System.Drawing.Size(18, 13)
        Me.lblGradosC.TabIndex = 0
        Me.lblGradosC.Text = "°C"
        '
        'txtAltura
        '
        Me.txtAltura.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtAltura.Location = New System.Drawing.Point(393, 84)
        Me.txtAltura.MaxLength = 5
        Me.txtAltura.Name = "txtAltura"
        Me.txtAltura.Size = New System.Drawing.Size(47, 20)
        Me.txtAltura.TabIndex = 58
        '
        'txtTemperatura
        '
        Me.txtTemperatura.BackColor = System.Drawing.Color.FromArgb(CType(CType(255, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.txtTemperatura.Location = New System.Drawing.Point(235, 40)
        Me.txtTemperatura.MaxLength = 5
        Me.txtTemperatura.Name = "txtTemperatura"
        Me.txtTemperatura.Size = New System.Drawing.Size(49, 20)
        Me.txtTemperatura.TabIndex = 54
        '
        'lblGlucosa
        '
        Me.lblGlucosa.AutoSize = True
        Me.lblGlucosa.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGlucosa.Location = New System.Drawing.Point(186, 84)
        Me.lblGlucosa.Name = "lblGlucosa"
        Me.lblGlucosa.Size = New System.Drawing.Size(46, 13)
        Me.lblGlucosa.TabIndex = 142
        Me.lblGlucosa.Text = "Glucosa"
        '
        'lblAltura
        '
        Me.lblAltura.AutoSize = True
        Me.lblAltura.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAltura.Location = New System.Drawing.Point(347, 87)
        Me.lblAltura.Name = "lblAltura"
        Me.lblAltura.Size = New System.Drawing.Size(34, 13)
        Me.lblAltura.TabIndex = 150
        Me.lblAltura.Text = "Altura"
        '
        'gbxSintomas
        '
        Me.gbxSintomas.Controls.Add(Me.pnlPalidez)
        Me.gbxSintomas.Controls.Add(Me.pnlEstConciencia)
        Me.gbxSintomas.Controls.Add(Me.pnlDisnea)
        Me.gbxSintomas.Controls.Add(Me.pnlNausea)
        Me.gbxSintomas.Controls.Add(Me.pnlHemorragia)
        Me.gbxSintomas.Controls.Add(Me.pnlDolorEpigastrico)
        Me.gbxSintomas.Controls.Add(Me.pnlDolorObstetrico)
        Me.gbxSintomas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxSintomas.ImeMode = System.Windows.Forms.ImeMode.NoControl
        Me.gbxSintomas.Location = New System.Drawing.Point(3, 235)
        Me.gbxSintomas.Name = "gbxSintomas"
        Me.gbxSintomas.Size = New System.Drawing.Size(709, 184)
        Me.gbxSintomas.TabIndex = 0
        Me.gbxSintomas.TabStop = False
        Me.gbxSintomas.Text = "Observacion"
        '
        'pnlPalidez
        '
        Me.pnlPalidez.Controls.Add(Me.lblPalidez)
        Me.pnlPalidez.Controls.Add(Me.rbtPalidezA)
        Me.pnlPalidez.Controls.Add(Me.rbtPalidezM)
        Me.pnlPalidez.Controls.Add(Me.rbtPalidezB)
        Me.pnlPalidez.Location = New System.Drawing.Point(6, 154)
        Me.pnlPalidez.Name = "pnlPalidez"
        Me.pnlPalidez.Size = New System.Drawing.Size(697, 20)
        Me.pnlPalidez.TabIndex = 88
        '
        'lblPalidez
        '
        Me.lblPalidez.AutoSize = True
        Me.lblPalidez.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPalidez.Location = New System.Drawing.Point(78, 3)
        Me.lblPalidez.Name = "lblPalidez"
        Me.lblPalidez.Size = New System.Drawing.Size(65, 13)
        Me.lblPalidez.TabIndex = 0
        Me.lblPalidez.Text = "Color de piel"
        '
        'rbtPalidezA
        '
        Me.rbtPalidezA.AutoSize = True
        Me.rbtPalidezA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPalidezA.Location = New System.Drawing.Point(519, 1)
        Me.rbtPalidezA.Name = "rbtPalidezA"
        Me.rbtPalidezA.Size = New System.Drawing.Size(69, 17)
        Me.rbtPalidezA.TabIndex = 90
        Me.rbtPalidezA.TabStop = True
        Me.rbtPalidezA.Text = "Cianotica"
        Me.rbtPalidezA.UseVisualStyleBackColor = True
        '
        'rbtPalidezM
        '
        Me.rbtPalidezM.AutoSize = True
        Me.rbtPalidezM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPalidezM.Location = New System.Drawing.Point(359, 1)
        Me.rbtPalidezM.Name = "rbtPalidezM"
        Me.rbtPalidezM.Size = New System.Drawing.Size(54, 17)
        Me.rbtPalidezM.TabIndex = 89
        Me.rbtPalidezM.TabStop = True
        Me.rbtPalidezM.Text = "Palida"
        Me.rbtPalidezM.UseVisualStyleBackColor = True
        '
        'rbtPalidezB
        '
        Me.rbtPalidezB.AutoSize = True
        Me.rbtPalidezB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPalidezB.Location = New System.Drawing.Point(187, 1)
        Me.rbtPalidezB.Name = "rbtPalidezB"
        Me.rbtPalidezB.Size = New System.Drawing.Size(58, 17)
        Me.rbtPalidezB.TabIndex = 88
        Me.rbtPalidezB.TabStop = True
        Me.rbtPalidezB.Text = "Normal"
        Me.rbtPalidezB.UseVisualStyleBackColor = True
        '
        'pnlEstConciencia
        '
        Me.pnlEstConciencia.BackColor = System.Drawing.SystemColors.Window
        Me.pnlEstConciencia.Controls.Add(Me.lblEstadoConciencia)
        Me.pnlEstConciencia.Controls.Add(Me.rbtEstadoConcienciaA)
        Me.pnlEstConciencia.Controls.Add(Me.rbtEstadoConcienciaM)
        Me.pnlEstConciencia.Controls.Add(Me.rbtEstadoConcienciaB)
        Me.pnlEstConciencia.Location = New System.Drawing.Point(6, 16)
        Me.pnlEstConciencia.Name = "pnlEstConciencia"
        Me.pnlEstConciencia.Size = New System.Drawing.Size(697, 20)
        Me.pnlEstConciencia.TabIndex = 80
        '
        'lblEstadoConciencia
        '
        Me.lblEstadoConciencia.AutoSize = True
        Me.lblEstadoConciencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEstadoConciencia.Location = New System.Drawing.Point(36, 3)
        Me.lblEstadoConciencia.Name = "lblEstadoConciencia"
        Me.lblEstadoConciencia.Size = New System.Drawing.Size(110, 13)
        Me.lblEstadoConciencia.TabIndex = 0
        Me.lblEstadoConciencia.Text = "Estado de conciencia"
        '
        'rbtEstadoConcienciaA
        '
        Me.rbtEstadoConcienciaA.AutoSize = True
        Me.rbtEstadoConcienciaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEstadoConcienciaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtEstadoConcienciaA.Name = "rbtEstadoConcienciaA"
        Me.rbtEstadoConcienciaA.Size = New System.Drawing.Size(143, 17)
        Me.rbtEstadoConcienciaA.TabIndex = 82
        Me.rbtEstadoConcienciaA.TabStop = True
        Me.rbtEstadoConcienciaA.Text = "Inconsiente/Somnolienta"
        Me.rbtEstadoConcienciaA.UseVisualStyleBackColor = True
        '
        'rbtEstadoConcienciaM
        '
        Me.rbtEstadoConcienciaM.AutoSize = True
        Me.rbtEstadoConcienciaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEstadoConcienciaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtEstadoConcienciaM.Name = "rbtEstadoConcienciaM"
        Me.rbtEstadoConcienciaM.Size = New System.Drawing.Size(72, 17)
        Me.rbtEstadoConcienciaM.TabIndex = 81
        Me.rbtEstadoConcienciaM.TabStop = True
        Me.rbtEstadoConcienciaM.Text = "Estresada"
        Me.rbtEstadoConcienciaM.UseVisualStyleBackColor = True
        '
        'rbtEstadoConcienciaB
        '
        Me.rbtEstadoConcienciaB.AutoSize = True
        Me.rbtEstadoConcienciaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEstadoConcienciaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtEstadoConcienciaB.Name = "rbtEstadoConcienciaB"
        Me.rbtEstadoConcienciaB.Size = New System.Drawing.Size(58, 17)
        Me.rbtEstadoConcienciaB.TabIndex = 80
        Me.rbtEstadoConcienciaB.Text = "Normal"
        Me.rbtEstadoConcienciaB.UseVisualStyleBackColor = True
        '
        'pnlDisnea
        '
        Me.pnlDisnea.Controls.Add(Me.lblRespiracon1)
        Me.pnlDisnea.Controls.Add(Me.rbtRespiracionA)
        Me.pnlDisnea.Controls.Add(Me.rbtRespiracionM)
        Me.pnlDisnea.Controls.Add(Me.rbtRespiracionB)
        Me.pnlDisnea.Location = New System.Drawing.Point(6, 130)
        Me.pnlDisnea.Name = "pnlDisnea"
        Me.pnlDisnea.Size = New System.Drawing.Size(697, 20)
        Me.pnlDisnea.TabIndex = 35
        '
        'lblRespiracon1
        '
        Me.lblRespiracon1.AutoSize = True
        Me.lblRespiracon1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRespiracon1.Location = New System.Drawing.Point(83, 3)
        Me.lblRespiracon1.Name = "lblRespiracon1"
        Me.lblRespiracon1.Size = New System.Drawing.Size(63, 13)
        Me.lblRespiracon1.TabIndex = 0
        Me.lblRespiracon1.Text = "Respiracion"
        '
        'rbtRespiracionA
        '
        Me.rbtRespiracionA.AutoSize = True
        Me.rbtRespiracionA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtRespiracionA.Location = New System.Drawing.Point(519, 1)
        Me.rbtRespiracionA.Name = "rbtRespiracionA"
        Me.rbtRespiracionA.Size = New System.Drawing.Size(64, 17)
        Me.rbtRespiracionA.TabIndex = 38
        Me.rbtRespiracionA.TabStop = True
        Me.rbtRespiracionA.Text = "Alterada"
        Me.rbtRespiracionA.UseVisualStyleBackColor = True
        '
        'rbtRespiracionM
        '
        Me.rbtRespiracionM.AutoSize = True
        Me.rbtRespiracionM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtRespiracionM.Location = New System.Drawing.Point(359, 1)
        Me.rbtRespiracionM.Name = "rbtRespiracionM"
        Me.rbtRespiracionM.Size = New System.Drawing.Size(73, 17)
        Me.rbtRespiracionM.TabIndex = 37
        Me.rbtRespiracionM.TabStop = True
        Me.rbtRespiracionM.Text = "Moderada"
        Me.rbtRespiracionM.UseVisualStyleBackColor = True
        '
        'rbtRespiracionB
        '
        Me.rbtRespiracionB.AutoSize = True
        Me.rbtRespiracionB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtRespiracionB.Location = New System.Drawing.Point(187, 1)
        Me.rbtRespiracionB.Name = "rbtRespiracionB"
        Me.rbtRespiracionB.Size = New System.Drawing.Size(58, 17)
        Me.rbtRespiracionB.TabIndex = 36
        Me.rbtRespiracionB.TabStop = True
        Me.rbtRespiracionB.Text = "Normal"
        Me.rbtRespiracionB.UseVisualStyleBackColor = True
        '
        'pnlNausea
        '
        Me.pnlNausea.Controls.Add(Me.lblNausea)
        Me.pnlNausea.Controls.Add(Me.rbtNauseaA)
        Me.pnlNausea.Controls.Add(Me.rbtNauseaM)
        Me.pnlNausea.Controls.Add(Me.rbtNauseaB)
        Me.pnlNausea.Location = New System.Drawing.Point(6, 107)
        Me.pnlNausea.Name = "pnlNausea"
        Me.pnlNausea.Size = New System.Drawing.Size(697, 20)
        Me.pnlNausea.TabIndex = 31
        '
        'lblNausea
        '
        Me.lblNausea.AutoSize = True
        Me.lblNausea.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNausea.Location = New System.Drawing.Point(102, 3)
        Me.lblNausea.Name = "lblNausea"
        Me.lblNausea.Size = New System.Drawing.Size(44, 13)
        Me.lblNausea.TabIndex = 0
        Me.lblNausea.Text = "Nausea"
        '
        'rbtNauseaA
        '
        Me.rbtNauseaA.AutoSize = True
        Me.rbtNauseaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNauseaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtNauseaA.Name = "rbtNauseaA"
        Me.rbtNauseaA.Size = New System.Drawing.Size(141, 17)
        Me.rbtNauseaA.TabIndex = 34
        Me.rbtNauseaA.TabStop = True
        Me.rbtNauseaA.Text = "Nausea y vomito intenso"
        Me.rbtNauseaA.UseVisualStyleBackColor = True
        '
        'rbtNauseaM
        '
        Me.rbtNauseaM.AutoSize = True
        Me.rbtNauseaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNauseaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtNauseaM.Name = "rbtNauseaM"
        Me.rbtNauseaM.Size = New System.Drawing.Size(67, 17)
        Me.rbtNauseaM.TabIndex = 33
        Me.rbtNauseaM.TabStop = True
        Me.rbtNauseaM.Text = "Presente"
        Me.rbtNauseaM.UseVisualStyleBackColor = True
        '
        'rbtNauseaB
        '
        Me.rbtNauseaB.AutoSize = True
        Me.rbtNauseaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNauseaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtNauseaB.Name = "rbtNauseaB"
        Me.rbtNauseaB.Size = New System.Drawing.Size(49, 17)
        Me.rbtNauseaB.TabIndex = 32
        Me.rbtNauseaB.TabStop = True
        Me.rbtNauseaB.Text = "Leve"
        Me.rbtNauseaB.UseVisualStyleBackColor = True
        '
        'pnlHemorragia
        '
        Me.pnlHemorragia.Controls.Add(Me.lblHemorragia)
        Me.pnlHemorragia.Controls.Add(Me.rbtHemorragiaA)
        Me.pnlHemorragia.Controls.Add(Me.rbtHemorragiaM)
        Me.pnlHemorragia.Controls.Add(Me.rbtHemorragiaB)
        Me.pnlHemorragia.Location = New System.Drawing.Point(6, 38)
        Me.pnlHemorragia.Name = "pnlHemorragia"
        Me.pnlHemorragia.Size = New System.Drawing.Size(697, 20)
        Me.pnlHemorragia.TabIndex = 23
        '
        'lblHemorragia
        '
        Me.lblHemorragia.AutoSize = True
        Me.lblHemorragia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHemorragia.Location = New System.Drawing.Point(85, 3)
        Me.lblHemorragia.Name = "lblHemorragia"
        Me.lblHemorragia.Size = New System.Drawing.Size(61, 13)
        Me.lblHemorragia.TabIndex = 0
        Me.lblHemorragia.Text = "Hemorragia"
        Me.lblHemorragia.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        'rbtHemorragiaA
        '
        Me.rbtHemorragiaA.AutoSize = True
        Me.rbtHemorragiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHemorragiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtHemorragiaA.Name = "rbtHemorragiaA"
        Me.rbtHemorragiaA.Size = New System.Drawing.Size(77, 17)
        Me.rbtHemorragiaA.TabIndex = 26
        Me.rbtHemorragiaA.TabStop = True
        Me.rbtHemorragiaA.Text = "Abundante"
        Me.rbtHemorragiaA.UseVisualStyleBackColor = True
        '
        'rbtHemorragiaM
        '
        Me.rbtHemorragiaM.AutoSize = True
        Me.rbtHemorragiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHemorragiaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtHemorragiaM.Name = "rbtHemorragiaM"
        Me.rbtHemorragiaM.Size = New System.Drawing.Size(73, 17)
        Me.rbtHemorragiaM.TabIndex = 25
        Me.rbtHemorragiaM.TabStop = True
        Me.rbtHemorragiaM.Text = "Moderada"
        Me.rbtHemorragiaM.UseVisualStyleBackColor = True
        '
        'rbtHemorragiaB
        '
        Me.rbtHemorragiaB.AutoSize = True
        Me.rbtHemorragiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHemorragiaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtHemorragiaB.Name = "rbtHemorragiaB"
        Me.rbtHemorragiaB.Size = New System.Drawing.Size(60, 17)
        Me.rbtHemorragiaB.TabIndex = 24
        Me.rbtHemorragiaB.TabStop = True
        Me.rbtHemorragiaB.Text = "Escasa"
        Me.rbtHemorragiaB.UseVisualStyleBackColor = True
        '
        'pnlDolorEpigastrico
        '
        Me.pnlDolorEpigastrico.Controls.Add(Me.lblCrisisConvulsiva)
        Me.pnlDolorEpigastrico.Controls.Add(Me.rbtCrisisConvulsivaA)
        Me.pnlDolorEpigastrico.Controls.Add(Me.rbtCrisisConvulsivaB)
        Me.pnlDolorEpigastrico.Location = New System.Drawing.Point(6, 83)
        Me.pnlDolorEpigastrico.Name = "pnlDolorEpigastrico"
        Me.pnlDolorEpigastrico.Size = New System.Drawing.Size(697, 20)
        Me.pnlDolorEpigastrico.TabIndex = 19
        '
        'lblCrisisConvulsiva
        '
        Me.lblCrisisConvulsiva.AutoSize = True
        Me.lblCrisisConvulsiva.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCrisisConvulsiva.Location = New System.Drawing.Point(57, 4)
        Me.lblCrisisConvulsiva.Name = "lblCrisisConvulsiva"
        Me.lblCrisisConvulsiva.Size = New System.Drawing.Size(91, 13)
        Me.lblCrisisConvulsiva.TabIndex = 0
        Me.lblCrisisConvulsiva.Text = "Crisis Convulsivas"
        '
        'rbtCrisisConvulsivaA
        '
        Me.rbtCrisisConvulsivaA.AutoSize = True
        Me.rbtCrisisConvulsivaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCrisisConvulsivaA.Location = New System.Drawing.Point(359, 2)
        Me.rbtCrisisConvulsivaA.Name = "rbtCrisisConvulsivaA"
        Me.rbtCrisisConvulsivaA.Size = New System.Drawing.Size(67, 17)
        Me.rbtCrisisConvulsivaA.TabIndex = 22
        Me.rbtCrisisConvulsivaA.TabStop = True
        Me.rbtCrisisConvulsivaA.Text = "Presente"
        Me.rbtCrisisConvulsivaA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rbtCrisisConvulsivaA.UseVisualStyleBackColor = True
        '
        'rbtCrisisConvulsivaB
        '
        Me.rbtCrisisConvulsivaB.AutoSize = True
        Me.rbtCrisisConvulsivaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCrisisConvulsivaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtCrisisConvulsivaB.Name = "rbtCrisisConvulsivaB"
        Me.rbtCrisisConvulsivaB.Size = New System.Drawing.Size(63, 17)
        Me.rbtCrisisConvulsivaB.TabIndex = 20
        Me.rbtCrisisConvulsivaB.TabStop = True
        Me.rbtCrisisConvulsivaB.Text = "ausente"
        Me.rbtCrisisConvulsivaB.UseVisualStyleBackColor = True
        '
        'pnlDolorObstetrico
        '
        Me.pnlDolorObstetrico.Controls.Add(Me.lblDolorObstetrico)
        Me.pnlDolorObstetrico.Controls.Add(Me.rbtDolorObstetricoA)
        Me.pnlDolorObstetrico.Controls.Add(Me.rbtDolorObstetricoM)
        Me.pnlDolorObstetrico.Controls.Add(Me.rbtDolorObstetricoB)
        Me.pnlDolorObstetrico.Location = New System.Drawing.Point(6, 60)
        Me.pnlDolorObstetrico.Name = "pnlDolorObstetrico"
        Me.pnlDolorObstetrico.Size = New System.Drawing.Size(697, 20)
        Me.pnlDolorObstetrico.TabIndex = 15
        '
        'lblDolorObstetrico
        '
        Me.lblDolorObstetrico.AutoSize = True
        Me.lblDolorObstetrico.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDolorObstetrico.Location = New System.Drawing.Point(65, 3)
        Me.lblDolorObstetrico.Name = "lblDolorObstetrico"
        Me.lblDolorObstetrico.Size = New System.Drawing.Size(81, 13)
        Me.lblDolorObstetrico.TabIndex = 0
        Me.lblDolorObstetrico.Text = "Dolor obstetrico"
        '
        'rbtDolorObstetricoA
        '
        Me.rbtDolorObstetricoA.AccessibleName = ""
        Me.rbtDolorObstetricoA.AutoSize = True
        Me.rbtDolorObstetricoA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtDolorObstetricoA.Location = New System.Drawing.Point(519, 1)
        Me.rbtDolorObstetricoA.Name = "rbtDolorObstetricoA"
        Me.rbtDolorObstetricoA.Size = New System.Drawing.Size(65, 17)
        Me.rbtDolorObstetricoA.TabIndex = 18
        Me.rbtDolorObstetricoA.TabStop = True
        Me.rbtDolorObstetricoA.Text = "Instenso"
        Me.rbtDolorObstetricoA.UseVisualStyleBackColor = True
        '
        'rbtDolorObstetricoM
        '
        Me.rbtDolorObstetricoM.AccessibleName = ""
        Me.rbtDolorObstetricoM.AutoSize = True
        Me.rbtDolorObstetricoM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtDolorObstetricoM.Location = New System.Drawing.Point(359, 1)
        Me.rbtDolorObstetricoM.Name = "rbtDolorObstetricoM"
        Me.rbtDolorObstetricoM.Size = New System.Drawing.Size(73, 17)
        Me.rbtDolorObstetricoM.TabIndex = 17
        Me.rbtDolorObstetricoM.TabStop = True
        Me.rbtDolorObstetricoM.Text = "Moderado"
        Me.rbtDolorObstetricoM.UseVisualStyleBackColor = True
        '
        'rbtDolorObstetricoB
        '
        Me.rbtDolorObstetricoB.AccessibleName = ""
        Me.rbtDolorObstetricoB.AutoSize = True
        Me.rbtDolorObstetricoB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtDolorObstetricoB.Location = New System.Drawing.Point(187, 1)
        Me.rbtDolorObstetricoB.Name = "rbtDolorObstetricoB"
        Me.rbtDolorObstetricoB.Size = New System.Drawing.Size(64, 17)
        Me.rbtDolorObstetricoB.TabIndex = 16
        Me.rbtDolorObstetricoB.TabStop = True
        Me.rbtDolorObstetricoB.Text = "Ausente"
        Me.rbtDolorObstetricoB.UseVisualStyleBackColor = True
        '
        'tabPagInterrogatorio
        '
        Me.tabPagInterrogatorio.BackColor = System.Drawing.SystemColors.Window
        Me.tabPagInterrogatorio.Controls.Add(Me.lblObservaciones)
        Me.tabPagInterrogatorio.Controls.Add(Me.Textvaloracion)
        Me.tabPagInterrogatorio.Controls.Add(Me.pnlSangradoTrans)
        Me.tabPagInterrogatorio.Controls.Add(Me.gbxInfoEmbarazo)
        Me.tabPagInterrogatorio.Location = New System.Drawing.Point(4, 22)
        Me.tabPagInterrogatorio.Name = "tabPagInterrogatorio"
        Me.tabPagInterrogatorio.Padding = New System.Windows.Forms.Padding(3)
        Me.tabPagInterrogatorio.Size = New System.Drawing.Size(721, 436)
        Me.tabPagInterrogatorio.TabIndex = 1
        Me.tabPagInterrogatorio.Text = "Interrogatorio"
        '
        'lblObservaciones
        '
        Me.lblObservaciones.AutoSize = True
        Me.lblObservaciones.Font = New System.Drawing.Font("Comic Sans MS", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblObservaciones.Location = New System.Drawing.Point(11, 337)
        Me.lblObservaciones.Name = "lblObservaciones"
        Me.lblObservaciones.Size = New System.Drawing.Size(97, 19)
        Me.lblObservaciones.TabIndex = 123
        Me.lblObservaciones.Text = "Observaciones"
        '
        'Textvaloracion
        '
        Me.Textvaloracion.BackColor = System.Drawing.Color.Moccasin
        Me.Textvaloracion.Location = New System.Drawing.Point(13, 359)
        Me.Textvaloracion.Multiline = True
        Me.Textvaloracion.Name = "Textvaloracion"
        Me.Textvaloracion.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me.Textvaloracion.Size = New System.Drawing.Size(693, 71)
        Me.Textvaloracion.TabIndex = 61
        '
        'pnlSangradoTrans
        '
        Me.pnlSangradoTrans.Controls.Add(Me.Panel10)
        Me.pnlSangradoTrans.Controls.Add(Me.Panel9)
        Me.pnlSangradoTrans.Controls.Add(Me.Panel7)
        Me.pnlSangradoTrans.Controls.Add(Me.Panel6)
        Me.pnlSangradoTrans.Controls.Add(Me.Panel5)
        Me.pnlSangradoTrans.Controls.Add(Me.Panel1)
        Me.pnlSangradoTrans.Controls.Add(Me.pnlHiper)
        Me.pnlSangradoTrans.Controls.Add(Me.pnlSudoracion)
        Me.pnlSangradoTrans.Controls.Add(Me.pnlCefalea)
        Me.pnlSangradoTrans.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.pnlSangradoTrans.Location = New System.Drawing.Point(4, 6)
        Me.pnlSangradoTrans.Name = "pnlSangradoTrans"
        Me.pnlSangradoTrans.Size = New System.Drawing.Size(709, 237)
        Me.pnlSangradoTrans.TabIndex = 121
        Me.pnlSangradoTrans.TabStop = False
        Me.pnlSangradoTrans.Text = "Interrogatorio"
        '
        'Panel10
        '
        Me.Panel10.Controls.Add(Me.lblfetal)
        Me.Panel10.Controls.Add(Me.rbtMotilidadFetalA)
        Me.Panel10.Controls.Add(Me.rbtMotilidadFetalM)
        Me.Panel10.Controls.Add(Me.rbtMotilidadFetalB)
        Me.Panel10.Location = New System.Drawing.Point(8, 207)
        Me.Panel10.Name = "Panel10"
        Me.Panel10.Size = New System.Drawing.Size(697, 20)
        Me.Panel10.TabIndex = 89
        '
        'lblfetal
        '
        Me.lblfetal.AutoSize = True
        Me.lblfetal.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfetal.Location = New System.Drawing.Point(73, 3)
        Me.lblfetal.Name = "lblfetal"
        Me.lblfetal.Size = New System.Drawing.Size(75, 13)
        Me.lblfetal.TabIndex = 0
        Me.lblfetal.Text = "Motilidad Fetal"
        '
        'rbtMotilidadFetalA
        '
        Me.rbtMotilidadFetalA.AutoSize = True
        Me.rbtMotilidadFetalA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtMotilidadFetalA.Location = New System.Drawing.Point(519, 1)
        Me.rbtMotilidadFetalA.Name = "rbtMotilidadFetalA"
        Me.rbtMotilidadFetalA.Size = New System.Drawing.Size(64, 17)
        Me.rbtMotilidadFetalA.TabIndex = 86
        Me.rbtMotilidadFetalA.TabStop = True
        Me.rbtMotilidadFetalA.Text = "Ausente"
        Me.rbtMotilidadFetalA.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rbtMotilidadFetalA.UseVisualStyleBackColor = True
        '
        'rbtMotilidadFetalM
        '
        Me.rbtMotilidadFetalM.AutoSize = True
        Me.rbtMotilidadFetalM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtMotilidadFetalM.Location = New System.Drawing.Point(359, 1)
        Me.rbtMotilidadFetalM.Name = "rbtMotilidadFetalM"
        Me.rbtMotilidadFetalM.Size = New System.Drawing.Size(76, 17)
        Me.rbtMotilidadFetalM.TabIndex = 85
        Me.rbtMotilidadFetalM.TabStop = True
        Me.rbtMotilidadFetalM.Text = "Disminuida"
        Me.rbtMotilidadFetalM.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rbtMotilidadFetalM.UseVisualStyleBackColor = True
        '
        'rbtMotilidadFetalB
        '
        Me.rbtMotilidadFetalB.AutoSize = True
        Me.rbtMotilidadFetalB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtMotilidadFetalB.Location = New System.Drawing.Point(187, 1)
        Me.rbtMotilidadFetalB.Name = "rbtMotilidadFetalB"
        Me.rbtMotilidadFetalB.Size = New System.Drawing.Size(58, 17)
        Me.rbtMotilidadFetalB.TabIndex = 84
        Me.rbtMotilidadFetalB.TabStop = True
        Me.rbtMotilidadFetalB.Text = "Normal"
        Me.rbtMotilidadFetalB.UseVisualStyleBackColor = True
        '
        'Panel9
        '
        Me.Panel9.Controls.Add(Me.Panel8)
        Me.Panel9.Controls.Add(Me.lblsindromefebril)
        Me.Panel9.Controls.Add(Me.rbtSindromeFebrilA)
        Me.Panel9.Controls.Add(Me.rbtSindromeFebrilB)
        Me.Panel9.Location = New System.Drawing.Point(7, 158)
        Me.Panel9.Name = "Panel9"
        Me.Panel9.Size = New System.Drawing.Size(697, 20)
        Me.Panel9.TabIndex = 87
        '
        'Panel8
        '
        Me.Panel8.Controls.Add(Me.Label6)
        Me.Panel8.Controls.Add(Me.RadioButton13)
        Me.Panel8.Controls.Add(Me.RadioButton14)
        Me.Panel8.Controls.Add(Me.RadioButton15)
        Me.Panel8.Location = New System.Drawing.Point(1, 19)
        Me.Panel8.Name = "Panel8"
        Me.Panel8.Size = New System.Drawing.Size(697, 20)
        Me.Panel8.TabIndex = 87
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label6.Location = New System.Drawing.Point(94, 3)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(55, 13)
        Me.Label6.TabIndex = 0
        Me.Label6.Text = "Amaurosis"
        '
        'RadioButton13
        '
        Me.RadioButton13.AutoSize = True
        Me.RadioButton13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton13.Location = New System.Drawing.Point(519, 1)
        Me.RadioButton13.Name = "RadioButton13"
        Me.RadioButton13.Size = New System.Drawing.Size(61, 17)
        Me.RadioButton13.TabIndex = 86
        Me.RadioButton13.TabStop = True
        Me.RadioButton13.Text = "Profusa"
        Me.RadioButton13.UseVisualStyleBackColor = True
        '
        'RadioButton14
        '
        Me.RadioButton14.AutoSize = True
        Me.RadioButton14.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton14.Location = New System.Drawing.Point(359, 1)
        Me.RadioButton14.Name = "RadioButton14"
        Me.RadioButton14.Size = New System.Drawing.Size(49, 17)
        Me.RadioButton14.TabIndex = 85
        Me.RadioButton14.TabStop = True
        Me.RadioButton14.Text = "Leve"
        Me.RadioButton14.UseVisualStyleBackColor = True
        '
        'RadioButton15
        '
        Me.RadioButton15.AutoSize = True
        Me.RadioButton15.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.RadioButton15.Location = New System.Drawing.Point(187, 1)
        Me.RadioButton15.Name = "RadioButton15"
        Me.RadioButton15.Size = New System.Drawing.Size(64, 17)
        Me.RadioButton15.TabIndex = 84
        Me.RadioButton15.TabStop = True
        Me.RadioButton15.Text = "Ausente"
        Me.RadioButton15.UseVisualStyleBackColor = True
        '
        'lblsindromefebril
        '
        Me.lblsindromefebril.AutoSize = True
        Me.lblsindromefebril.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsindromefebril.Location = New System.Drawing.Point(70, 3)
        Me.lblsindromefebril.Name = "lblsindromefebril"
        Me.lblsindromefebril.Size = New System.Drawing.Size(79, 13)
        Me.lblsindromefebril.TabIndex = 0
        Me.lblsindromefebril.Text = "Sindrome Febril"
        '
        'rbtSindromeFebrilA
        '
        Me.rbtSindromeFebrilA.AutoSize = True
        Me.rbtSindromeFebrilA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSindromeFebrilA.Location = New System.Drawing.Point(358, 1)
        Me.rbtSindromeFebrilA.Name = "rbtSindromeFebrilA"
        Me.rbtSindromeFebrilA.Size = New System.Drawing.Size(67, 17)
        Me.rbtSindromeFebrilA.TabIndex = 85
        Me.rbtSindromeFebrilA.TabStop = True
        Me.rbtSindromeFebrilA.Text = "Presente"
        Me.rbtSindromeFebrilA.UseVisualStyleBackColor = True
        '
        'rbtSindromeFebrilB
        '
        Me.rbtSindromeFebrilB.AutoSize = True
        Me.rbtSindromeFebrilB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSindromeFebrilB.Location = New System.Drawing.Point(187, 1)
        Me.rbtSindromeFebrilB.Name = "rbtSindromeFebrilB"
        Me.rbtSindromeFebrilB.Size = New System.Drawing.Size(64, 17)
        Me.rbtSindromeFebrilB.TabIndex = 84
        Me.rbtSindromeFebrilB.TabStop = True
        Me.rbtSindromeFebrilB.Text = "Ausente"
        Me.rbtSindromeFebrilB.UseVisualStyleBackColor = True
        '
        'Panel7
        '
        Me.Panel7.Controls.Add(Me.lblliquidoamn)
        Me.Panel7.Controls.Add(Me.rbtLiquidoAmnioticoA2)
        Me.Panel7.Controls.Add(Me.rbtLiquidoAmnioticoA1)
        Me.Panel7.Controls.Add(Me.rbtLiquidoAmnioticoB)
        Me.Panel7.Location = New System.Drawing.Point(7, 182)
        Me.Panel7.Name = "Panel7"
        Me.Panel7.Size = New System.Drawing.Size(697, 20)
        Me.Panel7.TabIndex = 87
        '
        'lblliquidoamn
        '
        Me.lblliquidoamn.AutoSize = True
        Me.lblliquidoamn.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblliquidoamn.Location = New System.Drawing.Point(63, 3)
        Me.lblliquidoamn.Name = "lblliquidoamn"
        Me.lblliquidoamn.Size = New System.Drawing.Size(89, 13)
        Me.lblliquidoamn.TabIndex = 0
        Me.lblliquidoamn.Text = "Liquido amniotico"
        '
        'rbtLiquidoAmnioticoA2
        '
        Me.rbtLiquidoAmnioticoA2.AutoSize = True
        Me.rbtLiquidoAmnioticoA2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtLiquidoAmnioticoA2.Location = New System.Drawing.Point(519, 1)
        Me.rbtLiquidoAmnioticoA2.Name = "rbtLiquidoAmnioticoA2"
        Me.rbtLiquidoAmnioticoA2.Size = New System.Drawing.Size(86, 17)
        Me.rbtLiquidoAmnioticoA2.TabIndex = 86
        Me.rbtLiquidoAmnioticoA2.TabStop = True
        Me.rbtLiquidoAmnioticoA2.Text = "Polihidramios"
        Me.rbtLiquidoAmnioticoA2.UseVisualStyleBackColor = True
        '
        'rbtLiquidoAmnioticoA1
        '
        Me.rbtLiquidoAmnioticoA1.AutoSize = True
        Me.rbtLiquidoAmnioticoA1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtLiquidoAmnioticoA1.Location = New System.Drawing.Point(359, 1)
        Me.rbtLiquidoAmnioticoA1.Name = "rbtLiquidoAmnioticoA1"
        Me.rbtLiquidoAmnioticoA1.Size = New System.Drawing.Size(93, 17)
        Me.rbtLiquidoAmnioticoA1.TabIndex = 85
        Me.rbtLiquidoAmnioticoA1.TabStop = True
        Me.rbtLiquidoAmnioticoA1.Text = "Oligohidramios"
        Me.rbtLiquidoAmnioticoA1.UseVisualStyleBackColor = True
        '
        'rbtLiquidoAmnioticoB
        '
        Me.rbtLiquidoAmnioticoB.AutoSize = True
        Me.rbtLiquidoAmnioticoB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtLiquidoAmnioticoB.Location = New System.Drawing.Point(187, 1)
        Me.rbtLiquidoAmnioticoB.Name = "rbtLiquidoAmnioticoB"
        Me.rbtLiquidoAmnioticoB.Size = New System.Drawing.Size(58, 17)
        Me.rbtLiquidoAmnioticoB.TabIndex = 84
        Me.rbtLiquidoAmnioticoB.TabStop = True
        Me.rbtLiquidoAmnioticoB.Text = "Normal"
        Me.rbtLiquidoAmnioticoB.UseVisualStyleBackColor = True
        '
        'Panel6
        '
        Me.Panel6.Controls.Add(Me.lblepigastralgia)
        Me.Panel6.Controls.Add(Me.rbtEpigastragiaA)
        Me.Panel6.Controls.Add(Me.rbtEpigastragiaB)
        Me.Panel6.Location = New System.Drawing.Point(6, 113)
        Me.Panel6.Name = "Panel6"
        Me.Panel6.Size = New System.Drawing.Size(697, 20)
        Me.Panel6.TabIndex = 86
        '
        'lblepigastralgia
        '
        Me.lblepigastralgia.AutoSize = True
        Me.lblepigastralgia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblepigastralgia.Location = New System.Drawing.Point(82, 3)
        Me.lblepigastralgia.Name = "lblepigastralgia"
        Me.lblepigastralgia.Size = New System.Drawing.Size(67, 13)
        Me.lblepigastralgia.TabIndex = 0
        Me.lblepigastralgia.Text = "Epigastralgia"
        '
        'rbtEpigastragiaA
        '
        Me.rbtEpigastragiaA.AutoSize = True
        Me.rbtEpigastragiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEpigastragiaA.Location = New System.Drawing.Point(359, -1)
        Me.rbtEpigastragiaA.Name = "rbtEpigastragiaA"
        Me.rbtEpigastragiaA.Size = New System.Drawing.Size(67, 17)
        Me.rbtEpigastragiaA.TabIndex = 78
        Me.rbtEpigastragiaA.TabStop = True
        Me.rbtEpigastragiaA.Text = "Presente"
        Me.rbtEpigastragiaA.UseVisualStyleBackColor = True
        '
        'rbtEpigastragiaB
        '
        Me.rbtEpigastragiaB.AutoSize = True
        Me.rbtEpigastragiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEpigastragiaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtEpigastragiaB.Name = "rbtEpigastragiaB"
        Me.rbtEpigastragiaB.Size = New System.Drawing.Size(64, 17)
        Me.rbtEpigastragiaB.TabIndex = 76
        Me.rbtEpigastragiaB.TabStop = True
        Me.rbtEpigastragiaB.Text = "Ausente"
        Me.rbtEpigastragiaB.UseVisualStyleBackColor = True
        '
        'Panel5
        '
        Me.Panel5.Controls.Add(Me.lblfosfenos)
        Me.Panel5.Controls.Add(Me.rbtFosfenosA)
        Me.Panel5.Controls.Add(Me.rbtFosfenosB)
        Me.Panel5.Location = New System.Drawing.Point(6, 89)
        Me.Panel5.Name = "Panel5"
        Me.Panel5.Size = New System.Drawing.Size(697, 20)
        Me.Panel5.TabIndex = 85
        '
        'lblfosfenos
        '
        Me.lblfosfenos.AutoSize = True
        Me.lblfosfenos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblfosfenos.Location = New System.Drawing.Point(99, 3)
        Me.lblfosfenos.Name = "lblfosfenos"
        Me.lblfosfenos.Size = New System.Drawing.Size(50, 13)
        Me.lblfosfenos.TabIndex = 0
        Me.lblfosfenos.Text = "Fosfenos"
        '
        'rbtFosfenosA
        '
        Me.rbtFosfenosA.AutoSize = True
        Me.rbtFosfenosA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtFosfenosA.Location = New System.Drawing.Point(359, 2)
        Me.rbtFosfenosA.Name = "rbtFosfenosA"
        Me.rbtFosfenosA.Size = New System.Drawing.Size(67, 17)
        Me.rbtFosfenosA.TabIndex = 78
        Me.rbtFosfenosA.TabStop = True
        Me.rbtFosfenosA.Text = "Presente"
        Me.rbtFosfenosA.UseVisualStyleBackColor = True
        '
        'rbtFosfenosB
        '
        Me.rbtFosfenosB.AutoSize = True
        Me.rbtFosfenosB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtFosfenosB.Location = New System.Drawing.Point(187, 1)
        Me.rbtFosfenosB.Name = "rbtFosfenosB"
        Me.rbtFosfenosB.Size = New System.Drawing.Size(64, 17)
        Me.rbtFosfenosB.TabIndex = 76
        Me.rbtFosfenosB.TabStop = True
        Me.rbtFosfenosB.Text = "Ausente"
        Me.rbtFosfenosB.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.lblsangradotrans)
        Me.Panel1.Controls.Add(Me.rbtSangradotransA)
        Me.Panel1.Controls.Add(Me.rbtSangradotransM)
        Me.Panel1.Controls.Add(Me.rbtSangradotransB)
        Me.Panel1.Location = New System.Drawing.Point(6, 20)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(697, 20)
        Me.Panel1.TabIndex = 84
        '
        'lblsangradotrans
        '
        Me.lblsangradotrans.AutoSize = True
        Me.lblsangradotrans.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblsangradotrans.Location = New System.Drawing.Point(41, 3)
        Me.lblsangradotrans.Name = "lblsangradotrans"
        Me.lblsangradotrans.Size = New System.Drawing.Size(111, 13)
        Me.lblsangradotrans.TabIndex = 0
        Me.lblsangradotrans.Text = "sangrado transvaginal"
        '
        'rbtSangradotransA
        '
        Me.rbtSangradotransA.AutoSize = True
        Me.rbtSangradotransA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradotransA.Location = New System.Drawing.Point(519, 1)
        Me.rbtSangradotransA.Name = "rbtSangradotransA"
        Me.rbtSangradotransA.Size = New System.Drawing.Size(77, 17)
        Me.rbtSangradotransA.TabIndex = 74
        Me.rbtSangradotransA.TabStop = True
        Me.rbtSangradotransA.Text = "Abundante"
        Me.rbtSangradotransA.UseVisualStyleBackColor = True
        '
        'rbtSangradotransM
        '
        Me.rbtSangradotransM.AutoSize = True
        Me.rbtSangradotransM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradotransM.Location = New System.Drawing.Point(359, 1)
        Me.rbtSangradotransM.Name = "rbtSangradotransM"
        Me.rbtSangradotransM.Size = New System.Drawing.Size(112, 17)
        Me.rbtSangradotransM.TabIndex = 73
        Me.rbtSangradotransM.TabStop = True
        Me.rbtSangradotransM.Text = "Escaso/moderado"
        Me.rbtSangradotransM.UseVisualStyleBackColor = True
        '
        'rbtSangradotransB
        '
        Me.rbtSangradotransB.AutoSize = True
        Me.rbtSangradotransB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradotransB.Location = New System.Drawing.Point(187, 1)
        Me.rbtSangradotransB.Name = "rbtSangradotransB"
        Me.rbtSangradotransB.Size = New System.Drawing.Size(64, 17)
        Me.rbtSangradotransB.TabIndex = 72
        Me.rbtSangradotransB.TabStop = True
        Me.rbtSangradotransB.Text = "Ausente"
        Me.rbtSangradotransB.UseVisualStyleBackColor = True
        '
        'pnlHiper
        '
        Me.pnlHiper.Controls.Add(Me.lblAcufos)
        Me.pnlHiper.Controls.Add(Me.rbtAcufenosA)
        Me.pnlHiper.Controls.Add(Me.rbtAcufenosB)
        Me.pnlHiper.Location = New System.Drawing.Point(6, 67)
        Me.pnlHiper.Name = "pnlHiper"
        Me.pnlHiper.Size = New System.Drawing.Size(697, 20)
        Me.pnlHiper.TabIndex = 75
        '
        'lblAcufos
        '
        Me.lblAcufos.AutoSize = True
        Me.lblAcufos.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAcufos.Location = New System.Drawing.Point(97, 4)
        Me.lblAcufos.Name = "lblAcufos"
        Me.lblAcufos.Size = New System.Drawing.Size(52, 13)
        Me.lblAcufos.TabIndex = 0
        Me.lblAcufos.Text = "Acufenos"
        '
        'rbtAcufenosA
        '
        Me.rbtAcufenosA.AutoSize = True
        Me.rbtAcufenosA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtAcufenosA.Location = New System.Drawing.Point(359, 2)
        Me.rbtAcufenosA.Name = "rbtAcufenosA"
        Me.rbtAcufenosA.Size = New System.Drawing.Size(67, 17)
        Me.rbtAcufenosA.TabIndex = 78
        Me.rbtAcufenosA.TabStop = True
        Me.rbtAcufenosA.Text = "Presente"
        Me.rbtAcufenosA.UseVisualStyleBackColor = True
        '
        'rbtAcufenosB
        '
        Me.rbtAcufenosB.AutoSize = True
        Me.rbtAcufenosB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtAcufenosB.Location = New System.Drawing.Point(187, 1)
        Me.rbtAcufenosB.Name = "rbtAcufenosB"
        Me.rbtAcufenosB.Size = New System.Drawing.Size(64, 17)
        Me.rbtAcufenosB.TabIndex = 76
        Me.rbtAcufenosB.TabStop = True
        Me.rbtAcufenosB.Text = "Ausente"
        Me.rbtAcufenosB.UseVisualStyleBackColor = True
        '
        'pnlSudoracion
        '
        Me.pnlSudoracion.Controls.Add(Me.lblamaurosis)
        Me.pnlSudoracion.Controls.Add(Me.rbtAmaurosisA)
        Me.pnlSudoracion.Controls.Add(Me.rbtAmaurosisB)
        Me.pnlSudoracion.Location = New System.Drawing.Point(6, 136)
        Me.pnlSudoracion.Name = "pnlSudoracion"
        Me.pnlSudoracion.Size = New System.Drawing.Size(697, 20)
        Me.pnlSudoracion.TabIndex = 83
        '
        'lblamaurosis
        '
        Me.lblamaurosis.AutoSize = True
        Me.lblamaurosis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblamaurosis.Location = New System.Drawing.Point(94, 3)
        Me.lblamaurosis.Name = "lblamaurosis"
        Me.lblamaurosis.Size = New System.Drawing.Size(55, 13)
        Me.lblamaurosis.TabIndex = 0
        Me.lblamaurosis.Text = "Amaurosis"
        '
        'rbtAmaurosisA
        '
        Me.rbtAmaurosisA.AutoSize = True
        Me.rbtAmaurosisA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtAmaurosisA.Location = New System.Drawing.Point(359, 1)
        Me.rbtAmaurosisA.Name = "rbtAmaurosisA"
        Me.rbtAmaurosisA.Size = New System.Drawing.Size(61, 17)
        Me.rbtAmaurosisA.TabIndex = 86
        Me.rbtAmaurosisA.TabStop = True
        Me.rbtAmaurosisA.Text = "Profusa"
        Me.rbtAmaurosisA.UseVisualStyleBackColor = True
        '
        'rbtAmaurosisB
        '
        Me.rbtAmaurosisB.AutoSize = True
        Me.rbtAmaurosisB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtAmaurosisB.Location = New System.Drawing.Point(187, 1)
        Me.rbtAmaurosisB.Name = "rbtAmaurosisB"
        Me.rbtAmaurosisB.Size = New System.Drawing.Size(64, 17)
        Me.rbtAmaurosisB.TabIndex = 84
        Me.rbtAmaurosisB.TabStop = True
        Me.rbtAmaurosisB.Text = "Ausente"
        Me.rbtAmaurosisB.UseVisualStyleBackColor = True
        '
        'pnlCefalea
        '
        Me.pnlCefalea.Controls.Add(Me.lblCefalea)
        Me.pnlCefalea.Controls.Add(Me.rbtCefaleaA)
        Me.pnlCefalea.Controls.Add(Me.rbtCefaleaM)
        Me.pnlCefalea.Controls.Add(Me.rbtCefaleaB)
        Me.pnlCefalea.Location = New System.Drawing.Point(6, 44)
        Me.pnlCefalea.Name = "pnlCefalea"
        Me.pnlCefalea.Size = New System.Drawing.Size(697, 20)
        Me.pnlCefalea.TabIndex = 71
        '
        'lblCefalea
        '
        Me.lblCefalea.AutoSize = True
        Me.lblCefalea.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCefalea.Location = New System.Drawing.Point(106, 3)
        Me.lblCefalea.Name = "lblCefalea"
        Me.lblCefalea.Size = New System.Drawing.Size(43, 13)
        Me.lblCefalea.TabIndex = 0
        Me.lblCefalea.Text = "Cefalea"
        '
        'rbtCefaleaA
        '
        Me.rbtCefaleaA.AutoSize = True
        Me.rbtCefaleaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCefaleaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtCefaleaA.Name = "rbtCefaleaA"
        Me.rbtCefaleaA.Size = New System.Drawing.Size(67, 17)
        Me.rbtCefaleaA.TabIndex = 74
        Me.rbtCefaleaA.TabStop = True
        Me.rbtCefaleaA.Text = "Presente"
        Me.rbtCefaleaA.UseVisualStyleBackColor = True
        '
        'rbtCefaleaM
        '
        Me.rbtCefaleaM.AutoSize = True
        Me.rbtCefaleaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCefaleaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtCefaleaM.Name = "rbtCefaleaM"
        Me.rbtCefaleaM.Size = New System.Drawing.Size(74, 17)
        Me.rbtCefaleaM.TabIndex = 73
        Me.rbtCefaleaM.TabStop = True
        Me.rbtCefaleaM.Text = "No pulsatil"
        Me.rbtCefaleaM.UseVisualStyleBackColor = True
        '
        'rbtCefaleaB
        '
        Me.rbtCefaleaB.AutoSize = True
        Me.rbtCefaleaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCefaleaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtCefaleaB.Name = "rbtCefaleaB"
        Me.rbtCefaleaB.Size = New System.Drawing.Size(64, 17)
        Me.rbtCefaleaB.TabIndex = 72
        Me.rbtCefaleaB.TabStop = True
        Me.rbtCefaleaB.Text = "Ausente"
        Me.rbtCefaleaB.UseVisualStyleBackColor = True
        '
        'gbxInfoEmbarazo
        '
        Me.gbxInfoEmbarazo.Controls.Add(Me.pnlSangrado)
        Me.gbxInfoEmbarazo.Controls.Add(Me.pnlCesarea)
        Me.gbxInfoEmbarazo.Controls.Add(Me.pnlTrauma)
        Me.gbxInfoEmbarazo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxInfoEmbarazo.Location = New System.Drawing.Point(4, 247)
        Me.gbxInfoEmbarazo.Name = "gbxInfoEmbarazo"
        Me.gbxInfoEmbarazo.Size = New System.Drawing.Size(709, 92)
        Me.gbxInfoEmbarazo.TabIndex = 100
        Me.gbxInfoEmbarazo.TabStop = False
        Me.gbxInfoEmbarazo.Text = "Información conocida del embarazo "
        '
        'pnlSangrado
        '
        Me.pnlSangrado.Controls.Add(Me.lblSangrado)
        Me.pnlSangrado.Controls.Add(Me.rbtSangradoA)
        Me.pnlSangrado.Controls.Add(Me.rbtSangradoM)
        Me.pnlSangrado.Controls.Add(Me.rbtSangradoB)
        Me.pnlSangrado.Location = New System.Drawing.Point(9, 65)
        Me.pnlSangrado.Name = "pnlSangrado"
        Me.pnlSangrado.Size = New System.Drawing.Size(691, 22)
        Me.pnlSangrado.TabIndex = 104
        '
        'lblSangrado
        '
        Me.lblSangrado.AutoSize = True
        Me.lblSangrado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSangrado.Location = New System.Drawing.Point(49, 3)
        Me.lblSangrado.Name = "lblSangrado"
        Me.lblSangrado.Size = New System.Drawing.Size(104, 13)
        Me.lblSangrado.TabIndex = 0
        Me.lblSangrado.Text = "Sangrado 1° 2°  sem"
        '
        'rbtSangradoA
        '
        Me.rbtSangradoA.AutoSize = True
        Me.rbtSangradoA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradoA.Location = New System.Drawing.Point(519, 1)
        Me.rbtSangradoA.Name = "rbtSangradoA"
        Me.rbtSangradoA.Size = New System.Drawing.Size(58, 17)
        Me.rbtSangradoA.TabIndex = 107
        Me.rbtSangradoA.TabStop = True
        Me.rbtSangradoA.Text = "Franco"
        Me.rbtSangradoA.UseVisualStyleBackColor = True
        '
        'rbtSangradoM
        '
        Me.rbtSangradoM.AutoSize = True
        Me.rbtSangradoM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradoM.Location = New System.Drawing.Point(359, 1)
        Me.rbtSangradoM.Name = "rbtSangradoM"
        Me.rbtSangradoM.Size = New System.Drawing.Size(62, 17)
        Me.rbtSangradoM.TabIndex = 106
        Me.rbtSangradoM.TabStop = True
        Me.rbtSangradoM.Text = "Dudoso"
        Me.rbtSangradoM.UseVisualStyleBackColor = True
        '
        'rbtSangradoB
        '
        Me.rbtSangradoB.AutoSize = True
        Me.rbtSangradoB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtSangradoB.Location = New System.Drawing.Point(187, 3)
        Me.rbtSangradoB.Name = "rbtSangradoB"
        Me.rbtSangradoB.Size = New System.Drawing.Size(64, 17)
        Me.rbtSangradoB.TabIndex = 105
        Me.rbtSangradoB.TabStop = True
        Me.rbtSangradoB.Text = "Ausente"
        Me.rbtSangradoB.UseVisualStyleBackColor = True
        '
        'pnlCesarea
        '
        Me.pnlCesarea.Controls.Add(Me.lblCirUterina)
        Me.pnlCesarea.Controls.Add(Me.rbtCirUterinaM)
        Me.pnlCesarea.Controls.Add(Me.rbtCirUterinaB)
        Me.pnlCesarea.Location = New System.Drawing.Point(9, 17)
        Me.pnlCesarea.Name = "pnlCesarea"
        Me.pnlCesarea.Size = New System.Drawing.Size(691, 20)
        Me.pnlCesarea.TabIndex = 102
        '
        'lblCirUterina
        '
        Me.lblCirUterina.AutoSize = True
        Me.lblCirUterina.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCirUterina.Location = New System.Drawing.Point(46, 5)
        Me.lblCirUterina.Name = "lblCirUterina"
        Me.lblCirUterina.Size = New System.Drawing.Size(107, 13)
        Me.lblCirUterina.TabIndex = 0
        Me.lblCirUterina.Text = "Cesarea, cir. uternina"
        '
        'rbtCirUterinaM
        '
        Me.rbtCirUterinaM.AutoSize = True
        Me.rbtCirUterinaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCirUterinaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtCirUterinaM.Name = "rbtCirUterinaM"
        Me.rbtCirUterinaM.Size = New System.Drawing.Size(34, 17)
        Me.rbtCirUterinaM.TabIndex = 104
        Me.rbtCirUterinaM.TabStop = True
        Me.rbtCirUterinaM.Text = "Si"
        Me.rbtCirUterinaM.UseVisualStyleBackColor = True
        '
        'rbtCirUterinaB
        '
        Me.rbtCirUterinaB.AutoSize = True
        Me.rbtCirUterinaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCirUterinaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtCirUterinaB.Name = "rbtCirUterinaB"
        Me.rbtCirUterinaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtCirUterinaB.TabIndex = 103
        Me.rbtCirUterinaB.TabStop = True
        Me.rbtCirUterinaB.Text = "No"
        Me.rbtCirUterinaB.UseVisualStyleBackColor = True
        '
        'pnlTrauma
        '
        Me.pnlTrauma.Controls.Add(Me.lblTrauma)
        Me.pnlTrauma.Controls.Add(Me.rbtTraumaA)
        Me.pnlTrauma.Controls.Add(Me.rbtTraumaM)
        Me.pnlTrauma.Controls.Add(Me.rbtTraumaB)
        Me.pnlTrauma.Location = New System.Drawing.Point(9, 41)
        Me.pnlTrauma.Name = "pnlTrauma"
        Me.pnlTrauma.Size = New System.Drawing.Size(691, 20)
        Me.pnlTrauma.TabIndex = 107
        '
        'lblTrauma
        '
        Me.lblTrauma.AutoSize = True
        Me.lblTrauma.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTrauma.Location = New System.Drawing.Point(38, 3)
        Me.lblTrauma.Name = "lblTrauma"
        Me.lblTrauma.Size = New System.Drawing.Size(116, 13)
        Me.lblTrauma.TabIndex = 0
        Me.lblTrauma.Text = "Trauma, agresion fisica"
        '
        'rbtTraumaA
        '
        Me.rbtTraumaA.AutoSize = True
        Me.rbtTraumaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTraumaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtTraumaA.Name = "rbtTraumaA"
        Me.rbtTraumaA.Size = New System.Drawing.Size(77, 17)
        Me.rbtTraumaA.TabIndex = 110
        Me.rbtTraumaA.TabStop = True
        Me.rbtTraumaA.Text = "Abundante"
        Me.rbtTraumaA.UseVisualStyleBackColor = True
        '
        'rbtTraumaM
        '
        Me.rbtTraumaM.AutoSize = True
        Me.rbtTraumaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTraumaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtTraumaM.Name = "rbtTraumaM"
        Me.rbtTraumaM.Size = New System.Drawing.Size(49, 17)
        Me.rbtTraumaM.TabIndex = 109
        Me.rbtTraumaM.TabStop = True
        Me.rbtTraumaM.Text = "Leve"
        Me.rbtTraumaM.UseVisualStyleBackColor = True
        '
        'rbtTraumaB
        '
        Me.rbtTraumaB.AutoSize = True
        Me.rbtTraumaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTraumaB.Location = New System.Drawing.Point(187, 1)
        Me.rbtTraumaB.Name = "rbtTraumaB"
        Me.rbtTraumaB.Size = New System.Drawing.Size(64, 17)
        Me.rbtTraumaB.TabIndex = 108
        Me.rbtTraumaB.TabStop = True
        Me.rbtTraumaB.Text = "Ausente"
        Me.rbtTraumaB.UseVisualStyleBackColor = True
        '
        'TabpagObservaciones
        '
        Me.TabpagObservaciones.BackColor = System.Drawing.SystemColors.Window
        Me.TabpagObservaciones.Controls.Add(Me.gbxEnfAsociadas)
        Me.TabpagObservaciones.Controls.Add(Me.GroupBox1)
        Me.TabpagObservaciones.Location = New System.Drawing.Point(4, 22)
        Me.TabpagObservaciones.Name = "TabpagObservaciones"
        Me.TabpagObservaciones.Padding = New System.Windows.Forms.Padding(3)
        Me.TabpagObservaciones.Size = New System.Drawing.Size(721, 436)
        Me.TabpagObservaciones.TabIndex = 2
        Me.TabpagObservaciones.Text = "Pruebas Y Observaciones"
        '
        'gbxEnfAsociadas
        '
        Me.gbxEnfAsociadas.Controls.Add(Me.pnlHemopatia)
        Me.gbxEnfAsociadas.Controls.Add(Me.pnlEndocrinopatia)
        Me.gbxEnfAsociadas.Controls.Add(Me.pnlNefropatia)
        Me.gbxEnfAsociadas.Controls.Add(Me.pnlCardiopatia)
        Me.gbxEnfAsociadas.Controls.Add(Me.pnlHepatopatia)
        Me.gbxEnfAsociadas.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxEnfAsociadas.Location = New System.Drawing.Point(4, 22)
        Me.gbxEnfAsociadas.Name = "gbxEnfAsociadas"
        Me.gbxEnfAsociadas.Size = New System.Drawing.Size(709, 142)
        Me.gbxEnfAsociadas.TabIndex = 144
        Me.gbxEnfAsociadas.TabStop = False
        Me.gbxEnfAsociadas.Text = "Enfermedades asociadas conocidas"
        '
        'pnlHemopatia
        '
        Me.pnlHemopatia.Controls.Add(Me.lblHematopatia)
        Me.pnlHemopatia.Controls.Add(Me.rbtHematopatiaA)
        Me.pnlHemopatia.Controls.Add(Me.rbtHematopatiaM)
        Me.pnlHemopatia.Controls.Add(Me.rbtHematopatiaB)
        Me.pnlHemopatia.Location = New System.Drawing.Point(9, 66)
        Me.pnlHemopatia.Name = "pnlHemopatia"
        Me.pnlHemopatia.Size = New System.Drawing.Size(691, 20)
        Me.pnlHemopatia.TabIndex = 135
        '
        'lblHematopatia
        '
        Me.lblHematopatia.AutoSize = True
        Me.lblHematopatia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHematopatia.Location = New System.Drawing.Point(87, 3)
        Me.lblHematopatia.Name = "lblHematopatia"
        Me.lblHematopatia.Size = New System.Drawing.Size(67, 13)
        Me.lblHematopatia.TabIndex = 0
        Me.lblHematopatia.Text = "Hematopatia"
        '
        'rbtHematopatiaA
        '
        Me.rbtHematopatiaA.AutoSize = True
        Me.rbtHematopatiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHematopatiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtHematopatiaA.Name = "rbtHematopatiaA"
        Me.rbtHematopatiaA.Size = New System.Drawing.Size(59, 17)
        Me.rbtHematopatiaA.TabIndex = 137
        Me.rbtHematopatiaA.TabStop = True
        Me.rbtHematopatiaA.Text = "Severo"
        Me.rbtHematopatiaA.UseVisualStyleBackColor = True
        '
        'rbtHematopatiaM
        '
        Me.rbtHematopatiaM.AutoSize = True
        Me.rbtHematopatiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHematopatiaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtHematopatiaM.Name = "rbtHematopatiaM"
        Me.rbtHematopatiaM.Size = New System.Drawing.Size(49, 17)
        Me.rbtHematopatiaM.TabIndex = 136
        Me.rbtHematopatiaM.TabStop = True
        Me.rbtHematopatiaM.Text = "Leve"
        Me.rbtHematopatiaM.UseVisualStyleBackColor = True
        '
        'rbtHematopatiaB
        '
        Me.rbtHematopatiaB.AutoSize = True
        Me.rbtHematopatiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHematopatiaB.Location = New System.Drawing.Point(188, 1)
        Me.rbtHematopatiaB.Name = "rbtHematopatiaB"
        Me.rbtHematopatiaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtHematopatiaB.TabIndex = 135
        Me.rbtHematopatiaB.TabStop = True
        Me.rbtHematopatiaB.Text = "No"
        Me.rbtHematopatiaB.UseVisualStyleBackColor = True
        '
        'pnlEndocrinopatia
        '
        Me.pnlEndocrinopatia.Controls.Add(Me.lblEndocrinopatia)
        Me.pnlEndocrinopatia.Controls.Add(Me.rbtEndocrinopatiaA)
        Me.pnlEndocrinopatia.Controls.Add(Me.rbtEndocrinopatiaM)
        Me.pnlEndocrinopatia.Controls.Add(Me.rbtEndocrinopatiaB)
        Me.pnlEndocrinopatia.Location = New System.Drawing.Point(9, 92)
        Me.pnlEndocrinopatia.Name = "pnlEndocrinopatia"
        Me.pnlEndocrinopatia.Size = New System.Drawing.Size(691, 20)
        Me.pnlEndocrinopatia.TabIndex = 130
        '
        'lblEndocrinopatia
        '
        Me.lblEndocrinopatia.AutoSize = True
        Me.lblEndocrinopatia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndocrinopatia.Location = New System.Drawing.Point(77, 1)
        Me.lblEndocrinopatia.Name = "lblEndocrinopatia"
        Me.lblEndocrinopatia.Size = New System.Drawing.Size(78, 13)
        Me.lblEndocrinopatia.TabIndex = 0
        Me.lblEndocrinopatia.Text = "Endocrinopatia"
        '
        'rbtEndocrinopatiaA
        '
        Me.rbtEndocrinopatiaA.AutoSize = True
        Me.rbtEndocrinopatiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEndocrinopatiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtEndocrinopatiaA.Name = "rbtEndocrinopatiaA"
        Me.rbtEndocrinopatiaA.Size = New System.Drawing.Size(154, 17)
        Me.rbtEndocrinopatiaA.TabIndex = 133
        Me.rbtEndocrinopatiaA.TabStop = True
        Me.rbtEndocrinopatiaA.Text = "Inconsiente o convulsiones"
        Me.rbtEndocrinopatiaA.UseVisualStyleBackColor = True
        '
        'rbtEndocrinopatiaM
        '
        Me.rbtEndocrinopatiaM.AutoSize = True
        Me.rbtEndocrinopatiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEndocrinopatiaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtEndocrinopatiaM.Name = "rbtEndocrinopatiaM"
        Me.rbtEndocrinopatiaM.Size = New System.Drawing.Size(49, 17)
        Me.rbtEndocrinopatiaM.TabIndex = 132
        Me.rbtEndocrinopatiaM.TabStop = True
        Me.rbtEndocrinopatiaM.Text = "Leve"
        Me.rbtEndocrinopatiaM.UseVisualStyleBackColor = True
        '
        'rbtEndocrinopatiaB
        '
        Me.rbtEndocrinopatiaB.AutoSize = True
        Me.rbtEndocrinopatiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtEndocrinopatiaB.Location = New System.Drawing.Point(188, 1)
        Me.rbtEndocrinopatiaB.Name = "rbtEndocrinopatiaB"
        Me.rbtEndocrinopatiaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtEndocrinopatiaB.TabIndex = 131
        Me.rbtEndocrinopatiaB.Text = "No"
        Me.rbtEndocrinopatiaB.UseVisualStyleBackColor = True
        '
        'pnlNefropatia
        '
        Me.pnlNefropatia.Controls.Add(Me.lblNefropatia)
        Me.pnlNefropatia.Controls.Add(Me.rbtNefropatiaA)
        Me.pnlNefropatia.Controls.Add(Me.rbtNefropatiaM)
        Me.pnlNefropatia.Controls.Add(Me.rbtNefropatiaB)
        Me.pnlNefropatia.Location = New System.Drawing.Point(9, 41)
        Me.pnlNefropatia.Name = "pnlNefropatia"
        Me.pnlNefropatia.Size = New System.Drawing.Size(691, 20)
        Me.pnlNefropatia.TabIndex = 126
        '
        'lblNefropatia
        '
        Me.lblNefropatia.AutoSize = True
        Me.lblNefropatia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNefropatia.Location = New System.Drawing.Point(98, 3)
        Me.lblNefropatia.Name = "lblNefropatia"
        Me.lblNefropatia.Size = New System.Drawing.Size(56, 13)
        Me.lblNefropatia.TabIndex = 0
        Me.lblNefropatia.Text = "Nefropatia"
        '
        'rbtNefropatiaA
        '
        Me.rbtNefropatiaA.AutoSize = True
        Me.rbtNefropatiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNefropatiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtNefropatiaA.Name = "rbtNefropatiaA"
        Me.rbtNefropatiaA.Size = New System.Drawing.Size(67, 17)
        Me.rbtNefropatiaA.TabIndex = 129
        Me.rbtNefropatiaA.TabStop = True
        Me.rbtNefropatiaA.Text = "Presente"
        Me.rbtNefropatiaA.UseVisualStyleBackColor = True
        '
        'rbtNefropatiaM
        '
        Me.rbtNefropatiaM.AutoSize = True
        Me.rbtNefropatiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNefropatiaM.Location = New System.Drawing.Point(359, 2)
        Me.rbtNefropatiaM.Name = "rbtNefropatiaM"
        Me.rbtNefropatiaM.Size = New System.Drawing.Size(49, 17)
        Me.rbtNefropatiaM.TabIndex = 128
        Me.rbtNefropatiaM.TabStop = True
        Me.rbtNefropatiaM.Text = "Leve"
        Me.rbtNefropatiaM.UseVisualStyleBackColor = True
        '
        'rbtNefropatiaB
        '
        Me.rbtNefropatiaB.AutoSize = True
        Me.rbtNefropatiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtNefropatiaB.Location = New System.Drawing.Point(188, 1)
        Me.rbtNefropatiaB.Name = "rbtNefropatiaB"
        Me.rbtNefropatiaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtNefropatiaB.TabIndex = 127
        Me.rbtNefropatiaB.TabStop = True
        Me.rbtNefropatiaB.Text = "No"
        Me.rbtNefropatiaB.UseVisualStyleBackColor = True
        '
        'pnlCardiopatia
        '
        Me.pnlCardiopatia.Controls.Add(Me.lblCardiopatia)
        Me.pnlCardiopatia.Controls.Add(Me.rbtCardiopatiaA)
        Me.pnlCardiopatia.Controls.Add(Me.rbtCardiopatiaM)
        Me.pnlCardiopatia.Controls.Add(Me.rbtCardiopatiaB)
        Me.pnlCardiopatia.Location = New System.Drawing.Point(9, 17)
        Me.pnlCardiopatia.Name = "pnlCardiopatia"
        Me.pnlCardiopatia.Size = New System.Drawing.Size(691, 20)
        Me.pnlCardiopatia.TabIndex = 122
        '
        'lblCardiopatia
        '
        Me.lblCardiopatia.AutoSize = True
        Me.lblCardiopatia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCardiopatia.Location = New System.Drawing.Point(94, 4)
        Me.lblCardiopatia.Name = "lblCardiopatia"
        Me.lblCardiopatia.Size = New System.Drawing.Size(60, 13)
        Me.lblCardiopatia.TabIndex = 0
        Me.lblCardiopatia.Text = "Cardiopatia"
        '
        'rbtCardiopatiaA
        '
        Me.rbtCardiopatiaA.AutoSize = True
        Me.rbtCardiopatiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCardiopatiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtCardiopatiaA.Name = "rbtCardiopatiaA"
        Me.rbtCardiopatiaA.Size = New System.Drawing.Size(73, 17)
        Me.rbtCardiopatiaA.TabIndex = 125
        Me.rbtCardiopatiaA.TabStop = True
        Me.rbtCardiopatiaA.Text = "Tipo 2 ó >"
        Me.rbtCardiopatiaA.UseVisualStyleBackColor = True
        '
        'rbtCardiopatiaM
        '
        Me.rbtCardiopatiaM.AutoSize = True
        Me.rbtCardiopatiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCardiopatiaM.Location = New System.Drawing.Point(359, 2)
        Me.rbtCardiopatiaM.Name = "rbtCardiopatiaM"
        Me.rbtCardiopatiaM.Size = New System.Drawing.Size(55, 17)
        Me.rbtCardiopatiaM.TabIndex = 124
        Me.rbtCardiopatiaM.TabStop = True
        Me.rbtCardiopatiaM.Text = "Tipo 1"
        Me.rbtCardiopatiaM.UseVisualStyleBackColor = True
        '
        'rbtCardiopatiaB
        '
        Me.rbtCardiopatiaB.AutoSize = True
        Me.rbtCardiopatiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtCardiopatiaB.Location = New System.Drawing.Point(188, 1)
        Me.rbtCardiopatiaB.Name = "rbtCardiopatiaB"
        Me.rbtCardiopatiaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtCardiopatiaB.TabIndex = 123
        Me.rbtCardiopatiaB.TabStop = True
        Me.rbtCardiopatiaB.Text = "No"
        Me.rbtCardiopatiaB.UseVisualStyleBackColor = True
        '
        'pnlHepatopatia
        '
        Me.pnlHepatopatia.Controls.Add(Me.lblHepatopatia)
        Me.pnlHepatopatia.Controls.Add(Me.rbtHepatopatiaA)
        Me.pnlHepatopatia.Controls.Add(Me.rbtHepatopatiaM)
        Me.pnlHepatopatia.Controls.Add(Me.rbtHepatopatiaB)
        Me.pnlHepatopatia.Location = New System.Drawing.Point(9, 116)
        Me.pnlHepatopatia.Name = "pnlHepatopatia"
        Me.pnlHepatopatia.Size = New System.Drawing.Size(691, 20)
        Me.pnlHepatopatia.TabIndex = 134
        '
        'lblHepatopatia
        '
        Me.lblHepatopatia.AutoSize = True
        Me.lblHepatopatia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblHepatopatia.Location = New System.Drawing.Point(90, 3)
        Me.lblHepatopatia.Name = "lblHepatopatia"
        Me.lblHepatopatia.Size = New System.Drawing.Size(65, 13)
        Me.lblHepatopatia.TabIndex = 0
        Me.lblHepatopatia.Text = "Hepatopatia"
        '
        'rbtHepatopatiaA
        '
        Me.rbtHepatopatiaA.AutoSize = True
        Me.rbtHepatopatiaA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHepatopatiaA.Location = New System.Drawing.Point(519, 1)
        Me.rbtHepatopatiaA.Name = "rbtHepatopatiaA"
        Me.rbtHepatopatiaA.Size = New System.Drawing.Size(61, 17)
        Me.rbtHepatopatiaA.TabIndex = 137
        Me.rbtHepatopatiaA.TabStop = True
        Me.rbtHepatopatiaA.Text = "Profusa"
        Me.rbtHepatopatiaA.UseVisualStyleBackColor = True
        '
        'rbtHepatopatiaM
        '
        Me.rbtHepatopatiaM.AutoSize = True
        Me.rbtHepatopatiaM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHepatopatiaM.Location = New System.Drawing.Point(359, 1)
        Me.rbtHepatopatiaM.Name = "rbtHepatopatiaM"
        Me.rbtHepatopatiaM.Size = New System.Drawing.Size(49, 17)
        Me.rbtHepatopatiaM.TabIndex = 136
        Me.rbtHepatopatiaM.TabStop = True
        Me.rbtHepatopatiaM.Text = "Leve"
        Me.rbtHepatopatiaM.UseVisualStyleBackColor = True
        '
        'rbtHepatopatiaB
        '
        Me.rbtHepatopatiaB.AutoSize = True
        Me.rbtHepatopatiaB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtHepatopatiaB.Location = New System.Drawing.Point(188, 1)
        Me.rbtHepatopatiaB.Name = "rbtHepatopatiaB"
        Me.rbtHepatopatiaB.Size = New System.Drawing.Size(39, 17)
        Me.rbtHepatopatiaB.TabIndex = 135
        Me.rbtHepatopatiaB.TabStop = True
        Me.rbtHepatopatiaB.Text = "No"
        Me.rbtHepatopatiaB.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.Panel2)
        Me.GroupBox1.Controls.Add(Me.Panel3)
        Me.GroupBox1.Controls.Add(Me.Panel4)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.Location = New System.Drawing.Point(6, 170)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(709, 128)
        Me.GroupBox1.TabIndex = 141
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Enfermedades de tamiz: VIH, Sifilis"
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.lblTiraUroanalisis)
        Me.Panel2.Controls.Add(Me.rbtTiraUroanalisisA)
        Me.Panel2.Controls.Add(Me.rbtTiraUroanalisisM)
        Me.Panel2.Controls.Add(Me.rbtTiraUroanalisisB)
        Me.Panel2.Location = New System.Drawing.Point(11, 98)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(688, 30)
        Me.Panel2.TabIndex = 150
        '
        'lblTiraUroanalisis
        '
        Me.lblTiraUroanalisis.AutoSize = True
        Me.lblTiraUroanalisis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTiraUroanalisis.Location = New System.Drawing.Point(75, 9)
        Me.lblTiraUroanalisis.Name = "lblTiraUroanalisis"
        Me.lblTiraUroanalisis.Size = New System.Drawing.Size(79, 13)
        Me.lblTiraUroanalisis.TabIndex = 0
        Me.lblTiraUroanalisis.Text = "Tira Uroanalisis"
        '
        'rbtTiraUroanalisisA
        '
        Me.rbtTiraUroanalisisA.AutoSize = True
        Me.rbtTiraUroanalisisA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTiraUroanalisisA.Location = New System.Drawing.Point(519, 9)
        Me.rbtTiraUroanalisisA.Name = "rbtTiraUroanalisisA"
        Me.rbtTiraUroanalisisA.Size = New System.Drawing.Size(110, 17)
        Me.rbtTiraUroanalisisA.TabIndex = 153
        Me.rbtTiraUroanalisisA.TabStop = True
        Me.rbtTiraUroanalisisA.Text = "Proteinas > 30 mg"
        Me.rbtTiraUroanalisisA.UseVisualStyleBackColor = True
        '
        'rbtTiraUroanalisisM
        '
        Me.rbtTiraUroanalisisM.AutoSize = True
        Me.rbtTiraUroanalisisM.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTiraUroanalisisM.Location = New System.Drawing.Point(359, 9)
        Me.rbtTiraUroanalisisM.Name = "rbtTiraUroanalisisM"
        Me.rbtTiraUroanalisisM.Size = New System.Drawing.Size(110, 17)
        Me.rbtTiraUroanalisisM.TabIndex = 152
        Me.rbtTiraUroanalisisM.TabStop = True
        Me.rbtTiraUroanalisisM.Text = "Proteinas < 30 mg"
        Me.rbtTiraUroanalisisM.UseVisualStyleBackColor = True
        '
        'rbtTiraUroanalisisB
        '
        Me.rbtTiraUroanalisisB.AutoSize = True
        Me.rbtTiraUroanalisisB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtTiraUroanalisisB.Location = New System.Drawing.Point(188, 8)
        Me.rbtTiraUroanalisisB.Name = "rbtTiraUroanalisisB"
        Me.rbtTiraUroanalisisB.Size = New System.Drawing.Size(68, 17)
        Me.rbtTiraUroanalisisB.TabIndex = 151
        Me.rbtTiraUroanalisisB.TabStop = True
        Me.rbtTiraUroanalisisB.Text = "Negativo"
        Me.rbtTiraUroanalisisB.UseVisualStyleBackColor = True
        '
        'Panel3
        '
        Me.Panel3.Controls.Add(Me.cbxResultadoSIFILIS)
        Me.Panel3.Controls.Add(Me.lblPruebaSifilis)
        Me.Panel3.Controls.Add(Me.rbtPruebaSifilisA)
        Me.Panel3.Controls.Add(Me.rbtPruebaSifilisB)
        Me.Panel3.Location = New System.Drawing.Point(12, 58)
        Me.Panel3.Name = "Panel3"
        Me.Panel3.Size = New System.Drawing.Size(688, 34)
        Me.Panel3.TabIndex = 146
        '
        'cbxResultadoSIFILIS
        '
        Me.cbxResultadoSIFILIS.Enabled = False
        Me.cbxResultadoSIFILIS.FormattingEnabled = True
        Me.cbxResultadoSIFILIS.Items.AddRange(New Object() {"Negativo", "Positivo"})
        Me.cbxResultadoSIFILIS.Location = New System.Drawing.Point(427, 8)
        Me.cbxResultadoSIFILIS.Name = "cbxResultadoSIFILIS"
        Me.cbxResultadoSIFILIS.Size = New System.Drawing.Size(246, 21)
        Me.cbxResultadoSIFILIS.Sorted = True
        Me.cbxResultadoSIFILIS.TabIndex = 146
        '
        'lblPruebaSifilis
        '
        Me.lblPruebaSifilis.AutoSize = True
        Me.lblPruebaSifilis.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPruebaSifilis.Location = New System.Drawing.Point(41, 13)
        Me.lblPruebaSifilis.Name = "lblPruebaSifilis"
        Me.lblPruebaSifilis.Size = New System.Drawing.Size(112, 13)
        Me.lblPruebaSifilis.TabIndex = 0
        Me.lblPruebaSifilis.Text = "Prueba rapida de sifilis"
        '
        'rbtPruebaSifilisA
        '
        Me.rbtPruebaSifilisA.AutoSize = True
        Me.rbtPruebaSifilisA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPruebaSifilisA.Location = New System.Drawing.Point(359, 9)
        Me.rbtPruebaSifilisA.Name = "rbtPruebaSifilisA"
        Me.rbtPruebaSifilisA.Size = New System.Drawing.Size(35, 17)
        Me.rbtPruebaSifilisA.TabIndex = 148
        Me.rbtPruebaSifilisA.TabStop = True
        Me.rbtPruebaSifilisA.Text = "SI"
        Me.rbtPruebaSifilisA.UseVisualStyleBackColor = True
        '
        'rbtPruebaSifilisB
        '
        Me.rbtPruebaSifilisB.AutoSize = True
        Me.rbtPruebaSifilisB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPruebaSifilisB.Location = New System.Drawing.Point(187, 9)
        Me.rbtPruebaSifilisB.Name = "rbtPruebaSifilisB"
        Me.rbtPruebaSifilisB.Size = New System.Drawing.Size(39, 17)
        Me.rbtPruebaSifilisB.TabIndex = 147
        Me.rbtPruebaSifilisB.TabStop = True
        Me.rbtPruebaSifilisB.Text = "No"
        Me.rbtPruebaSifilisB.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.cbxResultadoVIH)
        Me.Panel4.Controls.Add(Me.lblPruebaVIH)
        Me.Panel4.Controls.Add(Me.rbtPruebaVIHA)
        Me.Panel4.Controls.Add(Me.rbtPruebaVIHB)
        Me.Panel4.Location = New System.Drawing.Point(12, 19)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(688, 33)
        Me.Panel4.TabIndex = 142
        '
        'cbxResultadoVIH
        '
        Me.cbxResultadoVIH.Enabled = False
        Me.cbxResultadoVIH.FormattingEnabled = True
        Me.cbxResultadoVIH.Items.AddRange(New Object() {"Negativo", "Positivo"})
        Me.cbxResultadoVIH.Location = New System.Drawing.Point(427, 7)
        Me.cbxResultadoVIH.Name = "cbxResultadoVIH"
        Me.cbxResultadoVIH.Size = New System.Drawing.Size(246, 21)
        Me.cbxResultadoVIH.Sorted = True
        Me.cbxResultadoVIH.TabIndex = 145
        '
        'lblPruebaVIH
        '
        Me.lblPruebaVIH.AutoSize = True
        Me.lblPruebaVIH.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPruebaVIH.Location = New System.Drawing.Point(44, 10)
        Me.lblPruebaVIH.Name = "lblPruebaVIH"
        Me.lblPruebaVIH.Size = New System.Drawing.Size(109, 13)
        Me.lblPruebaVIH.TabIndex = 0
        Me.lblPruebaVIH.Text = "Prueba rapida de VIH"
        '
        'rbtPruebaVIHA
        '
        Me.rbtPruebaVIHA.AutoSize = True
        Me.rbtPruebaVIHA.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPruebaVIHA.Location = New System.Drawing.Point(359, 8)
        Me.rbtPruebaVIHA.Name = "rbtPruebaVIHA"
        Me.rbtPruebaVIHA.Size = New System.Drawing.Size(35, 17)
        Me.rbtPruebaVIHA.TabIndex = 144
        Me.rbtPruebaVIHA.TabStop = True
        Me.rbtPruebaVIHA.Text = "SI"
        Me.rbtPruebaVIHA.UseVisualStyleBackColor = True
        '
        'rbtPruebaVIHB
        '
        Me.rbtPruebaVIHB.AutoSize = True
        Me.rbtPruebaVIHB.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.rbtPruebaVIHB.Location = New System.Drawing.Point(187, 8)
        Me.rbtPruebaVIHB.Name = "rbtPruebaVIHB"
        Me.rbtPruebaVIHB.Size = New System.Drawing.Size(39, 17)
        Me.rbtPruebaVIHB.TabIndex = 143
        Me.rbtPruebaVIHB.TabStop = True
        Me.rbtPruebaVIHB.Text = "No"
        Me.rbtPruebaVIHB.UseVisualStyleBackColor = True
        '
        'txtEdad
        '
        Me.txtEdad.BackColor = System.Drawing.SystemColors.Window
        Me.txtEdad.Location = New System.Drawing.Point(223, 45)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(36, 20)
        Me.txtEdad.TabIndex = 2
        '
        'lblEdad
        '
        Me.lblEdad.AutoSize = True
        Me.lblEdad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEdad.Location = New System.Drawing.Point(193, 50)
        Me.lblEdad.Name = "lblEdad"
        Me.lblEdad.Size = New System.Drawing.Size(32, 13)
        Me.lblEdad.TabIndex = 0
        Me.lblEdad.Text = "Edad"
        '
        'gbxValoracion
        '
        Me.gbxValoracion.BackColor = System.Drawing.SystemColors.Window
        Me.gbxValoracion.Controls.Add(Me.txtCurp)
        Me.gbxValoracion.Controls.Add(Me.lblCurp)
        Me.gbxValoracion.Controls.Add(Me.cbxUnidad1)
        Me.gbxValoracion.Controls.Add(Me.lblAños)
        Me.gbxValoracion.Controls.Add(Me.txtfechaactual)
        Me.gbxValoracion.Controls.Add(Me.txtfechadeingreso)
        Me.gbxValoracion.Controls.Add(Me.lblFechaAhora)
        Me.gbxValoracion.Controls.Add(Me.rbtReferido)
        Me.gbxValoracion.Controls.Add(Me.txtEdad)
        Me.gbxValoracion.Controls.Add(Me.lblEdad)
        Me.gbxValoracion.Controls.Add(Me.cbxPaciente)
        Me.gbxValoracion.Controls.Add(Me.txtDerechohabiencia)
        Me.gbxValoracion.Controls.Add(Me.lblFechaIngreso)
        Me.gbxValoracion.Controls.Add(Me.lblDerechohabiencia)
        Me.gbxValoracion.Controls.Add(Me.lblPaciente)
        Me.gbxValoracion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxValoracion.Location = New System.Drawing.Point(13, 2)
        Me.gbxValoracion.Name = "gbxValoracion"
        Me.gbxValoracion.Size = New System.Drawing.Size(726, 120)
        Me.gbxValoracion.TabIndex = 0
        Me.gbxValoracion.TabStop = False
        Me.gbxValoracion.Text = " Valoración inicial urgencias "
        '
        'txtCurp
        '
        Me.txtCurp.BackColor = System.Drawing.SystemColors.Window
        Me.txtCurp.Location = New System.Drawing.Point(30, 45)
        Me.txtCurp.Name = "txtCurp"
        Me.txtCurp.ReadOnly = True
        Me.txtCurp.Size = New System.Drawing.Size(165, 20)
        Me.txtCurp.TabIndex = 112
        '
        'lblCurp
        '
        Me.lblCurp.AutoSize = True
        Me.lblCurp.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCurp.Location = New System.Drawing.Point(2, 45)
        Me.lblCurp.Name = "lblCurp"
        Me.lblCurp.Size = New System.Drawing.Size(29, 13)
        Me.lblCurp.TabIndex = 111
        Me.lblCurp.Text = "Curp"
        '
        'lblAños
        '
        Me.lblAños.AutoSize = True
        Me.lblAños.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblAños.Location = New System.Drawing.Point(265, 50)
        Me.lblAños.Name = "lblAños"
        Me.lblAños.Size = New System.Drawing.Size(30, 13)
        Me.lblAños.TabIndex = 20
        Me.lblAños.Text = "años"
        '
        'txtfechaactual
        '
        Me.txtfechaactual.BackColor = System.Drawing.SystemColors.Window
        Me.txtfechaactual.Location = New System.Drawing.Point(539, 88)
        Me.txtfechaactual.Name = "txtfechaactual"
        Me.txtfechaactual.ReadOnly = True
        Me.txtfechaactual.Size = New System.Drawing.Size(169, 20)
        Me.txtfechaactual.TabIndex = 19
        '
        'txtfechadeingreso
        '
        Me.txtfechadeingreso.BackColor = System.Drawing.SystemColors.Window
        Me.txtfechadeingreso.Location = New System.Drawing.Point(294, 88)
        Me.txtfechadeingreso.Name = "txtfechadeingreso"
        Me.txtfechadeingreso.ReadOnly = True
        Me.txtfechadeingreso.Size = New System.Drawing.Size(197, 20)
        Me.txtfechadeingreso.TabIndex = 18
        '
        'lblFechaAhora
        '
        Me.lblFechaAhora.AutoSize = True
        Me.lblFechaAhora.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaAhora.Location = New System.Drawing.Point(536, 72)
        Me.lblFechaAhora.Name = "lblFechaAhora"
        Me.lblFechaAhora.Size = New System.Drawing.Size(105, 13)
        Me.lblFechaAhora.TabIndex = 17
        Me.lblFechaAhora.Text = "Fecha de Valoración" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        '
        'rbtReferido
        '
        Me.rbtReferido.AutoSize = True
        Me.rbtReferido.Location = New System.Drawing.Point(648, 49)
        Me.rbtReferido.Name = "rbtReferido"
        Me.rbtReferido.Size = New System.Drawing.Size(74, 17)
        Me.rbtReferido.TabIndex = 14
        Me.rbtReferido.Text = "Referido"
        Me.rbtReferido.UseVisualStyleBackColor = True
        '
        'cbxPaciente
        '
        Me.cbxPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.cbxPaciente.FormattingEnabled = True
        Me.cbxPaciente.Location = New System.Drawing.Point(61, 19)
        Me.cbxPaciente.Name = "cbxPaciente"
        Me.cbxPaciente.Size = New System.Drawing.Size(653, 21)
        Me.cbxPaciente.TabIndex = 1
        '
        'txtDerechohabiencia
        '
        Me.txtDerechohabiencia.BackColor = System.Drawing.SystemColors.Window
        Me.txtDerechohabiencia.Location = New System.Drawing.Point(9, 88)
        Me.txtDerechohabiencia.Name = "txtDerechohabiencia"
        Me.txtDerechohabiencia.ReadOnly = True
        Me.txtDerechohabiencia.Size = New System.Drawing.Size(216, 20)
        Me.txtDerechohabiencia.TabIndex = 4
        '
        'lblFechaIngreso
        '
        Me.lblFechaIngreso.AutoSize = True
        Me.lblFechaIngreso.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblFechaIngreso.Location = New System.Drawing.Point(291, 72)
        Me.lblFechaIngreso.Name = "lblFechaIngreso"
        Me.lblFechaIngreso.Size = New System.Drawing.Size(89, 13)
        Me.lblFechaIngreso.TabIndex = 4
        Me.lblFechaIngreso.Text = "Fecha de ingreso"
        '
        'lblDerechohabiencia
        '
        Me.lblDerechohabiencia.AutoSize = True
        Me.lblDerechohabiencia.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDerechohabiencia.Location = New System.Drawing.Point(6, 72)
        Me.lblDerechohabiencia.Name = "lblDerechohabiencia"
        Me.lblDerechohabiencia.Size = New System.Drawing.Size(94, 13)
        Me.lblDerechohabiencia.TabIndex = 4
        Me.lblDerechohabiencia.Text = "Derechohabiencia"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaciente.Location = New System.Drawing.Point(2, 27)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(52, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente:"
        '
        'gbxPrioridadSug
        '
        Me.gbxPrioridadSug.BackColor = System.Drawing.SystemColors.Window
        Me.gbxPrioridadSug.Controls.Add(Me.pnlPrioridad)
        Me.gbxPrioridadSug.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPrioridadSug.Location = New System.Drawing.Point(14, 594)
        Me.gbxPrioridadSug.Name = "gbxPrioridadSug"
        Me.gbxPrioridadSug.Size = New System.Drawing.Size(315, 94)
        Me.gbxPrioridadSug.TabIndex = 4
        Me.gbxPrioridadSug.TabStop = False
        Me.gbxPrioridadSug.Text = " Prioridad sugerida "
        '
        'pnlPrioridad
        '
        Me.pnlPrioridad.BackColor = System.Drawing.Color.Transparent
        Me.pnlPrioridad.Controls.Add(Me.lblActuacion)
        Me.pnlPrioridad.Controls.Add(Me.lblDenominacion)
        Me.pnlPrioridad.Controls.Add(Me.lblNivel)
        Me.pnlPrioridad.Dock = System.Windows.Forms.DockStyle.Fill
        Me.pnlPrioridad.Location = New System.Drawing.Point(3, 16)
        Me.pnlPrioridad.Name = "pnlPrioridad"
        Me.pnlPrioridad.Size = New System.Drawing.Size(309, 75)
        Me.pnlPrioridad.TabIndex = 100
        '
        'lblActuacion
        '
        Me.lblActuacion.AutoSize = True
        Me.lblActuacion.Location = New System.Drawing.Point(3, 49)
        Me.lblActuacion.Name = "lblActuacion"
        Me.lblActuacion.Size = New System.Drawing.Size(126, 13)
        Me.lblActuacion.TabIndex = 5
        Me.lblActuacion.Text = "Tiempo de actuación"
        '
        'lblDenominacion
        '
        Me.lblDenominacion.AutoSize = True
        Me.lblDenominacion.Location = New System.Drawing.Point(3, 26)
        Me.lblDenominacion.Name = "lblDenominacion"
        Me.lblDenominacion.Size = New System.Drawing.Size(87, 13)
        Me.lblDenominacion.TabIndex = 4
        Me.lblDenominacion.Text = "Denominación"
        '
        'lblNivel
        '
        Me.lblNivel.AutoSize = True
        Me.lblNivel.Location = New System.Drawing.Point(3, 2)
        Me.lblNivel.Name = "lblNivel"
        Me.lblNivel.Size = New System.Drawing.Size(36, 13)
        Me.lblNivel.TabIndex = 3
        Me.lblNivel.Text = "Nivel"
        '
        'lblColor
        '
        Me.lblColor.AutoSize = True
        Me.lblColor.Location = New System.Drawing.Point(6, 19)
        Me.lblColor.Name = "lblColor"
        Me.lblColor.Size = New System.Drawing.Size(36, 13)
        Me.lblColor.TabIndex = 6
        Me.lblColor.Text = "Color"
        '
        'gbxEvaluacion
        '
        Me.gbxEvaluacion.Controls.Add(Me.lblColor)
        Me.gbxEvaluacion.Controls.Add(Me.btnEvaluar)
        Me.gbxEvaluacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxEvaluacion.Location = New System.Drawing.Point(336, 606)
        Me.gbxEvaluacion.Name = "gbxEvaluacion"
        Me.gbxEvaluacion.Size = New System.Drawing.Size(126, 79)
        Me.gbxEvaluacion.TabIndex = 99
        Me.gbxEvaluacion.TabStop = False
        Me.gbxEvaluacion.Text = "Evaluación"
        '
        'btnEvaluar
        '
        Me.btnEvaluar.Location = New System.Drawing.Point(6, 49)
        Me.btnEvaluar.Name = "btnEvaluar"
        Me.btnEvaluar.Size = New System.Drawing.Size(117, 23)
        Me.btnEvaluar.TabIndex = 15
        Me.btnEvaluar.Text = "Evaluar"
        Me.btnEvaluar.UseVisualStyleBackColor = True
        '
        'gbxPrioridadSel
        '
        Me.gbxPrioridadSel.BackColor = System.Drawing.SystemColors.Window
        Me.gbxPrioridadSel.Controls.Add(Me.btnAceptar)
        Me.gbxPrioridadSel.Controls.Add(Me.LblTipoUrgencia)
        Me.gbxPrioridadSel.Controls.Add(Me.cbxTipoUrgencia)
        Me.gbxPrioridadSel.Controls.Add(Me.lblNivelAsig)
        Me.gbxPrioridadSel.Controls.Add(Me.cbxPrioridadSel)
        Me.gbxPrioridadSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPrioridadSel.Location = New System.Drawing.Point(470, 596)
        Me.gbxPrioridadSel.Name = "gbxPrioridadSel"
        Me.gbxPrioridadSel.Size = New System.Drawing.Size(276, 85)
        Me.gbxPrioridadSel.TabIndex = 100
        Me.gbxPrioridadSel.TabStop = False
        Me.gbxPrioridadSel.Text = " Seleccionar prioridad "
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(171, 58)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(95, 23)
        Me.btnAceptar.TabIndex = 18
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'LblTipoUrgencia
        '
        Me.LblTipoUrgencia.AutoSize = True
        Me.LblTipoUrgencia.Location = New System.Drawing.Point(6, 25)
        Me.LblTipoUrgencia.Name = "LblTipoUrgencia"
        Me.LblTipoUrgencia.Size = New System.Drawing.Size(103, 13)
        Me.LblTipoUrgencia.TabIndex = 17
        Me.LblTipoUrgencia.Text = "Tipo de urgencia"
        '
        'cbxTipoUrgencia
        '
        Me.cbxTipoUrgencia.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxTipoUrgencia.FormattingEnabled = True
        Me.cbxTipoUrgencia.Items.AddRange(New Object() {"AEV", "UC", "UNC"})
        Me.cbxTipoUrgencia.Location = New System.Drawing.Point(112, 22)
        Me.cbxTipoUrgencia.Name = "cbxTipoUrgencia"
        Me.cbxTipoUrgencia.Size = New System.Drawing.Size(154, 21)
        Me.cbxTipoUrgencia.TabIndex = 15
        '
        'lblNivelAsig
        '
        Me.lblNivelAsig.AutoSize = True
        Me.lblNivelAsig.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNivelAsig.Location = New System.Drawing.Point(6, 63)
        Me.lblNivelAsig.Name = "lblNivelAsig"
        Me.lblNivelAsig.Size = New System.Drawing.Size(36, 13)
        Me.lblNivelAsig.TabIndex = 11
        Me.lblNivelAsig.Text = "Nivel"
        '
        'cbxPrioridadSel
        '
        Me.cbxPrioridadSel.BackColor = System.Drawing.SystemColors.Window
        Me.cbxPrioridadSel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPrioridadSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPrioridadSel.FormattingEnabled = True
        Me.cbxPrioridadSel.Items.AddRange(New Object() {"1-Rojo", "2-Amarillo", "3-Verde"})
        Me.cbxPrioridadSel.Location = New System.Drawing.Point(48, 60)
        Me.cbxPrioridadSel.Name = "cbxPrioridadSel"
        Me.cbxPrioridadSel.Size = New System.Drawing.Size(118, 21)
        Me.cbxPrioridadSel.TabIndex = 16
        '
        'cbxUnidad1
        '
        Me.cbxUnidad1.Enabled = False
        Me.cbxUnidad1.FormattingEnabled = True
        Me.cbxUnidad1.Location = New System.Drawing.Point(294, 45)
        Me.cbxUnidad1.Name = "cbxUnidad1"
        Me.cbxUnidad1.Size = New System.Drawing.Size(347, 21)
        Me.cbxUnidad1.TabIndex = 110
        Me.cbxUnidad1.Visible = False
        '
        'frmEvaluarObstetrico
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(754, 688)
        Me.Controls.Add(Me.gbxEvaluacion)
        Me.Controls.Add(Me.gbxPrioridadSel)
        Me.Controls.Add(Me.gbxPrioridadSug)
        Me.Controls.Add(Me.gbxValoracion)
        Me.Controls.Add(Me.tabObstetrico)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmEvaluarObstetrico"
        Me.Text = "text"
        Me.tabObstetrico.ResumeLayout(False)
        Me.tabPagTriage.ResumeLayout(False)
        Me.tabPagTriage.PerformLayout()
        Me.gbxSignosVitales.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.GroupBox3.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox2.PerformLayout()
        Me.gbxSintomas.ResumeLayout(False)
        Me.pnlPalidez.ResumeLayout(False)
        Me.pnlPalidez.PerformLayout()
        Me.pnlEstConciencia.ResumeLayout(False)
        Me.pnlEstConciencia.PerformLayout()
        Me.pnlDisnea.ResumeLayout(False)
        Me.pnlDisnea.PerformLayout()
        Me.pnlNausea.ResumeLayout(False)
        Me.pnlNausea.PerformLayout()
        Me.pnlHemorragia.ResumeLayout(False)
        Me.pnlHemorragia.PerformLayout()
        Me.pnlDolorEpigastrico.ResumeLayout(False)
        Me.pnlDolorEpigastrico.PerformLayout()
        Me.pnlDolorObstetrico.ResumeLayout(False)
        Me.pnlDolorObstetrico.PerformLayout()
        Me.tabPagInterrogatorio.ResumeLayout(False)
        Me.tabPagInterrogatorio.PerformLayout()
        Me.pnlSangradoTrans.ResumeLayout(False)
        Me.Panel10.ResumeLayout(False)
        Me.Panel10.PerformLayout()
        Me.Panel9.ResumeLayout(False)
        Me.Panel9.PerformLayout()
        Me.Panel8.ResumeLayout(False)
        Me.Panel8.PerformLayout()
        Me.Panel7.ResumeLayout(False)
        Me.Panel7.PerformLayout()
        Me.Panel6.ResumeLayout(False)
        Me.Panel6.PerformLayout()
        Me.Panel5.ResumeLayout(False)
        Me.Panel5.PerformLayout()
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.pnlHiper.ResumeLayout(False)
        Me.pnlHiper.PerformLayout()
        Me.pnlSudoracion.ResumeLayout(False)
        Me.pnlSudoracion.PerformLayout()
        Me.pnlCefalea.ResumeLayout(False)
        Me.pnlCefalea.PerformLayout()
        Me.gbxInfoEmbarazo.ResumeLayout(False)
        Me.pnlSangrado.ResumeLayout(False)
        Me.pnlSangrado.PerformLayout()
        Me.pnlCesarea.ResumeLayout(False)
        Me.pnlCesarea.PerformLayout()
        Me.pnlTrauma.ResumeLayout(False)
        Me.pnlTrauma.PerformLayout()
        Me.TabpagObservaciones.ResumeLayout(False)
        Me.gbxEnfAsociadas.ResumeLayout(False)
        Me.pnlHemopatia.ResumeLayout(False)
        Me.pnlHemopatia.PerformLayout()
        Me.pnlEndocrinopatia.ResumeLayout(False)
        Me.pnlEndocrinopatia.PerformLayout()
        Me.pnlNefropatia.ResumeLayout(False)
        Me.pnlNefropatia.PerformLayout()
        Me.pnlCardiopatia.ResumeLayout(False)
        Me.pnlCardiopatia.PerformLayout()
        Me.pnlHepatopatia.ResumeLayout(False)
        Me.pnlHepatopatia.PerformLayout()
        Me.GroupBox1.ResumeLayout(False)
        Me.Panel2.ResumeLayout(False)
        Me.Panel2.PerformLayout()
        Me.Panel3.ResumeLayout(False)
        Me.Panel3.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        Me.Panel4.PerformLayout()
        Me.gbxValoracion.ResumeLayout(False)
        Me.gbxValoracion.PerformLayout()
        Me.gbxPrioridadSug.ResumeLayout(False)
        Me.pnlPrioridad.ResumeLayout(False)
        Me.pnlPrioridad.PerformLayout()
        Me.gbxEvaluacion.ResumeLayout(False)
        Me.gbxEvaluacion.PerformLayout()
        Me.gbxPrioridadSel.ResumeLayout(False)
        Me.gbxPrioridadSel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents tabPagTriage As System.Windows.Forms.TabPage
    Friend WithEvents tabPagInterrogatorio As System.Windows.Forms.TabPage
    Friend WithEvents gbxSignosVitales As System.Windows.Forms.GroupBox
    Friend WithEvents gbxInfoEmbarazo As System.Windows.Forms.GroupBox
    Friend WithEvents gbxValoracion As System.Windows.Forms.GroupBox
    Friend WithEvents cbxPaciente As System.Windows.Forms.ComboBox
    Friend WithEvents txtDerechohabiencia As System.Windows.Forms.TextBox
    Friend WithEvents txtEdad As System.Windows.Forms.TextBox
    Friend WithEvents lblFechaIngreso As System.Windows.Forms.Label
    Friend WithEvents lblDerechohabiencia As System.Windows.Forms.Label
    Friend WithEvents lblEdad As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents gbxPrioridadSug As System.Windows.Forms.GroupBox
    Friend WithEvents pnlPrioridad As System.Windows.Forms.Panel
    Friend WithEvents lblColor As System.Windows.Forms.Label
    Friend WithEvents lblActuacion As System.Windows.Forms.Label
    Friend WithEvents lblDenominacion As System.Windows.Forms.Label
    Friend WithEvents lblNivel As System.Windows.Forms.Label
    Friend WithEvents gbxEvaluacion As System.Windows.Forms.GroupBox
    Friend WithEvents btnEvaluar As System.Windows.Forms.Button
    Friend WithEvents gbxSintomas As System.Windows.Forms.GroupBox
    Friend WithEvents pnlDolorObstetrico As System.Windows.Forms.Panel
    Friend WithEvents pnlDolorEpigastrico As System.Windows.Forms.Panel
    Friend WithEvents pnlHemorragia As System.Windows.Forms.Panel
    Friend WithEvents pnlNausea As System.Windows.Forms.Panel
    Friend WithEvents pnlDisnea As System.Windows.Forms.Panel
    Friend WithEvents rbtDolorObstetricoA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtDolorObstetricoM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtDolorObstetricoB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCrisisConvulsivaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCrisisConvulsivaB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHemorragiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHemorragiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHemorragiaB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNauseaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNauseaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNauseaB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtRespiracionA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtRespiracionM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtRespiracionB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlSangrado As System.Windows.Forms.Panel
    Friend WithEvents pnlCesarea As System.Windows.Forms.Panel
    Friend WithEvents pnlTrauma As System.Windows.Forms.Panel
    Friend WithEvents rbtCirUterinaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCirUterinaB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtTraumaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtTraumaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtTraumaB As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSangradoA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSangradoM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSangradoB As System.Windows.Forms.RadioButton
    Friend WithEvents lblRespiracon1 As System.Windows.Forms.Label
    Friend WithEvents lblNausea As System.Windows.Forms.Label
    Friend WithEvents lblHemorragia As System.Windows.Forms.Label
    Friend WithEvents lblCrisisConvulsiva As System.Windows.Forms.Label
    Friend WithEvents lblDolorObstetrico As System.Windows.Forms.Label
    Friend WithEvents lblSangrado As System.Windows.Forms.Label
    Friend WithEvents lblCirUterina As System.Windows.Forms.Label
    Friend WithEvents lblTrauma As System.Windows.Forms.Label
    Friend WithEvents gbxPrioridadSel As System.Windows.Forms.GroupBox
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
    Friend WithEvents LblTipoUrgencia As System.Windows.Forms.Label
    Friend WithEvents cbxTipoUrgencia As System.Windows.Forms.ComboBox
    Friend WithEvents lblNivelAsig As System.Windows.Forms.Label
    Friend WithEvents cbxPrioridadSel As System.Windows.Forms.ComboBox
    Friend WithEvents txtFCFFeto As System.Windows.Forms.TextBox
    Friend WithEvents lblPulsoFeto As System.Windows.Forms.Label
    Friend WithEvents lblFCFeto As System.Windows.Forms.Label
    Friend WithEvents TabpagObservaciones As System.Windows.Forms.TabPage
    Friend WithEvents rbtReferido As System.Windows.Forms.CheckBox
    Friend WithEvents lblFechaAhora As System.Windows.Forms.Label
    Friend WithEvents txtfechaactual As System.Windows.Forms.TextBox
    Friend WithEvents txtfechadeingreso As System.Windows.Forms.TextBox
    Friend WithEvents lblAños As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents txtEdadGestacional As System.Windows.Forms.TextBox
    Friend WithEvents lblEdadGestacional As System.Windows.Forms.Label
    Friend WithEvents pnlSangradoTrans As System.Windows.Forms.GroupBox
    Friend WithEvents pnlHiper As System.Windows.Forms.Panel
    Friend WithEvents lblAcufos As System.Windows.Forms.Label
    Friend WithEvents rbtAcufenosA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtAcufenosB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlSudoracion As System.Windows.Forms.Panel
    Friend WithEvents lblamaurosis As System.Windows.Forms.Label
    Friend WithEvents rbtAmaurosisA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtAmaurosisB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlCefalea As System.Windows.Forms.Panel
    Friend WithEvents lblCefalea As System.Windows.Forms.Label
    Friend WithEvents rbtCefaleaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCefaleaB As System.Windows.Forms.RadioButton
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents lblTiraUroanalisis As System.Windows.Forms.Label
    Friend WithEvents rbtTiraUroanalisisM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtTiraUroanalisisB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel3 As System.Windows.Forms.Panel
    Friend WithEvents cbxResultadoSIFILIS As System.Windows.Forms.ComboBox
    Friend WithEvents lblPruebaSifilis As System.Windows.Forms.Label
    Friend WithEvents rbtPruebaSifilisA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtPruebaSifilisB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents cbxResultadoVIH As System.Windows.Forms.ComboBox
    Friend WithEvents lblPruebaVIH As System.Windows.Forms.Label
    Friend WithEvents rbtPruebaVIHA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtPruebaVIHB As System.Windows.Forms.RadioButton
    Friend WithEvents txtCurp As System.Windows.Forms.TextBox
    Friend WithEvents lblCurp As System.Windows.Forms.Label
    Friend WithEvents rbtCefaleaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtTiraUroanalisisA As System.Windows.Forms.RadioButton
    Friend WithEvents pnlEstConciencia As System.Windows.Forms.Panel
    Friend WithEvents lblEstadoConciencia As System.Windows.Forms.Label
    Friend WithEvents rbtEstadoConcienciaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtEstadoConcienciaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtEstadoConcienciaB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlPalidez As System.Windows.Forms.Panel
    Friend WithEvents lblPalidez As System.Windows.Forms.Label
    Friend WithEvents rbtPalidezA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtPalidezM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtPalidezB As System.Windows.Forms.RadioButton
    Friend WithEvents lblsangradotrans As System.Windows.Forms.Label
    Friend WithEvents rbtSangradotransA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSangradotransM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSangradotransB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel5 As System.Windows.Forms.Panel
    Friend WithEvents lblfosfenos As System.Windows.Forms.Label
    Friend WithEvents rbtFosfenosA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtFosfenosB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel6 As System.Windows.Forms.Panel
    Friend WithEvents lblepigastralgia As System.Windows.Forms.Label
    Friend WithEvents rbtEpigastragiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtEpigastragiaB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel10 As System.Windows.Forms.Panel
    Friend WithEvents lblfetal As System.Windows.Forms.Label
    Friend WithEvents rbtMotilidadFetalA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtMotilidadFetalM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtMotilidadFetalB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel9 As System.Windows.Forms.Panel
    Friend WithEvents Panel8 As System.Windows.Forms.Panel
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents RadioButton13 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton14 As System.Windows.Forms.RadioButton
    Friend WithEvents RadioButton15 As System.Windows.Forms.RadioButton
    Friend WithEvents lblsindromefebril As System.Windows.Forms.Label
    Friend WithEvents rbtSindromeFebrilA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtSindromeFebrilB As System.Windows.Forms.RadioButton
    Friend WithEvents Panel7 As System.Windows.Forms.Panel
    Friend WithEvents lblliquidoamn As System.Windows.Forms.Label
    Friend WithEvents rbtLiquidoAmnioticoA2 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtLiquidoAmnioticoA1 As System.Windows.Forms.RadioButton
    Friend WithEvents rbtLiquidoAmnioticoB As System.Windows.Forms.RadioButton
    Friend WithEvents gbxEnfAsociadas As System.Windows.Forms.GroupBox
    Friend WithEvents pnlHemopatia As System.Windows.Forms.Panel
    Friend WithEvents lblHematopatia As System.Windows.Forms.Label
    Friend WithEvents rbtHematopatiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHematopatiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHematopatiaB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlEndocrinopatia As System.Windows.Forms.Panel
    Friend WithEvents lblEndocrinopatia As System.Windows.Forms.Label
    Friend WithEvents rbtEndocrinopatiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtEndocrinopatiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtEndocrinopatiaB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlNefropatia As System.Windows.Forms.Panel
    Friend WithEvents lblNefropatia As System.Windows.Forms.Label
    Friend WithEvents rbtNefropatiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNefropatiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNefropatiaB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlCardiopatia As System.Windows.Forms.Panel
    Friend WithEvents lblCardiopatia As System.Windows.Forms.Label
    Friend WithEvents rbtCardiopatiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCardiopatiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtCardiopatiaB As System.Windows.Forms.RadioButton
    Friend WithEvents pnlHepatopatia As System.Windows.Forms.Panel
    Friend WithEvents lblHepatopatia As System.Windows.Forms.Label
    Friend WithEvents rbtHepatopatiaA As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHepatopatiaM As System.Windows.Forms.RadioButton
    Friend WithEvents rbtHepatopatiaB As System.Windows.Forms.RadioButton
    Friend WithEvents txtMotAtencion As System.Windows.Forms.TextBox
    Friend WithEvents lblMotatencion As System.Windows.Forms.Label
    Friend WithEvents lblObservaciones As System.Windows.Forms.Label
    Friend WithEvents Textvaloracion As System.Windows.Forms.TextBox
    Friend WithEvents Panel1 As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents GroupBox3 As System.Windows.Forms.GroupBox
    Friend WithEvents GroupBox2 As System.Windows.Forms.GroupBox
    Friend WithEvents lblPresionA As System.Windows.Forms.Label
    Friend WithEvents lblrpm As System.Windows.Forms.Label
    Friend WithEvents lblM As System.Windows.Forms.Label
    Friend WithEvents lblIndChoque As System.Windows.Forms.Label
    Friend WithEvents lblFrecCard As System.Windows.Forms.Label
    Friend WithEvents lblMmHg As System.Windows.Forms.Label
    Friend WithEvents txtPaSistolica As System.Windows.Forms.TextBox
    Friend WithEvents txtPaDiastolica As System.Windows.Forms.TextBox
    Friend WithEvents Label20 As System.Windows.Forms.Label
    Friend WithEvents txtFrecCard As System.Windows.Forms.TextBox
    Friend WithEvents lblRespiracion As System.Windows.Forms.Label
    Friend WithEvents lblKg As System.Windows.Forms.Label
    Friend WithEvents lblIMC As System.Windows.Forms.Label
    Friend WithEvents txtRespiracion As System.Windows.Forms.TextBox
    Friend WithEvents txtPeso As System.Windows.Forms.TextBox
    Friend WithEvents lblPeso As System.Windows.Forms.Label
    Friend WithEvents lblTemp As System.Windows.Forms.Label
    Friend WithEvents txtGlucosa As System.Windows.Forms.TextBox
    Friend WithEvents lblGradosC As System.Windows.Forms.Label
    Friend WithEvents txtAltura As System.Windows.Forms.TextBox
    Friend WithEvents txtTemperatura As System.Windows.Forms.TextBox
    Friend WithEvents lblGlucosa As System.Windows.Forms.Label
    Friend WithEvents lblAltura As System.Windows.Forms.Label
    Friend WithEvents txtIndChoque As System.Windows.Forms.TextBox
    Friend WithEvents txtIMC As System.Windows.Forms.TextBox
    Public WithEvents tabObstetrico As System.Windows.Forms.TabControl
    Public WithEvents lblPulso As System.Windows.Forms.Label
    Friend WithEvents cbxUnidad1 As System.Windows.Forms.ComboBox
End Class
