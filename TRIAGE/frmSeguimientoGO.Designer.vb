﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmSeguimientoGO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dvgNoatendido = New System.Windows.Forms.DataGridView()
        Me.Num_Control = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.valoracion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.fehaat = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.estado = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idpaciente2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Fechaatencion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idatencion1 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.gbxPaciente = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtcama = New System.Windows.Forms.TextBox()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.txtPrioridad = New System.Windows.Forms.TextBox()
        Me.lblPrioridad = New System.Windows.Forms.Label()
        Me.lblPaciente = New System.Windows.Forms.Label()
        Me.txtRegTriageGo = New System.Windows.Forms.Button()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.NotifyIcon1 = New System.Windows.Forms.NotifyIcon(Me.components)
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTipPersonal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.txtiempo = New System.Windows.Forms.TextBox()
        Me.txthoras = New System.Windows.Forms.Label()
        Me.lbltiempo = New System.Windows.Forms.Label()
        Me.ShapeContainer1 = New Microsoft.VisualBasic.PowerPacks.ShapeContainer()
        Me.RectangleShape3 = New Microsoft.VisualBasic.PowerPacks.RectangleShape()
        Me.Label4 = New System.Windows.Forms.Label()
        CType(Me.dvgNoatendido, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.SuspendLayout()
        '
        'dvgNoatendido
        '
        Me.dvgNoatendido.AllowUserToAddRows = False
        Me.dvgNoatendido.AllowUserToDeleteRows = False
        Me.dvgNoatendido.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        Me.dvgNoatendido.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dvgNoatendido.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Num_Control, Me.Nombre, Me.valoracion, Me.fehaat, Me.estado, Me.idpaciente2, Me.Fechaatencion, Me.idatencion1})
        Me.dvgNoatendido.Location = New System.Drawing.Point(12, 127)
        Me.dvgNoatendido.Name = "dvgNoatendido"
        Me.dvgNoatendido.ReadOnly = True
        Me.dvgNoatendido.Size = New System.Drawing.Size(843, 274)
        Me.dvgNoatendido.TabIndex = 0
        '
        'Num_Control
        '
        Me.Num_Control.Frozen = True
        Me.Num_Control.HeaderText = "Numero de  control"
        Me.Num_Control.Name = "Num_Control"
        Me.Num_Control.ReadOnly = True
        Me.Num_Control.Width = 80
        '
        'Nombre
        '
        Me.Nombre.Frozen = True
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 200
        '
        'valoracion
        '
        Me.valoracion.Frozen = True
        Me.valoracion.HeaderText = "valoracion"
        Me.valoracion.Name = "valoracion"
        Me.valoracion.ReadOnly = True
        '
        'fehaat
        '
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.fehaat.DefaultCellStyle = DataGridViewCellStyle1
        Me.fehaat.Frozen = True
        Me.fehaat.HeaderText = "Tiempo Transcurrido"
        Me.fehaat.MinimumWidth = 2
        Me.fehaat.Name = "fehaat"
        Me.fehaat.ReadOnly = True
        '
        'estado
        '
        Me.estado.Frozen = True
        Me.estado.HeaderText = "Estado"
        Me.estado.Name = "estado"
        Me.estado.ReadOnly = True
        '
        'idpaciente2
        '
        Me.idpaciente2.Frozen = True
        Me.idpaciente2.HeaderText = "curp"
        Me.idpaciente2.Name = "idpaciente2"
        Me.idpaciente2.ReadOnly = True
        '
        'Fechaatencion
        '
        Me.Fechaatencion.HeaderText = "Fecha de Atencion"
        Me.Fechaatencion.Name = "Fechaatencion"
        Me.Fechaatencion.ReadOnly = True
        Me.Fechaatencion.Visible = False
        '
        'idatencion1
        '
        Me.idatencion1.HeaderText = "idatencion"
        Me.idatencion1.Name = "idatencion1"
        Me.idatencion1.ReadOnly = True
        Me.idatencion1.Visible = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TRIAGE.My.Resources.Resources.triage
        Me.PictureBox1.Location = New System.Drawing.Point(227, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(400, 94)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.Color.Transparent
        Me.gbxPaciente.Controls.Add(Me.Label1)
        Me.gbxPaciente.Controls.Add(Me.txtcama)
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(12, 417)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(604, 86)
        Me.gbxPaciente.TabIndex = 14
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(473, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(45, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Motivo"
        '
        'txtcama
        '
        Me.txtcama.BackColor = System.Drawing.SystemColors.Window
        Me.txtcama.Location = New System.Drawing.Point(476, 41)
        Me.txtcama.Name = "txtcama"
        Me.txtcama.ReadOnly = True
        Me.txtcama.Size = New System.Drawing.Size(116, 20)
        Me.txtcama.TabIndex = 4
        Me.txtcama.TabStop = False
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(122, 41)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(348, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtPrioridad
        '
        Me.txtPrioridad.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrioridad.Location = New System.Drawing.Point(6, 41)
        Me.txtPrioridad.Name = "txtPrioridad"
        Me.txtPrioridad.ReadOnly = True
        Me.txtPrioridad.Size = New System.Drawing.Size(110, 20)
        Me.txtPrioridad.TabIndex = 3
        Me.txtPrioridad.TabStop = False
        '
        'lblPrioridad
        '
        Me.lblPrioridad.AutoSize = True
        Me.lblPrioridad.Location = New System.Drawing.Point(3, 25)
        Me.lblPrioridad.Name = "lblPrioridad"
        Me.lblPrioridad.Size = New System.Drawing.Size(57, 13)
        Me.lblPrioridad.TabIndex = 2
        Me.lblPrioridad.Text = "Prioridad"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(119, 25)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'txtRegTriageGo
        '
        Me.txtRegTriageGo.Location = New System.Drawing.Point(666, 442)
        Me.txtRegTriageGo.Name = "txtRegTriageGo"
        Me.txtRegTriageGo.Size = New System.Drawing.Size(151, 50)
        Me.txtRegTriageGo.TabIndex = 15
        Me.txtRegTriageGo.Text = "Regresar" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & " al" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "Triage"
        Me.txtRegTriageGo.UseVisualStyleBackColor = True
        '
        'Timer1
        '
        '
        'NotifyIcon1
        '
        Me.NotifyIcon1.Text = "NotifyIcon1"
        Me.NotifyIcon1.Visible = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor, Me.ToolStripStatusLabel4, Me.lblTipPersonal})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 511)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(878, 22)
        Me.StatusStrip1.TabIndex = 17
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(54, 17)
        Me.ToolStripStatusLabel4.Text = "personal"
        '
        'lblTipPersonal
        '
        Me.lblTipPersonal.BackColor = System.Drawing.Color.Transparent
        Me.lblTipPersonal.Name = "lblTipPersonal"
        Me.lblTipPersonal.Size = New System.Drawing.Size(25, 17)
        Me.lblTipPersonal.Text = "tipo"
        '
        'txtiempo
        '
        Me.txtiempo.Location = New System.Drawing.Point(787, 101)
        Me.txtiempo.Name = "txtiempo"
        Me.txtiempo.Size = New System.Drawing.Size(68, 20)
        Me.txtiempo.TabIndex = 18
        Me.txtiempo.Visible = False
        '
        'txthoras
        '
        Me.txthoras.AutoSize = True
        Me.txthoras.Location = New System.Drawing.Point(746, 104)
        Me.txthoras.Name = "txthoras"
        Me.txthoras.Size = New System.Drawing.Size(35, 13)
        Me.txthoras.TabIndex = 19
        Me.txthoras.Text = "Horas"
        Me.txthoras.Visible = False
        '
        'lbltiempo
        '
        Me.lbltiempo.AutoSize = True
        Me.lbltiempo.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbltiempo.Location = New System.Drawing.Point(784, 82)
        Me.lbltiempo.Name = "lbltiempo"
        Me.lbltiempo.Size = New System.Drawing.Size(61, 16)
        Me.lbltiempo.TabIndex = 20
        Me.lbltiempo.Text = "Tiempo"
        Me.lbltiempo.Visible = False
        '
        'ShapeContainer1
        '
        Me.ShapeContainer1.Location = New System.Drawing.Point(0, 0)
        Me.ShapeContainer1.Margin = New System.Windows.Forms.Padding(0)
        Me.ShapeContainer1.Name = "ShapeContainer1"
        Me.ShapeContainer1.Shapes.AddRange(New Microsoft.VisualBasic.PowerPacks.Shape() {Me.RectangleShape3})
        Me.ShapeContainer1.Size = New System.Drawing.Size(878, 533)
        Me.ShapeContainer1.TabIndex = 21
        Me.ShapeContainer1.TabStop = False
        '
        'RectangleShape3
        '
        Me.RectangleShape3.BackColor = System.Drawing.Color.Red
        Me.RectangleShape3.BackStyle = Microsoft.VisualBasic.PowerPacks.BackStyle.Opaque
        Me.RectangleShape3.Location = New System.Drawing.Point(621, 416)
        Me.RectangleShape3.Name = "RectangleShape3"
        Me.RectangleShape3.Size = New System.Drawing.Size(15, 13)
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.Label4.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.Label4.Location = New System.Drawing.Point(640, 419)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(117, 13)
        Me.Label4.TabIndex = 22
        Me.Label4.Text = "Proximo a Desaparecer"
        Me.Label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'frmSeguimientoGO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.ButtonHighlight
        Me.ClientSize = New System.Drawing.Size(878, 533)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbltiempo)
        Me.Controls.Add(Me.txthoras)
        Me.Controls.Add(Me.txtiempo)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.txtRegTriageGo)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dvgNoatendido)
        Me.Controls.Add(Me.ShapeContainer1)
        Me.Name = "frmSeguimientoGO"
        Me.Text = "Seguimiento "
        CType(Me.dvgNoatendido, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dvgNoatendido As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtcama As System.Windows.Forms.TextBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents txtPrioridad As System.Windows.Forms.TextBox
    Friend WithEvents lblPrioridad As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents txtRegTriageGo As System.Windows.Forms.Button
    Friend WithEvents idpacietne As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents NotifyIcon1 As System.Windows.Forms.NotifyIcon
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents txtiempo As System.Windows.Forms.TextBox
    Friend WithEvents txthoras As System.Windows.Forms.Label
    Friend WithEvents lbltiempo As System.Windows.Forms.Label
    Friend WithEvents ShapeContainer1 As Microsoft.VisualBasic.PowerPacks.ShapeContainer
    Friend WithEvents RectangleShape3 As Microsoft.VisualBasic.PowerPacks.RectangleShape
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTipPersonal As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents Num_Control As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents valoracion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents fehaat As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents estado As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idpaciente2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Fechaatencion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idatencion1 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
