<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmPrincipalTriage
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.menugineco = New System.Windows.Forms.MenuStrip()
        Me.HerramientasToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePacientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripMenuItem1 = New System.Windows.Forms.ToolStripSeparator()
        Me.EvaluarPacientesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.CambiarPrioridadToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.SeguimientoToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ReportesToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TSCodigoAzulToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.TrabajoSocialToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.ListadoDePacientesTSToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.tsBtnUsuario = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripStatusLabel2 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsLblUsuario = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripSplitButton1 = New System.Windows.Forms.ToolStripSplitButton()
        Me.ToolStripTextBox1 = New System.Windows.Forms.ToolStripTextBox()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.tsLblConexion = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel3 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTipoPersonal = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblTipo = New System.Windows.Forms.Label()
        Me.btncrsesion = New System.Windows.Forms.Button()
        Me.triage = New System.Windows.Forms.Label()
        Me.pnlGineco = New System.Windows.Forms.Panel()
        Me.lblRevaluarGO = New System.Windows.Forms.Label()
        Me.lblLisPaciente = New System.Windows.Forms.Label()
        Me.lblGO = New System.Windows.Forms.Label()
        Me.btnLisPaciente = New System.Windows.Forms.Button()
        Me.btnRevaluarGO = New System.Windows.Forms.Button()
        Me.Panel2 = New System.Windows.Forms.Panel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btnGO = New System.Windows.Forms.Button()
        Me.pnlGineco1 = New System.Windows.Forms.Panel()
        Me.lblRepyEst = New System.Windows.Forms.Label()
        Me.lblCodVerde = New System.Windows.Forms.Label()
        Me.btnSeguimiento = New System.Windows.Forms.Button()
        Me.lblSeguimiento = New System.Windows.Forms.Label()
        Me.btnRepyEst = New System.Windows.Forms.Button()
        Me.btnCodVerde = New System.Windows.Forms.Button()
        Me.Panel4 = New System.Windows.Forms.Panel()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.PictureBox3 = New System.Windows.Forms.PictureBox()
        Me.imghospital = New System.Windows.Forms.PictureBox()
        Me.menugineco.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.pnlGineco.SuspendLayout()
        Me.Panel2.SuspendLayout()
        Me.pnlGineco1.SuspendLayout()
        Me.Panel4.SuspendLayout()
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.imghospital, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'menugineco
        '
        Me.menugineco.BackColor = System.Drawing.SystemColors.GradientInactiveCaption
        Me.menugineco.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.HerramientasToolStripMenuItem, Me.ReportesToolStripMenuItem, Me.TrabajoSocialToolStripMenuItem})
        Me.menugineco.Location = New System.Drawing.Point(0, 0)
        Me.menugineco.Name = "menugineco"
        Me.menugineco.Size = New System.Drawing.Size(665, 24)
        Me.menugineco.TabIndex = 0
        Me.menugineco.Text = "MenuStrip1"
        '
        'HerramientasToolStripMenuItem
        '
        Me.HerramientasToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDePacientesToolStripMenuItem, Me.ToolStripMenuItem1, Me.EvaluarPacientesToolStripMenuItem, Me.CambiarPrioridadToolStripMenuItem, Me.SeguimientoToolStripMenuItem})
        Me.HerramientasToolStripMenuItem.Name = "HerramientasToolStripMenuItem"
        Me.HerramientasToolStripMenuItem.Size = New System.Drawing.Size(83, 20)
        Me.HerramientasToolStripMenuItem.Text = "Herramientas"
        '
        'ListadoDePacientesToolStripMenuItem
        '
        Me.ListadoDePacientesToolStripMenuItem.Name = "ListadoDePacientesToolStripMenuItem"
        Me.ListadoDePacientesToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ListadoDePacientesToolStripMenuItem.Text = "Listado de pacientes"
        '
        'ToolStripMenuItem1
        '
        Me.ToolStripMenuItem1.Name = "ToolStripMenuItem1"
        Me.ToolStripMenuItem1.Size = New System.Drawing.Size(169, 6)
        '
        'EvaluarPacientesToolStripMenuItem
        '
        Me.EvaluarPacientesToolStripMenuItem.Name = "EvaluarPacientesToolStripMenuItem"
        Me.EvaluarPacientesToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.EvaluarPacientesToolStripMenuItem.Text = "Evaluar pacientes"
        '
        'CambiarPrioridadToolStripMenuItem
        '
        Me.CambiarPrioridadToolStripMenuItem.Name = "CambiarPrioridadToolStripMenuItem"
        Me.CambiarPrioridadToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.CambiarPrioridadToolStripMenuItem.Text = "Cambiar prioridad"
        '
        'SeguimientoToolStripMenuItem
        '
        Me.SeguimientoToolStripMenuItem.Name = "SeguimientoToolStripMenuItem"
        Me.SeguimientoToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.SeguimientoToolStripMenuItem.Text = "Seguimiento"
        '
        'ReportesToolStripMenuItem
        '
        Me.ReportesToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.TSCodigoAzulToolStripMenuItem})
        Me.ReportesToolStripMenuItem.Name = "ReportesToolStripMenuItem"
        Me.ReportesToolStripMenuItem.Size = New System.Drawing.Size(63, 20)
        Me.ReportesToolStripMenuItem.Text = "Reportes"
        '
        'TSCodigoAzulToolStripMenuItem
        '
        Me.TSCodigoAzulToolStripMenuItem.Image = Global.TRIAGE.My.Resources.Resources.DocumentHS
        Me.TSCodigoAzulToolStripMenuItem.Name = "TSCodigoAzulToolStripMenuItem"
        Me.TSCodigoAzulToolStripMenuItem.Size = New System.Drawing.Size(153, 22)
        Me.TSCodigoAzulToolStripMenuItem.Text = "T.S. Código Azul"
        '
        'TrabajoSocialToolStripMenuItem
        '
        Me.TrabajoSocialToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ListadoDePacientesTSToolStripMenuItem})
        Me.TrabajoSocialToolStripMenuItem.Name = "TrabajoSocialToolStripMenuItem"
        Me.TrabajoSocialToolStripMenuItem.Size = New System.Drawing.Size(86, 20)
        Me.TrabajoSocialToolStripMenuItem.Text = "Trabajo Social"
        Me.TrabajoSocialToolStripMenuItem.Visible = False
        '
        'ListadoDePacientesTSToolStripMenuItem
        '
        Me.ListadoDePacientesTSToolStripMenuItem.Name = "ListadoDePacientesTSToolStripMenuItem"
        Me.ListadoDePacientesTSToolStripMenuItem.Size = New System.Drawing.Size(172, 22)
        Me.ListadoDePacientesTSToolStripMenuItem.Text = "Listado de Pacientes"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsBtnUsuario, Me.ToolStripStatusLabel2, Me.tsLblUsuario, Me.ToolStripSplitButton1, Me.ToolStripStatusLabel1, Me.tsLblConexion, Me.ToolStripStatusLabel3, Me.lblTipoPersonal})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 354)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(665, 22)
        Me.StatusStrip1.SizingGrip = False
        Me.StatusStrip1.TabIndex = 5
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'tsBtnUsuario
        '
        Me.tsBtnUsuario.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.tsBtnUsuario.DropDownButtonWidth = 0
        Me.tsBtnUsuario.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.tsBtnUsuario.Name = "tsBtnUsuario"
        Me.tsBtnUsuario.Size = New System.Drawing.Size(5, 20)
        Me.tsBtnUsuario.Text = "ToolStripSplitButton1"
        Me.tsBtnUsuario.ToolTipText = "Usuario"
        '
        'ToolStripStatusLabel2
        '
        Me.ToolStripStatusLabel2.BackColor = System.Drawing.SystemColors.Menu
        Me.ToolStripStatusLabel2.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.ToolStripStatusLabel2.Name = "ToolStripStatusLabel2"
        Me.ToolStripStatusLabel2.Size = New System.Drawing.Size(54, 17)
        Me.ToolStripStatusLabel2.Text = "Usuario:"
        '
        'tsLblUsuario
        '
        Me.tsLblUsuario.BackColor = System.Drawing.SystemColors.Menu
        Me.tsLblUsuario.Name = "tsLblUsuario"
        Me.tsLblUsuario.Size = New System.Drawing.Size(44, 17)
        Me.tsLblUsuario.Text = "Nombre"
        '
        'ToolStripSplitButton1
        '
        Me.ToolStripSplitButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image
        Me.ToolStripSplitButton1.DropDownButtonWidth = 0
        Me.ToolStripSplitButton1.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripTextBox1})
        Me.ToolStripSplitButton1.Image = Global.TRIAGE.My.Resources.Resources.world
        Me.ToolStripSplitButton1.ImageTransparentColor = System.Drawing.Color.Magenta
        Me.ToolStripSplitButton1.Name = "ToolStripSplitButton1"
        Me.ToolStripSplitButton1.Size = New System.Drawing.Size(21, 20)
        Me.ToolStripSplitButton1.Text = "ToolStripSplitButton1"
        '
        'ToolStripTextBox1
        '
        Me.ToolStripTextBox1.Name = "ToolStripTextBox1"
        Me.ToolStripTextBox1.Size = New System.Drawing.Size(100, 21)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.BackColor = System.Drawing.SystemColors.Menu
        Me.ToolStripStatusLabel1.Font = New System.Drawing.Font("Segoe UI", 9.0!, CType((System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Italic), System.Drawing.FontStyle))
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(80, 17)
        Me.ToolStripStatusLabel1.Text = "Conectado a:"
        '
        'tsLblConexion
        '
        Me.tsLblConexion.BackColor = System.Drawing.SystemColors.Menu
        Me.tsLblConexion.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Italic)
        Me.tsLblConexion.Name = "tsLblConexion"
        Me.tsLblConexion.Size = New System.Drawing.Size(23, 17)
        Me.tsLblConexion.Text = "BD"
        '
        'ToolStripStatusLabel3
        '
        Me.ToolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel3.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel3.Name = "ToolStripStatusLabel3"
        Me.ToolStripStatusLabel3.Size = New System.Drawing.Size(54, 17)
        Me.ToolStripStatusLabel3.Text = "personal"
        '
        'lblTipoPersonal
        '
        Me.lblTipoPersonal.BackColor = System.Drawing.Color.Transparent
        Me.lblTipoPersonal.Name = "lblTipoPersonal"
        Me.lblTipoPersonal.Size = New System.Drawing.Size(25, 17)
        Me.lblTipoPersonal.Text = "tipo"
        '
        'lblTipo
        '
        Me.lblTipo.AutoSize = True
        Me.lblTipo.BackColor = System.Drawing.Color.Transparent
        Me.lblTipo.Font = New System.Drawing.Font("Times New Roman", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblTipo.ForeColor = System.Drawing.Color.Navy
        Me.lblTipo.Location = New System.Drawing.Point(220, 305)
        Me.lblTipo.Margin = New System.Windows.Forms.Padding(0)
        Me.lblTipo.Name = "lblTipo"
        Me.lblTipo.Size = New System.Drawing.Size(245, 43)
        Me.lblTipo.TabIndex = 24
        Me.lblTipo.Text = "URGENCIAS"
        Me.lblTipo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btncrsesion
        '
        Me.btncrsesion.Location = New System.Drawing.Point(563, 307)
        Me.btncrsesion.Name = "btncrsesion"
        Me.btncrsesion.Size = New System.Drawing.Size(90, 39)
        Me.btncrsesion.TabIndex = 25
        Me.btncrsesion.Text = "cerrar sesion"
        Me.btncrsesion.UseMnemonic = False
        Me.btncrsesion.UseVisualStyleBackColor = False
        '
        'triage
        '
        Me.triage.AutoSize = True
        Me.triage.BackColor = System.Drawing.Color.Transparent
        Me.triage.Font = New System.Drawing.Font("Times New Roman", 28.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.triage.ForeColor = System.Drawing.Color.Navy
        Me.triage.Location = New System.Drawing.Point(247, 255)
        Me.triage.Margin = New System.Windows.Forms.Padding(0)
        Me.triage.Name = "triage"
        Me.triage.Size = New System.Drawing.Size(168, 43)
        Me.triage.TabIndex = 27
        Me.triage.Text = "TRIAGE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10)
        Me.triage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'pnlGineco
        '
        Me.pnlGineco.BackColor = System.Drawing.Color.Transparent
        Me.pnlGineco.Controls.Add(Me.lblRevaluarGO)
        Me.pnlGineco.Controls.Add(Me.lblLisPaciente)
        Me.pnlGineco.Controls.Add(Me.lblGO)
        Me.pnlGineco.Controls.Add(Me.btnLisPaciente)
        Me.pnlGineco.Controls.Add(Me.btnRevaluarGO)
        Me.pnlGineco.Controls.Add(Me.Panel2)
        Me.pnlGineco.Controls.Add(Me.btnGO)
        Me.pnlGineco.Location = New System.Drawing.Point(22, 74)
        Me.pnlGineco.Name = "pnlGineco"
        Me.pnlGineco.Size = New System.Drawing.Size(212, 228)
        Me.pnlGineco.TabIndex = 28
        '
        'lblRevaluarGO
        '
        Me.lblRevaluarGO.AutoSize = True
        Me.lblRevaluarGO.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRevaluarGO.Location = New System.Drawing.Point(88, 98)
        Me.lblRevaluarGO.Name = "lblRevaluarGO"
        Me.lblRevaluarGO.Size = New System.Drawing.Size(86, 34)
        Me.lblRevaluarGO.TabIndex = 38
        Me.lblRevaluarGO.Text = "REVALUAR " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PACIENTE"
        Me.lblRevaluarGO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblLisPaciente
        '
        Me.lblLisPaciente.AutoSize = True
        Me.lblLisPaciente.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblLisPaciente.Location = New System.Drawing.Point(88, 176)
        Me.lblLisPaciente.Name = "lblLisPaciente"
        Me.lblLisPaciente.Size = New System.Drawing.Size(91, 34)
        Me.lblLisPaciente.TabIndex = 34
        Me.lblLisPaciente.Text = "LISTADO DE" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "PACIENTES"
        Me.lblLisPaciente.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblGO
        '
        Me.lblGO.AutoSize = True
        Me.lblGO.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblGO.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(64, Byte), Integer), CType(CType(0, Byte), Integer))
        Me.lblGO.Location = New System.Drawing.Point(102, 36)
        Me.lblGO.Name = "lblGO"
        Me.lblGO.Size = New System.Drawing.Size(60, 17)
        Me.lblGO.TabIndex = 32
        Me.lblGO.Text = "TRIAGE"
        Me.lblGO.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnLisPaciente
        '
        Me.btnLisPaciente.FlatAppearance.BorderSize = 0
        Me.btnLisPaciente.FlatAppearance.MouseDownBackColor = System.Drawing.Color.White
        Me.btnLisPaciente.FlatAppearance.MouseOverBackColor = System.Drawing.Color.Transparent
        Me.btnLisPaciente.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnLisPaciente.Image = Global.TRIAGE.My.Resources.Resources.seguimiento__1_
        Me.btnLisPaciente.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnLisPaciente.Location = New System.Drawing.Point(3, 155)
        Me.btnLisPaciente.Name = "btnLisPaciente"
        Me.btnLisPaciente.Size = New System.Drawing.Size(70, 70)
        Me.btnLisPaciente.TabIndex = 30
        Me.btnLisPaciente.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnLisPaciente.UseVisualStyleBackColor = True
        '
        'btnRevaluarGO
        '
        Me.btnRevaluarGO.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnRevaluarGO.FlatAppearance.BorderSize = 0
        Me.btnRevaluarGO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRevaluarGO.Image = Global.TRIAGE.My.Resources.Resources.triage1
        Me.btnRevaluarGO.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnRevaluarGO.Location = New System.Drawing.Point(3, 80)
        Me.btnRevaluarGO.Name = "btnRevaluarGO"
        Me.btnRevaluarGO.Size = New System.Drawing.Size(70, 70)
        Me.btnRevaluarGO.TabIndex = 31
        Me.btnRevaluarGO.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnRevaluarGO.UseVisualStyleBackColor = True
        '
        'Panel2
        '
        Me.Panel2.Controls.Add(Me.Button1)
        Me.Panel2.Location = New System.Drawing.Point(435, 1)
        Me.Panel2.Name = "Panel2"
        Me.Panel2.Size = New System.Drawing.Size(200, 226)
        Me.Panel2.TabIndex = 29
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(55, 182)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(75, 23)
        Me.Button1.TabIndex = 26
        Me.Button1.Text = "gineco"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btnGO
        '
        Me.btnGO.AllowDrop = True
        Me.btnGO.AutoEllipsis = True
        Me.btnGO.BackColor = System.Drawing.Color.Transparent
        Me.btnGO.FlatAppearance.BorderColor = System.Drawing.Color.White
        Me.btnGO.FlatAppearance.BorderSize = 0
        Me.btnGO.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnGO.ForeColor = System.Drawing.Color.Transparent
        Me.btnGO.Image = Global.TRIAGE.My.Resources.Resources.triage2
        Me.btnGO.ImageAlign = System.Drawing.ContentAlignment.MiddleRight
        Me.btnGO.Location = New System.Drawing.Point(3, 4)
        Me.btnGO.Name = "btnGO"
        Me.btnGO.Size = New System.Drawing.Size(70, 70)
        Me.btnGO.TabIndex = 26
        Me.btnGO.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnGO.TextImageRelation = System.Windows.Forms.TextImageRelation.TextBeforeImage
        Me.btnGO.UseVisualStyleBackColor = False
        '
        'pnlGineco1
        '
        Me.pnlGineco1.BackColor = System.Drawing.Color.Transparent
        Me.pnlGineco1.Controls.Add(Me.lblRepyEst)
        Me.pnlGineco1.Controls.Add(Me.lblCodVerde)
        Me.pnlGineco1.Controls.Add(Me.btnSeguimiento)
        Me.pnlGineco1.Controls.Add(Me.lblSeguimiento)
        Me.pnlGineco1.Controls.Add(Me.btnRepyEst)
        Me.pnlGineco1.Controls.Add(Me.btnCodVerde)
        Me.pnlGineco1.Controls.Add(Me.Panel4)
        Me.pnlGineco1.Location = New System.Drawing.Point(444, 73)
        Me.pnlGineco1.Name = "pnlGineco1"
        Me.pnlGineco1.Size = New System.Drawing.Size(200, 226)
        Me.pnlGineco1.TabIndex = 30
        Me.pnlGineco1.Visible = False
        '
        'lblRepyEst
        '
        Me.lblRepyEst.AutoSize = True
        Me.lblRepyEst.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblRepyEst.Location = New System.Drawing.Point(76, 98)
        Me.lblRepyEst.Name = "lblRepyEst"
        Me.lblRepyEst.Size = New System.Drawing.Size(107, 34)
        Me.lblRepyEst.TabIndex = 33
        Me.lblRepyEst.Text = "REPORTES Y" & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "ESTADISTICAS"
        Me.lblRepyEst.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'lblCodVerde
        '
        Me.lblCodVerde.AutoSize = True
        Me.lblCodVerde.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblCodVerde.Location = New System.Drawing.Point(79, 17)
        Me.lblCodVerde.Name = "lblCodVerde"
        Me.lblCodVerde.Size = New System.Drawing.Size(116, 34)
        Me.lblCodVerde.TabIndex = 35
        Me.lblCodVerde.Text = "LISTADO DE " & Global.Microsoft.VisualBasic.ChrW(13) & Global.Microsoft.VisualBasic.ChrW(10) & "CODIGO VERDE"
        Me.lblCodVerde.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnSeguimiento
        '
        Me.btnSeguimiento.FlatAppearance.BorderSize = 0
        Me.btnSeguimiento.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnSeguimiento.Image = Global.TRIAGE.My.Resources.Resources.seguimiento
        Me.btnSeguimiento.Location = New System.Drawing.Point(3, 155)
        Me.btnSeguimiento.Name = "btnSeguimiento"
        Me.btnSeguimiento.Size = New System.Drawing.Size(67, 70)
        Me.btnSeguimiento.TabIndex = 34
        Me.btnSeguimiento.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnSeguimiento.UseVisualStyleBackColor = True
        '
        'lblSeguimiento
        '
        Me.lblSeguimiento.AutoSize = True
        Me.lblSeguimiento.Font = New System.Drawing.Font("Times New Roman", 10.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblSeguimiento.Location = New System.Drawing.Point(82, 189)
        Me.lblSeguimiento.Name = "lblSeguimiento"
        Me.lblSeguimiento.Size = New System.Drawing.Size(103, 17)
        Me.lblSeguimiento.TabIndex = 36
        Me.lblSeguimiento.Text = "SEGUIMIENTO"
        Me.lblSeguimiento.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'btnRepyEst
        '
        Me.btnRepyEst.FlatAppearance.BorderSize = 0
        Me.btnRepyEst.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnRepyEst.Image = Global.TRIAGE.My.Resources.Resources.g8711
        Me.btnRepyEst.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnRepyEst.Location = New System.Drawing.Point(3, 81)
        Me.btnRepyEst.Name = "btnRepyEst"
        Me.btnRepyEst.Size = New System.Drawing.Size(67, 70)
        Me.btnRepyEst.TabIndex = 33
        Me.btnRepyEst.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnRepyEst.UseVisualStyleBackColor = True
        '
        'btnCodVerde
        '
        Me.btnCodVerde.FlatAppearance.BorderSize = 0
        Me.btnCodVerde.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.btnCodVerde.Image = Global.TRIAGE.My.Resources.Resources.g8580
        Me.btnCodVerde.ImageAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCodVerde.Location = New System.Drawing.Point(0, 5)
        Me.btnCodVerde.Name = "btnCodVerde"
        Me.btnCodVerde.Size = New System.Drawing.Size(70, 70)
        Me.btnCodVerde.TabIndex = 32
        Me.btnCodVerde.TextAlign = System.Drawing.ContentAlignment.BottomCenter
        Me.btnCodVerde.UseVisualStyleBackColor = True
        '
        'Panel4
        '
        Me.Panel4.Controls.Add(Me.Button2)
        Me.Panel4.Location = New System.Drawing.Point(435, 1)
        Me.Panel4.Name = "Panel4"
        Me.Panel4.Size = New System.Drawing.Size(200, 226)
        Me.Panel4.TabIndex = 29
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(55, 182)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(75, 23)
        Me.Button2.TabIndex = 26
        Me.Button2.Text = "gineco"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'PictureBox3
        '
        Me.PictureBox3.Image = Global.TRIAGE.My.Resources.Resources.smarttag_triagetag
        Me.PictureBox3.Location = New System.Drawing.Point(252, 120)
        Me.PictureBox3.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.PictureBox3.Name = "PictureBox3"
        Me.PictureBox3.Size = New System.Drawing.Size(163, 122)
        Me.PictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox3.TabIndex = 3
        Me.PictureBox3.TabStop = False
        '
        'imghospital
        '
        Me.imghospital.Image = Global.TRIAGE.My.Resources.Resources.Captura
        Me.imghospital.Location = New System.Drawing.Point(0, 26)
        Me.imghospital.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.imghospital.Name = "imghospital"
        Me.imghospital.Size = New System.Drawing.Size(665, 74)
        Me.imghospital.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.imghospital.TabIndex = 1
        Me.imghospital.TabStop = False
        '
        'frmPrincipalTriage
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(665, 376)
        Me.Controls.Add(Me.pnlGineco1)
        Me.Controls.Add(Me.pnlGineco)
        Me.Controls.Add(Me.triage)
        Me.Controls.Add(Me.btncrsesion)
        Me.Controls.Add(Me.lblTipo)
        Me.Controls.Add(Me.PictureBox3)
        Me.Controls.Add(Me.imghospital)
        Me.Controls.Add(Me.menugineco)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Margin = New System.Windows.Forms.Padding(2, 3, 2, 3)
        Me.Name = "frmPrincipalTriage"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "SISTEMA TRIAGE V0.1    "
        Me.menugineco.ResumeLayout(False)
        Me.menugineco.PerformLayout()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.pnlGineco.ResumeLayout(False)
        Me.pnlGineco.PerformLayout()
        Me.Panel2.ResumeLayout(False)
        Me.pnlGineco1.ResumeLayout(False)
        Me.pnlGineco1.PerformLayout()
        Me.Panel4.ResumeLayout(False)
        CType(Me.PictureBox3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.imghospital, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents menugineco As System.Windows.Forms.MenuStrip
    Friend WithEvents HerramientasToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents EvaluarPacientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents imghospital As System.Windows.Forms.PictureBox
    Friend WithEvents ListadoDePacientesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents PictureBox3 As System.Windows.Forms.PictureBox
    Friend WithEvents TrabajoSocialToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ListadoDePacientesTSToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ReportesToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents TSCodigoAzulToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents CambiarPrioridadToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripMenuItem1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents ToolStripStatusLabel1 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsLblConexion As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsLblUsuario As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel2 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsBtnUsuario As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents ToolStripSplitButton1 As System.Windows.Forms.ToolStripSplitButton
    Friend WithEvents lblTipo As System.Windows.Forms.Label
    Friend WithEvents SeguimientoToolStripMenuItem As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents btncrsesion As System.Windows.Forms.Button
    Friend WithEvents btnGO As System.Windows.Forms.Button
    Friend WithEvents triage As System.Windows.Forms.Label
    Friend WithEvents pnlGineco As System.Windows.Forms.Panel
    Friend WithEvents Panel2 As System.Windows.Forms.Panel
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents pnlGineco1 As System.Windows.Forms.Panel
    Friend WithEvents Panel4 As System.Windows.Forms.Panel
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents btnLisPaciente As System.Windows.Forms.Button
    Friend WithEvents lblLisPaciente As System.Windows.Forms.Label
    Friend WithEvents lblRepyEst As System.Windows.Forms.Label
    Friend WithEvents lblGO As System.Windows.Forms.Label
    Friend WithEvents btnRevaluarGO As System.Windows.Forms.Button
    Friend WithEvents lblSeguimiento As System.Windows.Forms.Label
    Friend WithEvents lblCodVerde As System.Windows.Forms.Label
    Friend WithEvents btnSeguimiento As System.Windows.Forms.Button
    Friend WithEvents btnRepyEst As System.Windows.Forms.Button
    Friend WithEvents btnCodVerde As System.Windows.Forms.Button
    Friend WithEvents lblRevaluarGO As System.Windows.Forms.Label
    Friend WithEvents ToolStripTextBox1 As System.Windows.Forms.ToolStripTextBox
    Friend WithEvents ToolStripStatusLabel3 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblTipoPersonal As System.Windows.Forms.ToolStripStatusLabel
End Class
