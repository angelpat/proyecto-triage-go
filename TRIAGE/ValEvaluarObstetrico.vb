﻿Public Class ValEvaluarObstetrico
    Dim bajo As Integer
    Dim medio As Integer
    Dim alto As Integer
    Dim salvar As Integer
    Public presionart As PresionArterialGO = New PresionArterialGO()
    Public valorarGO As TriageGO = New TriageGO()

    '  Dim frmob As frmEvaluarObstetrico = New frmEvaluarObstetrico()

    Public Sub RecibeObjTGOReavaular(ByRef objTGO As TriageGO)
        'valorarGO = New TriageGO()
        valorarGO = objTGO
    End Sub

    Public Sub llenaObjTriageGo()
        With valorarGO
            .miestconciencia = .EstadoConciencia
            .miHemorragia = .Hemorragia
            .miDolorObstetrico = .DolorObstetrico
            .miCrisisConvulsiva = .CrisisConvulsivas
            .miNauseas = .Nausea
            .miRespiracion1 = .Respiracion
            .miColorPIel = .ColorPiel
            ''''''''''''''''''''''''''''''''
            .miSangradoTrans = .SangradoTransvaginal
            .miCefalea = .Cefalea
            .miAcufenos = .Acufenos
            .miFosfenos = .Fosfenos
            .miEpigastralgia = .Epigastralgia
            .miAmaurosis = .Amaurosis
            .miSondromeFebril = .SindromeFebril
            .miLiquidoAmniotico = .LiquidoAmniotico
            .mIMotalidadFetal = .MotilidadFetal
            '''''''''''''''''''''''''''''''''''''''
            .miCesarea = .CesareaCirUterina
            .miTrauma = .TraumaAgresion
            .miSangrado12 = .sangrado12
            '''''''''''''''''''''''''''''''''''''''''''''''
            .miCardiopatia = .Cardiopatia
            .miNefropatia = .Nefropatia
            .miHematopatia = .Hematopatia
            .miHepatopatia = .Hepatopiatia
            .miEndocrinopatia = .Endocrinologia
            .miHepatopatia = .Hepatopiatia
            '''''''''''''''''''''''''''''
            .miVIH = .PruebaVIH
            .miSifilis = .PruebaSifilis
            .mitirauroanalisis = .TiraUroanalisis








        End With
    End Sub


    Public Function validaFormulario() As Boolean
        'SIGNOS VITALES

        ' MAMA
        If frmEvaluarObstetrico.cbxPaciente.Text = "" Then
            MessageBox.Show("Falta seleccionar " & frmEvaluarObstetrico.lblPaciente.Text, "Advertencia")
            Return False
        End If

        If frmEvaluarObstetrico.txtFrecCard.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtFrecCard.Text) = False Then

            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblPulso.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If

        If frmEvaluarObstetrico.txtRespiracion.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtRespiracion.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblRespiracion.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtPeso.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtPeso.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblPeso.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtAltura.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtAltura.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblAltura.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtPaSistolica.Text = "" Or frmEvaluarObstetrico.txtPaDiastolica.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtPaSistolica.Text) = False Or IsNumeric(frmEvaluarObstetrico.txtPaDiastolica.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblPresionA.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtTemperatura.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtFrecCard.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblTemp.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtRespiracion.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtRespiracion.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblRespiracion.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        If frmEvaluarObstetrico.txtGlucosa.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtGlucosa.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblGlucosa.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If

        ' FETO
        If frmEvaluarObstetrico.txtFCFFeto.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtFCFFeto.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblFCFeto.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If

        If frmEvaluarObstetrico.txtEdadGestacional.Text = "" Or IsNumeric(frmEvaluarObstetrico.txtEdadGestacional.Text) = False Then
            MessageBox.Show("Falta escribir " & frmEvaluarObstetrico.lblEdadGestacional.Text & " o el valor no es numerico", "Advertencia")
            Return False
        End If
        ''''''''''''''''''''''''''''''''''''combobox feto
      
     

        Return True

    End Function

    Public Function ObtenerPrioridad() As Integer

        bajo = 0
        medio = 0
        alto = 0

        'signos vitales

        'tension aterial
        Dim salvar1 As Integer
        Dim salvar2 As Integer
        salvar2 = frmEvaluarObstetrico.txtPaSistolica.Text
        salvar1 = frmEvaluarObstetrico.txtPaDiastolica.Text



        salvar = presionart.Sistolica(salvar2)
        salvar1 = presionart.Diastolica(salvar1)
        If salvar = 1 And salvar1 = 1 Then
            bajo = bajo + 1
            frmEvaluarObstetrico.lblPresionA.ForeColor = Color.Black
            frmEvaluarObstetrico.lblPresionA.BackColor = Color.Transparent

        ElseIf salvar = 1 And salvar1 = 2 Or salvar = 2 And salvar1 = 1 Or salvar = 2 And salvar1 = 2 Then
            frmEvaluarObstetrico.lblPresionA.BackColor = Color.Yellow
            frmEvaluarObstetrico.lblPresionA.ForeColor = Color.Black
            medio = medio + 1

        Else
            frmEvaluarObstetrico.lblPresionA.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblPresionA.ForeColor = Color.White
            alto = alto + 1
        End If


        ''fecuencia cardiaca
        salvar = frmEvaluarObstetrico.txtFrecCard.Text
        If salvar >= 60 And salvar <= 80 Then
            bajo = bajo + 1
            frmEvaluarObstetrico.lblPulso.ForeColor = Color.Black
            frmEvaluarObstetrico.lblPulso.BackColor = Color.Transparent

        ElseIf salvar >= 50 And salvar < 60 Or salvar > 80 And salvar <= 110 Then
            frmEvaluarObstetrico.lblPulso.ForeColor = Color.Black
            frmEvaluarObstetrico.lblPulso.BackColor = Color.Yellow
            medio = medio + 1

        Else
            frmEvaluarObstetrico.lblPulso.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblPulso.ForeColor = Color.White
            alto = alto + 1
        End If
        ''indice de choque
        Dim InChoq As Double
        InChoq = Math.Round(frmEvaluarObstetrico.txtFrecCard.Text / frmEvaluarObstetrico.txtPaSistolica.Text, 3)
        frmEvaluarObstetrico.txtIndChoque.Text = InChoq
        If InChoq > 0.8 Then
            alto = alto + 1
            frmEvaluarObstetrico.lblIndChoque.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblIndChoque.ForeColor = Color.White

        ElseIf InChoq >= 0.7 And InChoq < 0.8 Then
            frmEvaluarObstetrico.lblIndChoque.ForeColor = Color.Black
            frmEvaluarObstetrico.lblIndChoque.BackColor = Color.Yellow
            medio = medio + 1

        Else
            frmEvaluarObstetrico.lblIndChoque.ForeColor = Color.Black
            frmEvaluarObstetrico.lblIndChoque.BackColor = Color.Transparent

            bajo = bajo + 1
        End If

        ''temperatura 
        salvar = frmEvaluarObstetrico.txtTemperatura.Text
        If salvar >= 35 And salvar <= 37 Then
            bajo = bajo + 1
            frmEvaluarObstetrico.lblTemp.ForeColor = Color.Black
            frmEvaluarObstetrico.lblTemp.BackColor = Color.Transparent

        ElseIf salvar >= 37.5 And salvar <= 38.9 Then
            frmEvaluarObstetrico.lblTemp.BackColor = Color.Yellow
            medio = medio + 1

        Else
            frmEvaluarObstetrico.lblTemp.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblTemp.ForeColor = Color.White
            alto = alto + 1
        End If


        ''frecuencia respiratoria 

        salvar = frmEvaluarObstetrico.txtRespiracion.Text
        If salvar >= 16 And salvar <= 20 Then
            bajo = bajo + 1
            frmEvaluarObstetrico.lblRespiracion.ForeColor = Color.Black
            frmEvaluarObstetrico.lblRespiracion.BackColor = Color.Transparent

        Else
            frmEvaluarObstetrico.lblRespiracion.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblRespiracion.ForeColor = Color.White
            alto = alto + 1
        End If

        ''imc

        frmEvaluarObstetrico.txtIMC.Text = Math.Round(frmEvaluarObstetrico.txtPeso.Text / Math.Pow(frmEvaluarObstetrico.txtAltura.Text, 2), 2)

        salvar = frmEvaluarObstetrico.txtIMC.Text
        If salvar >= 18 And salvar <= 24 Then
            frmEvaluarObstetrico.lblIMC.ForeColor = Color.Black
            frmEvaluarObstetrico.lblIMC.BackColor = Color.Transparent
        ElseIf salvar >= 25 And salvar <= 29 Then
            frmEvaluarObstetrico.lblIMC.BackColor = Color.Yellow
        Else
            frmEvaluarObstetrico.lblIMC.BackColor = Color.IndianRed
            frmEvaluarObstetrico.lblIMC.ForeColor = Color.White
        End If

        ' estado de conciencia
        Dim value As String     
        value = Mid(valorarGO.miestconciencia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblEstadoConciencia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblEstadoConciencia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtEstadoConcienciaB.Checked = True

            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblEstadoConciencia.BackColor = Color.Yellow
            medio = medio + 1
            frmEvaluarObstetrico.rbtEstadoConcienciaM.Checked = True
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblEstadoConciencia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtEstadoConcienciaA.Checked = True
            alto = alto + 1
        End If

        ''hemorragia

        value = Mid(valorarGO.miHemorragia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblHemorragia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblHemorragia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtHemorragiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblHemorragia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtHemorragiaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblHemorragia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtHemorragiaA.Checked = True
            alto = alto + 1
        End If


        ''Dolor obstetrico
        value = Mid(valorarGO.miDolorObstetrico, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblDolorObstetrico.ForeColor = Color.Black
            frmEvaluarObstetrico.lblDolorObstetrico.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtDolorObstetricoB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblDolorObstetrico.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtDolorObstetricoM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblDolorObstetrico.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtDolorObstetricoA.Checked = True
            alto = alto + 1
        End If

        ''crisis convulsiva
        value = Mid(valorarGO.miCrisisConvulsiva, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblCrisisConvulsiva.ForeColor = Color.Black
            frmEvaluarObstetrico.lblCrisisConvulsiva.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtCrisisConvulsivaB.Checked = True
            bajo = bajo + 1

        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblCrisisConvulsiva.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtCrisisConvulsivaA.Checked = True
            alto = alto + 1
        End If

        ''nauseas
        value = Mid(valorarGO.miNauseas, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblNausea.ForeColor = Color.Black
            frmEvaluarObstetrico.lblNausea.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtNauseaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblNausea.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtNauseaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblNausea.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtNauseaA.Checked = True
            alto = alto + 1
        End If

        ''respiracion
        value = Mid(valorarGO.miRespiracion1, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblRespiracon1.ForeColor = Color.Black
            frmEvaluarObstetrico.lblRespiracon1.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtRespiracionB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblRespiracon1.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtRespiracionM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblRespiracon1.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtRespiracionA.Checked = True
            alto = alto + 1
        End If

        ''colorpiel
        value = Mid(valorarGO.miColorPIel, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblPalidez.ForeColor = Color.Black
            frmEvaluarObstetrico.lblPalidez.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtPalidezB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblPalidez.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtPalidezM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblPalidez.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtPalidezA.Checked = True
            alto = alto + 1
        End If

        ''sangrado tranvaginal
        value = Mid(valorarGO.miSangradoTrans, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblsangradotrans.ForeColor = Color.Black
            frmEvaluarObstetrico.lblsangradotrans.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtSangradotransB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblsangradotrans.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtSangradotransM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblsangradotrans.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtSangradotransA.Checked = True
            alto = alto + 1
        End If
        ''cefalea
        value = Mid(valorarGO.miCefalea, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblCefalea.ForeColor = Color.Black
            frmEvaluarObstetrico.lblCefalea.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtCefaleaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblCefalea.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtCefaleaM.Checked = True
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblCefalea.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtCefaleaA.Checked = True
            alto = alto + 1
        End If
        ''acufenos
        value = Mid(valorarGO.miAcufenos, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblAcufos.ForeColor = Color.Black
            frmEvaluarObstetrico.lblAcufos.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtAcufenosB.Checked = True

            bajo = bajo + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblAcufos.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtAcufenosA.Checked = True

            alto = alto + 1
        End If

        ''fosfenos
        value = Mid(valorarGO.miFosfenos, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblfosfenos.ForeColor = Color.Black
            frmEvaluarObstetrico.lblfosfenos.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtFosfenosB.Checked = True
            bajo = bajo + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblfosfenos.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtFosfenosA.Checked = True
            alto = alto + 1
        End If


        ''epigastralgia
        value = Mid(valorarGO.miEpigastralgia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblepigastralgia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblepigastralgia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtEpigastragiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblepigastralgia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtEpigastragiaA.Checked = True
            alto = alto + 1
        End If
        ''amaurosis
        value = Mid(valorarGO.miAmaurosis, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblamaurosis.ForeColor = Color.Black
            frmEvaluarObstetrico.lblamaurosis.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtAmaurosisB.Checked = True
            bajo = bajo + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblamaurosis.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtAmaurosisA.Checked = True
            alto = alto + 1
        End If

        ''sondrome febvril
        value = Mid(valorarGO.miSondromeFebril, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblsindromefebril.ForeColor = Color.Black
            frmEvaluarObstetrico.lblsindromefebril.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtSindromeFebrilB.Checked = True
            bajo = bajo + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblsindromefebril.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtSindromeFebrilA.Checked = True
            alto = alto + 1
       
        End If

        ''liquido admiotico
        value = Mid(valorarGO.miLiquidoAmniotico, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblliquidoamn.ForeColor = Color.Black
            frmEvaluarObstetrico.lblliquidoamn.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtLiquidoAmnioticoB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblliquidoamn.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtLiquidoAmnioticoA1.Checked = True
            alto = alto + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblliquidoamn.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtLiquidoAmnioticoA2.Checked = True
            alto = alto + 1
        End If
        ''motilidad fetal
        value = Mid(valorarGO.mIMotalidadFetal, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblfetal.ForeColor = Color.Black
            frmEvaluarObstetrico.lblfetal.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtMotilidadFetalB.Checked = True

            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblfetal.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtMotilidadFetalM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblfetal.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtMotilidadFetalA.Checked = True
            alto = alto + 1
        End If
        ''cecarea
        value = Mid(valorarGO.miCesarea, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblCirUterina.ForeColor = Color.Black
            frmEvaluarObstetrico.lblCirUterina.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtCirUterinaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblCirUterina.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtCirUterinaM.Checked = True
        End If

        ''agrecion
        value = Mid(valorarGO.miTrauma, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblTrauma.ForeColor = Color.Black
            frmEvaluarObstetrico.lblTrauma.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtTraumaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblTrauma.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtTraumaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblTrauma.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtTraumaA.Checked = True
            alto = alto + 1
        End If

        ''sangrado 12
        value = Mid(valorarGO.miSangrado12, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblSangrado.ForeColor = Color.Black
            frmEvaluarObstetrico.lblSangrado.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtSangradoB.Checked = True

            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblSangrado.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtSangradoM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblSangrado.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtSangradoM.Checked = True
            alto = alto + 1
        End If
        ''cardiopatia
        value = Mid(valorarGO.miCardiopatia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblCardiopatia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblCardiopatia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtCardiopatiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblCardiopatia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtCardiopatiaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblCardiopatia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtCardiopatiaA.Checked = True
            alto = alto + 1
        End If
        ''nefropatia
        value = Mid(valorarGO.miNefropatia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblNefropatia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblNefropatia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtNefropatiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblNefropatia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtNefropatiaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblNefropatia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtNefropatiaA.Checked = True
            alto = alto + 1
        End If
        ''endocrinopatia
        value = Mid(valorarGO.miHematopatia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblHematopatia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblHematopatia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtHematopatiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblHematopatia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtHematopatiaM.Checked = True

            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblHematopatia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtHematopatiaA.Checked = True
            alto = alto + 1
        End If

        ''hematopatia
        value = Mid(valorarGO.miEndocrinopatia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblEndocrinopatia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblEndocrinopatia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtEndocrinopatiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblEndocrinopatia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtEndocrinopatiaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblEndocrinopatia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtEndocrinopatiaA.Checked = True
            alto = alto + 1
        End If

        ''hepatopatia
        value = Mid(valorarGO.miHepatopatia, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblHepatopatia.ForeColor = Color.Black
            frmEvaluarObstetrico.lblHepatopatia.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtHepatopatiaB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblHepatopatia.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtHepatopatiaM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblHepatopatia.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtHepatopatiaA.Checked = True
            alto = alto + 1
        End If

        ''prueba de VIH
        value = Mid(valorarGO.miVIH, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblPruebaVIH.ForeColor = Color.Black
            frmEvaluarObstetrico.lblPruebaVIH.BackColor = Color.Transparent

            frmEvaluarObstetrico.rbtPruebaVIHB.Checked = True

            bajo = bajo + 1

        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblPruebaVIH.BackColor = Color.IndianRed
            If frmEvaluarObstetrico.cbxResultadoVIH.Text = "" Then
                frmEvaluarObstetrico.cbxResultadoVIH.Text = Mid(valorarGO.miVIH, 3, 9)
            Else
                frmEvaluarObstetrico.rbtPruebaVIHA.Checked = True
                alto = alto + 1
            End If
        End If
        ''prueba rapida de sifilis

        value = Mid(valorarGO.miSifilis, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblPruebaSifilis.BackColor = Color.Black
            frmEvaluarObstetrico.lblPruebaSifilis.BackColor = Color.Transparent

            frmEvaluarObstetrico.rbtPruebaSifilisB.Checked = True
            bajo = bajo + 1

        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblPruebaSifilis.BackColor = Color.IndianRed
            frmEvaluarObstetrico.cbxResultadoSIFILIS.Text = Mid(valorarGO.miSifilis, 3, 9)
            frmEvaluarObstetrico.rbtPruebaSifilisA.Checked = True
            alto = alto + 1
        End If
        ''tira uroanalisis
        value = Mid(valorarGO.mitirauroanalisis, 1, 1)
        If value = "B" Then
            frmEvaluarObstetrico.lblTiraUroanalisis.ForeColor = Color.Black
            frmEvaluarObstetrico.lblTiraUroanalisis.BackColor = Color.Transparent
            frmEvaluarObstetrico.rbtTiraUroanalisisB.Checked = True
            bajo = bajo + 1
        ElseIf value = "M" Then
            frmEvaluarObstetrico.lblTiraUroanalisis.BackColor = Color.Yellow
            frmEvaluarObstetrico.rbtTiraUroanalisisM.Checked = True
            medio = medio + 1
        ElseIf value = "A" Then
            frmEvaluarObstetrico.lblTiraUroanalisis.BackColor = Color.IndianRed
            frmEvaluarObstetrico.rbtTiraUroanalisisA.Checked = True
            alto = alto + 1
        End If


        'MessageBox.Show("Bajo: " & bajo.ToString() & " Medio: " & medio.ToString() & " Alto: " & alto.ToString(), "Advertencia")

        If alto >= 1 Then
            Return 1
        ElseIf medio >= 1 Then
            Return 2
        Else
            Return 3
        End If


    End Function

End Class
