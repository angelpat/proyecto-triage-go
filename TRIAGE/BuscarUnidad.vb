﻿Imports System.Data.SqlClient

Public Class BuscarUnidad
    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader
    Dim dt As DataTable
    Dim Da As New SqlDataAdapter
    Dim db As conexionbd = New conexionbd
    Dim UNIDAD As String

    Public Sub Mostar_Unidad(ByVal unidad As Integer)

        Try
            'lblNombreRevisor.Text = RegresaEmpleado()
            db.validarbd()
            cnn = New SqlClient.SqlConnection(db.conexion)

            comando = New SqlClient.SqlCommand("EXEC zspTRI_selUnidadMedica;", cnn)
            comando.CommandTimeout() = 5000
            cnn.Open()
            Da.SelectCommand = comando
            dt = New DataTable
            Da.Fill(dt)
            If unidad = 1 Then
                With frmEvaluarObstetrico.cbxUnidad1
                    .DataSource = dt
                    .DisplayMember = "cnombre"
                    .ValueMember = "idUmedica"
                End With
            Else
                With frmListadoPacientesTSCVGO.cbxUnidad2
                    .DataSource = dt
                    .DisplayMember = "cnombre"
                    .ValueMember = "idUmedica"
                End With

            End If
            'lector = comando.ExecuteReader()
            'frmEvaluarObstetrico.cbxUnida1.Items.Clear()
            'frmEvaluarObstetrico.cbxUnidad1.Text = ""
            'While lector.Read()
            '    frmEvaluarObstetrico.cbxUnidad1.Items.Add(lector.Item("cnombre").ToString)
            '    frmEvaluarObstetrico.cbxUnidad1.SelectedIndex = 0
            'End While
            'lector.Close()
        Catch ex As Exception
            MessageBox.Show("Ocurrio un error al cargar las unidades." + vbCrLf + vbCrLf + "Error: " + ex.Message, "Error")
        End Try
        cnn.Close()

    End Sub
End Class
