﻿Public Class EvalTriGO

    Public Function Valestado(ByRef prioridad1 As Integer) As String

        If frmEvaluarObstetrico.lblNivel.Text = "Nivel" Then
            frmEvaluarObstetrico.lblNivel.Text = frmEvaluarObstetrico.lblNivel.Text + " " + System.Convert.ToString(prioridad1)
        End If
        ' tmrPrioridad.Enabled = True
        Select Case prioridad1

            Case 1
                frmEvaluarObstetrico.lblColor.Text = "Rojo"
                frmEvaluarObstetrico.lblDenominacion.Text = "Atención inmediata"
                frmEvaluarObstetrico.lblActuacion.Text = "Activando codigo matter y ERIO"
                frmEvaluarObstetrico.pnlPrioridad.BackColor = Color.Red

                If MessageBox.Show("La valoracion arrojo codigo rojo Quiere Activar Codigo matter y codigo Erio?", "Codigo matter", _
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question) _
                    = DialogResult.Yes Then
                    Return "1-Rojo"
                End If

            Case 2
                frmEvaluarObstetrico.lblColor.Text = "Amarillo"
                frmEvaluarObstetrico.lblDenominacion.Text = "Urgencia"
                frmEvaluarObstetrico.lblActuacion.Text = "Valoración médica < 15 min"
                frmEvaluarObstetrico.pnlPrioridad.BackColor = Color.Yellow
                Return "2-Amarillo"
            Case 3
                frmEvaluarObstetrico.lblColor.Text = "Verde"
                frmEvaluarObstetrico.lblDenominacion.Text = "Urgencia menor"
                frmEvaluarObstetrico.lblActuacion.Text = "Valoración médica 15 a 30 min"
                'panPrioridad.BackColor = Color.Green
                frmEvaluarObstetrico.pnlPrioridad.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                Return "3-Verde"
        End Select

    End Function
End Class
