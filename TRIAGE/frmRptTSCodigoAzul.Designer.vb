﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmRptTSCodigoAzul
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmRptTSCodigoAzul))
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.gbxPaciente = New System.Windows.Forms.GroupBox
        Me.btnAceptar = New System.Windows.Forms.Button
        Me.dtpFin = New System.Windows.Forms.DateTimePicker
        Me.dtpIni = New System.Windows.Forms.DateTimePicker
        Me.lblFechaFin = New System.Windows.Forms.Label
        Me.lblFechaIni = New System.Windows.Forms.Label
        Me.crv = New CrystalDecisions.Windows.Forms.CrystalReportViewer
        Me.lbl = New System.Windows.Forms.Label
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 6
        Me.PictureBox1.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left), System.Windows.Forms.AnchorStyles)
        Me.gbxPaciente.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.gbxPaciente.Controls.Add(Me.btnAceptar)
        Me.gbxPaciente.Controls.Add(Me.dtpFin)
        Me.gbxPaciente.Controls.Add(Me.dtpIni)
        Me.gbxPaciente.Controls.Add(Me.lblFechaFin)
        Me.gbxPaciente.Controls.Add(Me.lblFechaIni)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(12, 158)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(221, 477)
        Me.gbxPaciente.TabIndex = 7
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Periodo"
        '
        'btnAceptar
        '
        Me.btnAceptar.Location = New System.Drawing.Point(114, 139)
        Me.btnAceptar.Name = "btnAceptar"
        Me.btnAceptar.Size = New System.Drawing.Size(95, 23)
        Me.btnAceptar.TabIndex = 3
        Me.btnAceptar.Text = "Aceptar"
        Me.btnAceptar.UseVisualStyleBackColor = True
        '
        'dtpFin
        '
        Me.dtpFin.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpFin.CustomFormat = "dd 'de' MMMM 'de' yyyy"
        Me.dtpFin.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpFin.Location = New System.Drawing.Point(9, 101)
        Me.dtpFin.MaxDate = New Date(3000, 12, 31, 0, 0, 0, 0)
        Me.dtpFin.MinDate = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.dtpFin.Name = "dtpFin"
        Me.dtpFin.Size = New System.Drawing.Size(200, 20)
        Me.dtpFin.TabIndex = 1
        '
        'dtpIni
        '
        Me.dtpIni.Cursor = System.Windows.Forms.Cursors.Hand
        Me.dtpIni.CustomFormat = "dd 'de' MMMM 'de' yyyy"
        Me.dtpIni.Format = System.Windows.Forms.DateTimePickerFormat.Custom
        Me.dtpIni.Location = New System.Drawing.Point(9, 46)
        Me.dtpIni.MaxDate = New Date(3000, 12, 31, 0, 0, 0, 0)
        Me.dtpIni.MinDate = New Date(2012, 1, 1, 0, 0, 0, 0)
        Me.dtpIni.Name = "dtpIni"
        Me.dtpIni.Size = New System.Drawing.Size(200, 20)
        Me.dtpIni.TabIndex = 1
        '
        'lblFechaFin
        '
        Me.lblFechaFin.AutoSize = True
        Me.lblFechaFin.Location = New System.Drawing.Point(6, 85)
        Me.lblFechaFin.Name = "lblFechaFin"
        Me.lblFechaFin.Size = New System.Drawing.Size(24, 13)
        Me.lblFechaFin.TabIndex = 0
        Me.lblFechaFin.Text = "Fin"
        '
        'lblFechaIni
        '
        Me.lblFechaIni.AutoSize = True
        Me.lblFechaIni.Location = New System.Drawing.Point(6, 30)
        Me.lblFechaIni.Name = "lblFechaIni"
        Me.lblFechaIni.Size = New System.Drawing.Size(38, 13)
        Me.lblFechaIni.TabIndex = 0
        Me.lblFechaIni.Text = "Inicio"
        '
        'crv
        '
        Me.crv.ActiveViewIndex = -1
        Me.crv.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
                    Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.crv.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.crv.DisplayGroupTree = False
        Me.crv.Location = New System.Drawing.Point(239, 164)
        Me.crv.Name = "crv"
        Me.crv.SelectionFormula = ""
        Me.crv.Size = New System.Drawing.Size(613, 471)
        Me.crv.TabIndex = 9
        Me.crv.ViewTimeSelectionFormula = ""
        '
        'lbl
        '
        Me.lbl.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.lbl.BackColor = System.Drawing.Color.DodgerBlue
        Me.lbl.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.lbl.Font = New System.Drawing.Font("Nina", 17.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lbl.Location = New System.Drawing.Point(12, 101)
        Me.lbl.Name = "lbl"
        Me.lbl.Size = New System.Drawing.Size(840, 51)
        Me.lbl.TabIndex = 8
        Me.lbl.Text = "Reporte de pacientes valorados con Código Azul"
        Me.lbl.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'frmRptTSCodigoAzul
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.ClientSize = New System.Drawing.Size(862, 654)
        Me.Controls.Add(Me.crv)
        Me.Controls.Add(Me.lbl)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.PictureBox1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmRptTSCodigoAzul"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Reporte - T. S. Código Azul"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents lblFechaIni As System.Windows.Forms.Label
    Friend WithEvents dtpFin As System.Windows.Forms.DateTimePicker
    Friend WithEvents dtpIni As System.Windows.Forms.DateTimePicker
    Friend WithEvents lblFechaFin As System.Windows.Forms.Label
    Friend WithEvents lbl As System.Windows.Forms.Label
    Friend WithEvents crv As CrystalDecisions.Windows.Forms.CrystalReportViewer
    Friend WithEvents btnAceptar As System.Windows.Forms.Button
End Class
