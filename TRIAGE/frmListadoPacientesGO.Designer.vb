<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPacientesGO
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.dgvListado = New System.Windows.Forms.DataGridView()
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.Prevaloracion = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.idpaciente2 = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cnumcontrol = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cama = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.cmsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmMenu = New System.Windows.Forms.ToolStripMenuItem()
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator()
        Me.tsmAtender = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmEliminar = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmpediatria = New System.Windows.Forms.ToolStripMenuItem()
        Me.tsmTriage = New System.Windows.Forms.ToolStripMenuItem()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel()
        Me.gbxPaciente = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.txtcama = New System.Windows.Forms.TextBox()
        Me.txtPaciente = New System.Windows.Forms.TextBox()
        Me.txtPrioridad = New System.Windows.Forms.TextBox()
        Me.lblPrioridad = New System.Windows.Forms.Label()
        Me.lblPaciente = New System.Windows.Forms.Label()
        Me.btnAtendido = New System.Windows.Forms.Button()
        Me.btnEliminar = New System.Windows.Forms.Button()
        Me.ToolTipAtendido = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTipEliminar = New System.Windows.Forms.ToolTip(Me.components)
        Me.Reloj = New System.Windows.Forms.Label()
        Me.Timer1 = New System.Windows.Forms.Timer(Me.components)
        Me.rbtvalrojo = New System.Windows.Forms.CheckBox()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.cbxNoatendido = New System.Windows.Forms.ComboBox()
        Me.txtNoatendido = New System.Windows.Forms.TextBox()
        Me.ToolStripStatusLabel4 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.ToolStripStatusLabel5 = New System.Windows.Forms.ToolStripStatusLabel()
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsMenu.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        Me.gbxPaciente.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.ControlLight
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.colNumCtrl, Me.Prioridad, Me.Nombre, Me.Sexo, Me.Prevaloracion, Me.idpaciente2, Me.cnumcontrol, Me.cama})
        Me.dgvListado.ContextMenuStrip = Me.cmsMenu
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.Color.Black
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle6
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.GridColor = System.Drawing.SystemColors.Window
        Me.dgvListado.Location = New System.Drawing.Point(0, 104)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle7.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle7.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle7.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle7.SelectionBackColor = System.Drawing.Color.Transparent
        DataGridViewCellStyle7.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle7.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle7
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.RowTemplate.DefaultCellStyle.BackColor = System.Drawing.Color.White
        Me.dgvListado.RowTemplate.DefaultCellStyle.SelectionBackColor = System.Drawing.SystemColors.ControlLight
        Me.dgvListado.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgvListado.Size = New System.Drawing.Size(840, 300)
        Me.dgvListado.TabIndex = 0
        Me.dgvListado.TabStop = False
        '
        'colNumCtrl
        '
        Me.colNumCtrl.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.colNumCtrl.HeaderText = "Tiempo estimado"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Width = 116
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'Nombre
        '
        Me.Nombre.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle3
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Nombre.Width = 75
        '
        'Sexo
        '
        Me.Sexo.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCellsExceptHeader
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sexo.DefaultCellStyle = DataGridViewCellStyle4
        Me.Sexo.HeaderText = "Hora de atencion"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        Me.Sexo.Resizable = System.Windows.Forms.DataGridViewTriState.[False]
        Me.Sexo.Width = 5
        '
        'Prevaloracion
        '
        Me.Prevaloracion.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prevaloracion.DefaultCellStyle = DataGridViewCellStyle5
        Me.Prevaloracion.HeaderText = "Prevaloracion"
        Me.Prevaloracion.Name = "Prevaloracion"
        Me.Prevaloracion.ReadOnly = True
        Me.Prevaloracion.Width = 110
        '
        'idpaciente2
        '
        Me.idpaciente2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.idpaciente2.HeaderText = "idpaciente"
        Me.idpaciente2.Name = "idpaciente2"
        Me.idpaciente2.ReadOnly = True
        Me.idpaciente2.Visible = False
        '
        'cnumcontrol
        '
        Me.cnumcontrol.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        Me.cnumcontrol.HeaderText = "numero de control"
        Me.cnumcontrol.Name = "cnumcontrol"
        Me.cnumcontrol.ReadOnly = True
        Me.cnumcontrol.Visible = False
        '
        'cama
        '
        Me.cama.HeaderText = "cama"
        Me.cama.Name = "cama"
        Me.cama.ReadOnly = True
        '
        'cmsMenu
        '
        Me.cmsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmMenu, Me.ToolStripSeparator1, Me.tsmAtender, Me.tsmEliminar, Me.tsmpediatria, Me.tsmTriage})
        Me.cmsMenu.Name = "cmsOpciones"
        Me.cmsMenu.Size = New System.Drawing.Size(177, 120)
        '
        'tsmMenu
        '
        Me.tsmMenu.Name = "tsmMenu"
        Me.tsmMenu.Size = New System.Drawing.Size(176, 22)
        Me.tsmMenu.Text = "Menu"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(173, 6)
        '
        'tsmAtender
        '
        Me.tsmAtender.Name = "tsmAtender"
        Me.tsmAtender.Size = New System.Drawing.Size(176, 22)
        Me.tsmAtender.Text = "Atender Consulta"
        '
        'tsmEliminar
        '
        Me.tsmEliminar.Name = "tsmEliminar"
        Me.tsmEliminar.Size = New System.Drawing.Size(176, 22)
        Me.tsmEliminar.Text = "Eliminar"
        '
        'tsmpediatria
        '
        Me.tsmpediatria.Name = "tsmpediatria"
        Me.tsmpediatria.Size = New System.Drawing.Size(176, 22)
        Me.tsmpediatria.Text = "Urgencias Pediatricas"
        '
        'tsmTriage
        '
        Me.tsmTriage.Name = "tsmTriage"
        Me.tsmTriage.Size = New System.Drawing.Size(176, 22)
        Me.tsmTriage.Text = "Atendido Triage"
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor, Me.ToolStripStatusLabel4, Me.ToolStripStatusLabel5})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 554)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(865, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.Color.Transparent
        Me.gbxPaciente.Controls.Add(Me.Label1)
        Me.gbxPaciente.Controls.Add(Me.txtcama)
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(10, 421)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(840, 74)
        Me.gbxPaciente.TabIndex = 13
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(621, 25)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(37, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "cama"
        '
        'txtcama
        '
        Me.txtcama.BackColor = System.Drawing.SystemColors.Window
        Me.txtcama.Location = New System.Drawing.Point(624, 41)
        Me.txtcama.Name = "txtcama"
        Me.txtcama.ReadOnly = True
        Me.txtcama.Size = New System.Drawing.Size(106, 20)
        Me.txtcama.TabIndex = 4
        Me.txtcama.TabStop = False
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(122, 41)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(486, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtPrioridad
        '
        Me.txtPrioridad.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrioridad.Location = New System.Drawing.Point(6, 41)
        Me.txtPrioridad.Name = "txtPrioridad"
        Me.txtPrioridad.ReadOnly = True
        Me.txtPrioridad.Size = New System.Drawing.Size(110, 20)
        Me.txtPrioridad.TabIndex = 3
        Me.txtPrioridad.TabStop = False
        '
        'lblPrioridad
        '
        Me.lblPrioridad.AutoSize = True
        Me.lblPrioridad.Location = New System.Drawing.Point(3, 25)
        Me.lblPrioridad.Name = "lblPrioridad"
        Me.lblPrioridad.Size = New System.Drawing.Size(57, 13)
        Me.lblPrioridad.TabIndex = 2
        Me.lblPrioridad.Text = "Prioridad"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(119, 25)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'btnAtendido
        '
        Me.btnAtendido.Enabled = False
        Me.btnAtendido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAtendido.Location = New System.Drawing.Point(10, 511)
        Me.btnAtendido.Name = "btnAtendido"
        Me.btnAtendido.Size = New System.Drawing.Size(140, 23)
        Me.btnAtendido.TabIndex = 3
        Me.btnAtendido.Text = "Atendido"
        Me.ToolTipAtendido.SetToolTip(Me.btnAtendido, "Permite inciar consulta del paciente")
        Me.btnAtendido.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(171, 511)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(140, 23)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "No atendido"
        Me.ToolTipEliminar.SetToolTip(Me.btnEliminar, "Borra al paciente de la lista debido a que no se presento")
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'ToolTipAtendido
        '
        Me.ToolTipAtendido.ToolTipTitle = "Atiende al paciente en el consultorio"
        '
        'ToolTipEliminar
        '
        Me.ToolTipEliminar.AutoPopDelay = 5000
        Me.ToolTipEliminar.InitialDelay = 500
        Me.ToolTipEliminar.ReshowDelay = 10
        Me.ToolTipEliminar.ToolTipTitle = "Elimina paciente"
        '
        'Reloj
        '
        Me.Reloj.AutoSize = True
        Me.Reloj.BackColor = System.Drawing.SystemColors.ControlLight
        Me.Reloj.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Reloj.Location = New System.Drawing.Point(630, 554)
        Me.Reloj.Name = "Reloj"
        Me.Reloj.Size = New System.Drawing.Size(80, 22)
        Me.Reloj.TabIndex = 14
        Me.Reloj.Text = "00:00:00"
        '
        'Timer1
        '
        '
        'rbtvalrojo
        '
        Me.rbtvalrojo.AutoSize = True
        Me.rbtvalrojo.BackColor = System.Drawing.Color.Red
        Me.rbtvalrojo.ForeColor = System.Drawing.Color.Snow
        Me.rbtvalrojo.Location = New System.Drawing.Point(758, 515)
        Me.rbtvalrojo.Name = "rbtvalrojo"
        Me.rbtvalrojo.Size = New System.Drawing.Size(92, 17)
        Me.rbtvalrojo.TabIndex = 15
        Me.rbtvalrojo.Text = "valorados rojo"
        Me.rbtvalrojo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        Me.rbtvalrojo.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText
        Me.rbtvalrojo.UseVisualStyleBackColor = False
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = Global.TRIAGE.My.Resources.Resources.triage
        Me.PictureBox1.Location = New System.Drawing.Point(218, 4)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(400, 94)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'cbxNoatendido
        '
        Me.cbxNoatendido.Enabled = False
        Me.cbxNoatendido.FormattingEnabled = True
        Me.cbxNoatendido.Items.AddRange(New Object() {"No se presento", "no quizo atenderser", "atendido por servicio social", "otro"})
        Me.cbxNoatendido.Location = New System.Drawing.Point(317, 513)
        Me.cbxNoatendido.Name = "cbxNoatendido"
        Me.cbxNoatendido.Size = New System.Drawing.Size(151, 21)
        Me.cbxNoatendido.TabIndex = 17
        Me.cbxNoatendido.Visible = False
        '
        'txtNoatendido
        '
        Me.txtNoatendido.Enabled = False
        Me.txtNoatendido.Location = New System.Drawing.Point(474, 501)
        Me.txtNoatendido.Multiline = True
        Me.txtNoatendido.Name = "txtNoatendido"
        Me.txtNoatendido.Size = New System.Drawing.Size(266, 43)
        Me.txtNoatendido.TabIndex = 18
        Me.txtNoatendido.Visible = False
        '
        'ToolStripStatusLabel4
        '
        Me.ToolStripStatusLabel4.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel4.Font = New System.Drawing.Font("Segoe UI", 9.0!, System.Drawing.FontStyle.Bold)
        Me.ToolStripStatusLabel4.Name = "ToolStripStatusLabel4"
        Me.ToolStripStatusLabel4.Size = New System.Drawing.Size(54, 17)
        Me.ToolStripStatusLabel4.Text = "personal"
        '
        'ToolStripStatusLabel5
        '
        Me.ToolStripStatusLabel5.BackColor = System.Drawing.Color.Transparent
        Me.ToolStripStatusLabel5.Name = "ToolStripStatusLabel5"
        Me.ToolStripStatusLabel5.Size = New System.Drawing.Size(25, 17)
        Me.ToolStripStatusLabel5.Text = "tipo"
        '
        'frmListadoPacientesGO
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Window
        Me.ClientSize = New System.Drawing.Size(865, 576)
        Me.Controls.Add(Me.txtNoatendido)
        Me.Controls.Add(Me.cbxNoatendido)
        Me.Controls.Add(Me.rbtvalrojo)
        Me.Controls.Add(Me.Reloj)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvListado)
        Me.Controls.Add(Me.btnAtendido)
        Me.Controls.Add(Me.btnEliminar)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D
        Me.Name = "frmListadoPacientesGO"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de pacientes"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsMenu.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents cmsMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmAtender As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmEliminar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsmpediatria As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents txtPrioridad As System.Windows.Forms.TextBox
    Friend WithEvents lblPrioridad As System.Windows.Forms.Label
    Friend WithEvents btnAtendido As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents ToolTipAtendido As System.Windows.Forms.ToolTip
    Friend WithEvents ToolTipEliminar As System.Windows.Forms.ToolTip
    Friend WithEvents Reloj As System.Windows.Forms.Label
    Friend WithEvents Timer1 As System.Windows.Forms.Timer
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents txtcama As System.Windows.Forms.TextBox
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prevaloracion As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents idpaciente2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cnumcontrol As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents cama As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents rbtvalrojo As System.Windows.Forms.CheckBox
    Friend WithEvents cbxNoatendido As System.Windows.Forms.ComboBox
    Friend WithEvents txtNoatendido As System.Windows.Forms.TextBox
    Friend WithEvents ToolStripStatusLabel4 As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents ToolStripStatusLabel5 As System.Windows.Forms.ToolStripStatusLabel
End Class
