﻿Imports System.Data.SqlClient

Public Class frmSeguimiento

    Private Sub frmSeguimiento_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        Dim strSQL As String

        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand

        Try

            strSQL = "EXEC zspTRI_selSeguimiento 1" '1 -> trae solo la primera nota de la consulta

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)
            comando.CommandTimeout() = 5000
            cnn.Open()

            comando.CommandText = strSQL
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then

                Dim fecha_nota As String

                While lectorhere.Read
                    fecha_nota = ""

                    If lectorhere("fecha_nota").ToString() <> "01/01/1900 12:00:00 a.m." Then
                        fecha_nota = lectorhere("fecha_nota").ToString()
                    End If


                    dgvListado.Rows.Add(lectorhere("cnum_control").ToString(), lectorhere("cStatus").ToString(), lectorhere("paciente").ToString(), _
                                        lectorhere("csexo").ToString(), lectorhere("hora_ingreso").ToString(), lectorhere("area_inicial").ToString(), _
                                        lectorhere("area_actual").ToString(), lectorhere("medico_nota").ToString(), fecha_nota)

                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()
    End Sub

    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting

        For Each row As DataGridViewRow In dgvListado.Rows
            If row.Cells("cStatus").Value = "H" Then
                row.DefaultCellStyle.ForeColor = Color.Blue
            Else
                row.DefaultCellStyle.ForeColor = Color.Black
            End If
        Next
    End Sub

    Private Sub dgvListado_CellContentClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellContentClick

    End Sub

End Class