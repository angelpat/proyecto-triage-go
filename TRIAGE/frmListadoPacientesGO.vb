Imports System.Data.SqlClient

Public Class frmListadoPacientesGO
    'Dim cnn As SqlClient.SqlConnection = New System.Data.SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
    'Dim lector As System.Data.SqlClient.SqlDataReader
    'Dim comando As System.Data.SqlClient.SqlCommand = New System.Data.SqlClient.SqlCommand
    Dim IdTRIAGE As Integer  
    Dim nombre1 As String
    Dim valor As String
    Dim fecha As Date
    Public idpaciente As String
    Dim Matencion As String
    Public cnumcontrol1 As Int64
    Dim motivo As String
    Dim db As conexionbd = New conexionbd()

    Private Sub RefrescaLista()
        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim cama As String
        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand
        IdTRIAGE = 0
        Try

            VchCadena = "EXEC zspTRI_PacientesConsultaGO" '0 -> Se trae la lista completa
            db.validarbd()
            cnn = New SqlConnection(db.conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read
                    'If lectorhere("ALTA") <> "S" Then
                    nombre1 = lectorhere.getvalue(0)
                    valor = RTrim(Mid(lectorhere.getvalue(1), 3, 9))
                    fecha = FormatDateTime(lectorhere.getvalue(2), DateFormat.GeneralDate)
                    'fecha = lectorhere.getvalue(2)
                    idpaciente = lectorhere.getvalue(3)
                    Matencion = lectorhere.getvalue(4)
                    cnumcontrol1 = lectorhere.GetInt64(5)
                    cama = lectorhere.getvalue(6)
                    If valor = "Rojo" Then
                        If rbtvalrojo.Checked Then
                            dgvListado.Rows.Add("15 min", valor, nombre1, fecha, Matencion, idpaciente, cnumcontrol1, cama)
                        End If
                    ElseIf valor = "Amarillo" Then
                        dgvListado.Rows.Add("15 min", valor, nombre1, fecha, Matencion, idpaciente, cnumcontrol1, cama)
                    Else
                        dgvListado.Rows.Add("30 min", valor, nombre1, fecha, Matencion, idpaciente, cnumcontrol1, cama)
                    End If


                    'Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString

                    ' TerminaConsulta(nombre, frmPrincipalTriage.VchIdEmpleadoG, idpaciente)
                    'End If
                    Numero = Numero + 1
                End While
            End If
            lectorhere.Close()
            cnn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
      


    End Sub
    Public Sub TerminaConsulta(ByVal idpaciente As String, ByVal estado As String, ByVal idpersonal As String, num_control As Int64, ByVal Atendido As Boolean)
        Dim cnn As SqlConnection
        Try
            'Estado: 'A' -> Atendido, 'E'-> Espera, 'R'-> Se Retiro, 'O' -> Pasa a Observaci�n
            db.validarbd()
            cnn = New SqlConnection(db.conexion)
            cnn.Open()
            Dim comando As SqlCommand = New SqlCommand("", cnn)

            With comando
                .CommandType = CommandType.Text
                .CommandText = "zspTRI_ConsultaPacienteGO'" & idpaciente & "','" & estado & "','" & idpersonal & "'," & num_control & ",'" & Atendido & "';"
                valor = "zspTRI_ConsultaPacienteGO'" & idpaciente & "','" & estado & "','" & idpersonal & "','" & num_control & "'," & Atendido & ";"
                'comando.CommandText = "UPDATE ZTRI_TRIAGE SET ESTADO='" + Estado + "', FECHAS=GETDATE(), MODIFICADO_POR='" + frmPrincipalTriage.VchIdEmpleadoG + "', F_MODIFICACION=GETDATE() WHERE IDTRIAGE='" + Id + "'"
                .Connection = cnn
                .ExecuteNonQuery()
            End With
            cnn.Close()
          
        Catch ex As Exception

            MessageBox.Show(ex.Message.ToString, "Error no esperado")

        End Try
    End Sub




    Private Sub frmListadoPacientes_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        frmPrincipalTriage.validaAcceso()
        RefrescaLista()
        lblNombreRevisor.Text = frmEvaluar.RegresaEmpleado()
        ' Set to 1 second.
        Timer1.Interval = 1000
        ' Enable timer.
        Timer1.Enabled = True
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
    End Sub


    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting

        If dgvListado.Columns(e.ColumnIndex).Name = "Prioridad" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToString

                If ((LTrim(stringValue.ToString) = "Rojo")) Then
                    e.CellStyle.BackColor = Color.Red
                    e.CellStyle.SelectionBackColor = Color.Transparent

                ElseIf ((LTrim(stringValue.ToString) = "Amarillo")) Then
                    e.CellStyle.BackColor = Color.Yellow
                    e.CellStyle.SelectionBackColor = Color.Transparent

                ElseIf ((LTrim(stringValue.ToString) = "Verde")) Then
                    e.CellStyle.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                    e.CellStyle.SelectionBackColor = Color.Transparent

                End If
            End If
        End If

    End Sub


    Private Sub btnRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
        dgvListado.Rows.Clear()
        RefrescaLista()
        InicializaFormulario()

    End Sub


    'Private Sub btnAtender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs)
    '    TerminaConsulta("A", IdTRIAGE)
    '    dgvListado.Rows.Clear()
    '    RefrescaLista()
    'End Sub


    'Private Sub tsmAtender_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmAtender.Click, tsmpediatria.Click
    '    Try
    '        Dim IdT As String
    '        IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
    '        dgvListado.Rows.Clear()
    '        TerminaConsulta("A", IdT) 'A=Atendido
    '        RefrescaLista()
    '    Catch ex As Exception
    '        MessageBox.Show("No hay paciente seleccionado", "Advertencia")
    '    End Try
    'End Sub


    'Private Sub tsmEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmEliminar.Click
    '    Try
    '        If MsgBox("�Desea eliminar paciente?", MsgBoxStyle.YesNo) = MsgBoxResult.Yes Then
    '            Dim IdT As String
    '            IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
    '            CambiaEstadoHGC_ADMISION(dgvListado.SelectedRows(0).Cells(3).Value.ToString)
    '            dgvListado.Rows.Clear()
    '            TerminaConsulta("R", IdT) 'R=Retirado
    '            RefrescaLista()
    '        End If
    '    Catch ex As Exception
    '        MessageBox.Show("No hay paciente seleccionado", "Advertencia")
    '    End Try
    'End Sub


    'Private Sub tsmTriage_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles tsmTriage.Click
    '    Try
    '        Dim IdT As String
    '        IdT = dgvListado.SelectedRows(0).Cells(0).Value.ToString
    '        CambiaEstadoHGC_ADMISION(dgvListado.SelectedRows(0).Cells(3).Value.ToString)
    '        dgvListado.Rows.Clear()
    '        TerminaConsulta("R", IdT) 'R=Retirado
    '        RefrescaLista()
    '    Catch ex As Exception
    '        MessageBox.Show("No hay paciente seleccionado", "Advertencia")
    '    End Try
    'End Sub


    Private Sub btnAtendido_Click() Handles btnAtendido.Click
        If frmPrincipalTriage.vchTipo = "04" Then
            TerminaConsulta(idpaciente, "atendido", frmPrincipalTriage.VchIdEmpleadoG, cnumcontrol1, "1")
            dgvListado.Rows.Clear()
            RefrescaLista()
            InicializaFormulario()
        Else
            MsgBox("No cuenta con los privilegios ejecutar esta acci�n")
        End If
    End Sub


    'Private Sub btnEliminar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnEliminar.Click
    '    If frmPrincipalTriage.vchTipo = "04" Or frmPrincipalTriage.vchTipo = "02" Then
    '        CambiaEstadoHGC_ADMISION(idPaciente)
    '        TerminaConsulta("R", IdTRIAGE, NumControl, idPaciente)
    '        dgvListado.Rows.Clear()
    '        RefrescaLista()
    '        InicializaFormulario()
    '    Else
    '        MsgBox("No cuenta con los privilegios ejecutar esta acci�n")
    '    End If
    'End Sub

    Private Sub InicializaFormulario()
        txtPrioridad.BackColor = Color.White
        txtPrioridad.Text = ""
        txtPaciente.Text = ""
        btnAtendido.Enabled = False
        btnEliminar.Enabled = False
    End Sub


    Private Sub dgvListado_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellClick
        If e.RowIndex >= 0 AndAlso dgvListado.Item("Prioridad", e.RowIndex).Value IsNot Nothing Then

            ' IdTRIAGE = dgvListado.Item("idT", e.RowIndex).Value
            ' idPaciente = dgvListado.Item("", e.RowIndex).Value
            ' NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
            txtPrioridad.Text = dgvListado.Item("Prioridad", e.RowIndex).Value
            txtPaciente.Text = dgvListado.Item("nombre", e.RowIndex).Value
            cnumcontrol1 = dgvListado.Item("cnumcontrol", e.RowIndex).Value
            idpaciente = dgvListado.Item("idpaciente2", e.RowIndex).Value
            txtcama.Text = dgvListado.Item("cama", e.RowIndex).Value

            '    If (dgvListado.Item("Sexo", e.RowIndex).Value = "M") Then
            '        txtSexo.Text = "Masculino"
            '    Else
            '        txtSexo.Text = "Femenino"
            '    End If

            '    txtEdad.Text = dgvListado.Item("Edad", e.RowIndex).Value

            '    If (dgvListado.Item("Prioridad", e.RowIndex).Value = "1") Then
            '        txtPrioridad.BackColor = Color.Red
            '    ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "2") Then
            '        txtPrioridad.BackColor = Color.Yellow
            '    ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "3") Then
            '        txtPrioridad.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
            '    ElseIf (dgvListado.Item("Prioridad", e.RowIndex).Value = "4") Then
            '        txtPrioridad.BackColor = Color.SkyBlue
        End If

        btnAtendido.Enabled = True
        btnEliminar.Enabled = True

        'End If
    End Sub
    Public Sub relojactual()
        Dim hora As String
        Dim fecha As String
        fecha = DateAndTime.Now.ToString
        hora = DateAndTime.Now.ToString
        Reloj.Text = FormatDateTime(fecha, DateFormat.ShortDate) + "-" + FormatDateTime(hora, DateFormat.LongTime)

    End Sub

   
    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        relojactual()
        dgvListado.Rows.Clear()
        RefrescaLista()
    End Sub

    Private Sub btnEliminar_Click(sender As Object, e As EventArgs) Handles btnEliminar.Click

        If txtNoatendido.Text = "" And cbxNoatendido.SelectedItem = "" Then
            MessageBox.Show("Seleccione el Motivo de NO atencion")
            cbxNoatendido.Visible = True
            cbxNoatendido.Enabled = True
            txtNoatendido.Visible = True
            btnEliminar.Text = "Guardar"
        Else
            cbxNoatendido.Visible = False
            cbxNoatendido.Enabled = False
            txtNoatendido.Visible = False
            btnEliminar.Text = "No atendido"
            '  cbxNoatendido.SelectedIndex = -1
            If txtNoatendido.Text <> "" Then
                motivo = txtNoatendido.Text
            End If
            TerminaConsulta(idpaciente, motivo, frmPrincipalTriage.VchIdEmpleadoG, cnumcontrol1, "0")
            txtPrioridad.Text = ""
            txtPaciente.Text = ""
            txtcama.Text = ""
            txtNoatendido.Text = ""
            btnAtendido.Enabled = False
            btnEliminar.Enabled = False

        End If

        'TerminaConsulta(idpaciente, "Noatendido", frmPrincipalTriage.VchIdEmpleadoG, cnumcontrol1)
        'dgvListado.Rows.Clear()
        'RefrescaLista()
        cbxNoatendido.SelectedText = ""


    End Sub

   
    Private Sub cbxNoatendido_SelectedIndexChanged(sender As Object, e As EventArgs) Handles cbxNoatendido.SelectedIndexChanged
        If cbxNoatendido.SelectedItem = "otro" Then
            txtNoatendido.Enabled = True
            motivo = txtNoatendido.Text
        Else
            txtNoatendido.Enabled = False
            motivo = cbxNoatendido.SelectedItem.ToString
        End If
        'TerminaConsulta(idpaciente, motivo, frmPrincipalTriage.VchIdEmpleadoG, cnumcontrol1)
        'motivo = "."
        'btnEliminar.Text = "No atendido"
        'cbxNoatendido.Enabled = False
        'txtNoatendido.Enabled = False

    End Sub

End Class
