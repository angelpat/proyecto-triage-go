<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmBuscarPaciente
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmBuscarPaciente))
        Me.lblPaterno = New System.Windows.Forms.Label
        Me.lblMaterno = New System.Windows.Forms.Label
        Me.lblNombre = New System.Windows.Forms.Label
        Me.txtPaterno = New System.Windows.Forms.TextBox
        Me.txtMaterno = New System.Windows.Forms.TextBox
        Me.txtNombre = New System.Windows.Forms.TextBox
        Me.btnBuscarPaciente = New System.Windows.Forms.Button
        Me.dgvPacientes = New System.Windows.Forms.DataGridView
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Materno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Edad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnNuevaBusqueda = New System.Windows.Forms.Button
        Me.btnRegresar = New System.Windows.Forms.Button
        CType(Me.dgvPacientes, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'lblPaterno
        '
        Me.lblPaterno.AutoSize = True
        Me.lblPaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblPaterno.Location = New System.Drawing.Point(9, 19)
        Me.lblPaterno.Name = "lblPaterno"
        Me.lblPaterno.Size = New System.Drawing.Size(99, 13)
        Me.lblPaterno.TabIndex = 0
        Me.lblPaterno.Text = "Apellido paterno"
        '
        'lblMaterno
        '
        Me.lblMaterno.AutoSize = True
        Me.lblMaterno.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMaterno.Location = New System.Drawing.Point(260, 19)
        Me.lblMaterno.Name = "lblMaterno"
        Me.lblMaterno.Size = New System.Drawing.Size(101, 13)
        Me.lblMaterno.TabIndex = 1
        Me.lblMaterno.Text = "Apellido materno"
        '
        'lblNombre
        '
        Me.lblNombre.AutoSize = True
        Me.lblNombre.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNombre.Location = New System.Drawing.Point(511, 19)
        Me.lblNombre.Name = "lblNombre"
        Me.lblNombre.Size = New System.Drawing.Size(64, 13)
        Me.lblNombre.TabIndex = 2
        Me.lblNombre.Text = "Nombre(s)"
        '
        'txtPaterno
        '
        Me.txtPaterno.Location = New System.Drawing.Point(12, 35)
        Me.txtPaterno.Name = "txtPaterno"
        Me.txtPaterno.Size = New System.Drawing.Size(242, 20)
        Me.txtPaterno.TabIndex = 3
        '
        'txtMaterno
        '
        Me.txtMaterno.Location = New System.Drawing.Point(263, 35)
        Me.txtMaterno.Name = "txtMaterno"
        Me.txtMaterno.Size = New System.Drawing.Size(242, 20)
        Me.txtMaterno.TabIndex = 4
        '
        'txtNombre
        '
        Me.txtNombre.Location = New System.Drawing.Point(514, 35)
        Me.txtNombre.Name = "txtNombre"
        Me.txtNombre.Size = New System.Drawing.Size(242, 20)
        Me.txtNombre.TabIndex = 5
        '
        'btnBuscarPaciente
        '
        Me.btnBuscarPaciente.Location = New System.Drawing.Point(12, 63)
        Me.btnBuscarPaciente.Name = "btnBuscarPaciente"
        Me.btnBuscarPaciente.Size = New System.Drawing.Size(150, 23)
        Me.btnBuscarPaciente.TabIndex = 6
        Me.btnBuscarPaciente.Text = "Ejecuta búsqueda"
        Me.btnBuscarPaciente.UseVisualStyleBackColor = True
        '
        'dgvPacientes
        '
        Me.dgvPacientes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvPacientes.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.Curp, Me.Paterno, Me.Materno, Me.Nombre, Me.Sexo, Me.Edad})
        Me.dgvPacientes.Location = New System.Drawing.Point(12, 93)
        Me.dgvPacientes.Name = "dgvPacientes"
        Me.dgvPacientes.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.dgvPacientes.Size = New System.Drawing.Size(743, 262)
        Me.dgvPacientes.TabIndex = 7
        '
        'Curp
        '
        Me.Curp.HeaderText = "Curp"
        Me.Curp.Name = "Curp"
        Me.Curp.Width = 150
        '
        'Paterno
        '
        Me.Paterno.HeaderText = "A. Paterno"
        Me.Paterno.Name = "Paterno"
        Me.Paterno.Width = 150
        '
        'Materno
        '
        Me.Materno.HeaderText = "A. Materno"
        Me.Materno.Name = "Materno"
        Me.Materno.Width = 150
        '
        'Nombre
        '
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.Width = 150
        '
        'Sexo
        '
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.Width = 40
        '
        'Edad
        '
        Me.Edad.HeaderText = "Edad"
        Me.Edad.Name = "Edad"
        Me.Edad.Width = 40
        '
        'btnNuevaBusqueda
        '
        Me.btnNuevaBusqueda.Location = New System.Drawing.Point(168, 64)
        Me.btnNuevaBusqueda.Name = "btnNuevaBusqueda"
        Me.btnNuevaBusqueda.Size = New System.Drawing.Size(150, 23)
        Me.btnNuevaBusqueda.TabIndex = 8
        Me.btnNuevaBusqueda.Text = "Nueva búsqueda"
        Me.btnNuevaBusqueda.UseVisualStyleBackColor = True
        '
        'btnRegresar
        '
        Me.btnRegresar.Location = New System.Drawing.Point(606, 64)
        Me.btnRegresar.Name = "btnRegresar"
        Me.btnRegresar.Size = New System.Drawing.Size(150, 23)
        Me.btnRegresar.TabIndex = 9
        Me.btnRegresar.Text = "Regresar"
        Me.btnRegresar.UseVisualStyleBackColor = True
        '
        'frmBuscarPaciente
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(768, 367)
        Me.Controls.Add(Me.btnRegresar)
        Me.Controls.Add(Me.btnNuevaBusqueda)
        Me.Controls.Add(Me.dgvPacientes)
        Me.Controls.Add(Me.btnBuscarPaciente)
        Me.Controls.Add(Me.txtNombre)
        Me.Controls.Add(Me.txtMaterno)
        Me.Controls.Add(Me.txtPaterno)
        Me.Controls.Add(Me.lblNombre)
        Me.Controls.Add(Me.lblMaterno)
        Me.Controls.Add(Me.lblPaterno)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmBuscarPaciente"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Búsqueda de pacientes"
        CType(Me.dgvPacientes, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents lblPaterno As System.Windows.Forms.Label
    Friend WithEvents lblMaterno As System.Windows.Forms.Label
    Friend WithEvents lblNombre As System.Windows.Forms.Label
    Friend WithEvents txtPaterno As System.Windows.Forms.TextBox
    Friend WithEvents txtMaterno As System.Windows.Forms.TextBox
    Friend WithEvents txtNombre As System.Windows.Forms.TextBox
    Friend WithEvents btnBuscarPaciente As System.Windows.Forms.Button
    Friend WithEvents dgvPacientes As System.Windows.Forms.DataGridView
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Edad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents btnNuevaBusqueda As System.Windows.Forms.Button
    Friend WithEvents btnRegresar As System.Windows.Forms.Button
End Class
