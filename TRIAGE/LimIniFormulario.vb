﻿Public Class LimIniFormulario

    Public Sub limpiarFormulario()
        ' limpiar labels
        frmEvaluarObstetrico.lblPresionA.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPulso.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblIndChoque.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblTemp.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTemp.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblRespiracion.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblIMC.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblEstadoConciencia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHemorragia.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblDolorObstetrico.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCrisisConvulsiva.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblNausea.BackColor = Color.Transparent


        frmEvaluarObstetrico.lblRespiracon1.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPalidez.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblsangradotrans.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblCefalea.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblAcufos.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblfosfenos.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblepigastralgia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblamaurosis.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblsindromefebril.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblliquidoamn.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblfetal.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCirUterina.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTrauma.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblSangrado.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCardiopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblNefropatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHematopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblEndocrinopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHepatopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPruebaVIH.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPruebaSifilis.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTiraUroanalisis.BackColor = Color.Transparent

        '''''''''''''''''''''''''''''''''''''''
        frmEvaluarObstetrico.pnlPrioridad.BackColor = Color.Transparent



        'Datos del paciente     
        frmEvaluarObstetrico.cbxPaciente.Text = ""
        frmEvaluarObstetrico.txtCurp.Text = ""
        frmEvaluarObstetrico.txtEdad.Text = ""
        frmEvaluarObstetrico.txtEdadGestacional.Text = ""
        frmEvaluarObstetrico.cbxUnidad1.Text = ""
        frmEvaluarObstetrico.cbxUnidad1.Text = ""
        'rbtReferido.Text = ""
        frmEvaluarObstetrico.txtDerechohabiencia.Text = ""
        frmEvaluarObstetrico.txtfechadeingreso.Text = ""
        frmEvaluarObstetrico.txtfechaactual.Text = ""

        'SIGNOS VITALES
        ' MAMA
        frmEvaluarObstetrico.txtFrecCard.Text = ""
        frmEvaluarObstetrico.txtRespiracion.Text = ""
        frmEvaluarObstetrico.txtPeso.Text = ""
        frmEvaluarObstetrico.txtAltura.Text = ""
        frmEvaluarObstetrico.txtPaSistolica.Text = ""
        frmEvaluarObstetrico.txtPaDiastolica.Text = ""
        frmEvaluarObstetrico.txtTemperatura.Text = ""
        frmEvaluarObstetrico.txtGlucosa.Text = ""
        frmEvaluarObstetrico.txtIMC.Text = ""
        frmEvaluarObstetrico.txtIndChoque.Text = ""

        ' FETO
        frmEvaluarObstetrico.txtFCFFeto.Text = ""
        frmEvaluarObstetrico.txtEdadGestacional.Text = ""
        ' SINTOMAS
        frmEvaluarObstetrico.rbtDolorObstetricoB.Checked = False
        frmEvaluarObstetrico.rbtDolorObstetricoM.Checked = False
        frmEvaluarObstetrico.rbtDolorObstetricoA.Checked = False
        frmEvaluarObstetrico.rbtCrisisConvulsivaB.Checked = False
        frmEvaluarObstetrico.rbtCrisisConvulsivaA.Checked = False

        frmEvaluarObstetrico.rbtHemorragiaB.Checked = False
        frmEvaluarObstetrico.rbtHemorragiaM.Checked = False
        frmEvaluarObstetrico.rbtHemorragiaA.Checked = False

  

        frmEvaluarObstetrico.rbtNauseaB.Checked = False
        frmEvaluarObstetrico.rbtNauseaM.Checked = False
        frmEvaluarObstetrico.rbtNauseaA.Checked = False

        frmEvaluarObstetrico.rbtRespiracionB.Checked = False
        frmEvaluarObstetrico.rbtRespiracionM.Checked = False
        frmEvaluarObstetrico.rbtRespiracionA.Checked = False

        frmEvaluarObstetrico.rbtSangradoA.Checked = False
        frmEvaluarObstetrico.rbtSangradoM.Checked = False
        frmEvaluarObstetrico.rbtSangradoB.Checked = False




        'SIGNOS GENERALES

        frmEvaluarObstetrico.rbtCefaleaB.Checked = False
        frmEvaluarObstetrico.rbtCefaleaM.Checked = False
        frmEvaluarObstetrico.rbtCefaleaA.Checked = False

        frmEvaluarObstetrico.rbtAcufenosB.Checked = False

        frmEvaluarObstetrico.rbtAcufenosA.Checked = False

        frmEvaluarObstetrico.rbtEstadoConcienciaB.Checked = False
        frmEvaluarObstetrico.rbtEstadoConcienciaM.Checked = False
        frmEvaluarObstetrico.rbtEstadoConcienciaA.Checked = False

        frmEvaluarObstetrico.rbtAmaurosisB.Checked = False

        frmEvaluarObstetrico.rbtAmaurosisA.Checked = False

        frmEvaluarObstetrico.rbtPalidezB.Checked = False
        frmEvaluarObstetrico.rbtPalidezM.Checked = False
        frmEvaluarObstetrico.rbtPalidezA.Checked = False

        'INFORMACION CONOCIDA DEL EMBARAZO Y ANTECEDENTES DE INTERES

        frmEvaluarObstetrico.rbtCirUterinaB.Checked = False
        frmEvaluarObstetrico.rbtCirUterinaM.Checked = False


        frmEvaluarObstetrico.rbtTraumaB.Checked = False
        frmEvaluarObstetrico.rbtTraumaM.Checked = False
        frmEvaluarObstetrico.rbtTraumaA.Checked = False

        frmEvaluarObstetrico.rbtSangradoB.Checked = False
        frmEvaluarObstetrico.rbtSangradoM.Checked = False
        frmEvaluarObstetrico.rbtSangradoA.Checked = False

        'ENFERMEDADES ASOCIADAS CONOCIDAS

        frmEvaluarObstetrico.rbtCardiopatiaB.Checked = False
        frmEvaluarObstetrico.rbtCardiopatiaM.Checked = False
        frmEvaluarObstetrico.rbtCardiopatiaA.Checked = False

        frmEvaluarObstetrico.rbtNefropatiaB.Checked = False
        frmEvaluarObstetrico.rbtNefropatiaM.Checked = False
        frmEvaluarObstetrico.rbtNefropatiaA.Checked = False

        frmEvaluarObstetrico.rbtEndocrinopatiaB.Checked = False
        frmEvaluarObstetrico.rbtEndocrinopatiaM.Checked = False
        frmEvaluarObstetrico.rbtEndocrinopatiaA.Checked = False

        frmEvaluarObstetrico.rbtHepatopatiaB.Checked = False
        frmEvaluarObstetrico.rbtHepatopatiaM.Checked = False
        frmEvaluarObstetrico.rbtHepatopatiaA.Checked = False

        'ENFERMEDADES DE TAMIZ: VIH, SIFILIS

        frmEvaluarObstetrico.rbtPruebaVIHB.Checked = False
        frmEvaluarObstetrico.rbtPruebaVIHA.Checked = False
        frmEvaluarObstetrico.cbxResultadoVIH.Text = ""

        frmEvaluarObstetrico.rbtPruebaSifilisB.Checked = False
        frmEvaluarObstetrico.rbtPruebaSifilisA.Checked = False
        frmEvaluarObstetrico.cbxResultadoSIFILIS.Text = ""

        frmEvaluarObstetrico.rbtTiraUroanalisisB.Checked = False
        frmEvaluarObstetrico.rbtTiraUroanalisisM.Checked = False
        frmEvaluarObstetrico.rbtTiraUroanalisisA.Checked = False



    End Sub

    Public Sub inicializaFormulario()

        'SIGNOS VITALES
        ' MAMA
        'frmEvaluarObstetrico.txtFrecCard.Text = "0"
        'frmEvaluarObstetrico.txtRespiracion.Text = "0"
        'frmEvaluarObstetrico.txtPeso.Text = "0"
        'frmEvaluarObstetrico.txtAltura.Text = "0"
        'frmEvaluarObstetrico.txtPaSistolica.Text = "0"
        'frmEvaluarObstetrico.txtPaDiastolica.Text = "0"
        'frmEvaluarObstetrico.txtTemperatura.Text = "0"
        'frmEvaluarObstetrico.txtGlucosa.Text = "0"
        'frmEvaluarObstetrico.txtIMC.Text = Math.Round(frmEvaluarObstetrico.txtPeso.Text / Math.Pow(frmEvaluarObstetrico.txtAltura.Text, 2), 2)
        'frmEvaluarObstetrico.txtIndChoque.Text = Math.Round(frmEvaluarObstetrico.txtFrecCard.Text / frmEvaluarObstetrico.txtPaSistolica.Text, 3)
        '' FETO
        'frmEvaluarObstetrico.txtFCFFeto.Text = "0"
        'frmEvaluarObstetrico.txtEdadGestacional.Text = "0"
        ' SINTOMAS
        ' limpiar labels
        frmEvaluarObstetrico.lblPresionA.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPulso.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblIndChoque.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblTemp.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTemp.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblRespiracion.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblIMC.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblEstadoConciencia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHemorragia.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblDolorObstetrico.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCrisisConvulsiva.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblNausea.BackColor = Color.Transparent


        frmEvaluarObstetrico.lblRespiracon1.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPalidez.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblsangradotrans.BackColor = Color.Transparent

        frmEvaluarObstetrico.lblCefalea.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblAcufos.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblfosfenos.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblepigastralgia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblamaurosis.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblsindromefebril.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblliquidoamn.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblfetal.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCirUterina.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTrauma.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblSangrado.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblCardiopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblNefropatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHematopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblEndocrinopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblHepatopatia.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPruebaVIH.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblPruebaSifilis.BackColor = Color.Transparent
        frmEvaluarObstetrico.lblTiraUroanalisis.BackColor = Color.Transparent

        '''''''''''''''''''''''''''''''''''''''
        frmEvaluarObstetrico.pnlPrioridad.BackColor = Color.Transparent



        frmEvaluarObstetrico.rbtEstadoConcienciaB.Checked = True
        frmEvaluarObstetrico.rbtEstadoConcienciaM.Checked = False
        frmEvaluarObstetrico.rbtEstadoConcienciaA.Checked = False

        frmEvaluarObstetrico.rbtHemorragiaB.Checked = True
        frmEvaluarObstetrico.rbtHemorragiaM.Checked = False
        frmEvaluarObstetrico.rbtHemorragiaA.Checked = False

        frmEvaluarObstetrico.rbtDolorObstetricoB.Checked = True
        frmEvaluarObstetrico.rbtDolorObstetricoM.Checked = False
        frmEvaluarObstetrico.rbtDolorObstetricoA.Checked = False

        frmEvaluarObstetrico.rbtCrisisConvulsivaB.Checked = True
        frmEvaluarObstetrico.rbtCrisisConvulsivaA.Checked = False

        frmEvaluarObstetrico.rbtNauseaB.Checked = True
        frmEvaluarObstetrico.rbtNauseaM.Checked = False
        frmEvaluarObstetrico.rbtNauseaA.Checked = False

        frmEvaluarObstetrico.rbtRespiracionB.Checked = True
        frmEvaluarObstetrico.rbtRespiracionM.Checked = False
        frmEvaluarObstetrico.rbtRespiracionA.Checked = False

        frmEvaluarObstetrico.rbtPalidezB.Checked = True
        frmEvaluarObstetrico.rbtPalidezM.Checked = False
        frmEvaluarObstetrico.rbtPalidezA.Checked = False


        'interrogatorio
        frmEvaluarObstetrico.rbtSangradotransB.Checked = True
        frmEvaluarObstetrico.rbtSangradotransM.Checked = False
        frmEvaluarObstetrico.rbtSangradoA.Checked = False


        frmEvaluarObstetrico.rbtCefaleaB.Checked = True
        frmEvaluarObstetrico.rbtCefaleaM.Checked = False
        frmEvaluarObstetrico.rbtCefaleaA.Checked = False

        frmEvaluarObstetrico.rbtAcufenosB.Checked = True
        frmEvaluarObstetrico.rbtAcufenosA.Checked = False

        frmEvaluarObstetrico.rbtFosfenosB.Checked = True
        frmEvaluarObstetrico.rbtFosfenosA.Checked = False

        frmEvaluarObstetrico.rbtEpigastragiaB.Checked = True
        frmEvaluarObstetrico.rbtEpigastragiaA.Checked = False

        frmEvaluarObstetrico.rbtAmaurosisB.Checked = True
        frmEvaluarObstetrico.rbtAmaurosisA.Checked = False

        frmEvaluarObstetrico.rbtSindromeFebrilB.Checked = True
        frmEvaluarObstetrico.rbtSindromeFebrilA.Checked = False

        frmEvaluarObstetrico.rbtLiquidoAmnioticoB.Checked = True
        frmEvaluarObstetrico.rbtLiquidoAmnioticoA1.Checked = False
        frmEvaluarObstetrico.rbtLiquidoAmnioticoA2.Checked = False

        frmEvaluarObstetrico.rbtMotilidadFetalB.Checked = True
        frmEvaluarObstetrico.rbtMotilidadFetalM.Checked = False
        frmEvaluarObstetrico.rbtMotilidadFetalA.Checked = False

        'INFORMACION CONOCIDA DEL EMBARAZO Y ANTECEDENTES DE INTERES

        frmEvaluarObstetrico.rbtCirUterinaB.Checked = True
        frmEvaluarObstetrico.rbtCirUterinaM.Checked = False


        frmEvaluarObstetrico.rbtTraumaB.Checked = True
        frmEvaluarObstetrico.rbtTraumaM.Checked = False
        frmEvaluarObstetrico.rbtTraumaA.Checked = False

        frmEvaluarObstetrico.rbtSangradoB.Checked = True
        frmEvaluarObstetrico.rbtSangradoM.Checked = False
        frmEvaluarObstetrico.rbtSangradoA.Checked = False

        'ENFERMEDADES ASOCIADAS CONOCIDAS

        frmEvaluarObstetrico.rbtCardiopatiaB.Checked = True
        frmEvaluarObstetrico.rbtCardiopatiaM.Checked = False
        frmEvaluarObstetrico.rbtCardiopatiaA.Checked = False

        frmEvaluarObstetrico.rbtNefropatiaB.Checked = True
        frmEvaluarObstetrico.rbtNefropatiaM.Checked = False
        frmEvaluarObstetrico.rbtNefropatiaA.Checked = False

        frmEvaluarObstetrico.rbtHematopatiaB.Checked = True
        frmEvaluarObstetrico.rbtHematopatiaM.Checked = False
        frmEvaluarObstetrico.rbtHematopatiaA.Checked = False

        frmEvaluarObstetrico.rbtEndocrinopatiaB.Checked = True
        frmEvaluarObstetrico.rbtEndocrinopatiaM.Checked = False
        frmEvaluarObstetrico.rbtEndocrinopatiaA.Checked = False

        frmEvaluarObstetrico.rbtHepatopatiaB.Checked = True
        frmEvaluarObstetrico.rbtHepatopatiaM.Checked = False
        frmEvaluarObstetrico.rbtHepatopatiaA.Checked = False

        'ENFERMEDADES DE TAMIZ: VIH, SIFILIS

        frmEvaluarObstetrico.rbtPruebaVIHB.Checked = True
        frmEvaluarObstetrico.rbtPruebaVIHA.Checked = False
        frmEvaluarObstetrico.cbxResultadoVIH.Text = ""

        frmEvaluarObstetrico.rbtPruebaSifilisB.Checked = True
        frmEvaluarObstetrico.rbtPruebaSifilisA.Checked = False
        frmEvaluarObstetrico.cbxResultadoSIFILIS.Text = ""

        frmEvaluarObstetrico.rbtTiraUroanalisisB.Checked = True
        frmEvaluarObstetrico.rbtTiraUroanalisisM.Checked = False
        frmEvaluarObstetrico.rbtTiraUroanalisisA.Checked = False

    End Sub


End Class
