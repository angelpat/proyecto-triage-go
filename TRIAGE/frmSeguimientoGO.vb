﻿Imports System.Data.SqlClient
Imports System.Globalization
Public Class frmSeguimientoGO
    Dim IdTRIAGE As Integer
    Dim nombre1 As String
    Dim tiempo As Integer
    Dim valor As String
    Dim fecha As String
    Public idpaciente As String
    Dim Matencion As String
    Public cnumcontrol1 As Int64
    Dim db As conexionbd = New conexionbd()
    Public VchCadena As String
    Public Numero As Integer = 1
    Public fechaatencion1 As String
    Public lectorhere
    Public cnn As SqlConnection
    Public comando As SqlCommand
    Public idatencion As Integer
    Public Sub refrescar()
      
        IdTRIAGE = 0
        Try

            VchCadena = "EXEC zspTRI_PacienteSeguimientoGO " & tiempo & ";" '0 -> Se trae la lista completa
            db.validarbd()
            cnn = New SqlConnection(db.conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read
                    'If lectorhere("ALTA") <> "S" Then
                    nombre1 = lectorhere.getvalue(0)
                    valor = RTrim(Mid(lectorhere.getvalue(1), 3, 9))
                    fecha = lectorhere.getvalue(2)
                    'fecha = lectorhere.getvalue(2)
                    idpaciente = lectorhere.getvalue(4)
                    Matencion = lectorhere.getvalue(5)
                    cnumcontrol1 = lectorhere.GetInt64(6)
                    fechaatencion1 = lectorhere.getvalue(7)
                    idatencion = lectorhere.getvalue(8)
                    fechaatencion1 = fechaatencion1.ToString(System.Globalization.CultureInfo.CreateSpecificCulture("fr-FR"))
                    dvgNoatendido.Rows.Add(cnumcontrol1, nombre1, valor, fecha + " min", Matencion, idpaciente, fechaatencion1, idatencion)


                    'Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString

                    ' TerminaConsulta(nombre, frmPrincipalTriage.VchIdEmpleadoG, idpaciente)
                    'End If
                    Numero = Numero + 1
                End While
            End If
            lectorhere.Close()
            cnn.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try



    End Sub

    Private Sub frmSeguimientoGO_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        lblTipPersonal.Text = frmPrincipalTriage.lblTipoPersonal.Text
        lblNombreRevisor.Text = frmEvaluar.RegresaEmpleado()
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
        refrescar()
        Timer1.Interval = 1000
        ' Enable timer.
        Timer1.Enabled = True
        If frmPrincipalTriage.vchTipo = "04" Then
            txthoras.Visible = True
            txtiempo.Visible = True
            lbltiempo.Visible = True
        End If


    End Sub
    Private Sub dvgNoatendido_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dvgNoatendido.CellClick
        If e.RowIndex >= 0 AndAlso dvgNoatendido.Item("Nombre", e.RowIndex).Value IsNot Nothing Then

            ' IdTRIAGE = dgvListado.Item("idT", e.RowIndex).Value
            ' idPaciente = dgvListado.Item("", e.RowIndex).Value
            ' NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
            txtPrioridad.Text = dvgNoatendido.Item("Valoracion", e.RowIndex).Value
            txtPaciente.Text = dvgNoatendido.Item("nombre", e.RowIndex).Value
            cnumcontrol1 = dvgNoatendido.Item("Num_Control", e.RowIndex).Value
            idpaciente = dvgNoatendido.Item("idpaciente2", e.RowIndex).Value
            txtcama.Text = dvgNoatendido.Item("fehaat", e.RowIndex).Value
            fechaatencion1 = dvgNoatendido.Item("Fechaatencion", e.RowIndex).Value
            idatencion = dvgNoatendido.Item("idatencion1", e.RowIndex).Value
        End If

        'End If
    End Sub


    Private Sub Timer1_Tick(sender As Object, e As EventArgs) Handles Timer1.Tick
        dvgNoatendido.Rows.Clear()
        tiempo = 60
        If txtiempo.Text <> "" Then
            tiempo = txtiempo.Text * 60
        End If
        refrescar()

    End Sub

    Private Sub dvgNoatendido_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dvgNoatendido.CellFormatting
        If dvgNoatendido.Columns(e.ColumnIndex).Name = "fehaat" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToString
                If ((Mid(stringValue.ToString, 1, 2) > tiempo - 15)) Then
                    e.CellStyle.BackColor = Color.Red
                    e.CellStyle.SelectionBackColor = Color.Transparent

                End If

            End If
        End If


    End Sub

    
  
    Private Sub txtRegTriageGo_Click(sender As Object, e As EventArgs) Handles txtRegTriageGo.Click
        Dim motivo = "regresado"
        Try

            db.validarbd()
            cnn = New SqlConnection(db.conexion)
            cnn.Open()
            comando = New SqlCommand("", cnn)
            VchCadena = "zspTRI_RegresarTriageGO  '" & idpaciente & "','" & motivo & "','" & frmPrincipalTriage.VchIdEmpleadoG & "','" & cnumcontrol1 & "'," & idatencion & ";"
            comando.CommandText = VchCadena
            comando.ExecuteNonQuery()
            cnn.Close()
            MessageBox.Show("Persona Regresada Satisfactoriamente", "Reigreso Triage", MessageBoxButtons.OK, MessageBoxIcon.Information)
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try


    End Sub

    Private Sub Label4_Click(sender As Object, e As EventArgs) Handles Label4.Click

    End Sub
End Class