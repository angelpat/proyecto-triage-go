﻿Imports System.Data.SqlClient

Public Class frmListadoPacientesTS

    Dim IdTRIAGE As Integer
    Dim idSeleccionado As Integer
    Dim NumControl As String
    Dim idPaciente As String

    Dim cnn As SqlClient.SqlConnection
    Dim comando As SqlClient.SqlCommand
    Dim lector As SqlClient.SqlDataReader


    Private Function RegresaRespuesta() As Integer

        Dim respuesta As Integer = 0

        If rbtNoAcudio.Checked = True Then
            respuesta = 1
        ElseIf rbtNoMedico.Checked = True Then
            respuesta = 2
        ElseIf rbtNoFicha.Checked = True Then
            respuesta = 3
        ElseIf rbtNoInformacion.Checked = True Then
            respuesta = 4
        ElseIf rbtBuenaAtencion.Checked = True Then
            respuesta = 5
        End If

        Return respuesta

    End Function


    Private Sub CargaRespuesta(ByVal respuesta As Integer)

        If respuesta = 1 Then
            rbtNoAcudio.Checked = True
        ElseIf respuesta = 2 Then
            rbtNoMedico.Checked = True
        ElseIf respuesta = 3 Then
            rbtNoFicha.Checked = True
        ElseIf respuesta = 4 Then
            rbtNoInformacion.Checked = True
        ElseIf respuesta = 5 Then
            rbtBuenaAtencion.Checked = True
        Else
            LimpiaRespuesta()
        End If

    End Sub


    Private Sub LimpiaRespuesta()
        rbtNoAcudio.Checked = False
        rbtNoMedico.Checked = False
        rbtNoFicha.Checked = False
        rbtNoInformacion.Checked = False
        rbtBuenaAtencion.Checked = False
    End Sub


    Private Sub LimpiaFormulario()        
        txtPaciente.Text = ""
        txtEdad.Text = ""
        txtSexo.Text = ""
        txtDerechohabiencia.Text = ""
        txtCS.Text = ""
        txtObservacion.Text = ""
        LimpiaRespuesta()
    End Sub


    Private Sub ActivaFormulario(ByVal respuesta As Integer)

        If respuesta > 0 Then
            gbxPaciente.Enabled = False
            gbxCuestionario.Enabled = False
            CargaRespuesta(respuesta)
            btnGuardar.Enabled = False
            btnRetirado.Enabled = True
        Else
            gbxPaciente.Enabled = True
            gbxCuestionario.Enabled = True
            LimpiaRespuesta()
            btnGuardar.Enabled = True
            btnRetirado.Enabled = False
        End If

    End Sub


    Private Sub CargaFormulario(ByVal id As Integer)

        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim respuesta As Integer = 0
        Dim lectorhere
        Try

            VchCadena = "exec zspTRI_selPaciente " + id.ToString

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)
            cnn.Open()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read

                    txtSexo.Text = lectorhere("sexo")
                    txtPaciente.Text = lectorhere("nombre")
                    txtEdad.Text = frmEvaluar.ObtenerEdad(lectorhere("fechaActual"), lectorhere("dFechaNacimiento"))
                    txtDerechohabiencia.Text = lectorhere("derechohabiencia")

                    txtCS.Text = lectorhere("centroSalud")
                    respuesta = lectorhere("idRespuesta")
                    txtObservacion.Text = lectorhere("observacion")

                    ActivaFormulario(respuesta)

                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado frmCuestionario.CargaInfoPaciente")
        End Try
        cnn.Close()

    End Sub


    Private Sub RefrescaLista()
        Dim VchCadena As String
        Dim Numero As Integer = 1
        Dim Edad As String
        Dim lectorhere
        Dim cnn As SqlConnection
        Dim comando As SqlCommand
        Dim evaluado As String
        IdTRIAGE = 0
        Try

            VchCadena = "EXEC zspTRI_selListaPacientes 4"

            cnn = New SqlConnection(frmPrincipalTriage.Conexion)
            comando = New SqlCommand("", cnn)

            cnn.Open()
            comando.CommandText = "exec zspTRI_updPrioridad"
            comando.ExecuteNonQuery()

            comando.CommandText = VchCadena
            lectorhere = comando.ExecuteReader
            If lectorhere.HasRows Then
                While lectorhere.Read

                    If IdTRIAGE = 0 Then
                        IdTRIAGE = lectorhere.GetInt32(0) 'Asigna el 1ero de la lista IdTRIAGE 
                    End If
                    Edad = frmEvaluar.ObtenerEdad(lectorhere.GetDateTime(7).Date, lectorhere.GetDateTime(8).Date).ToString

                    If (lectorhere("evaluado").ToString() = "0") Then
                        evaluado = "No"
                    Else
                        evaluado = "Si"
                    End If

                    dgvListado.Rows.Add(lectorhere.GetInt32(0), lectorhere.GetInt32(1), Numero, lectorhere("horaeval").ToString(), lectorhere.GetString(2), lectorhere.GetString(3), lectorhere.GetString(4), lectorhere.GetString(5), lectorhere.GetString(6), Edad, lectorhere("cnum_control").ToString(), evaluado)
                  
                    Numero = Numero + 1
                End While
            End If
            lectorhere.Close()
        Catch ex As Exception
            MessageBox.Show(ex.Message.ToString, "Error no esperado")
        End Try
        cnn.Close()
    End Sub


    Private Sub frmListadoPacientesTS_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmPrincipalTriage.validaAcceso()
        RefrescaLista()
        lblNombreRevisor.Text = frmEvaluar.RegresaEmpleado()
        Text = Text + "  [" + frmAcceso.txtUsuario.Text.ToUpper + "]"
    End Sub


    Private Sub dgvListado_CellFormatting(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellFormattingEventArgs) Handles dgvListado.CellFormatting
        If dgvListado.Columns(e.ColumnIndex).Name = "Prioridad" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()
                If ((stringValue.IndexOf("1") > -1)) Then
                    e.CellStyle.BackColor = Color.Red
                    e.CellStyle.SelectionBackColor = Color.Red
                ElseIf ((stringValue.IndexOf("2") > -1)) Then
                    e.CellStyle.BackColor = Color.Yellow
                    e.CellStyle.SelectionBackColor = Color.Yellow
                    e.CellStyle.SelectionForeColor = Color.Black
                ElseIf ((stringValue.IndexOf("3") > -1)) Then
                    e.CellStyle.BackColor = Color.FromArgb(115, 189, 58) ' Color Verde logo del Hospital
                    e.CellStyle.SelectionBackColor = Color.FromArgb(115, 189, 58)
                    e.CellStyle.SelectionForeColor = Color.Black
                ElseIf ((stringValue.IndexOf("4") > -1)) Then
                    e.CellStyle.BackColor = Color.SkyBlue
                    e.CellStyle.SelectionBackColor = Color.SkyBlue
                    'e.CellStyle.BackColor = Color.FromArgb(8, 49, 99) ' Color Azul logo del Hospital
                End If
            End If
        ElseIf dgvListado.Columns(e.ColumnIndex).Name = "TS" Then
            If e.Value IsNot Nothing Then
                Dim stringValue As String = CType(e.Value, String)
                stringValue = stringValue.ToLower()
                If ((stringValue.IndexOf("si") > -1)) Then
                    e.CellStyle.BackColor = Color.SkyBlue
                    e.CellStyle.SelectionBackColor = Color.SkyBlue
                End If
            End If
        End If
    End Sub


    'Private Sub dgvListado_CellDoubleClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellDoubleClick
    '    idSeleccionado = dgvListado.Item("idT", e.RowIndex).Value
    '    idPaciente = dgvListado.Item("CURP", e.RowIndex).Value
    '    NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
    '    CargaFormulario(idSeleccionado)
    '    txtCS.Focus()
    'End Sub


    Private Sub btnGuardar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnGuardar.Click

        Dim resp As Integer = RegresaRespuesta()

        If ValidaFormulario(resp) Then

            Dim transaccion As SqlTransaction
            Try
                cnn = New SqlClient.SqlConnection(frmPrincipalTriage.Conexion)
                cnn.Open()
                comando = New SqlClient.SqlCommand("", cnn)
                transaccion = cnn.BeginTransaction()
                comando.Transaction = transaccion

                comando.CommandText = "exec zspTRI_insCuestionario " + idSeleccionado.ToString() + ", '" + frmPrincipalTriage.VchIdEmpleadoG + "', " + resp.ToString() + ", '" + txtCS.Text + "', '" + txtObservacion.Text + "'"
                comando.ExecuteNonQuery()
                transaccion.Commit()

                If (MessageBox.Show("¿El paciente se retiro voluntariamente?", "Titulo", MessageBoxButtons.YesNo, MessageBoxIcon.None, MessageBoxDefaultButton.Button2) = MsgBoxResult.Yes) Then
                    frmListadoPacientes.CambiaEstadoHGC_ADMISION(idPaciente)
                    frmListadoPacientes.TerminaConsulta("R", idSeleccionado.ToString(), NumControl, idPaciente)
                    MsgBox("Registro guardado satisfactoriamente")
                End If

                dgvListado.Rows.Clear()
                RefrescaLista()
                LimpiaFormulario()
                btnGuardar.Enabled = False

            Catch ex As Exception
                transaccion.Rollback()
                MessageBox.Show(ex.Message.ToString, "Triage - Error inesperado")
            End Try

        End If

    End Sub


    Private Sub btnRefrescar_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRefrescar.Click
        LimpiaFormulario()
        ActivaFormulario(0)
        dgvListado.Rows.Clear()
        RefrescaLista()
        btnGuardar.Enabled = False
    End Sub


    Private Function ValidaFormulario(ByVal resp As Integer) As Boolean

        Dim Validado As Boolean = False

        If txtCS.Text = "" Then
            MsgBox("El Centro de Salud no debe estar vacio")
        ElseIf txtCS.Text.Length <= 3 Then
            MsgBox("El Centro de Salud debe contener mas de 3 caracateres")
        ElseIf resp = 0 Then
            MsgBox("Debe seleccionar una respuesta")
        Else
            Validado = True
        End If

        Return Validado

    End Function


    Private Sub btnRetirado_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnRetirado.Click
        frmListadoPacientes.CambiaEstadoHGC_ADMISION(idPaciente)
        frmListadoPacientes.TerminaConsulta("R", idSeleccionado.ToString(), NumControl, idPaciente)
        dgvListado.Rows.Clear()
        RefrescaLista()
        MsgBox("Registro guardado satisfactoriamente")
        LimpiaFormulario()
        ActivaFormulario(0)
        btnGuardar.Enabled = False
    End Sub


    Private Sub dgvListado_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles dgvListado.CellClick
        If e.RowIndex >= 0 AndAlso dgvListado.Item("idT", e.RowIndex).Value IsNot Nothing Then
            idSeleccionado = dgvListado.Item("idT", e.RowIndex).Value
            idPaciente = dgvListado.Item("CURP", e.RowIndex).Value
            NumControl = dgvListado.Item("colNumCtrl", e.RowIndex).Value
            CargaFormulario(idSeleccionado)
            txtCS.Focus()
        End If
    End Sub


End Class