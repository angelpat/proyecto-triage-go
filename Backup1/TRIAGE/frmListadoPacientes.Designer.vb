<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPacientes
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        If disposing AndAlso components IsNot Nothing Then
            components.Dispose()
        End If
        MyBase.Dispose(disposing)
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListadoPacientes))
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.dgvListado = New System.Windows.Forms.DataGridView
        Me.cmsMenu = New System.Windows.Forms.ContextMenuStrip(Me.components)
        Me.tsmMenu = New System.Windows.Forms.ToolStripMenuItem
        Me.ToolStripSeparator1 = New System.Windows.Forms.ToolStripSeparator
        Me.tsmAtender = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmEliminar = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmpediatria = New System.Windows.Forms.ToolStripMenuItem
        Me.tsmTriage = New System.Windows.Forms.ToolStripMenuItem
        Me.btnRefrescar = New System.Windows.Forms.Button
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.gbxPaciente = New System.Windows.Forms.GroupBox
        Me.txtPaciente = New System.Windows.Forms.TextBox
        Me.txtSexo = New System.Windows.Forms.TextBox
        Me.txtHora = New System.Windows.Forms.TextBox
        Me.txtPrioridad = New System.Windows.Forms.TextBox
        Me.lblHora = New System.Windows.Forms.Label
        Me.txtEdad = New System.Windows.Forms.TextBox
        Me.lblPrioridad = New System.Windows.Forms.Label
        Me.lblSexo = New System.Windows.Forms.Label
        Me.lblEdad = New System.Windows.Forms.Label
        Me.lblPaciente = New System.Windows.Forms.Label
        Me.btnAtendido = New System.Windows.Forms.Button
        Me.btnEliminar = New System.Windows.Forms.Button
        Me.ToolTipAtendido = New System.Windows.Forms.ToolTip(Me.components)
        Me.ToolTipEliminar = New System.Windows.Forms.ToolTip(Me.components)
        Me.IdT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colHoraEval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Materno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Edad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.cmsMenu.SuspendLayout()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdT, Me.Prioridad, Me.Numero, Me.colHoraEval, Me.Curp, Me.Paterno, Me.Materno, Me.Nombre, Me.Sexo, Me.Edad, Me.colNumCtrl})
        Me.dgvListado.ContextMenuStrip = Me.cmsMenu
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(10, 104)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(840, 300)
        Me.dgvListado.TabIndex = 0
        Me.dgvListado.TabStop = False
        '
        'cmsMenu
        '
        Me.cmsMenu.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.tsmMenu, Me.ToolStripSeparator1, Me.tsmAtender, Me.tsmEliminar, Me.tsmpediatria, Me.tsmTriage})
        Me.cmsMenu.Name = "cmsOpciones"
        Me.cmsMenu.Size = New System.Drawing.Size(187, 120)
        '
        'tsmMenu
        '
        Me.tsmMenu.Name = "tsmMenu"
        Me.tsmMenu.Size = New System.Drawing.Size(186, 22)
        Me.tsmMenu.Text = "Menu"
        '
        'ToolStripSeparator1
        '
        Me.ToolStripSeparator1.Name = "ToolStripSeparator1"
        Me.ToolStripSeparator1.Size = New System.Drawing.Size(183, 6)
        '
        'tsmAtender
        '
        Me.tsmAtender.Name = "tsmAtender"
        Me.tsmAtender.Size = New System.Drawing.Size(186, 22)
        Me.tsmAtender.Text = "Atender Consulta"
        '
        'tsmEliminar
        '
        Me.tsmEliminar.Name = "tsmEliminar"
        Me.tsmEliminar.Size = New System.Drawing.Size(186, 22)
        Me.tsmEliminar.Text = "Eliminar"
        '
        'tsmpediatria
        '
        Me.tsmpediatria.Name = "tsmpediatria"
        Me.tsmpediatria.Size = New System.Drawing.Size(186, 22)
        Me.tsmpediatria.Text = "Urgencias Pediatricas"
        '
        'tsmTriage
        '
        Me.tsmTriage.Name = "tsmTriage"
        Me.tsmTriage.Size = New System.Drawing.Size(186, 22)
        Me.tsmTriage.Text = "Atendido Triage"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefrescar.Location = New System.Drawing.Point(710, 511)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(140, 23)
        Me.btnRefrescar.TabIndex = 3
        Me.btnRefrescar.Text = "Refrescar lista"
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 554)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(865, 22)
        Me.StatusStrip1.TabIndex = 12
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(13, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.Color.Transparent
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtSexo)
        Me.gbxPaciente.Controls.Add(Me.txtHora)
        Me.gbxPaciente.Controls.Add(Me.txtPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblHora)
        Me.gbxPaciente.Controls.Add(Me.txtEdad)
        Me.gbxPaciente.Controls.Add(Me.lblPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblSexo)
        Me.gbxPaciente.Controls.Add(Me.lblEdad)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(10, 420)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(840, 74)
        Me.gbxPaciente.TabIndex = 13
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(198, 41)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(420, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtSexo
        '
        Me.txtSexo.BackColor = System.Drawing.SystemColors.Window
        Me.txtSexo.Location = New System.Drawing.Point(624, 41)
        Me.txtSexo.Name = "txtSexo"
        Me.txtSexo.ReadOnly = True
        Me.txtSexo.Size = New System.Drawing.Size(130, 20)
        Me.txtSexo.TabIndex = 6
        Me.txtSexo.TabStop = False
        '
        'txtHora
        '
        Me.txtHora.BackColor = System.Drawing.SystemColors.Window
        Me.txtHora.Location = New System.Drawing.Point(86, 41)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.ReadOnly = True
        Me.txtHora.Size = New System.Drawing.Size(106, 20)
        Me.txtHora.TabIndex = 3
        Me.txtHora.TabStop = False
        '
        'txtPrioridad
        '
        Me.txtPrioridad.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrioridad.Location = New System.Drawing.Point(6, 41)
        Me.txtPrioridad.Name = "txtPrioridad"
        Me.txtPrioridad.ReadOnly = True
        Me.txtPrioridad.Size = New System.Drawing.Size(74, 20)
        Me.txtPrioridad.TabIndex = 3
        Me.txtPrioridad.TabStop = False
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.Location = New System.Drawing.Point(83, 25)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(101, 13)
        Me.lblHora.TabIndex = 2
        Me.lblHora.Text = "Hora Evaluación"
        '
        'txtEdad
        '
        Me.txtEdad.BackColor = System.Drawing.SystemColors.Window
        Me.txtEdad.Location = New System.Drawing.Point(760, 41)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(74, 20)
        Me.txtEdad.TabIndex = 3
        Me.txtEdad.TabStop = False
        '
        'lblPrioridad
        '
        Me.lblPrioridad.AutoSize = True
        Me.lblPrioridad.Location = New System.Drawing.Point(3, 25)
        Me.lblPrioridad.Name = "lblPrioridad"
        Me.lblPrioridad.Size = New System.Drawing.Size(57, 13)
        Me.lblPrioridad.TabIndex = 2
        Me.lblPrioridad.Text = "Prioridad"
        '
        'lblSexo
        '
        Me.lblSexo.AutoSize = True
        Me.lblSexo.Location = New System.Drawing.Point(621, 25)
        Me.lblSexo.Name = "lblSexo"
        Me.lblSexo.Size = New System.Drawing.Size(35, 13)
        Me.lblSexo.TabIndex = 4
        Me.lblSexo.Text = "Sexo"
        '
        'lblEdad
        '
        Me.lblEdad.AutoSize = True
        Me.lblEdad.Location = New System.Drawing.Point(757, 25)
        Me.lblEdad.Name = "lblEdad"
        Me.lblEdad.Size = New System.Drawing.Size(36, 13)
        Me.lblEdad.TabIndex = 2
        Me.lblEdad.Text = "Edad"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(195, 25)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'btnAtendido
        '
        Me.btnAtendido.Enabled = False
        Me.btnAtendido.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnAtendido.Location = New System.Drawing.Point(10, 511)
        Me.btnAtendido.Name = "btnAtendido"
        Me.btnAtendido.Size = New System.Drawing.Size(140, 23)
        Me.btnAtendido.TabIndex = 3
        Me.btnAtendido.Text = "Atendido"
        Me.ToolTipAtendido.SetToolTip(Me.btnAtendido, "Permite inciar consulta del paciente")
        Me.btnAtendido.UseVisualStyleBackColor = True
        '
        'btnEliminar
        '
        Me.btnEliminar.Enabled = False
        Me.btnEliminar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnEliminar.Location = New System.Drawing.Point(156, 511)
        Me.btnEliminar.Name = "btnEliminar"
        Me.btnEliminar.Size = New System.Drawing.Size(140, 23)
        Me.btnEliminar.TabIndex = 3
        Me.btnEliminar.Text = "Eliminar"
        Me.ToolTipEliminar.SetToolTip(Me.btnEliminar, "Borra al paciente de la lista debido a que no se presento")
        Me.btnEliminar.UseVisualStyleBackColor = True
        '
        'ToolTipAtendido
        '
        Me.ToolTipAtendido.ToolTipTitle = "Atiende al paciente en el consultorio"
        '
        'ToolTipEliminar
        '
        Me.ToolTipEliminar.AutoPopDelay = 5000
        Me.ToolTipEliminar.InitialDelay = 500
        Me.ToolTipEliminar.ReshowDelay = 10
        Me.ToolTipEliminar.ToolTipTitle = "Elimina paciente"
        '
        'IdT
        '
        Me.IdT.HeaderText = "IdTriage"
        Me.IdT.Name = "IdT"
        Me.IdT.ReadOnly = True
        Me.IdT.Visible = False
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'Numero
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero.DefaultCellStyle = DataGridViewCellStyle3
        Me.Numero.HeaderText = "No."
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Visible = False
        Me.Numero.Width = 35
        '
        'colHoraEval
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHoraEval.DefaultCellStyle = DataGridViewCellStyle4
        Me.colHoraEval.HeaderText = "Hora Evaluación"
        Me.colHoraEval.Name = "colHoraEval"
        Me.colHoraEval.ReadOnly = True
        '
        'Curp
        '
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Curp.DefaultCellStyle = DataGridViewCellStyle5
        Me.Curp.HeaderText = "CURP"
        Me.Curp.Name = "Curp"
        Me.Curp.ReadOnly = True
        Me.Curp.Visible = False
        Me.Curp.Width = 155
        '
        'Paterno
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paterno.DefaultCellStyle = DataGridViewCellStyle6
        Me.Paterno.HeaderText = "Apellido Paterno"
        Me.Paterno.Name = "Paterno"
        Me.Paterno.ReadOnly = True
        Me.Paterno.Width = 160
        '
        'Materno
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Materno.DefaultCellStyle = DataGridViewCellStyle7
        Me.Materno.HeaderText = "Apellido Materno"
        Me.Materno.Name = "Materno"
        Me.Materno.ReadOnly = True
        Me.Materno.Width = 160
        '
        'Nombre
        '
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle8
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 160
        '
        'Sexo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sexo.DefaultCellStyle = DataGridViewCellStyle9
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        Me.Sexo.Width = 75
        '
        'Edad
        '
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Edad.DefaultCellStyle = DataGridViewCellStyle10
        Me.Edad.HeaderText = "Edad"
        Me.Edad.Name = "Edad"
        Me.Edad.ReadOnly = True
        Me.Edad.Width = 75
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Visible = False
        '
        'frmListadoPacientes
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.ClientSize = New System.Drawing.Size(865, 576)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvListado)
        Me.Controls.Add(Me.btnRefrescar)
        Me.Controls.Add(Me.btnAtendido)
        Me.Controls.Add(Me.btnEliminar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmListadoPacientes"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de pacientes"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.cmsMenu.ResumeLayout(False)
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents btnRefrescar As System.Windows.Forms.Button
    Friend WithEvents cmsMenu As System.Windows.Forms.ContextMenuStrip
    Friend WithEvents tsmMenu As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents ToolStripSeparator1 As System.Windows.Forms.ToolStripSeparator
    Friend WithEvents tsmAtender As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmEliminar As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents tsmpediatria As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents tsmTriage As System.Windows.Forms.ToolStripMenuItem
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents txtSexo As System.Windows.Forms.TextBox
    Friend WithEvents txtEdad As System.Windows.Forms.TextBox
    Friend WithEvents lblSexo As System.Windows.Forms.Label
    Friend WithEvents lblEdad As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents txtHora As System.Windows.Forms.TextBox
    Friend WithEvents txtPrioridad As System.Windows.Forms.TextBox
    Friend WithEvents lblHora As System.Windows.Forms.Label
    Friend WithEvents lblPrioridad As System.Windows.Forms.Label
    Friend WithEvents btnAtendido As System.Windows.Forms.Button
    Friend WithEvents btnEliminar As System.Windows.Forms.Button
    Friend WithEvents ToolTipAtendido As System.Windows.Forms.ToolTip
    Friend WithEvents ToolTipEliminar As System.Windows.Forms.ToolTip
    Friend WithEvents IdT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHoraEval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Edad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
