Public Class frmPrincipalTriage
    Public Conexion As String = ""
    'Public Conexion As String = "Data Source=10.3.14.93;Initial Catalog=SIEC;User ID=sa;Password=siec"
    Public VchIdPacienteG As String
    Public VchIdEmpleadoG As String
    Public vchTipo As String

    Private Sub EvaluarPacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EvaluarPacientesToolStripMenuItem.Click
        frmEvaluar.Show()
    End Sub

    Private Sub SalirToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles SalirToolStripMenuItem.Click
        frmAcceso.Close()
    End Sub

    Private Sub ListadoDePacientesToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePacientesToolStripMenuItem.Click
        frmListadoPacientes.Show()
    End Sub

    Private Sub frmPrincipalTriage_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        frmAcceso.Hide()
        validaAcceso()
    End Sub

    Private Sub frmPrincipalTriage_FormClosing(ByVal sender As System.Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles MyBase.FormClosing
        Application.Exit()
    End Sub

    Private Sub ListadoDePacientesTSToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListadoDePacientesTSToolStripMenuItem.Click
        frmListadoPacientesTS.Show()
    End Sub

    Private Sub TSCodigoAzulToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TSCodigoAzulToolStripMenuItem.Click
        frmRptTSCodigoAzul.Show()
    End Sub

    Public Sub validaAcceso()
        If vchTipo <> "01" And vchTipo <> "02" And vchTipo <> "09" Then '01,02,09 -> Medico, Enfermero, Informatica
            EvaluarPacientesToolStripMenuItem.Visible = False
            If vchTipo = "07" Then '07 -> Trabajo Social
                TrabajoSocialToolStripMenuItem.Visible = True
            End If
            frmListadoPacientes.btnEliminar.Visible = False
            frmListadoPacientes.btnAtendido.Visible = False
            ' CambiarPrioridadToolStripMenuItem.Visible = False
        Else
            TrabajoSocialToolStripMenuItem.Visible = True            
            frmListadoPacientesTS.btnGuardar.Visible = False
            frmListadoPacientesTS.btnRetirado.Visible = False
        End If
    End Sub

    Private Sub CambiarPrioridadToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CambiarPrioridadToolStripMenuItem.Click
        frmCambiarPrioridad.Show()
    End Sub

End Class