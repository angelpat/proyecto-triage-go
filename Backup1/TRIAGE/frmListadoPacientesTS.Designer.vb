﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmListadoPacientesTS
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle13 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmListadoPacientesTS))
        Me.dgvListado = New System.Windows.Forms.DataGridView
        Me.IdT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colHoraEval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Materno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Edad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.TS = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.btnRefrescar = New System.Windows.Forms.Button
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip
        Me.lblRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.lblNombreRevisor = New System.Windows.Forms.ToolStripStatusLabel
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.gbxPaciente = New System.Windows.Forms.GroupBox
        Me.txtPaciente = New System.Windows.Forms.TextBox
        Me.txtSexo = New System.Windows.Forms.TextBox
        Me.txtCS = New System.Windows.Forms.TextBox
        Me.txtDerechohabiencia = New System.Windows.Forms.TextBox
        Me.txtEdad = New System.Windows.Forms.TextBox
        Me.lblCS = New System.Windows.Forms.Label
        Me.lblSexo = New System.Windows.Forms.Label
        Me.lblDerechohabiencia = New System.Windows.Forms.Label
        Me.lblEdad = New System.Windows.Forms.Label
        Me.lblPaciente = New System.Windows.Forms.Label
        Me.btnGuardar = New System.Windows.Forms.Button
        Me.btnRetirado = New System.Windows.Forms.Button
        Me.txtObservacion = New System.Windows.Forms.TextBox
        Me.Label1 = New System.Windows.Forms.Label
        Me.rbtNoAcudio = New System.Windows.Forms.RadioButton
        Me.rbtNoMedico = New System.Windows.Forms.RadioButton
        Me.rbtNoInformacion = New System.Windows.Forms.RadioButton
        Me.rbtNoFicha = New System.Windows.Forms.RadioButton
        Me.rbtBuenaAtencion = New System.Windows.Forms.RadioButton
        Me.gbxCuestionario = New System.Windows.Forms.GroupBox
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.StatusStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.gbxCuestionario.SuspendLayout()
        Me.SuspendLayout()
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdT, Me.Prioridad, Me.Numero, Me.colHoraEval, Me.Curp, Me.Paterno, Me.Materno, Me.Nombre, Me.Sexo, Me.Edad, Me.colNumCtrl, Me.TS})
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle12
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(14, 104)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle13.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle13.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle13.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle13.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle13.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle13.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle13.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle13
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(840, 300)
        Me.dgvListado.TabIndex = 0
        Me.dgvListado.TabStop = False
        '
        'IdT
        '
        Me.IdT.HeaderText = "IdTriage"
        Me.IdT.Name = "IdT"
        Me.IdT.ReadOnly = True
        Me.IdT.Visible = False
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'Numero
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero.DefaultCellStyle = DataGridViewCellStyle3
        Me.Numero.HeaderText = "No."
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Visible = False
        Me.Numero.Width = 35
        '
        'colHoraEval
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHoraEval.DefaultCellStyle = DataGridViewCellStyle4
        Me.colHoraEval.HeaderText = "Hora Evaluación"
        Me.colHoraEval.Name = "colHoraEval"
        Me.colHoraEval.ReadOnly = True
        '
        'Curp
        '
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Curp.DefaultCellStyle = DataGridViewCellStyle5
        Me.Curp.HeaderText = "CURP"
        Me.Curp.Name = "Curp"
        Me.Curp.ReadOnly = True
        Me.Curp.Visible = False
        Me.Curp.Width = 155
        '
        'Paterno
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paterno.DefaultCellStyle = DataGridViewCellStyle6
        Me.Paterno.HeaderText = "Apellido Paterno"
        Me.Paterno.Name = "Paterno"
        Me.Paterno.ReadOnly = True
        Me.Paterno.Width = 150
        '
        'Materno
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Materno.DefaultCellStyle = DataGridViewCellStyle7
        Me.Materno.HeaderText = "Apellido Materno"
        Me.Materno.Name = "Materno"
        Me.Materno.ReadOnly = True
        Me.Materno.Width = 150
        '
        'Nombre
        '
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle8
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 150
        '
        'Sexo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sexo.DefaultCellStyle = DataGridViewCellStyle9
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        Me.Sexo.Width = 40
        '
        'Edad
        '
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Edad.DefaultCellStyle = DataGridViewCellStyle10
        Me.Edad.HeaderText = "Edad"
        Me.Edad.Name = "Edad"
        Me.Edad.ReadOnly = True
        Me.Edad.Width = 40
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Visible = False
        '
        'TS
        '
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.TS.DefaultCellStyle = DataGridViewCellStyle11
        Me.TS.HeaderText = "Tra. Soc."
        Me.TS.Name = "TS"
        Me.TS.ReadOnly = True
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefrescar.Location = New System.Drawing.Point(714, 652)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(140, 23)
        Me.btnRefrescar.TabIndex = 5
        Me.btnRefrescar.Text = "Refrescar lista"
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.lblRevisor, Me.lblNombreRevisor})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 691)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(868, 22)
        Me.StatusStrip1.TabIndex = 6
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'lblRevisor
        '
        Me.lblRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblRevisor.Name = "lblRevisor"
        Me.lblRevisor.Size = New System.Drawing.Size(53, 17)
        Me.lblRevisor.Text = "Revisor:"
        '
        'lblNombreRevisor
        '
        Me.lblNombreRevisor.BackColor = System.Drawing.Color.Transparent
        Me.lblNombreRevisor.Font = New System.Drawing.Font("Tahoma", 8.25!, System.Drawing.FontStyle.Bold)
        Me.lblNombreRevisor.ForeColor = System.Drawing.Color.Navy
        Me.lblNombreRevisor.Name = "lblNombreRevisor"
        Me.lblNombreRevisor.Size = New System.Drawing.Size(51, 17)
        Me.lblNombreRevisor.Text = "Nombre"
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(14, 12)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 5
        Me.PictureBox1.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtSexo)
        Me.gbxPaciente.Controls.Add(Me.txtCS)
        Me.gbxPaciente.Controls.Add(Me.txtDerechohabiencia)
        Me.gbxPaciente.Controls.Add(Me.txtEdad)
        Me.gbxPaciente.Controls.Add(Me.lblCS)
        Me.gbxPaciente.Controls.Add(Me.lblSexo)
        Me.gbxPaciente.Controls.Add(Me.lblDerechohabiencia)
        Me.gbxPaciente.Controls.Add(Me.lblEdad)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(14, 414)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(334, 223)
        Me.gbxPaciente.TabIndex = 1
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(6, 46)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(322, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtSexo
        '
        Me.txtSexo.BackColor = System.Drawing.SystemColors.Window
        Me.txtSexo.Location = New System.Drawing.Point(86, 98)
        Me.txtSexo.Name = "txtSexo"
        Me.txtSexo.ReadOnly = True
        Me.txtSexo.Size = New System.Drawing.Size(242, 20)
        Me.txtSexo.TabIndex = 6
        Me.txtSexo.TabStop = False
        '
        'txtCS
        '
        Me.txtCS.BackColor = System.Drawing.SystemColors.Window
        Me.txtCS.Location = New System.Drawing.Point(6, 197)
        Me.txtCS.MaxLength = 255
        Me.txtCS.Name = "txtCS"
        Me.txtCS.Size = New System.Drawing.Size(322, 20)
        Me.txtCS.TabIndex = 0
        '
        'txtDerechohabiencia
        '
        Me.txtDerechohabiencia.BackColor = System.Drawing.SystemColors.Window
        Me.txtDerechohabiencia.Location = New System.Drawing.Point(6, 148)
        Me.txtDerechohabiencia.Name = "txtDerechohabiencia"
        Me.txtDerechohabiencia.ReadOnly = True
        Me.txtDerechohabiencia.Size = New System.Drawing.Size(322, 20)
        Me.txtDerechohabiencia.TabIndex = 8
        Me.txtDerechohabiencia.TabStop = False
        '
        'txtEdad
        '
        Me.txtEdad.BackColor = System.Drawing.SystemColors.Window
        Me.txtEdad.Location = New System.Drawing.Point(6, 98)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(74, 20)
        Me.txtEdad.TabIndex = 3
        Me.txtEdad.TabStop = False
        '
        'lblCS
        '
        Me.lblCS.AutoSize = True
        Me.lblCS.Location = New System.Drawing.Point(3, 181)
        Me.lblCS.Name = "lblCS"
        Me.lblCS.Size = New System.Drawing.Size(119, 13)
        Me.lblCS.TabIndex = 9
        Me.lblCS.Text = "CS Correspondiente"
        '
        'lblSexo
        '
        Me.lblSexo.AutoSize = True
        Me.lblSexo.Location = New System.Drawing.Point(83, 82)
        Me.lblSexo.Name = "lblSexo"
        Me.lblSexo.Size = New System.Drawing.Size(35, 13)
        Me.lblSexo.TabIndex = 4
        Me.lblSexo.Text = "Sexo"
        '
        'lblDerechohabiencia
        '
        Me.lblDerechohabiencia.AutoSize = True
        Me.lblDerechohabiencia.Location = New System.Drawing.Point(3, 132)
        Me.lblDerechohabiencia.Name = "lblDerechohabiencia"
        Me.lblDerechohabiencia.Size = New System.Drawing.Size(110, 13)
        Me.lblDerechohabiencia.TabIndex = 7
        Me.lblDerechohabiencia.Text = "Derechohabiencia"
        '
        'lblEdad
        '
        Me.lblEdad.AutoSize = True
        Me.lblEdad.Location = New System.Drawing.Point(3, 82)
        Me.lblEdad.Name = "lblEdad"
        Me.lblEdad.Size = New System.Drawing.Size(36, 13)
        Me.lblEdad.TabIndex = 2
        Me.lblEdad.Text = "Edad"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(3, 30)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'btnGuardar
        '
        Me.btnGuardar.Enabled = False
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(568, 652)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(140, 23)
        Me.btnGuardar.TabIndex = 4
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'btnRetirado
        '
        Me.btnRetirado.Enabled = False
        Me.btnRetirado.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRetirado.Location = New System.Drawing.Point(14, 652)
        Me.btnRetirado.Name = "btnRetirado"
        Me.btnRetirado.Size = New System.Drawing.Size(140, 23)
        Me.btnRetirado.TabIndex = 3
        Me.btnRetirado.Text = "Retiro voluntario"
        Me.btnRetirado.UseVisualStyleBackColor = True
        '
        'txtObservacion
        '
        Me.txtObservacion.Location = New System.Drawing.Point(6, 171)
        Me.txtObservacion.MaxLength = 4000
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.Size = New System.Drawing.Size(488, 46)
        Me.txtObservacion.TabIndex = 6
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(3, 148)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(82, 13)
        Me.Label1.TabIndex = 5
        Me.Label1.Text = "Observación:"
        '
        'rbtNoAcudio
        '
        Me.rbtNoAcudio.AutoSize = True
        Me.rbtNoAcudio.Location = New System.Drawing.Point(44, 28)
        Me.rbtNoAcudio.Name = "rbtNoAcudio"
        Me.rbtNoAcudio.Size = New System.Drawing.Size(83, 17)
        Me.rbtNoAcudio.TabIndex = 0
        Me.rbtNoAcudio.TabStop = True
        Me.rbtNoAcudio.Text = "No acudio"
        Me.rbtNoAcudio.UseVisualStyleBackColor = True
        '
        'rbtNoMedico
        '
        Me.rbtNoMedico.AutoSize = True
        Me.rbtNoMedico.Location = New System.Drawing.Point(44, 51)
        Me.rbtNoMedico.Name = "rbtNoMedico"
        Me.rbtNoMedico.Size = New System.Drawing.Size(109, 17)
        Me.rbtNoMedico.TabIndex = 1
        Me.rbtNoMedico.TabStop = True
        Me.rbtNoMedico.Text = "No hay médico"
        Me.rbtNoMedico.UseVisualStyleBackColor = True
        '
        'rbtNoInformacion
        '
        Me.rbtNoInformacion.AutoSize = True
        Me.rbtNoInformacion.Location = New System.Drawing.Point(44, 97)
        Me.rbtNoInformacion.Name = "rbtNoInformacion"
        Me.rbtNoInformacion.Size = New System.Drawing.Size(140, 17)
        Me.rbtNoInformacion.TabIndex = 3
        Me.rbtNoInformacion.TabStop = True
        Me.rbtNoInformacion.Text = "Falta de información"
        Me.rbtNoInformacion.UseVisualStyleBackColor = True
        '
        'rbtNoFicha
        '
        Me.rbtNoFicha.AutoSize = True
        Me.rbtNoFicha.Location = New System.Drawing.Point(44, 74)
        Me.rbtNoFicha.Name = "rbtNoFicha"
        Me.rbtNoFicha.Size = New System.Drawing.Size(121, 17)
        Me.rbtNoFicha.TabIndex = 2
        Me.rbtNoFicha.TabStop = True
        Me.rbtNoFicha.Text = "No alcanzo ficha"
        Me.rbtNoFicha.UseVisualStyleBackColor = True
        '
        'rbtBuenaAtencion
        '
        Me.rbtBuenaAtencion.AutoSize = True
        Me.rbtBuenaAtencion.Location = New System.Drawing.Point(44, 120)
        Me.rbtBuenaAtencion.Name = "rbtBuenaAtencion"
        Me.rbtBuenaAtencion.Size = New System.Drawing.Size(239, 17)
        Me.rbtBuenaAtencion.TabIndex = 4
        Me.rbtBuenaAtencion.TabStop = True
        Me.rbtBuenaAtencion.Text = "Me gusta mas la atención del hospital"
        Me.rbtBuenaAtencion.UseVisualStyleBackColor = True
        '
        'gbxCuestionario
        '
        Me.gbxCuestionario.Controls.Add(Me.rbtBuenaAtencion)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoFicha)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoInformacion)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoMedico)
        Me.gbxCuestionario.Controls.Add(Me.rbtNoAcudio)
        Me.gbxCuestionario.Controls.Add(Me.Label1)
        Me.gbxCuestionario.Controls.Add(Me.txtObservacion)
        Me.gbxCuestionario.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCuestionario.Location = New System.Drawing.Point(354, 414)
        Me.gbxCuestionario.Name = "gbxCuestionario"
        Me.gbxCuestionario.Size = New System.Drawing.Size(500, 223)
        Me.gbxCuestionario.TabIndex = 2
        Me.gbxCuestionario.TabStop = False
        Me.gbxCuestionario.Text = "¿Por qué no consulto en el Centro de Salud?"
        '
        'frmListadoPacientesTS
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.ClientSize = New System.Drawing.Size(868, 713)
        Me.Controls.Add(Me.btnRetirado)
        Me.Controls.Add(Me.btnGuardar)
        Me.Controls.Add(Me.gbxCuestionario)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.dgvListado)
        Me.Controls.Add(Me.btnRefrescar)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmListadoPacientesTS"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Listado de pacientes (Trabajo Social)"
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.StatusStrip1.ResumeLayout(False)
        Me.StatusStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.gbxCuestionario.ResumeLayout(False)
        Me.gbxCuestionario.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents btnRefrescar As System.Windows.Forms.Button
    Friend WithEvents StatusStrip1 As System.Windows.Forms.StatusStrip
    Friend WithEvents lblRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents lblNombreRevisor As System.Windows.Forms.ToolStripStatusLabel
    Friend WithEvents IdT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHoraEval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Edad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents TS As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents txtSexo As System.Windows.Forms.TextBox
    Friend WithEvents txtCS As System.Windows.Forms.TextBox
    Friend WithEvents txtDerechohabiencia As System.Windows.Forms.TextBox
    Friend WithEvents txtEdad As System.Windows.Forms.TextBox
    Friend WithEvents lblCS As System.Windows.Forms.Label
    Friend WithEvents lblSexo As System.Windows.Forms.Label
    Friend WithEvents lblDerechohabiencia As System.Windows.Forms.Label
    Friend WithEvents lblEdad As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnRetirado As System.Windows.Forms.Button
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents rbtNoAcudio As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoMedico As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoInformacion As System.Windows.Forms.RadioButton
    Friend WithEvents rbtNoFicha As System.Windows.Forms.RadioButton
    Friend WithEvents rbtBuenaAtencion As System.Windows.Forms.RadioButton
    Friend WithEvents gbxCuestionario As System.Windows.Forms.GroupBox
End Class
