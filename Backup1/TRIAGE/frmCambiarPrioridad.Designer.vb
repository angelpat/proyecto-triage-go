﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmCambiarPrioridad
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmCambiarPrioridad))
        Dim DataGridViewCellStyle1 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle11 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle12 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle2 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle7 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle8 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle9 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle10 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.PictureBox1 = New System.Windows.Forms.PictureBox
        Me.dgvListado = New System.Windows.Forms.DataGridView
        Me.gbxPaciente = New System.Windows.Forms.GroupBox
        Me.txtObservacion = New System.Windows.Forms.TextBox
        Me.txtPaciente = New System.Windows.Forms.TextBox
        Me.txtSexo = New System.Windows.Forms.TextBox
        Me.txtHora = New System.Windows.Forms.TextBox
        Me.txtPrioridad = New System.Windows.Forms.TextBox
        Me.lblHora = New System.Windows.Forms.Label
        Me.txtEdad = New System.Windows.Forms.TextBox
        Me.lblObservacion = New System.Windows.Forms.Label
        Me.lblPrioridad = New System.Windows.Forms.Label
        Me.lblSexo = New System.Windows.Forms.Label
        Me.lblEdad = New System.Windows.Forms.Label
        Me.lblPaciente = New System.Windows.Forms.Label
        Me.gbxCambioNivel = New System.Windows.Forms.GroupBox
        Me.btnRefrescar = New System.Windows.Forms.Button
        Me.lblMotivo = New System.Windows.Forms.Label
        Me.btnGuardar = New System.Windows.Forms.Button
        Me.lblNuevaPrioridad = New System.Windows.Forms.Label
        Me.cbxPrioridadSel = New System.Windows.Forms.ComboBox
        Me.txtMotivo = New System.Windows.Forms.TextBox
        Me.IdT = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Prioridad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Numero = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colHoraEval = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Curp = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Paterno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Materno = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Nombre = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Sexo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.Edad = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.colNumCtrl = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.motivo = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.tipoUrgencia = New System.Windows.Forms.DataGridViewTextBoxColumn
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.gbxPaciente.SuspendLayout()
        Me.gbxCambioNivel.SuspendLayout()
        Me.SuspendLayout()
        '
        'PictureBox1
        '
        Me.PictureBox1.Image = CType(resources.GetObject("PictureBox1.Image"), System.Drawing.Image)
        Me.PictureBox1.Location = New System.Drawing.Point(12, 11)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(840, 86)
        Me.PictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom
        Me.PictureBox1.TabIndex = 2
        Me.PictureBox1.TabStop = False
        '
        'dgvListado
        '
        Me.dgvListado.AllowUserToAddRows = False
        Me.dgvListado.AllowUserToDeleteRows = False
        Me.dgvListado.AllowUserToResizeRows = False
        Me.dgvListado.BackgroundColor = System.Drawing.Color.FromArgb(CType(CType(5, Byte), Integer), CType(CType(52, Byte), Integer), CType(CType(100, Byte), Integer))
        DataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle1
        Me.dgvListado.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgvListado.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.IdT, Me.Prioridad, Me.Numero, Me.colHoraEval, Me.Curp, Me.Paterno, Me.Materno, Me.Nombre, Me.Sexo, Me.Edad, Me.colNumCtrl, Me.motivo, Me.tipoUrgencia})
        DataGridViewCellStyle11.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle11.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle11.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle11.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle11.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle11.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle11.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.dgvListado.DefaultCellStyle = DataGridViewCellStyle11
        Me.dgvListado.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically
        Me.dgvListado.Location = New System.Drawing.Point(12, 109)
        Me.dgvListado.MultiSelect = False
        Me.dgvListado.Name = "dgvListado"
        Me.dgvListado.ReadOnly = True
        DataGridViewCellStyle12.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle12.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle12.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle12.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle12.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle12.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle12.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.dgvListado.RowHeadersDefaultCellStyle = DataGridViewCellStyle12
        Me.dgvListado.RowHeadersVisible = False
        Me.dgvListado.Size = New System.Drawing.Size(840, 278)
        Me.dgvListado.TabIndex = 3
        Me.dgvListado.TabStop = False
        '
        'gbxPaciente
        '
        Me.gbxPaciente.BackColor = System.Drawing.Color.Transparent
        Me.gbxPaciente.Controls.Add(Me.txtObservacion)
        Me.gbxPaciente.Controls.Add(Me.txtPaciente)
        Me.gbxPaciente.Controls.Add(Me.txtSexo)
        Me.gbxPaciente.Controls.Add(Me.txtHora)
        Me.gbxPaciente.Controls.Add(Me.txtPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblHora)
        Me.gbxPaciente.Controls.Add(Me.txtEdad)
        Me.gbxPaciente.Controls.Add(Me.lblObservacion)
        Me.gbxPaciente.Controls.Add(Me.lblPrioridad)
        Me.gbxPaciente.Controls.Add(Me.lblSexo)
        Me.gbxPaciente.Controls.Add(Me.lblEdad)
        Me.gbxPaciente.Controls.Add(Me.lblPaciente)
        Me.gbxPaciente.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxPaciente.Location = New System.Drawing.Point(12, 401)
        Me.gbxPaciente.Name = "gbxPaciente"
        Me.gbxPaciente.Size = New System.Drawing.Size(840, 124)
        Me.gbxPaciente.TabIndex = 14
        Me.gbxPaciente.TabStop = False
        Me.gbxPaciente.Text = "Datos del Paciente"
        '
        'txtObservacion
        '
        Me.txtObservacion.BackColor = System.Drawing.SystemColors.Window
        Me.txtObservacion.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtObservacion.Location = New System.Drawing.Point(86, 73)
        Me.txtObservacion.Multiline = True
        Me.txtObservacion.Name = "txtObservacion"
        Me.txtObservacion.ReadOnly = True
        Me.txtObservacion.Size = New System.Drawing.Size(748, 42)
        Me.txtObservacion.TabIndex = 8
        '
        'txtPaciente
        '
        Me.txtPaciente.BackColor = System.Drawing.SystemColors.Window
        Me.txtPaciente.Location = New System.Drawing.Point(198, 41)
        Me.txtPaciente.Name = "txtPaciente"
        Me.txtPaciente.ReadOnly = True
        Me.txtPaciente.Size = New System.Drawing.Size(420, 20)
        Me.txtPaciente.TabIndex = 1
        Me.txtPaciente.TabStop = False
        '
        'txtSexo
        '
        Me.txtSexo.BackColor = System.Drawing.SystemColors.Window
        Me.txtSexo.Location = New System.Drawing.Point(624, 41)
        Me.txtSexo.Name = "txtSexo"
        Me.txtSexo.ReadOnly = True
        Me.txtSexo.Size = New System.Drawing.Size(130, 20)
        Me.txtSexo.TabIndex = 6
        Me.txtSexo.TabStop = False
        '
        'txtHora
        '
        Me.txtHora.BackColor = System.Drawing.SystemColors.Window
        Me.txtHora.Location = New System.Drawing.Point(86, 41)
        Me.txtHora.Name = "txtHora"
        Me.txtHora.ReadOnly = True
        Me.txtHora.Size = New System.Drawing.Size(106, 20)
        Me.txtHora.TabIndex = 3
        Me.txtHora.TabStop = False
        '
        'txtPrioridad
        '
        Me.txtPrioridad.BackColor = System.Drawing.SystemColors.Window
        Me.txtPrioridad.Location = New System.Drawing.Point(6, 41)
        Me.txtPrioridad.Name = "txtPrioridad"
        Me.txtPrioridad.ReadOnly = True
        Me.txtPrioridad.Size = New System.Drawing.Size(74, 20)
        Me.txtPrioridad.TabIndex = 3
        Me.txtPrioridad.TabStop = False
        '
        'lblHora
        '
        Me.lblHora.AutoSize = True
        Me.lblHora.Location = New System.Drawing.Point(83, 25)
        Me.lblHora.Name = "lblHora"
        Me.lblHora.Size = New System.Drawing.Size(101, 13)
        Me.lblHora.TabIndex = 2
        Me.lblHora.Text = "Hora Evaluación"
        '
        'txtEdad
        '
        Me.txtEdad.BackColor = System.Drawing.SystemColors.Window
        Me.txtEdad.Location = New System.Drawing.Point(760, 41)
        Me.txtEdad.Name = "txtEdad"
        Me.txtEdad.ReadOnly = True
        Me.txtEdad.Size = New System.Drawing.Size(74, 20)
        Me.txtEdad.TabIndex = 3
        Me.txtEdad.TabStop = False
        '
        'lblObservacion
        '
        Me.lblObservacion.AutoSize = True
        Me.lblObservacion.Location = New System.Drawing.Point(6, 76)
        Me.lblObservacion.Name = "lblObservacion"
        Me.lblObservacion.Size = New System.Drawing.Size(78, 13)
        Me.lblObservacion.TabIndex = 2
        Me.lblObservacion.Text = "Observación"
        '
        'lblPrioridad
        '
        Me.lblPrioridad.AutoSize = True
        Me.lblPrioridad.Location = New System.Drawing.Point(3, 25)
        Me.lblPrioridad.Name = "lblPrioridad"
        Me.lblPrioridad.Size = New System.Drawing.Size(57, 13)
        Me.lblPrioridad.TabIndex = 2
        Me.lblPrioridad.Text = "Prioridad"
        '
        'lblSexo
        '
        Me.lblSexo.AutoSize = True
        Me.lblSexo.Location = New System.Drawing.Point(621, 25)
        Me.lblSexo.Name = "lblSexo"
        Me.lblSexo.Size = New System.Drawing.Size(35, 13)
        Me.lblSexo.TabIndex = 4
        Me.lblSexo.Text = "Sexo"
        '
        'lblEdad
        '
        Me.lblEdad.AutoSize = True
        Me.lblEdad.Location = New System.Drawing.Point(757, 25)
        Me.lblEdad.Name = "lblEdad"
        Me.lblEdad.Size = New System.Drawing.Size(36, 13)
        Me.lblEdad.TabIndex = 2
        Me.lblEdad.Text = "Edad"
        '
        'lblPaciente
        '
        Me.lblPaciente.AutoSize = True
        Me.lblPaciente.Location = New System.Drawing.Point(195, 25)
        Me.lblPaciente.Name = "lblPaciente"
        Me.lblPaciente.Size = New System.Drawing.Size(57, 13)
        Me.lblPaciente.TabIndex = 0
        Me.lblPaciente.Text = "Paciente"
        '
        'gbxCambioNivel
        '
        Me.gbxCambioNivel.BackColor = System.Drawing.Color.Transparent
        Me.gbxCambioNivel.Controls.Add(Me.btnRefrescar)
        Me.gbxCambioNivel.Controls.Add(Me.lblMotivo)
        Me.gbxCambioNivel.Controls.Add(Me.btnGuardar)
        Me.gbxCambioNivel.Controls.Add(Me.lblNuevaPrioridad)
        Me.gbxCambioNivel.Controls.Add(Me.cbxPrioridadSel)
        Me.gbxCambioNivel.Controls.Add(Me.txtMotivo)
        Me.gbxCambioNivel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gbxCambioNivel.Location = New System.Drawing.Point(12, 542)
        Me.gbxCambioNivel.Name = "gbxCambioNivel"
        Me.gbxCambioNivel.Size = New System.Drawing.Size(840, 96)
        Me.gbxCambioNivel.TabIndex = 15
        Me.gbxCambioNivel.TabStop = False
        Me.gbxCambioNivel.Text = "Cambio de Prioridad"
        '
        'btnRefrescar
        '
        Me.btnRefrescar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnRefrescar.Location = New System.Drawing.Point(537, 56)
        Me.btnRefrescar.Name = "btnRefrescar"
        Me.btnRefrescar.Size = New System.Drawing.Size(140, 23)
        Me.btnRefrescar.TabIndex = 21
        Me.btnRefrescar.Text = "Refrescar lista"
        Me.btnRefrescar.UseVisualStyleBackColor = True
        '
        'lblMotivo
        '
        Me.lblMotivo.AutoSize = True
        Me.lblMotivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblMotivo.Location = New System.Drawing.Point(35, 23)
        Me.lblMotivo.Name = "lblMotivo"
        Me.lblMotivo.Size = New System.Drawing.Size(45, 13)
        Me.lblMotivo.TabIndex = 17
        Me.lblMotivo.Text = "Motivo"
        '
        'btnGuardar
        '
        Me.btnGuardar.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btnGuardar.Location = New System.Drawing.Point(694, 56)
        Me.btnGuardar.Name = "btnGuardar"
        Me.btnGuardar.Size = New System.Drawing.Size(140, 23)
        Me.btnGuardar.TabIndex = 20
        Me.btnGuardar.Text = "Guardar"
        Me.btnGuardar.UseVisualStyleBackColor = True
        '
        'lblNuevaPrioridad
        '
        Me.lblNuevaPrioridad.AutoSize = True
        Me.lblNuevaPrioridad.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblNuevaPrioridad.Location = New System.Drawing.Point(23, 59)
        Me.lblNuevaPrioridad.Name = "lblNuevaPrioridad"
        Me.lblNuevaPrioridad.Size = New System.Drawing.Size(57, 13)
        Me.lblNuevaPrioridad.TabIndex = 17
        Me.lblNuevaPrioridad.Text = "Prioridad"
        '
        'cbxPrioridadSel
        '
        Me.cbxPrioridadSel.BackColor = System.Drawing.SystemColors.Window
        Me.cbxPrioridadSel.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList
        Me.cbxPrioridadSel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.cbxPrioridadSel.FormattingEnabled = True
        Me.cbxPrioridadSel.Items.AddRange(New Object() {"  I  -  R o j o ", "  II  -  A m a r i ll o", "  III  -  V e r d e", "  IV  -  A z u l"})
        Me.cbxPrioridadSel.Location = New System.Drawing.Point(86, 56)
        Me.cbxPrioridadSel.Name = "cbxPrioridadSel"
        Me.cbxPrioridadSel.Size = New System.Drawing.Size(138, 21)
        Me.cbxPrioridadSel.TabIndex = 18
        '
        'txtMotivo
        '
        Me.txtMotivo.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.txtMotivo.Location = New System.Drawing.Point(86, 20)
        Me.txtMotivo.Multiline = True
        Me.txtMotivo.Name = "txtMotivo"
        Me.txtMotivo.Size = New System.Drawing.Size(748, 24)
        Me.txtMotivo.TabIndex = 7
        '
        'IdT
        '
        Me.IdT.HeaderText = "IdTriage"
        Me.IdT.Name = "IdT"
        Me.IdT.ReadOnly = True
        Me.IdT.Visible = False
        '
        'Prioridad
        '
        Me.Prioridad.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells
        DataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Prioridad.DefaultCellStyle = DataGridViewCellStyle2
        Me.Prioridad.HeaderText = "Prioridad"
        Me.Prioridad.Name = "Prioridad"
        Me.Prioridad.ReadOnly = True
        Me.Prioridad.Resizable = System.Windows.Forms.DataGridViewTriState.[True]
        Me.Prioridad.Width = 82
        '
        'Numero
        '
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Numero.DefaultCellStyle = DataGridViewCellStyle3
        Me.Numero.HeaderText = "No."
        Me.Numero.Name = "Numero"
        Me.Numero.ReadOnly = True
        Me.Numero.Visible = False
        Me.Numero.Width = 35
        '
        'colHoraEval
        '
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.colHoraEval.DefaultCellStyle = DataGridViewCellStyle4
        Me.colHoraEval.HeaderText = "Hora Evaluación"
        Me.colHoraEval.Name = "colHoraEval"
        Me.colHoraEval.ReadOnly = True
        '
        'Curp
        '
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Curp.DefaultCellStyle = DataGridViewCellStyle5
        Me.Curp.HeaderText = "CURP"
        Me.Curp.Name = "Curp"
        Me.Curp.ReadOnly = True
        Me.Curp.Visible = False
        Me.Curp.Width = 155
        '
        'Paterno
        '
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Paterno.DefaultCellStyle = DataGridViewCellStyle6
        Me.Paterno.HeaderText = "Apellido Paterno"
        Me.Paterno.Name = "Paterno"
        Me.Paterno.ReadOnly = True
        Me.Paterno.Width = 160
        '
        'Materno
        '
        DataGridViewCellStyle7.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Materno.DefaultCellStyle = DataGridViewCellStyle7
        Me.Materno.HeaderText = "Apellido Materno"
        Me.Materno.Name = "Materno"
        Me.Materno.ReadOnly = True
        Me.Materno.Width = 160
        '
        'Nombre
        '
        DataGridViewCellStyle8.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Nombre.DefaultCellStyle = DataGridViewCellStyle8
        Me.Nombre.HeaderText = "Nombre"
        Me.Nombre.Name = "Nombre"
        Me.Nombre.ReadOnly = True
        Me.Nombre.Width = 160
        '
        'Sexo
        '
        DataGridViewCellStyle9.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle9.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Sexo.DefaultCellStyle = DataGridViewCellStyle9
        Me.Sexo.HeaderText = "Sexo"
        Me.Sexo.Name = "Sexo"
        Me.Sexo.ReadOnly = True
        Me.Sexo.Width = 75
        '
        'Edad
        '
        DataGridViewCellStyle10.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Edad.DefaultCellStyle = DataGridViewCellStyle10
        Me.Edad.HeaderText = "Edad"
        Me.Edad.Name = "Edad"
        Me.Edad.ReadOnly = True
        Me.Edad.Width = 75
        '
        'colNumCtrl
        '
        Me.colNumCtrl.HeaderText = "NumCtrl"
        Me.colNumCtrl.Name = "colNumCtrl"
        Me.colNumCtrl.ReadOnly = True
        Me.colNumCtrl.Visible = False
        '
        'motivo
        '
        Me.motivo.HeaderText = "Motivo"
        Me.motivo.Name = "motivo"
        Me.motivo.ReadOnly = True
        Me.motivo.Visible = False
        '
        'tipoUrgencia
        '
        Me.tipoUrgencia.HeaderText = "tipoUrgencia"
        Me.tipoUrgencia.Name = "tipoUrgencia"
        Me.tipoUrgencia.ReadOnly = True
        Me.tipoUrgencia.Visible = False
        '
        'frmCambiarPrioridad
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.InactiveBorder
        Me.ClientSize = New System.Drawing.Size(865, 654)
        Me.Controls.Add(Me.gbxCambioNivel)
        Me.Controls.Add(Me.gbxPaciente)
        Me.Controls.Add(Me.dgvListado)
        Me.Controls.Add(Me.PictureBox1)
        Me.Name = "frmCambiarPrioridad"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Cambiar Prioridad"
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.dgvListado, System.ComponentModel.ISupportInitialize).EndInit()
        Me.gbxPaciente.ResumeLayout(False)
        Me.gbxPaciente.PerformLayout()
        Me.gbxCambioNivel.ResumeLayout(False)
        Me.gbxCambioNivel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents PictureBox1 As System.Windows.Forms.PictureBox
    Friend WithEvents dgvListado As System.Windows.Forms.DataGridView
    Friend WithEvents gbxPaciente As System.Windows.Forms.GroupBox
    Friend WithEvents txtPaciente As System.Windows.Forms.TextBox
    Friend WithEvents txtSexo As System.Windows.Forms.TextBox
    Friend WithEvents txtHora As System.Windows.Forms.TextBox
    Friend WithEvents txtPrioridad As System.Windows.Forms.TextBox
    Friend WithEvents lblHora As System.Windows.Forms.Label
    Friend WithEvents txtEdad As System.Windows.Forms.TextBox
    Friend WithEvents lblPrioridad As System.Windows.Forms.Label
    Friend WithEvents lblSexo As System.Windows.Forms.Label
    Friend WithEvents lblEdad As System.Windows.Forms.Label
    Friend WithEvents lblPaciente As System.Windows.Forms.Label
    Friend WithEvents gbxCambioNivel As System.Windows.Forms.GroupBox
    Friend WithEvents txtMotivo As System.Windows.Forms.TextBox
    Friend WithEvents lblNuevaPrioridad As System.Windows.Forms.Label
    Friend WithEvents cbxPrioridadSel As System.Windows.Forms.ComboBox
    Friend WithEvents btnGuardar As System.Windows.Forms.Button
    Friend WithEvents btnRefrescar As System.Windows.Forms.Button
    Friend WithEvents lblMotivo As System.Windows.Forms.Label
    Friend WithEvents txtObservacion As System.Windows.Forms.TextBox
    Friend WithEvents lblObservacion As System.Windows.Forms.Label
    Friend WithEvents IdT As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Prioridad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Numero As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colHoraEval As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Curp As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Paterno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Materno As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Nombre As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Sexo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents Edad As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents colNumCtrl As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents motivo As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents tipoUrgencia As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
